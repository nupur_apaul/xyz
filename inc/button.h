/*****************************************************************************
 *  Buttons
 *  Modified for PIC24FJ64GA004 family with PPS.
 *****************************************************************************
 * FileName:        button.h
 * Dependencies:    
 * Processor:       
 * Compiler:       	
 * Linker:          
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES, 
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT, 
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR 
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 * Buttons processing defintions 
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Brant Ivey			 3/14/06	Modified for PIC24FJ64GA004 family with PPS.
 *****************************************************************************/

/*****************************************************************************
 * Buttons IOs PORT definitions for different versions
 * of Explorer 16 Development Board.
 *****************************************************************************/


    #include "port_def.h"

	#define BUTTON1   BUTTON1_IO
	#define BUTTON2   BUTTON2_IO
	#define BUTTON3   BUTTON3_IO
	#define BUTTON4   BUTTON4_IO
	#define BUTTON5   BUTTON5_IO
	#define BUTTON6   BUTTON6_IO
//	#define BUTTON7   BUTTON7_IO				

/*****************************************************************************
 * Function: BtnInit
 *
 * Preconditon: None.
 *
 * Overview: Setup debounce.
 *
 * Input: None.
 *
 * Output: None.
 *
 *****************************************************************************/
void BtnInit(void);

/*****************************************************************************
 * Function: BtnProcessEvents
 *
 * Preconditon: None.
 *
 * Overview: Must be called periodically to proccess buttons.
 *
 * Input: None.
 *
 * Output: None.
 *
 *****************************************************************************/
void BtnProcessEvents(void);
void key_scan(void);
/*****************************************************************************
 * Structure: BUTTONS _button_press
 *
 * Overview: the structure provides a n access to button bit indicators.
 *
 *****************************************************************************/
typedef struct tagButton {
	unsigned b1:1;
	unsigned b2:1;
	unsigned b3:1;
	unsigned b4:1;
	unsigned b5:1;
	unsigned b6:1;
//	unsigned b7:1;
}BUTTON;
extern BUTTON _button_press;

/*****************************************************************************
 * Debounce button counters
 *
 *****************************************************************************/
#define	BUTTON_MAX_DEBOUCE 	50
extern unsigned char _b1_cnt;
extern unsigned char _b2_cnt;
extern unsigned char _b3_cnt;
extern unsigned char _b4_cnt;
extern unsigned char _b5_cnt;
extern unsigned char _b6_cnt;
//extern unsigned char _b7_cnt;

/*****************************************************************************
 * Function: BtnIsPressed
 *
 * Preconditon: None.
 *
 * Overview: Macro detects if button is pressed
 *
 * Input: None.
 *
 * Output: Macro returns zero if button is not pressed.
 *
 *****************************************************************************/
#define BtnIsPressed(__btn)	_button_press.b##__btn

/*****************************************************************************
 * Function: BtnIsNotPressed
 *
 * Preconditon: None.
 *
 * Overview: Macro detects if button is not pressed
 *
 * Input: None.
 *
 * Output: Macro returns zero if button is pressed.
 *
 *****************************************************************************/
#define BtnIsNotPressed(__btn)	!_button_press.b##__btn

/*****************************************************************************
 * EOF
 *****************************************************************************/
