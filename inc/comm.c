#include "..\inc\headers.h"

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: check_fn_code_send_pkt_to_lhb(void)
//	ARGUMENTS	: none
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void check_fn_code_send_pkt_to_lhb(void)
{
	
	ubyte fn_code;
//	rx.data.timer.on = 1;
//	while(!lhb_comm.flags.bits.rx_comp && !rx.data.timer.timeout);
	if(lhb_comm.flags.bits.rx_comp)// && !rx.data.timer.timeout)
	{
		in_test_mode = 0;
	 	if(WDTCONbits.ON == 0)
		 	EnableWDT();
		ClearWDT();

		//		rx.data.timer.timeout = 0;
		//		rx.data.timer.count = T3_DEL_1SEC;
		
		if(verify_crc_modbus(rx.data.stx,lhb_comm.rx_cnt - L_CRC - L_ETX))
		{

			fn_code = rx.data.func_code[0];
			complete_plant_tx.info.coach_info = coach_info;
			complete_plant_tx.info.fault = fault;
			complete_plant_tx.info.set = set;
			complete_plant_tx.info.server = server;
			complete_plant_tx.info.trip = trip;
			complete_plant_tx.info.test1 = test1;
			complete_plant_tx.info.test2 = test2;
			complete_plant_tx.info.override = override;
			str_copy_ram_cnt(&disp_sw_vrs[0],&complete_plant_tx.info.disp_sw_vrs[0],6);
			complete_plant_tx.info.both_rmpu_sel = orphan_flags.bits.both_rmpu_sel+48;
			complete_plant_tx.info.comma_rmpu = ',';
			complete_plant_tx.info.double_decker_sel = orphan_flags.bits.double_decker_sel+48;
			complete_plant_tx.info.comma_double = ',';
			if(new_pkt_rdy_to_be_sent)
				fill_frame_data(lhb_comm_fn_code);	//TX TO LHB
			else	
			{
				decode_fn_code(fn_code);	//RX FROM LHB
				fill_frame_data(FC_DEFAULT);	//TX TO LHBlhb_comm_fn_code = FC_DEFAULT;
			}	
			
		}	
		else
			fn_code	= FC_CRC_FAIL;

	lhb_comm.rx_cnt=0;
	lhb_comm.flags.word =0;
	lhb_comm.flags.bits.rx_enable=1;
	orphan_flags.bits.send_pkt_to_disp = 0;	
	}	
}	
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: fill_frame_data
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	: 
//				  	
//#######################################################################################
void fill_frame_data(ubyte func_code)
{
	ubyte *char_dest_ptr = tx.data.data_crc_bytes;
	ubyte *char_src_ptr = rx.data.data_crc_bytes;
	while(lhb_comm.flags.bits.pkt_transmiting);	
	
	if(network.flags.bits.start_nw_mode)
	{
		complete_slave_plant[selected_slave] = complete_plant_tx;
		str_copy_ram_cnt( &complete_slave_plant[selected_slave].bytes[0], char_dest_ptr,(COMPLETE_PLANT_UNION*MAX_NO_OF_SLAVE_LHB));
		char_dest_ptr += (COMPLETE_PLANT_UNION*MAX_NO_OF_SLAVE_LHB);
	}
	else
	{
		str_copy_ram_cnt( complete_plant_tx.bytes, char_dest_ptr,COMPLETE_PLANT_UNION);
		char_dest_ptr += (COMPLETE_PLANT_UNION);
	}	
		
		
//	frames.bits.tx_frame_not_empty = 1;					
	fill_tx_frame_buff(func_code,(char_dest_ptr-tx.data.data_crc_bytes), (urombyte *)ID_DEST);
}
/*#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: init_dma_rly_rx
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:We require to 4 DMA's 
				 2 for tramissiom to PC and Relay Card
				 2 for reception from PC and Relay Card	
				 DMA channel 0 used for rly transmission						
*/
//#######################################################################################
void init_dma_tx_lhb(void)
{
	IEC1CLR		=	0x00010000; 			// disable DMA channel 0 interrupts
	IFS1CLR		=	0x00010000; 			// clear any existing DMA channel 0 interrupt flag
	DMACONSET	=	0x00008000; 			// enable the DMA controller
	DCH0CON		=	0x03; 					// channel off, priority 3, no chaining
	DCH0ECON 	= 	IRQ_U1BTX<<8|0x10; 			// start irq is UART2B TX, pattern match disabled
	DCH0SSA		=	KVA_TO_PA(tx.bytes); // transfer source physical address
	DCH0DSA		=	KVA_TO_PA(&U1BTXREG); 	// transfer destination physical address
	DCH0DSIZ	=	1; 						// destination size 1 bytes
	DCH0CSIZ	=	1; 						// 1 bytes transferred per event
	DCH0INTCLR	=	0x00ff00ff; 			// clear existing events, disable all interrupts
	DCH0INTSET	=	0x00090000; 			// enable Block Complete and error interrupts
	IPC9CLR		=	0x0000001f; 			// clear the DMA channel 0 priority and sub-priority
	IPC9SET		|=	0x00000016; 			// set IPL 5, sub-priority 2
	IEC1SET		|=	0x00010000; 			// enable DMA channel 0 interrupt
	DCH0ECONSET	=	0x00000080; 			// set CFORCE to 1
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: transmit_lcd
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	: process time out timer is for checking if a loop is active without there been any activity on any 1 of the treadles
//				  	
//#######################################################################################
void transmit_lhb(uinteger cnt)
{
	DCH0SSIZ=cnt; 					// source size  bytes
	DCH0CONSET=0x80; 				// turn channel on	
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: fill_tx_frame_buff
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	: 
//				  	
//#######################################################################################
void fill_tx_frame_buff(ubyte func_code,uinteger length, urombyte *dest_id)
{
	uinteger temp_dig_count = L_FRAME_LEN(length);
	str_copy_rom_cnt((urombyte *)STX_STR,tx.data.stx,L_STX);
	str_copy_rom_cnt((urombyte *)ID_SRC, tx.data.src_id,L_SRC_ID);
	str_copy_rom_cnt(dest_id, tx.data.dest_id,L_DEST_ID);
	dec_int_to_char_fixed(&temp_dig_count,tx.data.length,L_FR_LEN-1);
	tx.data.length[L_FR_LEN - 1] = ',';	
	tx.data.func_code[0] = func_code;
	tx.data.func_code[1] = ',';
	crc_generate_modbus(tx.bytes,(L_FRAME_LEN(length)-L_CRC-L_ETX));	
	str_copy_rom_cnt((urombyte *)ETX_STR,&tx.bytes[L_FRAME_LEN(length) - L_ETX],L_ETX);
	lhb_comm.total_tx_cnt = temp_dig_count;
	transmit_lhb(lhb_comm.total_tx_cnt);	
//	usart1B_write_str( tx.bytes, lhb_comm.total_tx_cnt,0);		//for testing
	lhb_comm.flags.bits.pkt_transmiting = 1;
	lhb_comm.flags.bits.ack_waiting = 1;
	
	if(new_pkt_rdy_to_be_sent)
		new_pkt_rdy_to_be_sent = 0;
//	Delays_Xtal_ms(5);
	if(func_code !=FC_CRC_FAIL)
	prev_tx_fn_code = func_code;
//	U1STAbits.UTXEN = 1;						//Enable Only when tx buffer is filled else tx interrupt will always be genearated..
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: 
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	: 
//				  	
//#######################################################################################
void decode_fn_code(ubyte fn_code)
{
	ubyte *char_dest_ptr = tx.data.data_crc_bytes;
	ubyte *char_src_ptr = rx.data.data_crc_bytes;
    ubyte *char_src_date_time_ptr = rx.data.data_crc_bytes;
    
	uinteger temp_frame_len;
    uinteger temp_frame_len_tx;
	ubyte *char_end_ptr;

	if(network.flags.bits.start_nw_mode)
	{
		str_copy_ram_cnt( char_src_ptr, &complete_slave_plant[selected_slave].bytes[0], (COMPLETE_PLANT_UNION*MAX_NO_OF_SLAVE_LHB));
		complete_plant_rx = complete_slave_plant[selected_slave];
	}
	else
		str_copy_ram_cnt( char_src_ptr, complete_plant_rx.bytes, COMPLETE_PLANT_UNION);
		
	coach_info = complete_plant_rx.info.coach_info;
//	event = complete_plant_rx.info.event;
	fault = complete_plant_rx.info.fault;
	set = complete_plant_rx.info.set;
	server = complete_plant_rx.info.server;
	trip = complete_plant_rx.info.trip;
	override = complete_plant_rx.info.override;
	server_s = complete_plant_rx.info.server_status;
	str_copy_ram_cnt(&complete_plant_rx.info.event_pkt[0],&event_pkt[0],4);
	str_copy_ram_cnt(&complete_plant_rx.info.fault_pkt[0],&fault_pkt[0],4);
    stat.byte = 0xFF;
	frame_flags.integer = 0xFFFFFFFF;
	frame_flags.bits.networking_frame_rcvd = 0;
	frame_flags.bits.restore_default_frame_rcvd = 0;
	frame_flags.bits.stored_fault_frame_rcvd = 0;
	frame_flags.bits.system_pen_drive_frame_rcvd = 0;
	frame_flags.bits.analysis_pen_drive_frame_rcvd = 0;
	frame_flags.bits.pen_drive_detected_frame_rcvd = 0;
    frame_flags.bits.stored_dutycycle_frame_rcvd   =0;//added
    frame_flags.bits.view_dutycycle_trip_log =0;
	switch(fn_code)
	{
//		case FC_DEFAULT :	frame_flags.bits.default_frame_rcvd = 1;
//							stat.bits.data_received =1;	
//		break;
//		case FC_DATE_TIME : frame_flags.bits.date_time_frame_rcvd = 1;
//		
//		break;			
//		case FC_RMPU_MAKE 	: frame_flags.bits.rmpu_make_frame_rcvd = 1;
//									
//		break;			
//		case FC_COACH_NO 	: frame_flags.bits.coach_no_frame_rcvd = 1;							
//		
//		break;			
//		case FC_RMPU_NO 	: frame_flags.bits.rmpu_no_frame_rcvd= 1;
//							
//		break;						
//		case FC_PLANT_STATUS : 	frame_flags.bits.plant_status_frame_rcvd = 1;
//								stat.bits.data_received =1;
//									
//		break;						
//		case FC_CRC_FAIL : frame_flags.bits.crc_fail_frame_rcvd = 1;
//		
//		break;
        case FC_VIEW_DUTYCYCLE_TRIP_LOG :
                                    if(!new_pkt_rdy_to_be_sent)
									{
                                        char_to_dec_int(rx.data.length, &temp_frame_len,',');
                                        char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
                                        no_of_logs_to_disp = char_src_ptr - char_end_ptr;
                                      
//                                        while(char_src_ptr<char_end_ptr)
//                                        {
                                            
                                            str_copy_ram_cnt ( char_src_ptr, &EquipmentLogs.data.trip, TRIP_STATUS_STRUCT);
                                            char_src_ptr += TRIP_STATUS_STRUCT;
                                            str_copy_ram_cnt ( char_src_ptr, &EquipmentLogs.data.DutyCycles.data.TripClock.StartTimeStamp, 255);
                                            //str_copy_ram_cnt ( char_src_date_time_ptr, &EquipmentLogs.data.DutyCycles.data.TripClock.StartDateStamp, 8);
                                       // }		  
                                    }
                                    frame_flags.bits.view_dutycycle_trip_log =1;
            
            break;
  
      case  FC_VIEW_STORED_DUTYCYCLE 	:
                if(!new_pkt_rdy_to_be_sent)
                {

                    char_to_dec_int(rx.data.length, &temp_frame_len,',');
                    char_to_dec_int(tx.data.length, &temp_frame_len_tx,',');
                    char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
                    no_of_logs_to_disp = temp_frame_len-temp_frame_len_tx;
                    //if(temp_duty_count == DUTYCYCLE_SERIAL_NO_COUNT)
                      //  temp_duty_count = 0;
                    //temp_duty_count = 0;
                    //while(char_src_ptr<char_end_ptr)
                   // {
                        str_copy_ram_cnt ( char_src_ptr, &lcd_dutylog.log.serial_num.byte[0], DUTYLOGSTRUCT);
                        char_src_ptr += DUTYLOGSTRUCT;
                        
                   // }		
                }

                frame_flags.bits.stored_dutycycle_frame_rcvd = 1;
		break;	
		case FC_VIEW_PREV_STORED_DUTYCYCLE	:
                                        if(!new_pkt_rdy_to_be_sent)
										{
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
                                            char_to_dec_int(tx.data.length, &temp_frame_len_tx,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											// no_of_logs_to_disp = temp_frame_len-temp_frame_len_tx;
//											if(temp_duty_count == DUTYCYCLE_SERIAL_NO_COUNT)
//												temp_duty_count = 0;
//											while(char_src_ptr<char_end_ptr)
//											{
													str_copy_ram_cnt ( char_src_ptr, &lcd_dutylog.log.serial_num.byte[0], DUTYLOGSTRUCT);
													char_src_ptr += DUTYLOGSTRUCT;
//											}
										} 
                                        frame_flags.bits.stored_dutycycle_frame_rcvd = 1;
		break;									
        
         case FC_VIEW_NEXT_STORED_DUTYCYCLE	:
                                        if(!new_pkt_rdy_to_be_sent)
										{
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
                                            char_to_dec_int(tx.data.length, &temp_frame_len_tx,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											//no_of_logs_to_disp = temp_frame_len-temp_frame_len_tx;
//											if(temp_duty_count == DUTYCYCLE_SERIAL_NO_COUNT)
//												temp_duty_count = 0;
//											while(char_src_ptr<char_end_ptr)
//											{
													str_copy_ram_cnt ( char_src_ptr, &lcd_dutylog.log.serial_num.byte[0], DUTYLOGSTRUCT);
													char_src_ptr += DUTYLOGSTRUCT;
//											}
										} 
                                        frame_flags.bits.stored_dutycycle_frame_rcvd = 1;
		break;	    
            
    /*        
		case  FC_VIEW_STORED_DUTYCYCLE 	:
                if(!new_pkt_rdy_to_be_sent)
                {

                    char_to_dec_int(rx.data.length, &temp_frame_len,',');
                    char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
                    no_of_logs_to_disp = char_src_ptr - char_end_ptr;
                    if(temp_duty_count == DUTYCYCLE_SERIAL_NO_COUNT)
                        temp_duty_count = 0;
                    temp_duty_count = 0;
                    while(char_src_ptr<char_end_ptr)
                    {
                        str_copy_ram_cnt ( char_src_ptr, &lcd_dutylog[temp_duty_count++].log.serial_num.byte[0], DUTYLOGSTRUCT);
                        char_src_ptr += DUTYLOGSTRUCT;
                    }		
                }

                frame_flags.bits.stored_dutycycle_frame_rcvd = 1;
		break;	
		case FC_VIEW_PREV_STORED_DUTYCYCLE	:
                                        if(!new_pkt_rdy_to_be_sent)
										{
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											no_of_logs_to_disp = char_src_ptr - char_end_ptr;
											if(temp_duty_count == DUTYCYCLE_SERIAL_NO_COUNT)
												temp_duty_count = 0;
											while(char_src_ptr<char_end_ptr)
											{
													str_copy_ram_cnt ( char_src_ptr, &lcd_dutylog[temp_duty_count++].log.serial_num.byte[0], DUTYLOGSTRUCT);
													char_src_ptr += DUTYLOGSTRUCT;
											}
										} 
                                        frame_flags.bits.stored_dutycycle_frame_rcvd = 1;
		break;									
        
     */  
        case FC_VIEW_STORED_FAULTS 	:
									if(!new_pkt_rdy_to_be_sent)
										{
										
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											no_of_logs_to_disp = char_src_ptr - char_end_ptr;
											if(temp_fault_count == FAULTS_ON_LCD)
												temp_fault_count = 0;
											temp_fault_count = 0;
											while(char_src_ptr<char_end_ptr)
											{
													str_copy_ram_cnt ( char_src_ptr, &fault_log[temp_fault_count++].fault_str[0], LCD_FAULT_LOG_STRUCT);
													char_src_ptr += LCD_FAULT_LOG_STRUCT;
											}		
										} frame_flags.bits.stored_fault_frame_rcvd = 1;
		break;	
		case FC_VIEW_PREV_STORED_FAULTS	:
									if(!new_pkt_rdy_to_be_sent)
										{
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											no_of_logs_to_disp = char_src_ptr - char_end_ptr;
											if(temp_fault_count == FAULTS_ON_LCD)
												temp_fault_count = 0;
											while(char_src_ptr<char_end_ptr)
											{
													str_copy_ram_cnt ( char_src_ptr, &fault_log[temp_fault_count++].fault_str[0], LCD_FAULT_LOG_STRUCT);
													char_src_ptr += LCD_FAULT_LOG_STRUCT;
											}
										} frame_flags.bits.stored_fault_frame_rcvd = 1;
		break;											
		case FC_VIEW_STORED_SYSTEM_LOG 	:
									if(!new_pkt_rdy_to_be_sent)
										{
										
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											no_of_logs_to_disp = char_src_ptr - char_end_ptr;
											if(temp_fault_count == FAULTS_ON_LCD)
												temp_fault_count = 0;
											temp_fault_count = 0;
											while(char_src_ptr<char_end_ptr)
											{
													str_copy_ram_cnt ( char_src_ptr, &fault_log[temp_fault_count++].fault_str[0], LCD_FAULT_LOG_STRUCT);
													char_src_ptr += LCD_FAULT_LOG_STRUCT;
											}		
										} frame_flags.bits.stored_fault_frame_rcvd = 1;
		break;	
		case FC_VIEW_PREV_STORED_SYSTEM_LOG	:
									if(!new_pkt_rdy_to_be_sent)
										{
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											no_of_logs_to_disp = char_src_ptr - char_end_ptr;
											if(temp_fault_count == FAULTS_ON_LCD)
												temp_fault_count = 0;
											while(char_src_ptr<char_end_ptr)
											{
													str_copy_ram_cnt ( char_src_ptr, &fault_log[temp_fault_count++].fault_str[0], LCD_FAULT_LOG_STRUCT);
													char_src_ptr += LCD_FAULT_LOG_STRUCT;
											}
										} frame_flags.bits.stored_fault_frame_rcvd = 1;
		break;											
		case FC_VIEW_STORED_ANALYSIS_LOG 	:
									if(!new_pkt_rdy_to_be_sent)
										{
										
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											no_of_logs_to_disp = char_src_ptr - char_end_ptr;
											if(temp_fault_count == FAULTS_ON_LCD)
												temp_fault_count = 0;
											temp_fault_count = 0;
											while(char_src_ptr<char_end_ptr)
											{
													str_copy_ram_cnt ( char_src_ptr, &fault_log[temp_fault_count++].fault_str[0], LCD_FAULT_LOG_STRUCT);
													char_src_ptr += LCD_FAULT_LOG_STRUCT;
											}		
										} frame_flags.bits.stored_fault_frame_rcvd = 1;
		break;	
		case FC_VIEW_PREV_STORED_ANALYSIS_LOG	:
									if(!new_pkt_rdy_to_be_sent)
										{
											char_to_dec_int(rx.data.length, &temp_frame_len,',');
											char_end_ptr = char_src_ptr + L_DATA_LEN(temp_frame_len);
											no_of_logs_to_disp = char_src_ptr - char_end_ptr;
											if(temp_fault_count == FAULTS_ON_LCD)
												temp_fault_count = 0;
											while(char_src_ptr<char_end_ptr)
											{
													str_copy_ram_cnt ( char_src_ptr, &fault_log[temp_fault_count++].fault_str[0], LCD_FAULT_LOG_STRUCT);
													char_src_ptr += LCD_FAULT_LOG_STRUCT;
											}
										} frame_flags.bits.stored_fault_frame_rcvd = 1;
		break;											
//		case FC_VIEW_STORED_LOG 	: frame_flags.bits.stored_log_frame_rcvd = 1;
//		
//		break;						
//		case FC_TEST_MODE 	: frame_flags.bits.test_mode_frame_rcvd = 1;
//		
//		break;						
//		case FC_DWNLD_TO_PEN_DRIVE 	: frame_flags.bits.pen_drive_frame_rcvd = 1;
//		
//		break;						
//		case FC_UPGRADE_FIRMWARE 	: frame_flags.bits.upgrade_firmware_frame_rcvd = 1;
//		
//		break;									
		case FC_NETWORKING : frame_flags.bits.networking_frame_rcvd = 1;
		break;									
//		case FC_VIEW_TEMP_SETPOINT 	: 	frame_flags.bits.view_set_point_frame_rcvd = 1;
//										stat.bits.data_received =1;
//		
//		break;									
//		case FC_VIEW_TRIPS 	: 	frame_flags.bits.view_trip_count_frame_rcvd = 1;
//								stat.bits.data_received =1;
//		break;	
//		case FC_SEL_DOUBLE_DECKER 	: 
//		
//		break;																
//		case FC_SEL_SET_POINT_ROTARY 	: 
//		
//		break;																
//		case FC_SEL_BOTH_RMPU 	: 
//		
//		break;																
//		case FC_SLEEP_HRS 	: 	frame_flags.bits.sleep_hrs_frame_rcvd = 1;
//								stat.bits.data_received =1;
//								
//		break;																
		case FC_PEN_DRIVE_DETECTED :frame_flags.bits.pen_drive_detected_frame_rcvd = 1;
									stat.bits.data_received =1;
		break;
		case FC_PEN_DRIVE_DETECTED_WAIT:
				pendrive_detected = 1;
//				home_clr();	
		break;
		case FC_SYSTEM_LOG_TO_PEN_DRIVE :frame_flags.bits.system_pen_drive_frame_rcvd = 1;
								stat.bits.data_received =1;
		break;									
		case FC_ANALYSIS_LOG_TO_PEN_DRIVE :frame_flags.bits.analysis_pen_drive_frame_rcvd = 1;
								stat.bits.data_received =1;
		break;									
		case FC_RESTORE_DEFAULT :frame_flags.bits.restore_default_frame_rcvd = 1;
								stat.bits.data_received =1;
		break;		
        
        case FC_VIEW_POWER_USESE:
            frame_flags.bits.power_uses_frame_rcvd = 1;
            frame_flags.bits.update_power_data = 1;
            memcpy(&PowerReadings[0].num,char_src_date_time_ptr,sizeof(PowerReadings));
            break;
            
//		case FC_EXIT 	: frame_flags.bits.exit_frame_rcvd = 1;
//		
//		break;			
	}
	
	
	while(pendrive_detected)//lakh added by aman
	{
//		if(fn_code != FC_PEN_DRIVE_DETECTED_WAIT)
//			pendrive_detected = 0;
//		home_clr();
		puts_graphical_lcd_rom_lim((urombyte *)"  Please wait, Pen Drive Detected...  ",0,13*9,1,0,arialNarrow_bld_13pix);
		ClearWDT();
		Delays_Xtal_ms(500);
		puts_graphical_lcd_rom_lim((urombyte *)"                                ",0,13*9,0,0,arialNarrow_bld_13pix);
		ClearWDT();
		Delays_Xtal_ms(300);
		ClearWDT();
		lhb_comm.flags.bits.rx_enable=1;
//				Delays_Xtal_ms(1000);
	}
	
}	

