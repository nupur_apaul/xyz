
enum MENUS_ENUM
{
	DEFAULT_SCREEN_MENU=0, 
	
	MAIN_MENU, 
	SEVEN_POINT_SELECT_MENU,
	
	CHANGE_DEFAULT_SETTINGS_MENU,
	STATUS_OF_PLANT_MENU, 
	VIEW_TRIP_COUNT, 
	SUPERVISOR_MENU,
	TEST_MENU,
	FACTORY_MENU,
	FAULT_DATA_MENU,
	DUTY_CYCLE_DATA_MENU,
    
	CHANGE_DATE_TIME_MENU,
	CHANGE_COACH_NO_MENU,
	CHANGE_RMPU_NO_MENU,
	CHANGE_RMPU_MAKE_MENU,
	SEVEN_POINT_CHANGE_MENU,
	SET_RTC_TIME_MENU,
	USB_MENU,
	SETPT_SOURCE_SELECT_MENU,
	NORMAL_TEST_MENU,
	EXCL_TEST_MENU,
	STATUS_OF_PRESSURE_SENSOR_MENU,
	
	NETWORK_MENU,
	NUMBER_OF_MENUES,
	
	BYPASS_FAULTS,
	SYSTEM_LOG_MENU,
    DUTYCYCLE_TRIP_MENU,
	ANALYSIS_LOG_MENU,
    POWER_USES_MENU
};
extern enum MENUS_ENUM MENUS;

enum ITEMS_ENUM
{
	SELECT_CHANGE_DATE_TIME = 0,
	SELECT_CHANGE_COACH_NO,
	SELECT_CHANGE_UNIT_NO,
	SELECT_CHANGE_RMPU_NO,
	SELECT_CHANGE_RMPU_MAKE,
	SELECT_DOWNLOAD_FAULTS_TO_USB,
	SELECT_DOWNLOAD_EVENTS_TO_USB,
	SELECT_UPGRADE_FIRMWARE_THROUGH_BOOTLOADER,
	SELECT_SLEEP_HRS_ENTRY,
	SELECT_RESTORE_FACTORY_SETTINGS,
	SELECT_CHANGE_SERVER_SETTINGS,
	SELECT_SINGLE_DOUBLE_RMPU,
	SELECT_SINGLE_DOUBLE_DECKER,
	SELECT_DOWNLOAD_SYSTEM_LOG_TO_USB,
	SELECT_DOWNLOAD_ANALYSIS_LOG_TO_USB
};
extern enum ITEMS_ENUM ITEMS;

//extern struct menu_struct menu_mode;
//extern struct menu_struct default_settings_mode;
extern unsigned short default_screen_putdata_x[10];
extern unsigned short double_rmpu_putdata_x[10];
extern unsigned short plantstatus_screen_putdata_x[45];
extern unsigned short sevenpt_select_screen_putdata_x[30];
extern unsigned short fault_screen_putdata_x[10];
extern unsigned short pressurestatus_screen_putdata_x[10];

extern unsigned short menu_options_putdata_x[25];

extern unsigned short sevenpt_change_screen_readdata_x[10];
extern unsigned short coachno_change_screen_readdata_x[3];
extern unsigned short rmpuno_change_screen_readdata_x[5];
extern unsigned short rmpumake_change_screen_readdata_x[5];
extern unsigned short datetime_change_screen_readdata_x[5];
extern unsigned short sleep_hrs_screen_readdata_x[4];
union union_system_status
		{
			struct
			{
				unsigned power_up:1  ;//;//__attribute__ ((packed));
				unsigned unused:15  ;//;//__attribute__ ((packed));
			}bits;
			unsigned short byte;
		};
extern union union_system_status system_status;

////#################################################################################################
////#######################################################################################
////				structures for timer2 module
////#######################################################################################
typedef struct 
{
	unsigned short timer ;//;//__attribute__ ((packed));
	struct
	{
		unsigned tick:1 ;//;//__attribute__ ((packed));
		unsigned unused:7 ;//;//__attribute__ ((packed));
	}status	__attribute__ ((packed));
}TIMER;

struct struct_timer2
{
	TIMER sec;
//	TIMER msec_100;	
	TIMER msec_50;
	TIMER msec_5;	
	TIMER display_timeout_5sec;
	TIMER display_timeout_10sec;
	union
	{
		unsigned char byte;
		struct
		{
			unsigned blinker_status:1;
			unsigned unused:7;
		}bits;
	}stat;		
}	__attribute__ ((packed));
extern struct struct_timer2 timer2;

struct struct_key
{
	unsigned state:1 ;//;//__attribute__ ((packed));
	unsigned pressed:1 ;//;//__attribute__ ((packed));
	unsigned continued_press:1 ;//;//__attribute__ ((packed));
	unsigned released:1 ;//;//__attribute__ ((packed));
	unsigned long_press:1 ;//;//__attribute__ ((packed));
	unsigned timer_on:1 ;//;//__attribute__ ((packed));
	unsigned timer_complete:1 ;//;//__attribute__ ((packed));
	unsigned cont_press_pulse:1 ;//;//__attribute__ ((packed));
	unsigned short timer_count ;//;//__attribute__ ((packed));
	unsigned short cont_press_timer ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
extern struct struct_key  sw[TOTAL_SW],last_sw[TOTAL_SW];


//////////////////////////////////////////////////////////////////////////////////////
//// prototypes for graphical LCD
//////////////////////////////////////////////////////////////////////////////////////
union LCD_union
{
	struct
	{ 
		unsigned  d0:1 ;//;//__attribute__ ((packed));
		unsigned  d1:1 ;//;//__attribute__ ((packed));
		unsigned  d2:1 ;//;//__attribute__ ((packed));
		unsigned  d3:1 ;//;//__attribute__ ((packed));
		unsigned  d4:1 ;//;//__attribute__ ((packed));
		unsigned  d5:1 ;//;//__attribute__ ((packed));
		unsigned  d6:1 ;//;//__attribute__ ((packed));
		unsigned  d7:1 ;//;//__attribute__ ((packed));
	}data 	__attribute__ ((packed));
	unsigned char byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));


union concat_union
{
	unsigned short word;
	unsigned char integer[2];
}	__attribute__ ((packed));

extern union concat_union concat;
extern union LCD_union LCD;	
//
union section_union
{
	unsigned char single_byte[MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2] ;//;//__attribute__ ((packed));	
	unsigned char byte[MAX_CHAR_HEIGHT][MAX_CHARS_IN_A_LINE*2] ;//;//__attribute__ ((packed));
	unsigned short  integer[MAX_CHAR_HEIGHT][MAX_CHARS_IN_A_LINE] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
extern union section_union section;


union union_temp_struct
{
	struct 
	{
		unsigned char total_data_fields;
		unsigned char data_fields_chars_cnt[10];
		unsigned char *data_field_address[10];	
	}data;
	ubyte buffer[21];
}	__attribute__ ((packed));
extern union union_temp_struct temp_struct;


struct sensor_struct
{
	float analog_val[8] ;//;//__attribute__ ((packed));
	unsigned ok:1 ;//;//__attribute__ ((packed));
	unsigned prev_ok:1 ;//;//__attribute__ ((packed));
	unsigned defective:1 ;//;//__attribute__ ((packed));
	unsigned shorted:1 ;//;//__attribute__ ((packed));
	unsigned open:1 ;//;//__attribute__ ((packed));
	unsigned char  fault_code_id ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
extern struct sensor_struct sensor[10];
/*#################################################################################################################
FAULT LOG
###################################################################################################################*/

struct date_struct
{
	ubyte dd[2] ;//;//__attribute__ ((packed));
	ubyte slash ;//;//__attribute__ ((packed));
	ubyte mm[2] ;//;//__attribute__ ((packed));
	ubyte slash1 ;//;//__attribute__ ((packed));
	ubyte yy[2] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

union date_union									
{
	struct date_struct value ;//;//__attribute__ ((packed));
	ubyte byte[8] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct time_struct
{
	ubyte hh[2] ;//;//__attribute__ ((packed));
	ubyte colon ;//;//__attribute__ ((packed));
	ubyte mm[2] ;//;//__attribute__ ((packed));
	ubyte colon1 ;//;//__attribute__ ((packed));
	ubyte ss[2] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct	temp_struct									//temperature 
{
	ubyte byte[4] ;//;//__attribute__ ((packed));
	ubyte comma ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));	
				
union time_union
{
	struct time_struct value ;//;//__attribute__ ((packed));
	ubyte byte[8] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct di1_bits_struct
{
	unsigned hvac:1 ;//;//__attribute__ ((packed));
	unsigned cnd11:1 ;//;//__attribute__ ((packed));
	unsigned cnd12:1 ;//;//__attribute__ ((packed));
	unsigned spfn11:1 ;//;//__attribute__ ((packed));
	unsigned spfn21:1 ;//;//__attribute__ ((packed));
	unsigned htr1:1 ;//;//__attribute__ ((packed));
	unsigned lp11:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
					
union di1_union
{		
	struct di1_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct di2_bits_struct
{
	unsigned lp12:1 ;//;//__attribute__ ((packed));
	unsigned cp1:1 ;//;//__attribute__ ((packed));
	unsigned hp11:1 ;//;//__attribute__ ((packed));
	unsigned hp12:1 ;//;//__attribute__ ((packed));
	unsigned spare1:1 ;//;//__attribute__ ((packed));
	unsigned v_ok:1 ;//;//__attribute__ ((packed));
	unsigned cnd21:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));

}	__attribute__ ((packed));
					
union di2_union
{		
	struct di2_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
				
struct di3_bits_struct
{
	unsigned cnd22:1 ;//;//__attribute__ ((packed));
	unsigned spfn12:1 ;//;//__attribute__ ((packed));
	unsigned spfn22:1 ;//;//__attribute__ ((packed));
	unsigned htr2:1 ;//;//__attribute__ ((packed));
	unsigned lp21:1 ;//;//__attribute__ ((packed));
	unsigned lp22:1 ;//;//__attribute__ ((packed));
	unsigned cp2:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

union di3_union
{		
	struct di3_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct di4_bits_struct
{
		
	unsigned hp21:1 ;//;//__attribute__ ((packed));
	unsigned hp22:1 ;//;//__attribute__ ((packed));
	unsigned spare2:1 ;//;//__attribute__ ((packed));
	unsigned unused2:1 ;//;//__attribute__ ((packed));
	unsigned unused3:1 ;//;//__attribute__ ((packed));
	unsigned unused4:1 ;//;//__attribute__ ((packed));
	unsigned unused5:1 ;//;//__attribute__ ((packed));
	unsigned unused6:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

union di4_union
{		
	struct di4_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct rel1_bits_struct
{
	unsigned comp_ok:1 ;//;//__attribute__ ((packed));
	unsigned spfn11:1 ;//;//__attribute__ ((packed));
	unsigned spfn21:1 ;//;//__attribute__ ((packed));
	unsigned cnd11:1 ;//;//__attribute__ ((packed));
	unsigned cnd12:1 ;//;//__attribute__ ((packed));
	unsigned cmp11:1 ;//;//__attribute__ ((packed));
	unsigned cmp12:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
					
union rel1_union
{		
	struct rel1_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));			

struct rel2_bits_struct
{

	unsigned htr1:1 ;//;//__attribute__ ((packed));
	unsigned spare1:1 ;//;//__attribute__ ((packed));
	unsigned spare2:1 ;//;//__attribute__ ((packed));
	unsigned fault:1 ;//;//__attribute__ ((packed));
	unsigned spfn12:1 ;//;//__attribute__ ((packed));
	unsigned spfn22:1 ;//;//__attribute__ ((packed));
	unsigned cnd21:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));

}	__attribute__ ((packed));
					
union rel2_union
{		
	struct rel2_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct rel3_bits_struct
{
	unsigned cnd22:1 ;//;//__attribute__ ((packed));
	unsigned cmp21:1 ;//;//__attribute__ ((packed));
	unsigned cmp22:1 ;//;//__attribute__ ((packed));
	unsigned htr2:1 ;//;//__attribute__ ((packed));
	unsigned spare3:1 ;//;//__attribute__ ((packed));
	unsigned spare4:1 ;//;//__attribute__ ((packed));
	unsigned unused1:1 ;//;//__attribute__ ((packed));
	unsigned unused2:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
	
union rel3_union
{		
	struct rel3_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));				



struct blk1_bits_struct
{
	unsigned spfn11:1 ;//;//__attribute__ ((packed));
	unsigned spfn21:1 ;//;//__attribute__ ((packed));
	unsigned cnd11:1 ;//;//__attribute__ ((packed));
	unsigned cnd12:1 ;//;//__attribute__ ((packed));
	unsigned cnd21:1 ;//;//__attribute__ ((packed));
	unsigned cnd22:1 ;//;//__attribute__ ((packed));
	unsigned spfn12:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
					
union blk1_union
{		
	struct blk1_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct blk2_bits_struct
{
	unsigned cmp11:1 ;//;//__attribute__ ((packed));
	unsigned cmp12:1 ;//;//__attribute__ ((packed));
	unsigned cmp21:1 ;//;//__attribute__ ((packed));
	unsigned cmp22:1 ;//;//__attribute__ ((packed));
	unsigned htr1:1 ;//;//__attribute__ ((packed));
	unsigned htr2:1 ;//;//__attribute__ ((packed));
	unsigned spfn22:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

union blk2_union
{		
	struct blk2_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));	


struct open_bits_struct
{
	unsigned rm1:1 ;//;//__attribute__ ((packed));
	unsigned rm2:1 ;//;//__attribute__ ((packed));
	unsigned out1:1 ;//;//__attribute__ ((packed));
	unsigned out2:1 ;//;//__attribute__ ((packed));
	unsigned duct1:1 ;//;//__attribute__ ((packed));
	unsigned duct2:1 ;//;//__attribute__ ((packed));
	unsigned st_pt:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
//************************************************************************
struct double_struct 
{
	ubyte byte;
	//ubyte comma;
}__attribute__((packed));

struct trip_status_struct
{
	struct double_struct  BLOWER[2][2]					;//__attribute__ ((packed));
	struct double_struct  LP[2][2]						;//__attribute__ ((packed));
	struct double_struct  HP[2][2]						;//__attribute__ ((packed));
	struct double_struct  COND[2][2]					;//__attribute__ ((packed));
	struct double_struct  HEATER[2]						;//__attribute__ ((packed));
}	__attribute__((packed));	

#define		TRIP_STATUS_STRUCT		sizeof(struct trip_status_struct)

union trip_status_union
{
	struct trip_status_struct count;
	ubyte bytes[TRIP_STATUS_STRUCT];
}	__attribute__((packed));


extern union trip_status_union trip;


union open_union
{		
	struct open_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));			

struct open1_bits_struct
{
	unsigned ext:1 ;//;//__attribute__ ((packed));
	unsigned hm1:1 ;//;//__attribute__ ((packed));
	unsigned hm2:1 ;//;//__attribute__ ((packed));
	unsigned unused1:1 ;//;//__attribute__ ((packed));
	unsigned unused2:1 ;//;//__attribute__ ((packed));
	unsigned unused3:1 ;//;//__attribute__ ((packed));
	unsigned unused4:1 ;//;//__attribute__ ((packed));
	unsigned unused5:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

union open1_union
{		
	struct open1_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));	
		
struct shrt_bits_struct
{
	unsigned rm1:1 ;//;//__attribute__ ((packed));
	unsigned rm2:1 ;//;//__attribute__ ((packed));
	unsigned out1:1 ;//;//__attribute__ ((packed));
	unsigned out2:1 ;//;//__attribute__ ((packed));
	unsigned duct1:1 ;//;//__attribute__ ((packed));
	unsigned duct2:1 ;//;//__attribute__ ((packed));
	unsigned st_pt:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
					
union shrt_union
{		
	struct shrt_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct shrt1_bits_struct
{
	unsigned ext:1 ;//;//__attribute__ ((packed));
	unsigned hm1:1 ;//;//__attribute__ ((packed));
	unsigned hm2:1 ;//;//__attribute__ ((packed));
	unsigned unused1:1 ;//;//__attribute__ ((packed));
	unsigned unused2:1 ;//;//__attribute__ ((packed));
	unsigned unused3:1 ;//;//__attribute__ ((packed));
	unsigned unused4:1 ;//;//__attribute__ ((packed));
	unsigned unused5:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
					
union shrt1_union
{		
	struct shrt1_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));


struct loop_open_bits_struct
{
	unsigned hp11:1 ;//;//__attribute__ ((packed));
	unsigned hp12:1 ;//;//__attribute__ ((packed));
	unsigned hp21:1 ;//;//__attribute__ ((packed));
	unsigned hp22:1 ;//;//__attribute__ ((packed));
	unsigned lp11:1 ;//;//__attribute__ ((packed));
	unsigned lp12:1 ;//;//__attribute__ ((packed));
	unsigned lp21:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

union loop_open_union
{		
	struct loop_open_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));			

struct loop_open1_bits_struct
{
	unsigned lp22:1 ;//;//__attribute__ ((packed));
	unsigned ext1:1 ;//;//__attribute__ ((packed));
	unsigned ext2:1 ;//;//__attribute__ ((packed));
	unsigned unused1:1 ;//;//__attribute__ ((packed));
	unsigned unused2:1 ;//;//__attribute__ ((packed));
	unsigned unused3:1 ;//;//__attribute__ ((packed));
	unsigned unused4:1 ;//;//__attribute__ ((packed));
	unsigned unused5:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

union loop_open1_union
{		
	struct loop_open1_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));	
		
struct loop_shrt_bits_struct
{
	unsigned hp11:1 ;//;//__attribute__ ((packed));
	unsigned hp12:1 ;//;//__attribute__ ((packed));
	unsigned hp21:1 ;//;//__attribute__ ((packed));
	unsigned hp22:1 ;//;//__attribute__ ((packed));
	unsigned lp11:1 ;//;//__attribute__ ((packed));
	unsigned lp12:1 ;//;//__attribute__ ((packed));
	unsigned lp21:1 ;//;//__attribute__ ((packed));
	unsigned unused:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
					
union loop_shrt_union
{		
	struct loop_shrt_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct loop_shrt1_bits_struct
{
	unsigned lp22:1 ;//;//__attribute__ ((packed));
	unsigned ext1:1 ;//;//__attribute__ ((packed));
	unsigned ext2:1 ;//;//__attribute__ ((packed));
	unsigned unused1:1 ;//;//__attribute__ ((packed));
	unsigned unused2:1 ;//;//__attribute__ ((packed));
	unsigned unused3:1 ;//;//__attribute__ ((packed));
	unsigned unused4:1 ;//;//__attribute__ ((packed));
	unsigned unused5:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
					
union loop_shrt1_union
{		
	struct loop_shrt1_bits_struct bits ;//;//__attribute__ ((packed));
	ubyte byte ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
													
struct log_status_struct
{
//	ubyte plant_opr_mode[17];
	union date_union date ;//;//__attribute__ ((packed));
	ubyte comma1 ;//;//__attribute__ ((packed));
	union time_union time ;//;//__attribute__ ((packed));
	ubyte comma2;
	struct	temp_struct temp[10] ;//;//__attribute__ ((packed));
//	ubyte comma23;
	struct	temp_struct loop[10] ;//;//__attribute__ ((packed));
//	ubyte comma24;
	union di1_union di1 ;//;//__attribute__ ((packed));
	ubyte comma3 ;//;//__attribute__ ((packed));
	union di2_union di2 ;//;//__attribute__ ((packed));
	ubyte comma4 ;//;//__attribute__ ((packed));	
	union di3_union di3 ;//;//__attribute__ ((packed));
	ubyte comma5 ;//;//__attribute__ ((packed));
	union di4_union di4 ;//;//__attribute__ ((packed));
	ubyte comma20 ;//;//__attribute__ ((packed));
	union rel1_union rel1 ;//;//__attribute__ ((packed));
	ubyte comma6 ;//;//__attribute__ ((packed));
	union rel2_union rel2 ;//;//__attribute__ ((packed));
	ubyte comma7 ;//;//__attribute__ ((packed));
	union rel3_union rel3 ;//;//__attribute__ ((packed));
	ubyte comma15 ;//;//__attribute__ ((packed));
	union blk1_union blk1 ;//;//__attribute__ ((packed));
	ubyte comma16 ;//;//__attribute__ ((packed));
	union blk2_union blk2 ;//;//__attribute__ ((packed));
	ubyte comma17 ;//;//__attribute__ ((packed));
	union open_union open ;//;//__attribute__ ((packed));
	ubyte comma18 ;//;//__attribute__ ((packed));
	union open1_union open1 ;//;//__attribute__ ((packed));
	ubyte comma21 ;//;//__attribute__ ((packed));
	union shrt_union shrt ;//;//__attribute__ ((packed));	
	ubyte comma8 ;//;//__attribute__ ((packed));
	union shrt1_union shrt1 ;//;//__attribute__ ((packed));	
	ubyte comma22 ;//;//__attribute__ ((packed));
	union loop_open_union loop_open ;//;//__attribute__ ((packed));
	ubyte comma25 ;//;//__attribute__ ((packed));
	union loop_open1_union loop_open1 ;//;//__attribute__ ((packed));
	ubyte comma26 ;//;//__attribute__ ((packed));
	union loop_shrt_union loop_shrt ;//;//__attribute__ ((packed));	
	ubyte comma27 ;//;//__attribute__ ((packed));
	union loop_shrt1_union loop_shrt1 ;//;//__attribute__ ((packed));	
	ubyte comma28 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
/*#################################################################################################################
 FAULT LOG STRUCTURE
###################################################################################################################*/
#define UNUSED_FAULT_BYTES	4
union fault_code_union
{
	ubyte byte[4] ;//;//__attribute__ ((packed));
	uinteger word[2] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
union fault_nature_union                     //added
{		
	ubyte byte[16] ;//__attribute__((packed));
	ubyte chars[16] ;//__attribute__((packed));
}	__attribute__((packed));

union fault_serial_no_union
{
	ubyte byte[4] ;//;//__attribute__ ((packed));
	uinteger word[2] ;//;//__attribute__ ((packed));
	unsigned long total ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct fault_log_struct							
{
	ubyte coach_id[7];
	ubyte coach_no[13];
	ubyte plant_opr_mode[17];
	ubyte nature_byte[16] ;//__attribute__((packed));
	ubyte comma9 ;//__attribute__((packed));
	union fault_code_union code ;//__attribute__((packed));
	ubyte comma10 ;//__attribute__((packed));
	union fault_serial_no_union serial_no ;//__attribute__((packed));
	ubyte comma11 ;//__attribute__((packed));
	struct log_status_struct status ;//__attribute__((packed));
	//ubyte network_info[16];
     union trip_status_union trip;
	ubyte unused[4] ;//__attribute__((packed));//24
//	ubyte comma12 ;//__attribute__((packed));
}	__attribute__ ((packed));


#define FAULT_LOG_STRUCT sizeof(struct fault_log_struct)
		
union fault_union
{
	struct fault_log_struct logs ;//;//__attribute__ ((packed));
	ubyte byte[FAULT_LOG_STRUCT] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

#define	FAULT_UNION	sizeof(union fault_union)

extern union fault_union fault;
/*#################################################################################################################
 EVENT LOG STRUCTURE
###################################################################################################################*/
#define UNUSED_EVENT_BYTES	3
union event_serail_no_union
{
	ubyte byte[4] ;//;//__attribute__ ((packed));
	uinteger word[2] ;//;//__attribute__ ((packed));
	unsigned long total ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));
		
struct event_log_struct
{
	ubyte coach_id[7];
	ubyte coach_no[13];
	ubyte plant_opr_mode[17];
	union event_serail_no_union serial_no ;//__attribute__((packed));
	ubyte comma13 ;//__attribute__((packed));;
	struct log_status_struct status ;//__attribute__((packed));;
	//ubyte comma14 ;//__attribute__((packed));;   //changes done in main code
	//ubyte network_info[16];
    union fault_nature_union nature;
	ubyte comma15 ;//__attribute__((packed));;
	union fault_code_union code ;//__attribute__((packed));
	ubyte comma16 ;//__attribute__((packed));;
    union trip_status_union trip;
	ubyte unused[3] ;//__attribute__((packed));;//45
}	__attribute__ ((packed));	


#define EVENT_LOG_STRUCT sizeof(struct event_log_struct)
	
union event_union
{ 							
	struct event_log_struct log ;//;//__attribute__ ((packed));
	ubyte byte[EVENT_LOG_STRUCT] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

#define	EVENT_UNION	sizeof(union event_union)

extern union event_union event;

//struct unit_id_struct
//{
//	ubyte month[2];
//	ubyte year[2];
//	ubyte serial_no[5];
//}	__attribute__ ((packed));
//
//extern struct unit_id_struct unit_id;

# define MAX_FAULT_STR_SIZE		(ubyte)17
struct coach_info_struct
{
	union date_union date ;//;//__attribute__ ((packed));
	ubyte comma1;
	union time_union time ;//;//__attribute__ ((packed));
	ubyte comma2;
	ubyte unit_no[13];
	ubyte rmpu_make_pp_npp[17];
	ubyte rmpu_no_pp_npp[26];
	ubyte sw_ver[6];
	ubyte plant_opr_mode[17];
	ubyte fault[3][MAX_FAULT_STR_SIZE];
	ubyte fault_index;
	ubyte comma3;
	ubyte coach_no[7];
}	__attribute__ ((packed));
#define	COACH_INFO_STRUCT	sizeof(struct coach_info_struct)
extern struct coach_info_struct coach_info;

struct timers_struct
{
	unsigned on:1							;//__attribute__ ((packed));
	unsigned timeout:1						;//__attribute__ ((packed));
	uinteger count							;//__attribute__ ((packed));
}	__attribute__ ((packed));	

union comm_flags_union
{
	struct
	{
		unsigned frame_avail:1 					;//__attribute__ ((packed));
		unsigned timer:1 						;//__attribute__ ((packed));
		unsigned rx_enable:1 					;//__attribute__ ((packed));
		unsigned rx_comp:1 						;//__attribute__ ((packed));
		unsigned rx_stx:1 						;//__attribute__ ((packed));
		unsigned pkt_transmiting:1 				;//__attribute__ ((packed));
		unsigned ack_waiting:1 					;//__attribute__ ((packed));		
	}bits 										;//__attribute__ ((packed));
	uword word	 								;//__attribute__ ((packed));
} 												__attribute__ ((aligned (4)));
struct comm_struct
{
	union comm_flags_union flags				;//__attribute__ ((packed));
	uinteger rx_cnt								;//__attribute__ ((packed));
	uinteger rx_timer_cnt						;//__attribute__ ((packed));
	uinteger stx_timer_cnt						;//__attribute__ ((packed));
	uinteger total_tx_cnt						;//__attribute__ ((packed));	
} 												__attribute__ ((aligned (2), packed));
#define	COMM_STRUCT								sizeof(struct comm_struct)
extern struct comm_struct lhb_comm,prev_lhb_comm;

struct set_temp_struct
{
	ubyte curr_set_point_no;
	ubyte comma;
	ubyte curr_cool[5];
	ubyte curr_heat[5];
	ubyte cool1[5];
	ubyte heat1[5];
	ubyte cool2[5];
	ubyte heat2[5];
	ubyte cool3[5];
	ubyte heat3[5];
	ubyte cool4[5];
	ubyte heat4[5];
	ubyte cool5[5];
	ubyte heat5[5];
	ubyte cool6[5];
	ubyte heat6[5];
	ubyte cool7[5];
	ubyte heat7[5];
	struct	temp_struct temp[10] ;//;//__attribute__ ((packed));
	struct	temp_struct loop[10] ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

#define SET_TEMP_STRUCT sizeof( struct set_temp_struct)

union set_point_union
{
	struct set_temp_struct temp;
	ubyte bytes[SET_TEMP_STRUCT];
}	__attribute__ ((packed));

#define SET_POINT_UNION sizeof( union set_point_union)

extern union set_point_union set;		
	
union orphan_flags_union
{
	struct
	{
		unsigned default_timer:1				;//__attribute__ ((packed));
		unsigned default_time_out:1 			;//__attribute__ ((packed));
		unsigned delay_timer:1 					;//__attribute__ ((packed));
		unsigned u2_rx_byte:1					;//__attribute__ ((packed));
		unsigned send_pkt_to_disp:1				;//__attribute__ ((packed));
		unsigned both_rmpu_sel:1				;//__attribute__ ((packed));
		unsigned double_decker_sel:1				;//__attribute__ ((packed));
	
	}bits										;//__attribute__ ((packed));
	uinteger integ								;//__attribute__ ((packed));
} 												__attribute__ ((aligned (2), packed));
extern union orphan_flags_union orphan_flags;


struct menu_bits_struct
{
	unsigned refresh_display:1;
	unsigned data_received:1;
	unsigned use_up_dwn_keys:1;
	unsigned request_nxt_fault_frame:1;
	unsigned unused:12;
}	__attribute__ ((packed));
	
union menu_bits_union
{
	struct menu_bits_struct bits;
	unsigned short byte;
}	__attribute__ ((packed));

extern union menu_bits_union stat;

struct menu_struct
{
	urombyte *rec_heading_add;//address of heading for menu displayed
	uromptr *rec_options_add;//address of options strings in the menu
	ubyte window_st_num;
	ubyte window_size;
	ubyte hlight_line_num;
	ubyte total_size;
	ubyte font_size;
	ubyte font_index_ptr;
	ubyte curr_list_sel;
}	__attribute__ ((packed));

union orphan_flags2_union
{
	struct
	{
		unsigned header_logged:1				;//__attribute__ ((packed));
		unsigned coach_count_selected:1			;//__attribute__ ((packed));
		unsigned mode_selected:1				;//__attribute__ ((packed));
		unsigned right_angle:1					;//__attribute__ ((packed));
		unsigned led_line_ptr:1					;//__attribute__ ((packed));
		unsigned voice_page_excess:1			;//__attribute__ ((packed));
		unsigned usb_delay:1					;//__attribute__ ((packed));
		unsigned usb_detected:1					;//__attribute__ ((packed));
		unsigned first_gps_data:1				;//__attribute__ ((packed));
		unsigned shift_up:1						;//__attribute__ ((packed));
		unsigned shift_dwn:1					;//__attribute__ ((packed));
		unsigned route_selected:1				;//__attribute__ ((packed));
		unsigned event_latlong_log:1			;//__attribute__ ((packed));
		unsigned flash3_present:1				;//__attribute__ ((packed));
		unsigned voice_db_ver_present:1			;//__attribute__ ((packed));
		unsigned usb_gps_simulation:1			;//__attribute__ ((packed));
	}bits										;//__attribute__ ((packed));
	uinteger integ								;//__attribute__ ((packed));
} 												__attribute__ ((aligned (2), packed));
extern union orphan_flags2_union orphan_flags2;


union orphan_flags1_union
{
	struct
	{
		unsigned towards_stn:1					;//__attribute__ ((packed));
		unsigned chk_distance:1					;//__attribute__ ((packed));
		unsigned no_arrival:1					;//__attribute__ ((packed));
		unsigned shortro_msg:1					;//__attribute__ ((packed));
		unsigned shortro_config:1					;//__attribute__ ((packed));
		unsigned gen_msg:1						;//__attribute__ ((packed));
		unsigned param_info:1					;//__attribute__ ((packed));
		unsigned halt_msg:1						;//__attribute__ ((packed));
		unsigned end_msg:1						;//__attribute__ ((packed));
		unsigned shortro_timer:1					;//__attribute__ ((packed));
		unsigned config_timer:1					;//__attribute__ ((packed));
		unsigned announce_timer:1				;//__attribute__ ((packed));
		unsigned voice_timer:1					;//__attribute__ ((packed));
		unsigned menu_timer:1					;//__attribute__ ((packed));
		unsigned menu_time_out:1				;//__attribute__ ((packed));
		unsigned halt_timer_started:1			;//__attribute__ ((packed));
	}bits										;//__attribute__ ((packed));
	uinteger integ								;//__attribute__ ((packed));
} 												__attribute__ ((aligned (2), packed));
extern union orphan_flags1_union orphan_flags1;

struct frame_flags_struct
{
		unsigned default_frame_rcvd:1 			;//__attribute__ ((packed));
		unsigned date_time_frame_rcvd:1 		;//__attribute__ ((packed));
		unsigned coach_no_frame_rcvd:1		    ;//__attribute__ ((packed));
		unsigned unit_no_frame_rcvd:1		    ;//__attribute__ ((packed));
		unsigned rmpu_make_frame_rcvd:1		    ;//__attribute__ ((packed));
		unsigned rmpu_no_frame_rcvd:1 			;//__attribute__ ((packed));
		unsigned plant_status_frame_rcvd:1 		;//__attribute__ ((packed));
		unsigned crc_fail_frame_rcvd:1 			;//__attribute__ ((packed));
		unsigned stored_fault_frame_rcvd:1		;//__attribute__ ((packed));
		unsigned stored_log_frame_rcvd:1 		;//__attribute__ ((packed));
		unsigned test_mode_frame_rcvd:1			;//__attribute__ ((packed));
		unsigned pen_drive_frame_rcvd:1 		;//__attribute__ ((packed));
		unsigned upgrade_firmware_frame_rcvd:1	;//__attribute__ ((packed));
		unsigned networking_frame_rcvd:1		;//__attribute__ ((packed));
		unsigned view_set_point_frame_rcvd:1	;//__attribute__ ((packed));
		unsigned set_set_point_frame_rcvd:1 	;//__attribute__ ((packed));
		unsigned view_trip_count_frame_rcvd:1 	;//__attribute__ ((packed));
		unsigned sleep_hrs_frame_rcvd:1 		;//__attribute__ ((packed));
		unsigned restore_default_frame_rcvd:1 	;//__attribute__ ((packed));
		unsigned exit_frame_rcvd:1 				;//__attribute__ ((packed));
		unsigned system_pen_drive_frame_rcvd:1 	;//__attribute__ ((packed));
		unsigned analysis_pen_drive_frame_rcvd:1;//__attribute__ ((packed));
		unsigned pen_drive_detected_frame_rcvd:1;//	__attribute__ ((packed));
		unsigned download_complete_frame_rcvd:1 ;//__attribute__ ((packed));
		unsigned stored_dutycycle_frame_rcvd:1		;//__attribute__ ((packed));//added
        unsigned view_dutycycle_trip_log:1;
        unsigned power_uses_frame_rcvd:1;
        unsigned update_power_data:1;
        unsigned unused:4 			 			;//__attribute__ ((packed));
		
}	__attribute__ ((packed));

union frame_flags_union
{
	struct frame_flags_struct bits;	
	uword integer;
}	__attribute__ ((packed));	
		

extern union frame_flags_union frame_flags;	
//#######################################################################################
//				structures for keypad module
//#######################################################################################
union key_num_union
{
	struct
	{
		unsigned col_4:1						;//__attribute__ ((packed));
		unsigned col_3:1						;//__attribute__ ((packed));
		unsigned col_2:1						;//__attribute__ ((packed));
		unsigned col_1:1						;//__attribute__ ((packed));
		unsigned row_1:1						;//__attribute__ ((packed));
		unsigned row_2:1						;//__attribute__ ((packed));
		unsigned row_3:1						;//__attribute__ ((packed));
		unsigned row_4:1						;//__attribute__ ((packed));
		unsigned keyreleased:1					;//__attribute__ ((packed));
		unsigned key_pressed:1					;//__attribute__ ((packed));
		unsigned key_debounce:1					;//__attribute__ ((packed));
		unsigned lcd_disp_timer_on:1			;//__attribute__ ((packed));
		unsigned star_hash_press:1				;//__attribute__ ((packed));
		unsigned long_key_press:1				;//__attribute__ ((packed));
		unsigned prev_state:1					;//__attribute__ ((packed));
		unsigned unused:1						;//__attribute__ ((packed));
	}bits										;//__attribute__ ((packed));
	struct
	{
		ubyte port								;//__attribute__ ((packed));
		ubyte flags								;//__attribute__ ((packed));
	}bytes										;//__attribute__ ((packed));
	uinteger integ								;//__attribute__ ((packed));
}												;//__attribute__ ((packed));
extern union key_num_union key_num;


struct key_struct
{
	unsigned state:1;
	unsigned pressed:1;
	unsigned continued_press:1;
	unsigned released:1;
	unsigned long_press:1;
	unsigned timer_on:1;
	unsigned timer_complete:1;
	unsigned cont_press_pulse:1;
	unsigned short timer_count;
	unsigned short cont_press_timer;
}	__attribute__ ((packed));

struct key_name_struct
{
	unsigned enter_pressed:1;
	unsigned enter_released:1;
	unsigned up_pressed:1;
	unsigned up_released:1;
	unsigned dwn_pressed:1;
	unsigned dwn_released:1;
	unsigned cancel_pressed:1;
	unsigned cancel_released:1;
}	__attribute__ ((packed));
extern struct key_struct keys[7],last_keys[7];
extern struct key_name_struct key_name;


struct test1_bits_struct
{
	unsigned t1:1 ;//;//__attribute__ ((packed));
	unsigned t2:1 ;//;//__attribute__ ((packed));
	unsigned t3:1 ;//;//__attribute__ ((packed));
	unsigned t4:1 ;//;//__attribute__ ((packed));
	unsigned t5:1 ;//;//__attribute__ ((packed));
	unsigned t6:1 ;//;//__attribute__ ((packed));
	unsigned t7:1 ;//;//__attribute__ ((packed));
	unsigned t8:1 ;//;//__attribute__ ((packed));
	unsigned t9:1 ;//;//__attribute__ ((packed));
	unsigned t10:1 ;//;//__attribute__ ((packed));
	unsigned t11:1 ;//;//__attribute__ ((packed));
	unsigned t12:1 ;//;//__attribute__ ((packed));
	unsigned t13:1 ;//;//__attribute__ ((packed));
	unsigned t14:1 ;//;//__attribute__ ((packed));
	unsigned t15:1 ;//;//__attribute__ ((packed));
	unsigned t16:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct test1_bytes_struct
{
	ubyte	byte1;
	ubyte	byte2;
}	__attribute__ ((packed));	


union test1_union
{

	struct test1_bits_struct bits ;//;//__attribute__ ((packed));
	struct test1_bytes_struct bytes ;//;//__attribute__ ((packed));
	uinteger	test_int;	
}	__attribute__ ((packed));		


struct test2_bits_struct
{
	
	unsigned t17:1 ;//;//__attribute__ ((packed));
	unsigned t18:1 ;//;//__attribute__ ((packed));
	unsigned t19:1 ;//;//__attribute__ ((packed));
	unsigned t20:1 ;//;//__attribute__ ((packed));
	unsigned t21:1 ;//;//__attribute__ ((packed));
	unsigned t22:1 ;//;//__attribute__ ((packed));
	unsigned t23:1 ;//;//__attribute__ ((packed));
	unsigned t24:1 ;//;//__attribute__ ((packed));
	unsigned t25:1 ;//;//__attribute__ ((packed));
	unsigned t26:1 ;//;//__attribute__ ((packed));
	unsigned t27:1 ;//;//__attribute__ ((packed));
	unsigned t28:1 ;//;//__attribute__ ((packed));
	unsigned t29:1 ;//;//__attribute__ ((packed));
	unsigned t30:1 ;//;//__attribute__ ((packed));
	unsigned t31:1 ;//;//__attribute__ ((packed));
	unsigned t32:1 ;//;//__attribute__ ((packed));
}	__attribute__ ((packed));

struct test2_bytes_struct
{
	ubyte	byte1;
	ubyte	byte2;
}	__attribute__ ((packed));	
		
union test2_union
{

	struct test2_bits_struct bits ;//;//__attribute__ ((packed));
	struct test2_bytes_struct bytes ;//;//__attribute__ ((packed));
	uinteger	test_int;	
}	__attribute__ ((packed));

extern union test1_union test1;
extern union test2_union test2;


struct network_flags_struct
{
		unsigned start_nw_mode:1 	;//__attribute__ ((packed));
		unsigned unused:15 				;//__attribute__ ((packed));
}	__attribute__ ((packed));

union network_flags_union
{
	struct network_flags_struct bits;	
	uinteger integer;
}	__attribute__ ((packed));
	

struct network_struct
{
	ubyte coach_no[13] 	;//__attribute__ ((packed));
	union network_flags_union flags	;//__attribute__ ((packed));
	
}	__attribute__ ((packed));
extern struct network_struct network;

extern unsigned char fact_pswd[4];

struct lcd_fault_log_struct
{
	ubyte fault_str[17]							;//;//__attribute__ ((packed));
	union date_union date 					;//;//__attribute__ ((packed));
	ubyte comma1 							;//;//__attribute__ ((packed));
	union time_union time 					;//;//__attribute__ ((packed));
	ubyte comma2 							;//;//__attribute__ ((packed));   
}	__attribute__ ((packed));	
#define LCD_FAULT_LOG_STRUCT				sizeof(struct lcd_fault_log_struct)
extern struct lcd_fault_log_struct fault_log[FAULTS_ON_LCD];
//*************************************************************************************


union dc_serial_no_union
{
	ubyte byte[4] ;//__attribute__((packed));
	uinteger word[2] ;//__attribute__((packed));
	unsigned long total ;//__attribute__((packed));
}	__attribute__((packed));

struct dc_time_struct
{
    uinteger  hrs;
    ubyte     min;
    ubyte     sec;
}__attribute__((packed));

typedef struct eClock_union 
{
    struct dc_time_struct  time;
    union time_union StartTimeStamp ;
    union date_union StartDateStamp;
}eClock   __attribute__((packed));

typedef struct EquipmentCycleStruct
{
    eClock clock;
//    uword OnCounts;
    ubyte duty;
}EQUIPMENT_CYCLE  __attribute__((packed));
typedef struct LHBEquipment_struct
{
    EQUIPMENT_CYCLE   CurrCycle;
    EQUIPMENT_CYCLE   TripCycle;
    EQUIPMENT_CYCLE   MainCycle; 
}LHB_EQUIPMENT __attribute__((packed));;
struct DutyCycle_Struct
{
  eClock MainClock;
  eClock TripClock;
  eClock CycleClock;
  LHB_EQUIPMENT  Blowers     [2][2];
  LHB_EQUIPMENT  Condenser   [2][2];
  LHB_EQUIPMENT  Compressors [2][2];
  LHB_EQUIPMENT  Heaters     [2];
} __attribute__((packed));
#define SIZEOF_DutyCycle_Struct sizeof(struct DutyCycle_Struct)

typedef union DutyCycle_union   
{
    struct DutyCycle_Struct data;
    ubyte  bytes[SIZEOF_DutyCycle_Struct];
 }eDutyCycle __attribute__((packed));
 struct dutycycle_structure
 {
    eDutyCycle   DutyCycles;
    union trip_status_union trip;
	ubyte crc_high;//__attribute__ ((packed));
	ubyte crc_low;//__attribute__ ((packed));
 };

 union dutycycle_union
 {
     struct dutycycle_structure data;
     ubyte bytes[sizeof( struct dutycycle_structure)];
 };
 extern  union dutycycle_union DutyCycleLog,EquipmentLogs;

struct dutylogstruct
{
    union dc_serial_no_union serial_num;
    union time_union StartTimeStamp ;
    union date_union StartDateStamp;  
    union time_union CurrTimeStamp ;
    union date_union CurrDateStamp;
    ubyte  Blowers     [2][2];
    ubyte  Condenser   [2][2];
    ubyte  Compressors [2][2];
    ubyte  Heaters     [2];
    
};
#define DUTYLOGSTRUCT				sizeof(struct dutylogstruct)
//extern struct dutylogstruct D_log[FAULTS_ON_LCD];
union dutylogunion
{
    
    struct dutylogstruct log;
    ubyte bytes[sizeof(struct dutylogstruct)];
};
extern union dutylogunion DutyLog, lcd_dutylog;
//********************************************************************************************************************************


extern union time_union sleep_hrs_start_bytes;
extern union time_union sleep_hrs_end_bytes;

union key_alpha_union
{
	struct
	{
		unsigned timer_on:1						;//__attribute__ ((packed));
		unsigned lcd_disp_on:1					;//__attribute__ ((packed));
		unsigned key_pressed:1					;//__attribute__ ((packed));
		unsigned unused:13						;//__attribute__ ((packed));
	}bits										;//__attribute__ ((packed));
	uinteger integ								;//__attribute__ ((packed));
}												;//__attribute__ ((packed));
extern union key_alpha_union key_alpha;

struct overide_stat_struct
{
	unsigned blower_fan_00_fault_override:1;
	unsigned blower_fan_01_fault_override:1;
	unsigned blower_fan_10_fault_override:1;
	unsigned blower_fan_11_fault_override:1;
	unsigned heater_0_fault_override:1;
	unsigned heater_1_fault_override:1;
	unsigned cond_00_fault_override:1;
	unsigned cond_01_fault_override:1;
	unsigned cond_10_fault_override:1;
	unsigned cond_11_fault_override:1;
	unsigned LP_00_fault_override:1;
	unsigned LP_01_fault_override:1;
	unsigned LP_10_fault_override:1;
	unsigned LP_11_fault_override:1;
	unsigned HP_00_fault_override:1;
	unsigned HP_01_fault_override:1;
	unsigned HP_10_fault_override:1;
	unsigned HP_11_fault_override:1;
}	__attribute__((packed));	

union override_stat_union
{
	struct overide_stat_struct stat;
	uword override_stat_word;
}	__attribute__((packed));	
	
extern union override_stat_union	override;	
	
struct server_struct
{
	ubyte ip_add[16];
	ubyte port_no[2][5];
}					__attribute__((packed));
extern struct server_struct server, temp_server;

struct server_status_struct
{
	union date_union last_ack_date ;//__attribute__((packed));
	ubyte comma1;
	union time_union last_ack_time ;//__attribute__((packed));
	ubyte comma2;
	union date_union last_attmpt_date ;//__attribute__((packed));
	ubyte comma3;
	union time_union last_attmpt_time ;//__attribute__((packed));
	ubyte comma4;
	ubyte fault;
//	ubyte event_pkt[4];
//	ubyte fault_pkt[4];
}					__attribute__((packed));
extern struct server_status_struct server_s;

struct plant_info_struct
{
	struct coach_info_struct coach_info			;//__attribute__ ((packed));
	union set_point_union set					;//__attribute__ ((packed));
//	union event_union event						;//__attribute__ ((packed));
	union fault_union fault						;//__attribute__ ((packed));
	union trip_status_union trip				;//__attribute__ ((packed));
	union test1_union test1						;//__attribute__ ((packed));
	union test2_union test2						;//__attribute__ ((packed));
	union override_stat_union	override;		;
	struct server_struct server					;
	struct server_status_struct server_status	;
	ubyte event_pkt[5]							;
	ubyte fault_pkt[5]							;	
	ubyte flap_open[4]							;
	ubyte fault_close[4]						;
	ubyte both_rmpu_sel							;
	ubyte comma_rmpu							;
	ubyte double_decker_sel						;
	ubyte comma_double							;
	ubyte disp_sw_vrs[6]						;
	ubyte gprs_sw_ver[6]						;
}	__attribute__((packed));	

union complete_plant_union
{
	struct plant_info_struct info					;//__attribute__ ((packed));
	ubyte bytes[sizeof(struct plant_info_struct)]	;//__attribute__ ((packed));
}	__attribute__ ((packed));	

#define	COMPLETE_PLANT_UNION	sizeof(union complete_plant_union)
extern union complete_plant_union	complete_plant_tx,complete_plant_rx;	

#define	MAX_NO_OF_SLAVE_LHB		25
extern union complete_plant_union complete_slave_plant[MAX_NO_OF_SLAVE_LHB];

struct rxtx_frame_struct
{
	ubyte stx[L_STX]							;//__attribute__ ((packed));
	ubyte length[L_FR_LEN]						;//__attribute__ ((packed));
	ubyte src_id[L_SRC_ID]						;//__attribute__ ((packed));
	ubyte dest_id[L_DEST_ID]					;//__attribute__ ((packed));
	ubyte func_code[L_FUNC_CODE]				;//__attribute__ ((packed));
	ubyte data_crc_bytes[L_DATA]				;//__attribute__ ((packed));
	struct timers_struct timer					;//__attribute__ ((packed));
	unsigned new_pkt_rcvd:1						;//__attribute__ ((packed));
}												__attribute__ ((aligned (2), packed));
#define		RXTX_FRAME_STRUCT					sizeof(struct rxtx_frame_struct)

union rxtx_frame_union
{
	struct rxtx_frame_struct data				;//__attribute__ ((packed));
	uinteger integ[RXTX_FRAME_STRUCT / 2]		;//__attribute__ ((packed));
	ubyte bytes[RXTX_FRAME_STRUCT] 				;//__attribute__ ((packed));
} 												__attribute__ ((aligned (32), packed));
#define		RXTX_FRAME_UNION					sizeof(union rxtx_frame_union)
extern union rxtx_frame_union rx;
extern union rxtx_frame_union tx;

struct set_value_struct
{
	ubyte serial[1];
	ubyte cooling[4];
	ubyte heating[4];
} 													__attribute__ ((aligned (2)));			//172 bytes
#define		SET_VALUE_STRUCT						sizeof(struct set_value_struct)

typedef union{
  struct {
      signed short high;
      signed short low;
  }data;
 float num;  
}tF_DATA;
#define MAX_POWER_CREADINGS 15
extern tF_DATA PowerReadings[MAX_POWER_CREADINGS];