extern ubyte disp_sw_vrs[6];
extern ubyte gprs_sw_ver[6];

extern urombyte alpha_keys_lookup[5][4][4];
extern urombyte set_values_arr[7][30];

extern uromint baud_rate_lookup[];

extern urombyte disp_view_press_sens_stat[];
extern urombyte  disp_HP_11[];
extern urombyte  disp_HP_12[];
extern urombyte  disp_HP_21[];
extern urombyte  disp_HP_22[];

extern urombyte  disp_LP_11[];
extern urombyte  disp_LP_12[];
extern urombyte  disp_LP_21[];
extern urombyte  disp_LP_22[];

extern urombyte disp_view_usb_menu[];

extern urombyte disp_switch_on_computer_ok_relay[];
extern urombyte disp_switch_on_sf11_sf12_relay[];
extern urombyte disp_switch_on_sf21_sf22_relay[];
extern urombyte disp_switch_on_cond11_cond12[];
extern urombyte disp_switch_on_cond21_cond22[];

extern urombyte disp_sorry_time_out[];
extern urombyte disp_enter_pswd[];
extern urombyte disp_pswd_not_stored[];
extern urombyte disp_pswd_not_ok[];
extern urombyte disp_file_open_error[];
extern urombyte disp_pswd_ok[];
extern urombyte disp_invalid_entry[];
extern urombyte disp_try_again[];

extern uromint baud_rate_lookup[];
extern urombyte look_table_high[];
extern urombyte look_table_low [];
extern urombyte disp_sorry_time_out[];


extern urombyte disp_menu_mode[];
extern urombyte disp_change_default_settings[];// =  		"CHANGE DEFAULT SETTINGS              ";//main menu//0
extern urombyte disp_view_plant_status[];// =    		    "VIEW PLANT STATUS                    ";//1
extern urombyte disp_view_test_menu[];// = 				"VIEW TEST MENU                       ";//2
extern urombyte disp_view_factory_menu[];// = 			"VIEW FACTORY MENU                    ";//3
extern urombyte disp_view_supervisor_menu[];// = 			"VIEW SUPERVISOR MENU                 ";//4
extern urombyte disp_view_fault_data[];// = 				"VIEW FAULT DATA                      ";//5
extern urombyte disp_view_dutycycle_data[];
extern urombyte disp_view_dutycycle_trip_log[];
extern urombyte disp_networking_mode[];// = 				"NETWORKING MODE                      ";//6
extern urombyte disp_return_to_default_screen[];// = 		"RETURN TO DEFAULT SCREEN             ";//7
extern urombyte disp_view_system_log[];
extern urombyte disp_view_analysis_log[];
extern urombyte disp_supervisor_menu[];//=				"SUPERVISOR MENU                      ";
extern urombyte disp_sleep_hrs[];//=						"SLEEP HOURS                          ",
extern urombyte disp_set_default[];//=					"RESTORE DEFAULT SETTINGS             ",
extern urombyte disp_set_point_sel_source_menu[];//=		"VIEW SET POINT SELECTION SOURCE MENU ";
extern urombyte disp_select_both_rmpu[];//=				"SELECT BOTH RMPU         	          ";//93
extern urombyte disp_select_single_rmpu[];//=				"SELECT SINGLE RMPU                   ";//94
extern urombyte disp_select_double_decker[];//=			"SELECT DOUBLE DECKER                 ";//95
//extern urombyte disp_return_to_main_menu[];//=		    "RETURN TO MAIN MENU";//43
extern urombyte disp_view_version_nos[];// =    		    "VIEW PLANT STATUS                    ";//1


extern urombyte disp_select_display_setpt[];//=			"SELECT DISPLAY SET POINT             ";
extern urombyte disp_select_rotary_setpt[];//=			"SELECT ROTARY SET POINT              ";
extern urombyte disp_return_to_supervisor_menu[];//=		"RETURN TO SUPERVISOR MENU";//43


extern urombyte disp_change_date_time[];
extern urombyte disp_change_coach_no[];
extern urombyte disp_change_unit_no[];
extern 	urombyte disp_change_rmpu_no[];
extern 	urombyte disp_change_rmpu_make[];
extern 	urombyte disp_return_to_main_menu[];

//extern 	urombyte disp_change_7_set_temperatures[];
////extern 	urombyte disp_return_to_main_menu

extern 	urombyte disp_test_computer_ok[];
extern urombyte disp_test_computer_notok[];
extern 	urombyte disp_test_air_motor_rev[];
extern 	urombyte disp_test_supply_air_fan11[];
extern 	urombyte disp_test_bypass_cooling1[];
extern 	urombyte disp_test_condenser_fan11[];
extern 	urombyte disp_test_condenser_fan12[];
extern urombyte disp_test_compressor11[];
extern 	urombyte disp_test_compressor12[];
extern 	urombyte disp_test_heater1[];
extern 	urombyte disp_test_fault_bit[];
extern 	urombyte disp_test_air_motor_fwd[];
extern 	urombyte disp_test_supply_air_fan12[];
extern 	urombyte disp_test_exhaust_fans[];
extern 	urombyte disp_test_bypass_cooling2[];
extern 	urombyte disp_test_condenser_fan21[];
extern 	urombyte disp_test_condenser_fan22[];
extern 	urombyte disp_test_compressor21[];
extern 	urombyte disp_test_compressor22[];
extern 	urombyte disp_test_heater2[];
extern urombyte disp_test_EXTRA_REL1[];
extern urombyte disp_test_EXTRA_REL2[];
extern urombyte disp_test_EXTRA_REL3[];
extern urombyte disp_test_EXTRA_REL4[];
//extern 	urombyte disp_return_to_main_menu

extern 	urombyte disp_set_rtc_time[];
extern 	urombyte disp_select_usb_menu[];
//extern 	urombyte disp_return_to_main_menu
extern urombyte disp_usb_menu[];

extern 	urombyte disp_download_faults_to_usb[];
extern 	urombyte disp_download_event_log_to_usb[];
extern 	urombyte disp_upgrade_firmware_bootloader[];
extern 	urombyte disp_return_to_factory_menu[];
extern 	urombyte disp_download_faults_to_usb[];//usb menu//92
extern 	urombyte disp_download_event_log_to_usb[];//93

extern 	uromptr disp_excl_test_messages[LEN_MESSAGES];
extern 	uromptr disp_menu_mode_options[LEN_MAIN_MENU];
extern 	uromptr disp_default_settings_options[LEN_DEFAULT_MENU];
extern 	uromptr disp_test_menu_options[LEN_TEST_MENU];	
extern 	uromptr disp_factory_menu_options[LEN_FACTORY_MENU];
extern 	uromptr disp_usb_menu_options[LEN_USB_MENU];
extern uromptr disp_sup_menu_options[LEN_SUPERVISOR_MENU];
extern uromptr disp_setpt_src_sel_menu_options[LEN_SP_SOURCE_MENU];



extern void (*const exec_menu_mode_func_ptr[LEN_MAIN_MENU])(void);
extern void (*const exec_default_settings_func_ptr[LEN_DEFAULT_MENU])(void);
extern void (*const exec_factory_menu_func_ptr[LEN_FACTORY_MENU])(void);
extern void (*const exec_sup_menu_func_ptr[LEN_SUPERVISOR_MENU])(void);
extern void (*const exec_test_menu_func_ptr[LEN_TEST_MENU])(void);
extern void (*const exec_usb_menu_func_ptr[LEN_USB_MENU])(void);
//extern void (*const exec_factory_menu_func_ptr[3])(void);
//extern void (*const exec_usb_menu_func_ptr[4])(void);

extern urombyte disp_date[];
extern urombyte disp_time[];
extern urombyte disp_temp_coach_no[];
extern urombyte disp_coach_no[];
extern urombyte disp_temp_unit_no[];
extern urombyte disp_unit_no[];
extern urombyte disp_rmpu_make[];
extern urombyte disp_rmpu_no[];
extern urombyte disp_rmpu_no_pp[];
extern urombyte disp_rmpu_no_npp[];
extern urombyte disp_lhb_sw_ver[];
extern urombyte disp_operational_mode[];
extern urombyte disp_curr_flt1[];
extern urombyte disp_curr_flt2[];
extern urombyte disp_curr_flt3[];
extern urombyte disp_press_menu_for_options[];


extern urombyte disp_set_pt[];
extern urombyte disp_set_pt_ref_table[];
extern urombyte disp_coolsp[];
//extern urombyte disp_heatsp[];
extern urombyte disp_sp[];
extern urombyte view_next_faults[];
//extern urombyte disp_cool[];
//extern urombyte disp_heat[];
extern urombyte disp_cutin[];
extern urombyte disp_cutout[];
extern urombyte disp_1[];
extern urombyte disp_2[];
extern urombyte disp_3[];
extern urombyte disp_4[];
extern urombyte disp_5[];
extern urombyte disp_6[];
extern urombyte disp_7[];
extern urombyte disp_8[];
extern urombyte disp_RA1[];
extern urombyte disp_RA2[];
extern urombyte disp_SA1[];
extern urombyte disp_SA2[];
extern urombyte disp_RH1[];
extern urombyte disp_RH2[];
extern urombyte disp_AT1[];
extern urombyte disp_AT2[];
extern urombyte disp_return_to_default_screen[];

extern urombyte disp_local[];
extern urombyte disp_hvac[];
extern urombyte disp_mode[];
extern urombyte disp_400V[];
extern urombyte disp_Sensors[];
extern urombyte disp_Cpr1[];
extern urombyte disp_Cpr2[];
extern urombyte disp_Blowers[];
extern urombyte disp_Cfans[];
extern urombyte disp_Comp[];
extern urombyte disp_Heater[];	
extern urombyte disp_1_1[];
extern urombyte disp_1_2[];
extern urombyte disp_2_1[];
extern urombyte disp_2_2[];
extern urombyte disp_Th[];
extern urombyte disp_LP[];	
extern urombyte disp_HP[];
extern urombyte disp_OHP[];

extern urombyte disp_dutycycle_data[];
extern urombyte disp_dutycycle_trip_log[];
extern urombyte disp_fault_data[];
extern urombyte disp_event_log[];
extern urombyte disp_strt_date_time[];
extern urombyte disp_curr_date_time[];
extern urombyte disp_change_date_and_time[];
extern urombyte disp_change_Coach_no[];
extern urombyte disp_change_Unit_no[];
extern urombyte disp_change_RMPU_no[];
extern urombyte disp_change_RMPU_make[];

extern urombyte disp_Press_updown_to_chng_data[];
extern urombyte disp_Press_ack_to_save[];
extern urombyte disp_Press_enter_for_default_menu[];
extern urombyte char_value_arr[38];
extern urombyte disp_settemp_heading1[];
extern urombyte disp_settemp_heading2[];
extern urombyte disp_return_to_operating_menu[];

extern urombyte disp_temp_server_set[];
extern urombyte disp_act_server_set[];
extern urombyte disp_server_set[];

extern urombyte disp_temp_set_pt[];
extern urombyte disp_temp_rmpu_no[];
extern urombyte disp_temp_rmpu_make[];
extern urombyte disp_date_and_time_from_lhb[];

extern urombyte disp_view_normal_test_menu[];
extern urombyte disp_view_exlus_test_menu[];

extern urombyte disp_select_faults_to_bypass[];

extern uromptr disp_test_normal_menu_options[LEN_NORMAL_TEST_MENU];
extern uromptr disp_test_exlusive_menu_options[LEN_EXCL_TEST_MENU];
extern uromptr disp_select_faults_to_bypass_options[LEN_FAULT_BYPASS];

extern urombyte disp_return_to_test_menu[];

extern const struct set_value_struct set_temp[7];
extern urombyte menu_disp_change_default_settings[];
extern urombyte disp_return_to_main_menu_from_rmpu_details[];
extern urombyte disp_test_mode[];
extern urombyte disp_return_to_main_menu_from_test_mode[];
extern urombyte disp_normal_test_menu[];
extern urombyte disp_exlus_test_menu[];
extern urombyte disp_return_to_main_menu_from_supervisor_menu[];
extern urombyte disp_return_to_main_menu_from_factory_menu[];
extern urombyte disp_factory_menu[];
