#define		COMMAND								0
#define		DATA								1



#define 	lcd_cursor_right()					lcd_byte(0x14, COMMAND)
#define 	lcd_cursor_left()					lcd_byte(0x10, COMMAND)
#define 	lcd_disp_shift()					lcd_byte(0x1C, COMMAND)
#define 	lcd_clr()							lcd_byte(0x01, COMMAND) 
#define 	home_it()							lcd_byte(0x02, COMMAND) 
#define 	lcd_line_2()						lcd_byte(0xC0, COMMAND) 
#define 	lcd_line_1()						lcd_byte(0x80, COMMAND)
#define 	lcd_cursor_blink_on()				lcd_byte(0x0F, COMMAND)
#define 	lcd_cursor_blink_off()				lcd_byte(0x0E, COMMAND)
	
#define DEFAULT_SCREEN_MENU_SIZE 			2
#define MAIN_MENU_SIZE 						8
#define CHANGE_DEFAULT_SETTINGS_MENU_SIZE 	5
#define	CHANGE_DATE_TIME_MENU_SIZE 			4
#define	CHANGE_COACH_NO_MENU_SIZE 			4
#define	CHANGE_UNIT_NO_MENU_SIZE 			4
#define	CHANGE_RMPU_NO_MENU_SIZE 			4
#define	CHANGE_RMPU_MAKE_MENU_SIZE 			4
#define STATUS_OF_PLANT_MENU_SIZE 			11
#define SET_OPERATING_PARAMETERS_MENU_SIZE	2
#define	SEVEN_POINT_CHANGE_MENU_SIZE 		8
#define TEST_MENU_SIZE 						8// NEEDS TO CHANGE
#define FACTORY_MENU_SIZE 					4			
#define	SET_RTC_TIME_MENU_SIZE 				4
#define USB_MENU_SIZE 						4	
#define FAULT_DATA_MENU_SIZE 				8
#define EVENT_LOG_MENU_SIZE					8	
#define SEVEN_POINT_SELECT_MENU_SIZE 		10


#define  TOTAL_SW							6

//GENERAL MACROS
#define	nop()								asm("nop")
#define	NOT_NUMBER							(unsigned)0
#define	NOT_ALPHABET						(unsigned)0
#define	NOT_ALPHANUM						(unsigned)0
#define	NOT_VALID							(unsigned)0
#define	NUMBER								(unsigned)1
#define	ALPHABET							(unsigned)1
#define	ALPHANUM							(unsigned)1
#define	VALID								(unsigned)1
#define	CARRIAGE_RETURN						(ubyte)13			//0x0D
#define	LINE_FEED							(ubyte)10			//0x0A
#define	H_TAB								(ubyte)9			//0x09
#define	PI									(float)(22/7)

//function codes
#define	STX_STR								"STX"
#define	ETX_STR								"ETX"
#define ID_DEST								"LHB         ,"
#define	ID_SRC								"LCD_DISP    ,"

#define	L_STX								(uinteger)(sizeof(STX_STR)-1)
#define	L_FR_LEN							(uinteger)5
#define	L_SRC_ID							(uinteger)(sizeof(ID_SRC)-1)
#define	L_DEST_ID							(uinteger)(sizeof(ID_DEST)-1)
#define	L_FUNC_CODE							(uinteger)2
#define	L_CRC								(uinteger)2
#define	L_ETX								(uinteger)(sizeof(ETX_STR)-1)
#define	L_DATA								(uinteger)((COMPLETE_PLANT_UNION*MAX_NO_OF_SLAVE_LHB)+L_CRC+L_ETX)
#define	L_FRAME_INIT						(uinteger)(L_STX+L_FR_LEN+L_SRC_ID+L_DEST_ID+L_FUNC_CODE)
#define	L_FRAME_LEN(DATA)					(uinteger)(L_STX+L_FR_LEN+L_SRC_ID+L_DEST_ID+L_FUNC_CODE+DATA+L_CRC+L_ETX)
#define	L_DATA_LEN(FRAME)					(uinteger)(FRAME-L_STX-L_FR_LEN-L_SRC_ID-L_DEST_ID-L_FUNC_CODE-L_CRC-L_ETX)

#define		FC_DEFAULT							'A'
#define 	FC_DATE_TIME						'B'
#define		FC_RMPU_MAKE						'C'
#define 	FC_RMPU_NO							'D'
#define 	FC_PLANT_STATUS						'E' 
#define		FC_CRC_FAIL							'F'
#define		FC_VIEW_STORED_FAULTS				'G'
#define		FC_VIEW_STORED_LOG					'H'
#define		FC_TEST_MODE						'I'
#define		FC_DWNLD_TO_PEN_DRIVE				'J'
#define		FC_UPGRADE_FIRMWARE					'K'
#define		FC_NETWORKING						'L'
#define		FC_VIEW_TEMP_SETPOINT				'M'
#define		FC_SET_TEMP_SETPOINT				'N'
#define		FC_VIEW_TRIPS						'O'
#define		FC_SEL_DOUBLE_DECKER				'P'
#define		FC_SEL_SET_POINT_ROTARY				'Q'
#define		FC_SEL_BOTH_RMPU					'R'
#define		FC_COACH_NO							'S'
#define		FC_SLEEP_HRS						'T'
#define		FC_RESTORE_DEFAULT					'U'
#define		FC_VIEW_PREV_STORED_FAULTS			'V'
#define		FC_FAULT_OVERRIDE					'W'
#define		FC_EXIT								'X'
#define		FC_SERVER							'Y'
#define		FC_UNIT_NO							'Z'
#define 	FC_PEN_DRIVE_DETECTED				'0'
#define 	FC_SYSTEM_LOG_TO_PEN_DRIVE			'1'
#define		FC_ANALYSIS_LOG_TO_PEN_DRIVE		'2'
#define		FC_VIEW_STORED_SYSTEM_LOG			'3'
#define		FC_VIEW_PREV_STORED_SYSTEM_LOG		'4'
#define		FC_VIEW_STORED_ANALYSIS_LOG			'5'
#define		FC_VIEW_PREV_STORED_ANALYSIS_LOG	'6'
#define		FC_NXT_FAULT_SCREEN					'9'
#define		FC_PEN_DRIVE_DETECTED_WAIT        	'7'
#define		FC_VIEW_STORED_DUTYCYCLE		    '8'
#define		FC_VIEW_PREV_STORED_DUTYCYCLE       '#'
#define		FC_VIEW_NEXT_STORED_DUTYCYCLE       '$'
#define     FC_VIEW_DUTYCYCLE_TRIP_LOG          '@'
#define     FC_VIEW_POWER_USESE                 '`'
//#define		STN_UPLD_CODE			'S'
//#define		FONT_UPLD_CODE			'F'
//#define		OLD_FONT_UPLD_CODE		'O'
//#define		UPLD_ACK				'R'
//#define		INIT_COMM				'U'



#define		 FALSE								(ubyte)0
#define 	 TRUE								(ubyte)1

#define 	 FAIL								(ubyte)0
#define 	 SUCCESS							(ubyte)1

#define 	 NO									(ubyte)0
#define 	 YES								(ubyte)1

#define 	 false								(ubyte)0
#define 	 true								(ubyte)1

#define 			room_sensor1			sensor[0]	//RA1
#define 			room_sensor2 			sensor[1]	//RA2
#define				outdoor_sensor1 		sensor[2]	//AT1
#define				outdoor_sensor2 		sensor[3]	//AT2
#define				humidity_sensor			sensor[4]	//SA1
#define 			duct_sensor1 			sensor[5]	//SA2
#define 			duct_sensor2 			sensor[6]	//HUM1
#define 			set_pt_gen_sensor		sensor[7]	//SET POINT
#define				extra1_sensor			sensor[8]	//HUM2
#define				extra2_sensor			sensor[9]	//EXTRA1

//#######################################################################################
//				 #define for Communcation module
//#######################################################################################
//for crc
#define		LastTableElementHigh				0x2
#define		LastTableElementLow					0x2
//#######################################################################################

#define NO_NETWORK					(ubyte)'N'
#define NO_RESPONSE_FROM_MODULE		(ubyte)'M'
#define NO_GRPS_CONNECTIVITY		(ubyte)'G'
#define ASYNC_SOCKET_FAILED			(ubyte)'A'
#define SYNC_SOCKET_FAILED			(ubyte)'S'
#define NO_SERVER_REPONSE			(ubyte)'R'
//#######################################################################################
//				 #define for Communcation module
//#######################################################################################

#define			stx_test							'T'
#define			stx_upl								'S'
#define    		dle_upl								'D'
#define    		ack_upl								'A'
#define    		nak_upl								'N'
#define    		etx_upl								'E'

#define			font_check_etx						','
#define			font_check_delimiter				','
#define			tot_char_limit_for_font_check 		48

//#define	   	duty_cycle							20			// 80in multiples of 20 (cannot be 100%)
#define	   		time_period							5			// x 100usec (timer1) = 500usec
#define	   	    frame_refresh_period				41			// (41 - 1) x 500usec (timer2) = 20msec

#define			FAULTS_ON_LCD			96
#define         DUTYCYCLE_SERIAL_NO_COUNT 1024

//Push Button I/O Mapping
#define 	BUTTON1_IO		PORTAbits.RA9
#define 	BUTTON2_IO		PORTAbits.RA10
#define		BUTTON3_IO		PORTBbits.RB8
#define 	BUTTON4_IO		PORTBbits.RB9
#define 	BUTTON5_IO		PORTBbits.RB10
#define 	BUTTON6_IO		PORTBbits.RB11
#define  BUTTON7_IO          PORTAbits.RA1 

#define 	BUTTON1_TRIS	TRISAbits.TRISA9
#define 	BUTTON2_TRIS	TRISAbits.TRISA10
#define 	BUTTON3_TRIS	TRISBbits.TRISB8
#define 	BUTTON4_TRIS	TRISBbits.TRISB9
#define 	BUTTON5_TRIS	TRISBbits.TRISB10
#define 	BUTTON6_TRIS	TRISBbits.TRISB11
#define BUTTON7_TRIS  TRISAbits.TRISA1

#define		KEY1			PORTAbits.RA9
#define		KEY2			PORTAbits.RA10
#define		KEY3			PORTBbits.RB8
#define		KEY4			PORTBbits.RB9
#define		KEY5			PORTBbits.RB10
#define		KEY6			PORTBbits.RB11

#define     KEY1_TRIS		TRISAbits.TRISA9
#define		KEY2_TRIS		TRISAbits.TRISA10
#define		KEY3_TRIS		TRISBbits.TRISB8
#define		KEY4_TRIS		TRISBbits.TRISB9  
#define		KEY5_TRIS		TRISBbits.TRISB10
#define		KEY6_TRIS		TRISBbits.TRISB11	
#define        KEY7_TRIS       TRISAbits.TRISA1	
//#################################################################################################
//								4MB Flash Definitions
//#################################################################################################
//#define		cs_flash_tris 		TRISBbits.TRISB14
//#define		cs_flash			LATBbits.LATB14 
//#define		rst_flash_tris		TRISBbits.TRISB15
//#define		rst_flash			LATBbits.LATB15  
//#define		clk_flash_tris 		TRISFbits.TRISF6
//#define		clk_flash			LATFbits.LATF6          
////#define		reset_flash_64mb 	LATCbits.LATC2          
//#define		miso_flash_tris		TRISFbits.TRISF2
//#define		miso_flash 			LATFbits.LATF2         
//#define		mosi_flash_tris		TRISFbits.TRISF3
//#define		mosi_flash 			PORTFbits.RF3
//#################################################################################################
//								RS 485 and RS 232 Definitions
//#################################################################################################
//#define 	RE_DE				LATGbits.LATG3
//#define		UART2_RX			PORTFbits.RF4
//#define		UART2_TX			LATFbits.LATF5
//#define		SEL_232_485			LATDbits.LATD9
//#define		DTR_PIN				PORTDbits.RD10
#define		EN_SDFC				LATBbits.LATB14
//
//// Directions
//#define		RE_DE_TRIS			TRISGbits.TRISG3
//#define		UART2_RX_TRIS		TRISFbits.TRISF4
//#define		UART2_TX_TRIS		TRISFbits.TRISF5
//
//#define		SEL_232_485_TRIS	TRISDbits.TRISD9
//#define		DTR_PIN_TRIS		TRISDbits.TRISD10
#define		EN_SDFC_TRIS		TRISBbits.TRISB14

#define		TEST_PIN			LATDbits.LATD9
#define		TEST_PIN_TRIS		TRISDbits.TRISD9
//#################################################################################################
//#define		analog_pin			PORTBbits.RB5
//#define		analog_pin_tris		TRISBbits.TRISB5

#define			BKLT_CTRL		LATGbits.LATG15
//#define			cs_RAM			LATCbits.LATC1
//#define			RAM_HOLD		LATCbits.LATC2
//#define			clk_RAM			LATGbits.LATG6
//#define			miso_RAM		PORTGbits.RG7
//#define			mosi_RAM		LATGbits.LATG8
//#define			key				PORTDbits.RD0
//
//#define		SPIxSTAT							SPI1STAT
//#define		SPIxSTATbits						SPI1STATbits
//#define		SPIxCON1							SPI1CON1
//#define		SPIxCON1bits						SPI1CON1bits
//#define		SPIxCON2							SPI1CON2
//#define		SPIxCON2bits						SPI1CON2bits
//#define		SPIxBUF								SPI1BUF

#define			GRAPHICAL_LCD

//#######################################################################################
//				Constant Definitions
//#######################################################################################
#define		INT_PRIORITY_1						1
#define		INT_PRIORITY_2						2
#define		INT_PRIORITY_3						3
#define		INT_PRIORITY_4						4
#define		INT_PRIORITY_5						5
#define		INT_PRIORITY_6						6
#define		INT_PRIORITY_7						7

#define		OSC_FREQ_FOSC						(uword)80000000
#define		CYCLE_FREQ_FCYC						(uword)OSC_FREQ_FOSC 
//#######################################################################################
//				end of Constant Definitions
//#######################################################################################
#ifdef GRAPHICAL_LCD

	#define		arialNarrow_bld_25pix   arialNarrow_bld_13pix   
	#define		puts_lcd_ram_lim(rec_ram_add, rec_delim)		puts_graphical_lcd_ram_lim(rec_ram_add, 0, HEADER_RES_SPACE, 0, rec_delim,arialNarrow_bld_25pix)
	#define		puts_lcd_rom_lim(rec_rom_add, rec_delim)		puts_graphical_lcd_rom_lim(rec_rom_add, 0, HEADER_RES_SPACE, 0, rec_delim,arialNarrow_bld_25pix)
	#define		puts_lcd_ram_cnt(rec_ram_add, rec_cnt)			puts_graphical_lcd_ram_cnt(rec_ram_add, 0, HEADER_RES_SPACE, 0, rec_cnt,arialNarrow_bld_25pix)
	#define		puts_lcd_rom_cnt(rec_rom_add, rec_cnt)			puts_graphical_lcd_rom_cnt(rec_rom_add, 0, HEADER_RES_SPACE, 0, rec_cnt,arialNarrow_bld_25pix)
	#define		puts_lcd_ram(rec_ram_add,rec_count,rec_delim)	puts_graphical_lcd_ram(rec_ram_add, 0, HEADER_RES_SPACE, 0,rec_count, rec_delim,arialNarrow_bld_25pix)
	#define		puts_lcd_rom(rec_ram_add,rec_count,rec_delim)	puts_graphical_lcd_rom(rec_rom_add, 0, HEADER_RES_SPACE, 0,rec_count, rec_delim,arialNarrow_bld_25pix)

#endif

#define			OFFSET					32
#define 		LINE_OFFSET				30
//#define			CHAR_HEIGHT_FONT		26
#define			SPACE_BW_CHARACTERS		1
#define			MAX_CHARS_IN_A_LINE		40		//integers in a line
#define			MAX_CHAR_HEIGHT			60
//#######################################################################################
//				 #define for SRAM module
//#######################################################################################
//#define  	SRAMWRSR  					0x01     	//Write the status register
//#define  	SRAMWrite  					0x02     	//Write Command for SRAM
//#define  	SRAMRead   					0x03     	//Read Command for SRAM
//#define 	SRAMRDSR   					0x05     	//Read the status register
//
//#define		SRAMByteMode				0x01		//testing actual 01;
//#define		SRAMPageMode				0x81
//#define		SRAMSeqMode					0x41
//#define		SRAMPageSize				32
//#define		DummyByte					0xFF
//

			
//#define 	RTC_ADDR							0x00D0			// device address of RTC
//#define		SPI2CON2Value			0x0000					//FRAME_ENABLE_OFF
////#define		SPI2STATValue			SPI_ENABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR
//#define		SPI2STATValue			SPI_DISABLE & SPI_IDLE_CON & SPI_RX_OVFLOW_CLR
//#define		SPI2_Rx_Buf_Full		SPI2STATbits.SPIRBF

#define		DELAY_5uS				Delay_1us();Delay_1us();Delay_1us();Delay_1us();Delay_1us();
#define		T3_DEL_500MSEC						10
#define		T3_DEL_1SEC							20
#define		T3_DEL_2SEC							40
#define		T3_DEL_3SEC							60
#define		T3_DEL_4SEC							80
#define		T3_DEL_5SEC							100
#define		T3_DEL_6SEC							120
#define		T3_DEL_10SEC						200
#define		T3_DEL_15SEC						300
#define		T3_DEL_100SEC						2000

#define		T3_DEL_1SECSCAL						2

#define		T1_DEL_1mSEC						20
#define		T1_DEL_10mSEC						200
#define		T1_DEL_25mSEC						500
#define		T1_DEL_50mSEC						1000
#define		T1_DEL_100mSEC						2000
#define		T1_DEL_500mSEC						10000
#define		T1_DEL_1SEC							20000
#define		T1_DEL_2SEC							40000
#define		T1_DEL_3SEC							60000

#define		ONTIME_50US							5
#define		PERIOD_TICKS_50US					10

//#define		BATT_CUTTOFF_VOLT					1092
//#######################################################################################
//				end of #define for timer1 module
//#######################################################################################
//#######################################################################################
//				#define for handshaking in communication purpose
//#######################################################################################
#define		STX									'S'
#define		ETX									'E'
#define		DLE									'D'
#define		ACK									'A'
#define 	NAK									'N'

// lookup table for baud rate values
#define		BAUD_RATE_1200						0
#define		BAUD_RATE_2400						1
#define		BAUD_RATE_4800						2
#define		BAUD_RATE_9600						3
#define		BAUD_RATE_19200						4
#define		BAUD_RATE_38400						5
#define		BAUD_RATE_57600						6
#define		BAUD_RATE_115200					7

#define		UBRG_1200							((uinteger)(CYCLE_FREQ_FCYC / (1200 * 4)) - 1)
#define		UBRG_2400							((uinteger)((uword)(CYCLE_FREQ_FCYC / 2400) / 4) - 1)
#define		UBRG_4800							((uinteger)((uword)(CYCLE_FREQ_FCYC / 4800) / 4) - 1)
#define		UBRG_9600							((uinteger)((uword)(CYCLE_FREQ_FCYC / 9600) / 4) - 1)
#define		UBRG_19200							((uinteger)((uword)(CYCLE_FREQ_FCYC / 19200) / 4) - 1)
#define		UBRG_38400							((uinteger)((uword)(CYCLE_FREQ_FCYC / 38400) / 4) - 1)
#define		UBRG_57600							((uinteger)((uword)(CYCLE_FREQ_FCYC / 57600) / 4) - 1)
#define		UBRG_115200							((uinteger)((uword)(CYCLE_FREQ_FCYC / 115200) / 4) - 1)
//#######################################################################################
//				end of #define for handshaking in communication purpose
//#######################################################################################
//#######################################################################################
//				#define for RTU module
//#######################################################################################
#define 	LAST_TABLE_ELEMENT_LOW 				0x2
#define 	LAST_TABLE_ELEMENT_HIGH 			0x2

#define 	WDTCLR			Nop();

#define		NO_STX_ETX							10
#define		ONLY_STX							11
#define		ONLY_ETX							12
#define		STX_ETX								13

#define 	ALPHANUMERIC						1
#define		NUMERIC								2
#define		PASSWORD_DIG_COUNT					(ubyte)4

#define		LEN_MESSAGES						(ubyte)10
#define		LEN_MAIN_MENU						(ubyte)7
#define		LEN_DEFAULT_MENU					(ubyte)6
#define		LEN_TEST_MENU						(ubyte)4
#define		LEN_NORMAL_TEST_MENU				(ubyte)21
#define		LEN_EXCL_TEST_MENU					(ubyte)21
#define		LEN_FACTORY_MENU					(ubyte)6//4//3
#define		LEN_USB_MENU				    	(ubyte)3//5
#define		LEN_SUPERVISOR_MENU			    	(ubyte)8//7//6//5
#define		LEN_SP_SOURCE_MENU			    	(ubyte)3

#define		LEN_FAULT_BYPASS					(ubyte)19


#define		WAIT_FOR_USB_DELAY					;//while(orphan_flags2.bits.usb_delay);
#define		SET_USB_DELAY						;//orphan_flags2.bits.usb_delay = 1; usb_timer_count = T1_DEL_25mSEC;
#define		SEL_FLASH_4MB						flash.flags.bytes = 1;
#define		SEL_FLASH_1							flash.flags.bytes = 2;
#define		SEL_FLASH_2							flash.flags.bytes = 4;
#define		SEL_FLASH_3							flash.flags.bytes = 8;
#define		_FILE_OPEN_ERROR(FILE_NUM)			show_file_open_error(FILE_NUM);
#define		_USB_ERROR(ERR_NUM)					show_usb_error(ERR_NUM);
	
#define		_IF_ALPHA(REC_CHAR)					(unsigned)('@' < REC_CHAR && REC_CHAR < '[')// || ('`' < REC_CHAR && REC_CHAR < '{')
#define		_IF_NUM(REC_CHAR)					(unsigned)('/' < REC_CHAR && REC_CHAR < ':')
#define		_IF_ALPHA_NUM(REC_CHAR)				(unsigned)(_IF_NUM(REC_CHAR) || _IF_ALPHA(REC_CHAR))
#define		_IF_SPACE(REC_CHAR)					(unsigned)(REC_CHAR == ' ')
#define		_IF_HASH(REC_CHAR)					(unsigned)(REC_CHAR == '#')
#define		_IF_QUOTES(REC_CHAR)				(unsigned)(REC_CHAR == '"')
#define		_IF_COMMA(REC_CHAR)					(unsigned)(REC_CHAR == ',')
#define		_IF_BACKSLASH(REC_CHAR)				(unsigned)(REC_CHAR == '/')
#define		_IF_PLUS(REC_CHAR)					(unsigned)(REC_CHAR == '+')
#define		_IF_COLON(REC_CHAR)					(unsigned)(REC_CHAR == ':')
#define		_IF_SEMICOLON(REC_CHAR)				(unsigned)(REC_CHAR == ';')
#define		_IF_LESSTHAN(REC_CHAR)				(unsigned)(REC_CHAR == '<')
#define		_IF_GREATTHAN(REC_CHAR)				(unsigned)(REC_CHAR == '>')
#define		_IF_EQUALTO(REC_CHAR)				(unsigned)(REC_CHAR == '=')
#define		_IF_SQBRACK1(REC_CHAR)				(unsigned)(REC_CHAR == '[')
#define		_IF_FRONTSLASH(REC_CHAR)			(unsigned)(REC_CHAR == '\\')
#define		_IF_SQBRACK2(REC_CHAR)				(unsigned)(REC_CHAR == ']')
#define		_IF_OR(REC_CHAR)					(unsigned)(REC_CHAR == '|')
#define		_IF_DOT(REC_CHAR)					(unsigned)(REC_CHAR == '.')
#define		_IF_DASH(REC_CHAR)					(unsigned)(REC_CHAR == '-')
#define		_IF_PERCENT(REC_CHAR)				(unsigned)(REC_CHAR == '%')

#define		_IF_SYMBOL(REC_CHAR)				(unsigned) (_IF_HASH(REC_CHAR) || _IF_QUOTES(REC_CHAR) || _IF_COMMA(REC_CHAR) || \
															_IF_BACKSLASH(REC_CHAR) || _IF_PLUS(REC_CHAR) || _IF_COLON(REC_CHAR) || \
															_IF_SEMICOLON(REC_CHAR) || _IF_LESSTHAN(REC_CHAR) || \
															_IF_GREATTHAN(REC_CHAR) || _IF_EQUALTO(REC_CHAR) || \
															_IF_SQBRACK1(REC_CHAR) || _IF_FRONTSLASH(REC_CHAR) || _IF_PERCENT(REC_CHAR) || \
															_IF_SQBRACK2(REC_CHAR) || _IF_OR(REC_CHAR) || _IF_DOT(REC_CHAR))

#define		DEVICE_ID_SIZE						(ubyte)0x01
#define		FUNC_CODE_SIZE						(ubyte)0x01
#define		PKT_LEN_SIZE						(ubyte)0x02
#define		PKT_NUM_SIZE						(ubyte)0x02
#define		CRC_SIZE							(ubyte)0x02
#define		FRAME_LEN(REC_DATA_LEN)				(uinteger)(	DEVICE_ID_SIZE + FUNC_CODE_SIZE + PKT_LEN_SIZE + \
															PKT_NUM_SIZE + REC_DATA_LEN + CRC_SIZE)
//#################################################################################################
//					PORT DEFINITIONS			For LED cards
//#################################################################################################
#define			lcd_en				LATDbits.LATD3
#define			rs					LATDbits.LATD12	//0-data, 1-command
#define			lcd_reset			LATDbits.LATD13

#define			lcd_d0				LATDbits.LATD4 
#define			lcd_d1				LATDbits.LATD5
#define			lcd_d2				LATFbits.LATF0
#define			lcd_d3				LATFbits.LATF1
#define			lcd_d4				LATGbits.LATG1
#define			lcd_d5				LATGbits.LATG0
#define			lcd_d6				LATGbits.LATG14
#define			lcd_d7				LATGbits.LATG12

#define			lcd_font_sel		LATGbits.LATG13
#define			lcd_rd				LATDbits.LATD2
#define			lcd_wr				LATDbits.LATD1

#define			lcd_d0_tris			TRISDbits.TRISD4
#define			lcd_d1_tris			TRISDbits.TRISD5
#define			lcd_d2_tris			TRISFbits.TRISF0
#define			lcd_d3_tris			TRISFbits.TRISF1
#define			lcd_d4_tris			TRISGbits.TRISG1
#define			lcd_d5_tris			TRISGbits.TRISG0
#define			lcd_d6_tris			TRISGbits.TRISG14
#define			lcd_d7_tris			TRISGbits.TRISG12

//#define			cs_lcd1				LATFbits.LATF1
//#define			cs_lcd2				LATGbits.LATG1
//#######################################################################################
//			Communication Module
//#######################################################################################
#define 	IRQ_U2ARX			(ubyte)38
#define 	IRQ_U2ATX			(ubyte)39	
#define 	IRQ_U2BTX			(ubyte)72
#define 	IRQ_U3ATX			(ubyte)42
#define 	IRQ_U3BTX			(ubyte)75
#define 	IRQ_U3ARX			(ubyte)41
#define 	IRQ_U3BRX			(ubyte)74
#define 	IRQ_U1BRX			(ubyte)68
#define 	IRQ_U1BTX			(ubyte)69
//#######################################################################################
//#######################################################################################




