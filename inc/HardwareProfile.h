
/******************************************************************************

    Hardware Profile Configuration File
    
This file contains definitions of hardware-specific options.


Software License Agreement

The software supplied herewith by Microchip Technology Incorporated
(the �Company�) for its PICmicro� Microcontroller is intended and
supplied to you, the Company�s customer, for use solely and
exclusively on Microchip PICmicro Microcontroller products. The
software is owned by the Company and/or its supplier, and is
protected under applicable copyright laws. All rights are reserved.
Any use in violation of the foregoing restrictions may subject the
user to criminal sanctions under applicable laws, as well as to
civil liability for the breach of the terms and conditions of this
license.

THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.

*******************************************************************************/

#ifndef _HARDWARE_PROFILE_H_
#define _HARDWARE_PROFILE_H_

//#include "..\inc\plib.h"
#include "..\inc\usb.h"
#include "..\inc\usb_host_msd.h"
#include "..\inc\usb_host_msd_scsi.h"

// Clock Definitions
//#define SYS_CLOCK		(80000000)
#ifndef SYS_CLOCK
    #error "Define SYS_CLOCK (ex. -DSYS_CLOCK=80000000) on compiler command line"
#endif
#define GetSystemClock()            SYS_CLOCK
#define GetPeripheralClock()        SYS_CLOCK
#define GetInstructionClock()

// Calculate the timer values
#define SYS_FREQ                    GetSystemClock()
#define PB_DIV                      1
#define PRESCALE                    256
#define TOGGLES_PER_SEC             100
#define TIMER_PERIOD                (SYS_FREQ/PB_DIV/PRESCALE/TOGGLES_PER_SEC)
#define MILLISECONDS_PER_TICK       (1000/TOGGLES_PER_SEC)

// Text IO routines
#define PIC32_STARTER_KIT
//#include "db_utils.h"
#define InitTextIO()    DBINIT()
#define PutChar(c)
#define PutHex(h)
#define PrintString(s)  DBPUTS(s)


#endif  // _HARDWARE_PROFILE_H_

