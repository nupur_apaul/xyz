//#include "Generic.h"
#include "PIC24F_periph_features.h"

#define I2C1RCV_VALUE               0x0000
#define I2C1TRN_VALUE               0x00FF
#define I2C1BRG_VALUE               0x0000
#define I2C1CON_VALUE               0x0000
#define I2C1STAT_VALUE              0x0000
#define I2C1ADD_VALUE               0x0000

#define I2C2RCV_VALUE               0x0000
#define I2C2TRN_VALUE               0x00FF
#define I2C2BRG_VALUE               0x0000
#define I2C2CON_VALUE               0x0000
#define I2C2STAT_VALUE              0x0000
#define I2C2ADD_VALUE               0x0000

#define I2C3RCV_VALUE               0x0000
#define I2C3TRN_VALUE               0x00FF
#define I2C3BRG_VALUE               0x0000
#define I2C3CON_VALUE               0x0000
#define I2C3STAT_VALUE              0x0000
#define I2C3ADD_VALUE               0x0000


#ifdef USE_AND_OR /* Format for AND_OR based bit setting */
// I2CxCON Register Configuration Bit Definitions
#define I2C_ON           			0x8000 /*I2C module enabled */
#define I2C_OFF          			0x0000 /*I2C module disabled */
#define I2C_ON_OFF_MASK  			(~I2C_ON)

#define I2C_IDLE_STOP    			0x2000 /*stop I2C module in Idle mode */
#define I2C_IDLE_CON     			0x0000 /*continue I2C module in Idle mode */
#define I2C_IDLE_MASK    			(~I2C_IDLE_STOP)

#define I2C_CLK_REL      			0x1000 /*release clock */
#define I2C_CLK_HLD      			0x0000 /*hold clock  */
#define I2C_CLK_MASK     			(~I2C_CLK_REL)

#define I2C_IPMI_EN      			0x0800 /*IPMI mode enabled */
#define I2C_IPMI_DIS     			0x0000 /*IPMI mode not enabled */
#define I2C_IPMI_EN_DIS_MASK    	(~I2C_IPMI_EN)

#define I2C_10BIT_ADD    			0x0400 /*I2CADD is 10-bit address */
#define I2C_7BIT_ADD     			0x0000 /*I2CADD is 7-bit address */
#define I2C_10BIT_7BIT_MASK    		(~I2C_10BIT_ADD)

#define I2C_SLW_DIS       			0x0200 /*Disable Slew Rate Control for 100KHz */
#define I2C_SLW_EN        			0x0000 /*Enable Slew Rate Control for 400KHz */
#define I2C_SLW_EN_DIS_MASK     	(~I2C_SLW_DIS)

#define I2C_SM_EN        			0x0100 /*Enable SM bus specification */
#define I2C_SM_DIS       			0x0000 /*Disable SM bus specification */
#define I2C_SM_EN_DIS_MASK      	(~I2C_SM_EN)

#define I2C_GCALL_EN     			0x0080 /*Enable Interrupt when General call address is received. */
#define I2C_GCALL_DIS    			0x0000 /*Disable General call address. */
#define I2C_GCALL_EN_DIS_MASK   	(~I2C_GCALL_EN)

#define I2C_STR_EN       			0x0040 /*Enable clock stretching */
#define I2C_STR_DIS      			0x0000 /*disable clock stretching */
#define I2C_STR_EN_DIS_MASK     	(~I2C_STR_EN)

#define I2C_NACK         			0x0020 /*Transmit 0 to send ACK as acknowledge */
#define I2C_ACK         			0x0000 /*Transmit 1 to send NACK as acknowledge*/
#define I2C_ACK_MASK     			(~I2C_NACK)

#define I2C_ACK_EN       			0x0010 /*Initiate Acknowledge sequence */
#define I2C_ACK_DIS      			0x0000 /*Acknowledge condition Idle */
#define I2C_TX_ACK_MASK  			(~I2C_ACK_EN)

#define I2C_RCV_EN       			0x0008 /*Enable receive mode */
#define I2C_RCV_DIS      			0x0000 /*Receive sequence not in progress */
#define I2C_RCV_EN_DIS_MASK         (~I2C_RCV_EN)

#define I2C_STOP_EN      			0x0004 /*Initiate Stop sequence */
#define I2C_STOP_DIS    			0x0000 /*Stop condition Idle */
#define I2C_STOP_EN_DIS_MASK    	(~I2C_STOP_EN)

#define I2C_RESTART_EN   			0x0002 /*Initiate Restart sequence */
#define I2C_RESTART_DIS  			0x0000 /*Start condition Idle */
#define I2C_RESTART_MASK 			(~I2C_RESTART_EN)

#define I2C_START_EN     			0x0001 /*Initiate Start sequence */
#define I2C_START_DIS    			0x0000 /*Start condition Idle */
#define I2C_START_MASK   			(~I2C_START_EN)

// Slave I2C Interrupt Priority
#define SI2C_INT_PRI_0      		0x0000
#define SI2C_INT_PRI_1      		0x0001
#define SI2C_INT_PRI_2     			0x0002
#define SI2C_INT_PRI_3      		0x0003
#define SI2C_INT_PRI_4      		0x0004
#define SI2C_INT_PRI_5      		0x0005
#define SI2C_INT_PRI_6      		0x0006
#define SI2C_INT_PRI_7      		0x0007
#define SI2C_SRC_DIS        		SI2C_INT_PRI_0
#define SI2C_INT_PRI_MASK  			(~SI2C_INT_PRI_7)

// Slave I2C Interrupt Enable/Disable
#define SI2C_INT_ON     			0x0008
#define SI2C_INT_OFF    			0x0000
#define SI2C_INT_MASK   			(~SI2C_INT_ON)

// Master I2C Interrupt Priority
#define MI2C_INT_PRI_0  			0x0000
#define MI2C_INT_PRI_1  			0x0010
#define MI2C_INT_PRI_2  			0x0020
#define MI2C_INT_PRI_3  			0x0030
#define MI2C_INT_PRI_4  			0x0040
#define MI2C_INT_PRI_5  			0x0050
#define MI2C_INT_PRI_6  			0x0060
#define MI2C_INT_PRI_7  			0x0070
#define MI2C_SRC_DIS    			MI2C_INT_PRI_0
#define MI2C_INT_PRI_MASK  			(~MI2C_INT_PRI_7)

// Master I2C Interrupt Enable/Disable
#define MI2C_INT_ON     			0x0080
#define MI2C_INT_OFF    			0x0000
#define MI2C_INT_MASK   			(~MI2C_INT_ON)

#else /* Format for backward compatibility (AND based bit setting). */

/* I2CCON register Configuration bit definitions */
#define I2C_ON                      0xFFFF /*I2C module enabled */
#define I2C_OFF                     0x7FFF /*I2C module disabled */

#define I2C_IDLE_STOP               0xFFFF /*stop I2C module in Idle mode */
#define I2C_IDLE_CON                0xDFFF /*continue I2C module in Idle mode */
#define I2C_CLK_REL                 0xFFFF /*release clock */
#define I2C_CLK_HLD                 0xEFFF /*hold clock  */

#define I2C_IPMI_EN                 0xFFFF /*IPMI mode enabled */
#define I2C_IPMI_DIS                0xF7FF /*IPMI mode not enabled */

#define I2C_10BIT_ADD               0xFFFF /*I2CADD is 10-bit address */
#define I2C_7BIT_ADD                0xFBFF /*I2CADD is 7-bit address */

#define I2C_SLW_DIS                 0xFFFF /*Disable Slew Rate Control for 100KHz */
#define I2C_SLW_EN                  0xFDFF /*Enable Slew Rate Control for 400KHz */

#define I2C_SM_EN                   0xFFFF /*Enable SM bus specification */
#define I2C_SM_DIS                  0xFEFF /*Disable SM bus specification */

#define I2C_GCALL_EN                0xFFFF /*Enable Interrupt when General call address is received. */
#define I2C_GCALL_DIS               0xFF7F /*Disable General call address. */

#define I2C_STR_EN                  0xFFFF /*Enable clock stretching */
#define I2C_STR_DIS                 0xFFBF/*disable clock stretching */

#define I2C_ACK                     0xFFDF /*Transmit 0 to send ACK as acknowledge */
#define I2C_NACK                    0xFFFF /*Transmit 1 to send NACK as acknowledge*/

#define I2C_ACK_EN                  0xFFFF/*Initiate Acknowledge sequence */
#define I2C_ACK_DIS                 0xFFEF /*Acknowledge condition Idle */

#define I2C_RCV_EN                  0xFFFF /*Enable receive mode */
#define I2C_RCV_DIS                 0xFFF7 /*Receive sequence not in progress */
 
#define I2C_STOP_EN                 0xFFFF /*Initiate Stop sequence */
#define I2C_STOP_DIS                0xFFFB /*Stop condition Idle */

#define I2C_RESTART_EN              0xFFFF /*Initiate Restart sequence */
#define I2C_RESTART_DIS             0xFFFD /*Start condition Idle */

#define I2C_START_EN                0xFFFF /*Initiate Start sequence */
#define I2C_START_DIS               0xFFFE /*Start condition Idle */

/* Priority for Slave I2C1 Interrupt */
#define SI2C_INT_PRI_7              0xFFFF
#define SI2C_INT_PRI_6              0xFFFE
#define SI2C_INT_PRI_5              0xFFFD
#define SI2C_INT_PRI_4              0xFFFC
#define SI2C_INT_PRI_3              0xFFFB
#define SI2C_INT_PRI_2              0xFFFA
#define SI2C_INT_PRI_1              0xFFF9
#define SI2C_INT_PRI_0              0xFFF8

/* Slave I2C1 Interrupt Enable/Disable */
#define SI2C_INT_ON                 0xFFFF
#define SI2C_INT_OFF                0xFFF7

/* Priority for Master I2C1 Interrupt */
#define MI2C_INT_PRI_7              0xFFFF
#define MI2C_INT_PRI_6              0xFFEF
#define MI2C_INT_PRI_5              0xFFDF
#define MI2C_INT_PRI_4              0xFFCF
#define MI2C_INT_PRI_3              0xFFBF
#define MI2C_INT_PRI_2              0xFFAF
#define MI2C_INT_PRI_1              0xFF9F
#define MI2C_INT_PRI_0              0xFF8F

/* Master I2C1 Interrupt Enable/Disable */
#define MI2C_INT_ON                 0xFFFF
#define MI2C_INT_OFF                0xFF7F

#endif /* USE_AND_OR */
