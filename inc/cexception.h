//#ifndef _CEXCEPTION_H
//#define _CEXCEPTION_H
//
//#include <setjmp.h>
//
//enum {
//	EXCEP_IRQ = 0,			// interrupt
//	EXCEP_AdEL = 4,			// address error exception (load or ifetch)
//	EXCEP_AdES,				// address error exception (store)
//	EXCEP_IBE,				// bus error (ifetch)
//	EXCEP_DBE,				// bus error (load/store)
//	EXCEP_Sys,				// syscall
//	EXCEP_Bp,				// breakpoint
//	EXCEP_RI,				// reserved instruction
//	EXCEP_CpU,				// coprocessor unusable
//	EXCEP_Overflow,			// arithmetic overflow
//	EXCEP_Trap,				// trap (possible divide by zero)
//	EXCEP_IS1 = 16,			// implementation specfic 1
//	EXCEP_CEU,				// CorExtend Unuseable
//	EXCEP_C2E				// coprocessor 2
//} _excep_code;
//
//extern jmp_buf				exception_env[];
//extern unsigned int			error_code;
//extern unsigned int			current_env;
//extern unsigned int			docatch;
//
//#define ctry if(!(error_code = setjmp(exception_env[current_env++])))
//
//#define ccatch --current_env; if(!(--docatch))
//
////Change this value increase allowable levels of ctry/ccatch nesting
//#define MAX_NESTED_TRIES	3
//
//#endif //_CEXCEPTION_H
//


