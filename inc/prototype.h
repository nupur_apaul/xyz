typedef unsigned char ubyte;
//typedef unsigned char datum;
//typedef 	unsigned short 			integer;
typedef char sbyte;
typedef	unsigned short uinteger;
typedef	int sinteger;
typedef	unsigned long uword;
typedef	long int sword;
typedef	const unsigned char urombyte;
typedef	const char srombyte;
typedef	const unsigned short uromint;
typedef	const int sromint;
typedef urombyte *const uromptr;
typedef srombyte *const sromptr;
typedef ubyte *const uramptr;
typedef sbyte *const sramptr;
typedef long double lword;



typedef		unsigned char	uint_8;				
typedef		unsigned short  FONT_CHAR_INFO;


void sel_single_double_rmpu(void);
void sel_single_double_decker(void);
void view_server_status(void);
//#######################################################################################
//				prototypes for other general functions
//#######################################################################################
void config_mcu_pins(void);
void init_system(void);
void init_port(void);
//#######################################################################################
//				end of prototypes for other general functions
//#######################################################################################
//#######################################################################################
//				prototypes for IR module
//#######################################################################################
void handle_menu_request(const unsigned short * table_ptr);
void handle_cmd_request(const unsigned short * table_ptr);
void not_handled(void);
void menu_init(void);
void show_menu(void);
//#######################################################################################


//#######################################################################################
//				prototypes for 64mb flash module
//#######################################################################################
void init_flash(void);
void flash_test_temp(void);

void tx_flash(ubyte);
ubyte rx_flash(void);
void write_cmd_add(ubyte rec_flash_cmd, ubyte *rec_flash_add);
unsigned chip_sel_flash(void);
void chip_dis_flash(void);
void disp_flash_error(void);

unsigned write_flash_64mb(ubyte *, ubyte *, uinteger);
unsigned read_flash_64mb(ubyte *, ubyte *, uinteger);
unsigned write_pkt_64mb_flash(ubyte *, uinteger, uinteger, uinteger);
unsigned read_pkt_64mb_flash(ubyte *, uinteger, uinteger, uinteger);
unsigned write_bytes_64mb_flash(ubyte *, uinteger, uinteger, uinteger);
unsigned read_bytes_64mb_flash(ubyte *, uinteger, uinteger, uinteger);
unsigned write_page_64mb_flash(ubyte *, uinteger);
unsigned read_page_64mb_flash(ubyte *, uinteger);
unsigned auto_rewrite_event_data_64mb(void);
unsigned chk_for_new_64mb_flash(void);

unsigned write_flash_4mb(ubyte *, ubyte *, uinteger);
unsigned read_flash_4mb(ubyte *, ubyte *, uinteger);
unsigned write_pkt_4mb_flash(ubyte *, uinteger, uinteger, uinteger);
unsigned read_pkt_4mb_flash(ubyte *, uinteger, uinteger, uinteger);
unsigned write_bytes_4mb_flash(ubyte *, uinteger, uinteger, uinteger);
unsigned read_bytes_4mb_flash(ubyte *, uinteger, uinteger, uinteger);
unsigned write_page_4mb_flash(ubyte *, uinteger);
unsigned read_page_4mb_flash(ubyte *, uinteger);
unsigned chk_for_new_4mb_flash(void);
unsigned auto_rewrite_event_data_4mb(void);

unsigned check_flash3_for_logger(void);
//#######################################################################################
//				end of prototypes for 64mb flash module
//#######################################################################################


//#######################################################################################
//				end of prototypes for sram module
//#######################################################################################
void init_sram(void);
void spi2_open(void);
void spi2_close(void);
void write_sram_cmd(unsigned char AddLB, unsigned char AddHB, unsigned char RWCmd);
unsigned char spi2_rd_wr(unsigned char rec_byte);
void write_sram_stat_reg(unsigned char rec_byte);
unsigned char read_sram_stat_reg(void);
void read_sram_bytes(unsigned short rec_sram_add, ubyte *rec_ram_add, uinteger rec_byte_count);
void write_sram_bytes(unsigned short rec_sram_add, ubyte *rec_ram_add, uinteger rec_byte_count);
//#######################################################################################
//				end of prototypes for sram module
//#######################################################################################


//#######################################################################################
//				prototypes for peripheral deivers
//#######################################################################################
void usart1B_set_baud(ubyte);
void usart1B_write(ubyte);
void usart1B_write_str(ubyte *, uinteger, uinteger);
void usart1B_close(void);

void init_uart1B(void);


void timer2_open(void);
void timer2_int(void);
////#######################################################################################
////				end of prototypes for peripheral deivers
////#######################################################################################
//
//
////#######################################################################################
////				prototypes for lcd module
////#######################################################################################
void init_lcd(void);
void lcd_byte(ubyte rec_byte, unsigned rec_type);
void puts_lcd_ram_lim(ubyte *rec_ram_add , ubyte rec_delim);
void puts_lcd_ram_cnt(ubyte *rec_ram_add , ubyte rec_count);
void puts_lcd_ram(ubyte *rec_ram_add, ubyte rec_count, ubyte rec_delim);
void puts_lcd_rom_lim(urombyte *rec_ram_add , ubyte rec_delim);
void puts_lcd_rom_cnt(urombyte *rec_ram_add , ubyte rec_count);
void puts_lcd_rom(ubyte *rec_ram_add, ubyte rec_count, ubyte rec_delim);
void puts_lcd_srom_lim(srombyte *rec_ram_add , ubyte rec_delim);
void home_clr(void);
void lcd_port_update(void);
void lcd_clr_line_1(unsigned char rec_col_len);
void lcd_clr_line_2(unsigned char rec_col_len);
//#######################################################################################
//				end of prototypes for lcd module
//#######################################################################################


//#######################################################################################
//				prototypes for other project independent general functions
//#######################################################################################
// functions related to communication
unsigned str_validate_alpha_num(ubyte *res_ram_add, ubyte rec_count, ubyte rec_delim);
unsigned str_validate_num(ubyte *res_ram_add, ubyte rec_count, ubyte rec_delim);
unsigned str_validate_delim(ubyte *res_ram_add, uinteger rec_count, ubyte rec_delim);
unsigned str_validate_alpha(ubyte *res_ram_add, ubyte rec_count, ubyte rec_delim);

ubyte dec_long_to_char(uword *rec_src_ram_add, ubyte *rec_dest_ram_add);
ubyte dec_int_to_char(uinteger *rec_src_ram_add, ubyte *rec_dest_ram_add);
ubyte dec_byte_to_char (ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add);
ubyte dec_to_bcd(ubyte rec_byte);
ubyte bcd_to_dec(ubyte rec_byte);
ubyte char_to_dec_byte(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_delim);
void hex_byte_to_char(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add);
void hex_int_to_char(uinteger *rec_src_ram_add, ubyte *rec_dest_ram_add);
void hex_word_to_char(uword *rec_src_ram_add, ubyte *rec_dest_ram_add);
ubyte char_to_hex_byte(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_delim);
ubyte char_to_dec_int(ubyte *rec_src_ram_add, uinteger *rec_dest_ram_add, ubyte rec_delim);
ubyte char_to_dec_long(ubyte *rec_src_ram_add, uword *rec_dest_ram_add, ubyte rec_delim);
ubyte char_to_hex_int(ubyte *rec_src_ram_add, uinteger *rec_dest_ram_add, ubyte rec_delim);
void conv_integer_to_bytes(uinteger int_1, ubyte *rec_byte_add);
ubyte get_byte (uinteger rec_delay_count);
unsigned get_stx_cmd(uinteger rec_delay_ms);
ubyte get_ack_cmd(uinteger rec_delay_ms);
unsigned get_dle_cmd(uinteger rec_delay_ms);
unsigned stx_hex_dle_cmd(uinteger rec_delay_ms);
unsigned chk_for_comma(ubyte comma_test_byte);
ubyte chk_ascii_hex(ubyte test_byte);
unsigned chk_for_hash(ubyte test_byte);
unsigned chk_for_ascii_char(ubyte test_byte);
unsigned chk_for_ascii_num(ubyte test_byte);
uinteger concat_byte_to_int(ubyte rec_byte_1, ubyte rec_byte_2);
void fill_hash_wt_line_feed(ubyte *res_ram_add, ubyte rec_count);
// functions related to handshaking process
void send_stx_usart3A(void);
void send_dle_usart3A(void);
void send_etx_usart3A(void);
void send_nak_usart3A(void);
void send_stx_usart2A(void);
void send_dle_usart2A(void);
void send_etx_usart2A(void);
void send_nak_usart2A(void);

//prototypes for string related functions
uinteger str_search_ram(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, uinteger rec_count, uinteger rec_dest_lim);
void str_copy_ram_cnt(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, uinteger rec_count);
void str_copy_ram_int(uinteger *rec_src_ram_add, uinteger *rec_dest_ram_add, uinteger rec_count);
void str_copy_ram_lim(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_delimitter);
uinteger str_copy_ram_lim_ret(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_delimitter);
uinteger str_copy_rom_lim_ret(urombyte *rec_src_rom_add, ubyte *rec_dest_ram_add, ubyte rec_delimitter);
void str_copy_rom_cnt(urombyte *rec_src_rom_add, ubyte *rec_dest_ram_add, uinteger rec_count);
void str_copy_rom_lim(urombyte *rec_src_rom_add, ubyte *rec_dest_ram_add, ubyte rec_delimitter);
void str_fill(ubyte *rec_ram_add, uinteger rec_count, ubyte rec_char);
uinteger get_int_str_len(ubyte *rec_ram_add, ubyte rec_delim);
ubyte get_str_len(ubyte *rec_ram_add, ubyte rec_delim);
void str_fill_word(uword *rec_ram_add, uinteger rec_count, uword rec_char);
void str_fill_int(uinteger *rec_ram_add, uinteger rec_count, uinteger rec_char);
unsigned str_comp_ram(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, uinteger rec_count);
unsigned str_comp_rom(urombyte *rec_src_rom_add, ubyte *rec_dest_ram_add, uinteger rec_count);

//prototypes for functions related to CRC generation and chk
unsigned verify_crc(ubyte *rec_buff_address, uinteger rec_buff_length);
void crc_generate(ubyte *rec_buff_address, uinteger rec_buff_length);

unsigned verify_crc_modbus(ubyte *rec_buff_address, uinteger rec_buff_length);
void crc_generate_modbus(ubyte *rec_buff_address, uinteger rec_buff_length);

unsigned get_chk_sum(ubyte *rec_start_add, uinteger rec_byte_count);
void add_chk_sum(ubyte *rec_start_add, uinteger rec_byte_count);

ubyte dec_int_to_char_fixed(uinteger *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_dig_count);
//#######################################################################################
//				end of prototypes for other project independent general functions
//#######################################################################################


////#######################################################################################
////				prototypes for keypad module
////#######################################################################################
unsigned char scan_keypad(urombyte *rec_keypad_ptr);
unsigned char scan_num_keypad(void);
void keypad_update(void);
void keypress_detect_alpha(void);
void keypad_scan_menu_alpha(void);
void get_char_alpha (void);
void key_display(void);
void disp_time_out(void);
void scan_string(ubyte rec_key_type, ubyte rec_str_len);
unsigned char scan_alpha_keypad(void);


void display_apaul_graphics(void);
//#######################################################################################
//				prototypes for interrupt service routines
//#######################################################################################
void init_i2c(void);
void read_rtc(void);
void clk_reset(void);
void clock_disp(void);
void disp_ascii(unsigned char chvalue,unsigned char line);
void conv_time(void);
void set_rtc(void);
void conv_time1(void);
unsigned char bcd(unsigned char temp);
void change_rtcval(unsigned char addr);
void rtc_write(void);
void set_rtc_date(void);
void set_rtc_time(void);



//#######################################################################################
//				prototypes for I2C module
//#######################################################################################
void i2c2_idle(void);
void i2c2_start(void);
void i2c2_stop(void);
void i2c2_close(void);
void i2c2_open(unsigned short config1,unsigned short config2);
void i2c2_restart(void);
void i2c2_not_ack(void);
char i2c2_mast_write(unsigned char data_out);
char i2c2_data_ready(void);
unsigned char i2c2_mast_read(void);
unsigned char i2c2_get_str( unsigned char *rdptr, unsigned char length );
unsigned char i2c2_ee_seq_read( unsigned char control, unsigned char address, unsigned char *rdptr, unsigned char length );
unsigned char i2c2_eebyte_write( unsigned char control, unsigned char address, unsigned char data );
//#######################################################################################
//				end of prototypes for display unit
//#######################################################################################

void fill_tx_frame_buff(ubyte func_code,uinteger length, urombyte *dest_id );
void fill_frame_data(ubyte func_code);
void check_fn_code_send_pkt_to_lhb(void);


int usb_test (void);
//#######################################################################################
//				end of prototypes for voice unit
//#######################################################################################

void decode_fn_code(ubyte fn_code);
void view_version_nos(void);




//#######################################################################################
//				prototypes for usb unit
//#######################################################################################



void show_syntax_err(uinteger rec_line_num);
void usb_upload_error(void);
uinteger get_data_start_page_add(ubyte rec_data_type);


unsigned read_file(char *rec_file_name, unsigned char *rec_str_addr, unsigned short  rec_str_len);
unsigned write_file(char *rec_file_name, unsigned char *rec_str_addr, unsigned short  rec_str_len);
void init_usb(void);
unsigned chk_for_usb(void);
unsigned MonitorMedia( void );


//#######################################################################################
//					prototypes
//#######################################################################################
void exception_handling_bkup_rst(void);
//void _general_exception_handler(unsigned cause, unsigned status);




void init_peripheral(void);
void Init_Port(void); 
void Init_UART2(uinteger);
void Init_Timer1( void );
void Init_Timer2( void );
void data_display(void);
void retrieve_data(void);
void all_card_disable(void);
void load_data_latch(void);
void usart2_write_str(ubyte *rec_ram_add, uinteger rec_str_len, uinteger rec_ms_delay);
void uart2_write(unsigned char);
void send_data_to_rows(void);
void Clear_Display(void);
void Column_on(void);
void Column_off(void);
void Delay_ms(void);
void Delay_us(void);
void Delays_Xtal_ms(unsigned short multiplier_ms);
void Delays_Xtal_us(unsigned short multiplier_us);
void count_chars_in_data_fields(unsigned char string_index);
void load_default_data(void);
void load_status_of_plant(void);
void load_set7_temp(void);
void data_receive(void);
void comm_handling(void);
void send_stx(void);
void send_dle(void);
void load_data_into_buffer(void);
void  UART2PutChar(ubyte Ch);
char UART2GetChar();
unsigned char calculate_bcc(unsigned char *b, unsigned char n);
void ascii_conv(unsigned char n);
void send_cmd_pkt(void);
void send_ack(void);
void load_default_screen_data(void);
void load_status_of_plant_screen_data(void);

void handle_menu_request(const unsigned short * table_ptr);
void handle_cmd_request(const unsigned short * table_ptr);
void not_handled(void);
void menu_init(void);
void show_menu(void);
void load_fault_data_menu(void);

void load_event_log_menu(void);
void load_set7_temp_selection_screen(void);
void load_set7_temp_change_screen(void);
void count_chars_in_data_fields1(unsigned char string_index);


void show_time_out(void);
void display_set_temp_selection_screen(void);
void display_trip_count(void);
unsigned delay_key_chk_ms(uinteger rec_delay_ms);
void menu_mode_options(void);
void not_handled(void);
void default_settings_options (void);
void operating_parameters_menu_options (void);
void test_menu_options(void);
void factory_menu_options(void);
void usb_menu_options(void);
void load_fault_data(void);
void Dutycycle_trip_log(void);
void load_dutycycle_data(void);
void load_system_log(void);
void load_power_useses(void);
void load_analysis_log(void);
void load_event_log(void);
void change_RMPU_make(void);
void change_RMPU_no(void);
void change_Coach_no(void);
void change_Unit_no(void);
void change_server_settings(void);
void change_date_and_time(void);
void change_set_temp_values(void);
ubyte char_to_dec_byte_cnt(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_cnt);
void send_normal_test_cmd(unsigned char relay_type);
void start_networking_mode(void);
unsigned get_string(ubyte *rec_dest_add, urombyte *disp_start_add, ubyte rec_str_len, ubyte string_type);
void show_wrong_pswd(void);
void show_invalid_entry(void);
void supervisor_menu_options(void);
void setpt_source_select_options(void);
void send_exclu_test_cmd(unsigned char relay_type);
void test_normal_menu_options(void);
void test_exlusive_menu_options(void);
ubyte get_relay_status(unsigned char relay_type);
void load_pressure_status_screen(void);
void clear_keys_status(void);

void change_sleep_hrs(void);
void restore_dafault_settings(void);
void display_timeout_message(void);

void fault_bypass_mode(void);
void display_set_temp_values_screen(void);
void download_system_log_to_usb(void);
void download_analysis_log_to_usb(void);


