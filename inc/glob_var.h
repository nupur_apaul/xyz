//extern ubyte on_off;
extern ubyte pendrive_detected;
extern ubyte lhb_comm_fn_code;
extern ubyte selected_slave;
extern ubyte message_type;
extern ubyte new_pkt_rdy_to_be_sent;
extern ubyte no_of_logs_to_disp;
extern ubyte test_last_sent_byte;
extern unsigned short menu_timer_value;
extern ubyte event_pkt[4];
extern ubyte fault_pkt[4];
extern ubyte temp_fault_count;
extern uinteger temp_duty_count;
extern ubyte temp_trip_count;
extern ubyte in_test_mode;
//#######################################################################################
//				variable used for uart module
//#######################################################################################
extern ubyte u2_rx_byte;
extern ubyte prev_dup_crc_low,prev_dup_crc_high;
extern ubyte prev_tx_fn_code;;
extern unsigned short menu_selection;
extern unsigned short previous_menu_selection;
extern unsigned short item_selection;
extern uinteger menu_timer_count;
extern ubyte key_press;
extern uinteger delay_timer_count;

//#######################################################################################
//				variables for keypad module
//#######################################################################################
extern ubyte key_long_press_timer;
extern ubyte alpha_key_timer;
extern ubyte key_press;
extern ubyte key_debounce_count;
extern uinteger handshake_timer_count;
extern uinteger lcd_disp_timer_count;
//extern ubyte scanned_strg[];

//#######################################################################################
//				variables for timer module
//#######################################################################################
extern uinteger delay_timer_count;
extern uinteger default_timer_count;
extern uinteger intro_timer_count;
extern uinteger usb_timer_count;
extern uinteger ir_timer_count;
extern uinteger config_timer_count;
extern uinteger menu_timer_count;
extern uinteger watch_dog_timer;

extern enum menu_function_index_enum menu_function_index;
extern ubyte scanned_strg[16];

