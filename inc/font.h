//#define LCD_320x240
#define LCD_240x128

#ifdef LCD_320x240
#define LCD_X_PIXELS				320
#define LCD_Y_PIXELS				240
#endif

#ifdef LCD_240x128
#define LCD_X_PIXELS				240
#define LCD_Y_PIXELS				128
#endif

#define HEADING_FONT				arialNarrow_bld_13pix
#define HEADER_RES_SPACE			(uinteger)0
#define FOOTER_RES_SPACE			(uinteger)13
#define NO_OF_BYTES_PER_LINE		(uinteger)(LCD_X_PIXELS/8)


extern const uint_8 *FontCharBitmapsPtr[];
extern const FONT_CHAR_INFO *FontCharDescriptorsPtr[];
extern ubyte last_x_pos;

enum FONT_ENUM
{
//	arialNarrow_25pix,
//	arialNarrow_30pix,
//	arialNarrow_40pix,
//	arialNarrow_60pix,
//	arialNarrow_9pix,
//	arialNarrow_13pix,
//	arialNarrow_bld_60pix,
//	arialNarrow_bld_39pix,
//	arialNarrow_bld_25pix

	arialNarrow_bld_13pix,
	arialNarrow_bld_20pix
	
};

extern enum FONT_ENUM FONT;	

//#######################################################################################
//				variables for graphical lcd module
//#######################################################################################
extern unsigned short i,j,k,status;
extern unsigned char lcd_cmd,graph_lcd_data,str_len;
extern unsigned char CHAR_HEIGHT_IN_BITS,CHAR_WIDTH_IN_BITS;
//extern const FONT_INFO arial10ptFontInfo;

#ifdef LCD_320x240
// Font data for Arial Narrow 46pt
extern const uint_8 arialNarrow46ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrow46ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow46ptFontInfo;

// Font data for Arial Narrow 31pt
extern const uint_8 arialNarrow31ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrow31ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow31ptFontInfo;

// Font data for Arial Narrow 10pt
extern const uint_8 arialNarrowBld10ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrowBld10ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow10ptFontInfo;

// Font data for Arial Narrow 19pt
extern const uint_8 arialNarrowBld19ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrowBld19ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow19ptFontInfo;

// Font data for Arial Narrow 16pt
extern const uint_8 arialNarrow16ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrow16ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow16ptFontInfo;
#endif

#ifdef LCD_240x128

// Font data for Arial Narrow 5pt
extern const uint_8 arialNarrow5ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrow5ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow5ptFontInfo;

// Font data for Arial Narrow 23pt
extern const uint_8 arialNarrow23ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrow23ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow23ptFontInfo;

// Font data for Arial Narrow 16pt
extern const uint_8 arialNarrow16ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrow16ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow16ptFontInfo;

// Font data for Arial Narrow Bld 10pt
extern const uint_8 arialNarrowBld10ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrowBld10ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow10ptFontInfo;

// Font data for Arial Narrow 10pt
extern const uint_8 arialNarrow10ptCharBitmaps[];
extern const FONT_CHAR_INFO arialNarrow10ptCharDescriptors[][3];
//extern const FONT_INFO arialNarrow10ptFontInfo;

//// Font data for Arial Narrow Bld 16pt
//extern const uint_8 arialNarrow16ptCharBitmaps[];
//extern const FONT_CHAR_INFO arialNarrow16ptCharDescriptors[][3];
////extern const FONT_INFO arialNarrow10ptFontInfo;

#endif





//#######################################################################################
//					prototypes
//#######################################################################################


//########################### GRAPHICAL LCD ####################################################################
#ifdef LCD_320x240
void clr_disp(unsigned short value);
void initGraphicsLCD();
void update_lcd(unsigned char);
void data_wr(unsigned char);
void cmd_wr(unsigned char);
void data_pin_input(void);
void read_lcd(void);
void wcode(unsigned short,unsigned short);

unsigned long shift_coll(unsigned short first_byte, unsigned short sec_byte, unsigned char first_char_width, unsigned char sec_char_width);
void bit_shift( unsigned char col_val,unsigned char char_val);
void str_packing(urombyte *rec_ram_add,unsigned char *,const unsigned char *,const unsigned short *,unsigned char );
void load_rom_str_to_data_buff(urombyte *rec_ram_add,unsigned char *,unsigned char,const unsigned char *,const unsigned short *);
void load_ram_str_to_data_buff(ubyte *rec_ram_add,unsigned char *,unsigned char,const unsigned char *,const unsigned short *);
void graph_lcd_display(unsigned char,unsigned char,unsigned char *);
void scrolling_text(unsigned char *,unsigned char);
void testing_graph_lcd(void);
void busy(void);

//void puts_graphical_lcd_ram(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight,ubyte font_index_ptr);
//void puts_graphical_lcd_rom(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight,ubyte font_index_ptr);

void puts_graphical_lcd_ram_lim(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr);
void puts_graphical_lcd_rom_lim(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr);
void puts_graphical_lcd_ram_cnt(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt,ubyte font_index_ptr);
void puts_graphical_lcd_rom_cnt(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt,ubyte font_index_ptr);
void puts_graphical_lcd_ram(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt, ubyte rec_delim,ubyte font_index_ptr);
void puts_graphical_lcd_rom(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt, ubyte rec_delim,ubyte font_index_ptr);
#endif


#ifdef LCD_240x128
void set_mode(void);
void set_disp_mode(void);
void set_graph_area(void);
void set_graph_home();
void set_txt_area();
void set_txt_home();
void clr_disp(unsigned short value);
void data_addr_ptr(int addr);
void initGraphicsLCD();
void update_lcd(unsigned char);
void set_cursor_pattern(void);
void set_cursor_ptr(int addr);
void data_rd(void);
void data_wr(void);
void cmd_rd(void);
void cmd_wr(void);
void stat_rd(void);
void enable(void);
void data_pin_input(void);
void chk_stat1(void);
void chk_stat2(void);
void read_lcd(void);
void wcode(unsigned char,unsigned char);

unsigned long shift_coll(unsigned short first_byte, unsigned short sec_byte, unsigned char first_char_width, unsigned char sec_char_width);
void bit_shift( unsigned char col_val,unsigned char char_val);
void str_packing(urombyte *rec_ram_add,unsigned char *,const unsigned char *,const unsigned short *,unsigned char );
void load_rom_str_to_data_buff(urombyte *rec_ram_add,unsigned char *,unsigned char,const unsigned char *,const unsigned short *);
void load_ram_str_to_data_buff(ubyte *rec_ram_add,unsigned char *,unsigned char,const unsigned char *,const unsigned short *);
void graph_lcd_display(unsigned char,unsigned char,unsigned short *);
void scrolling_text(unsigned char *,unsigned char);
void testing_graph_lcd(void);
void busy(void);

//void puts_graphical_lcd_ram(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight,ubyte font_index_ptr);
//void puts_graphical_lcd_rom(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight,ubyte font_index_ptr);

void puts_graphical_lcd_ram_lim(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr);
void puts_graphical_lcd_rom_lim(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr);
void puts_graphical_lcd_ram_cnt(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt,ubyte font_index_ptr);
void puts_graphical_lcd_rom_cnt(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt,ubyte font_index_ptr);
void puts_graphical_lcd_ram(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt, ubyte rec_delim,ubyte font_index_ptr);
void puts_graphical_lcd_rom(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt, ubyte rec_delim,ubyte font_index_ptr);
#endif
