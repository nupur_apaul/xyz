/*
********************************************************************************
                                                                                
Software License Agreement                                                      
                                                                                
Copyright � 2008 Microchip Technology Inc. and its licensors.  All         
rights reserved.                                                                
                                                                                
Microchip licenses to you the right to: (1) install Software on a single        
computer and use the Software with Microchip 16-bit microcontrollers and        
16-bit digital signal controllers ("Microchip Product"); and (2) at your        
own discretion and risk, use, modify, copy and distribute the device            
driver files of the Software that are provided to you in Source Code;           
provided that such Device Drivers are only used with Microchip Products         
and that no open source or free software is incorporated into the Device        
Drivers without Microchip's prior written consent in each instance.             
                                                                                
You should refer to the license agreement accompanying this Software for        
additional information regarding your rights and obligations.                   
                                                                                
SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY         
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY              
WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A          
PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE             
LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY,               
CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY           
DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO ANY         
INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR         
LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY,                 
SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY         
DEFENSE THEREOF), OR OTHER SIMILAR COSTS.                                       
                                                                                
********************************************************************************

 Change History:
  Rev   Description
  ----  -----------------------------------------
  2.7   Updated to include PIC32 support

********************************************************************************
*/

#include "..\inc\Compiler.h"
#include "..\inc\GenericTypedefs.h"
#include "..\inc\HardwareProfile.h"
#include "..\inc\boot.h"
#include "..\inc\FSIO.h"
#include "..\inc\usb.h"
#include "..\inc\usb_host_msd_scsi.h"

// *****************************************************************************
// *****************************************************************************
// Configuration Bits
// *****************************************************************************
// *****************************************************************************

#define PLL_96MHZ_OFF   0xFFFF
#define PLL_96MHZ_ON    0xF7FF


// *****************************************************************************
// *****************************************************************************
// Configuration Bits
// *****************************************************************************
// *****************************************************************************

#ifdef __C30__
    #define PLL_96MHZ_OFF   0xFFFF
    #define PLL_96MHZ_ON    0xF7FF

    // Configuration Bit settings  for an Explorer 16 with USB PICtail Plus
    //      Primary Oscillator:             HS
    //      Internal USB 3.3v Regulator:    Disabled
    //      IOLOCK:                         Set Once
    //      Primary Oscillator Output:      Digital I/O
    //      Clock Switching and Monitor:    Both disabled
    //      Oscillator:                     Primary with PLL
    //      USB 96MHz PLL Prescale:         Divide by 2
    //      Internal/External Switch Over:  Enabled
    //      WDT Postscaler:                 1:32768
    //      WDT Prescaler:                  1:128
    //      WDT Window:                     Non-window Mode
    //      Comm Channel:                   EMUC2/EMUD2
    //      Clip on Emulation Mode:         Reset into Operation Mode
    //      Write Protect:                  Disabled
    //      Code Protect:                   Disabled
    //      JTAG Port Enable:               Disabled

    #if defined(__PIC24FJ256GB110__)
        _CONFIG2(FNOSC_PRIPLL & POSCMOD_HS & PLL_96MHZ_ON & PLLDIV_DIV2) // Primary HS OSC with PLL, USBPLL /2
        _CONFIG1(JTAGEN_OFF & FWDTEN_OFF & ICS_PGx2)   // JTAG off, watchdog timer off
        _CONFIG3(0xFFFF);
    #elif defined(__PIC24FJ64GB004__)
        _CONFIG1(WDTPS_PS1 & FWPSA_PR32 & WINDIS_OFF & FWDTEN_OFF & ICS_PGx1 & GWRP_OFF & GCP_OFF & JTAGEN_OFF)
        _CONFIG2(POSCMOD_HS & I2C1SEL_PRI & IOL1WAY_OFF & OSCIOFNC_ON & FCKSM_CSDCMD & FNOSC_PRIPLL & PLL96MHZ_ON & PLLDIV_DIV2 & IESO_ON)
        _CONFIG3(WPFP_WPFP0 & SOSCSEL_SOSC & WUTSEL_LEG & WPDIS_WPDIS & WPCFG_WPCFGDIS & WPEND_WPENDMEM)
        _CONFIG4(DSWDTPS_DSWDTPS3 & DSWDTOSC_LPRC & RTCOSC_SOSC & DSBOREN_OFF & DSWDTEN_OFF)
    #elif defined(__PIC24FJ256GB106__)
        _CONFIG1( JTAGEN_OFF & GCP_OFF & GWRP_OFF & COE_OFF & FWDTEN_OFF & ICS_PGx2) 
        _CONFIG2( 0xF7FF & IESO_OFF & FCKSM_CSDCMD & OSCIOFNC_OFF & POSCMOD_HS & FNOSC_PRIPLL & PLLDIV_DIV3 & IOL1WAY_ON)
    #elif defined(__PIC24FJ256DA210__) || defined(__PIC24FJ256GB210__) 
        _CONFIG1(FWDTEN_OFF & ICS_PGx2 & GWRP_OFF & GCP_OFF & JTAGEN_OFF)
        _CONFIG2(POSCMOD_HS & IOL1WAY_ON & OSCIOFNC_ON & FCKSM_CSDCMD & FNOSC_PRIPLL & PLL96MHZ_ON & PLLDIV_DIV2 & IESO_OFF)
    #endif
#elif defined( __PIC32MX__ )

		//config for LHB
		#pragma config UPLLEN   = ON            // USB PLL Enabled
		#pragma config FPLLMUL  = MUL_20        // PLL Multiplier
		#pragma config UPLLIDIV = DIV_4         // USB PLL Input Divider
		#pragma config FPLLIDIV = DIV_4         // PLL Input Divider
		#pragma config FPLLODIV = DIV_1         // PLL Output Divider
		#pragma config FPBDIV   = DIV_1         // Peripheral Clock divisor
		#pragma config FWDTEN   = OFF           // Watchdog Timer 
		#pragma config WDTPS    = PS4096           // Watchdog Timer Postscale//AMAN 05JUN2013: software watchdog with 2 secs 
//		#pragma config WDTPS    = PS1           // Watchdog Timer Postscale
		#pragma config FCKSM    = CSDCMD        // Clock Switching & Fail Safe Clock Monitor
		#pragma config OSCIOFNC = OFF           // CLKO Enable
		#pragma config POSCMOD  = HS            // Primary Oscillator
		#pragma config IESO     = OFF           // Internal/External Switch-over
		#pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable
		#pragma config FNOSC    = PRIPLL        // Oscillator Selection
		#pragma config CP       = OFF           // Code Protect
		#pragma config BWP      = OFF           // Boot Flash Write Protect
		#pragma config PWP      = OFF           // Program Flash Write Protect
		#pragma config ICESEL   = ICS_PGx2      // ICE/ICD Comm Channel Select
		#pragma config DEBUG    = OFF           // Debugger Disabled for Starter Kit
		#pragma config FVBUSONIO = OFF       // VBUSON not controlled by USB module,used as IO
		#pragma config FUSBIDIO = OFF


#else

    #warning Cannot define configuration bits.

#endif

// Macro used to call main application
/****************************************************************************
  Function:
    int BootApplication ( void )

  Description:
    This macro is used to launch the application.

  Precondition:
    The application image must be correctly programmed into Flash at the 
    appropriate entry point.

  Parameters:
    None

  Returns:
    This call does not normally return.

  Remarks:
    The application's entry point is defined by the APPLICATION_ADDRESS
    macro in the boot_config.h header file.
***************************************************************************/

#define BootApplication()       (((int(*)(void))(APPLICATION_ADDRESS))())

 void Init_Port(void);
/****************************************************************************
  Function:
    void LoadApplication ( void )

  Description:
    This routine attempts to initialize and attach to the boot medium, locate
    the boot image file, and load and program it to the flash.

  Precondition:
    The boot loader IO must have been initialized.

  Parameters:
    None

  Returns:
    None

  Remarks:
    None
***************************************************************************/
BYTE check_sum_rec[10]={0};
BYTE 	temp_filename[12]={0};
void LoadApplication ( void )
{
    BOOL    LoadingApplication      = TRUE;
    BOOL    TransportInitialized    = FALSE;
    //BOOL    FileSystemInitialized   = FALSE;
    BOOL    BootMediumAttached      = FALSE;
    BOOL    BootImageFileFound      = FALSE;
    BOOL    BootKeyFileFound      = FALSE;
    BOOL    BootImageFileError      = FALSE;
    int     ErrCount                = 0;
	FSFILE         *fp;             // File pointer
	BYTE temp_count=0,chk_sum_cnt=0;
	BYTE	usb_write_buffer[50]={0},usb_read_buffer[50]={0};
    // Loader main loop
    while (LoadingApplication)
    {
 //       if (TransportInitialized)
 //       {
            // Keep the boot medium alive
 //           TransportInitialized = BLMedia_MonitorMedia();
            
            // Check for the boot medium to attach
            BootMediumAttached = BLMedia_MediumAttached();
            if (BootMediumAttached)
            {
	            mLED_3_Off();
	            mLED_2_On();
//	            FSchdir( "\\SOURCE\\");
	            FSchdir( "\\LHBLCD.SRC\\");//AMAN 06/01/2014
                if (!BootImageFileError)
                {
	                // Attempt to locate the boot key file
	                BootKeyFileFound = BLMedia_LocateFile(BOOT_KEY_FILE);
	                if (BootKeyFileFound)
	                {
		                //boot.rep found,now get the filename.hex and CRC
		                if ( (fp=FSfopen( BOOT_KEY_FILE, "r" )) == NULL )
					    {
					        BLIO_ReportBootStatus (BL_FILE_ERR, "BL: USB Error - Unable to open file\r\n" );
					     //   return FALSE;
					    }
							                
		                FSfread(usb_read_buffer, 1, 50, fp );
    	                FSfclose( fp );
    	                temp_count=0;
    	                while(usb_read_buffer[temp_count]!=',')
    	                BOOT_FILE_NAME[temp_count]=usb_read_buffer[temp_count++];
    	                temp_count++;
    	                while(usb_read_buffer[temp_count]!=0)
    	                check_sum_rec[chk_sum_cnt++] = usb_read_buffer[temp_count++];
    	                Nop();
    	             } 
	                // Attempt to locate the boot image file
                    BootImageFileFound = BLMedia_LocateFile(BOOT_FILE_NAME);
                    /* terron: after locating open boot file,generate CRC
                    	open boot.rep and match the filename & CRC
                    	if ok proceeed with loading file.
                    	in any case create status.txt*/
                    if (BootImageFileFound)
                    {
                        BLIO_ReportBootStatus(BL_FOUND_FILE, "BL: Application image file has been found\r\n");
						
                        // Read the boot image file and program it to Flash
                        if (BLMedia_LoadFile(BOOT_FILE_NAME))
                        {
                            LoadingApplication = FALSE;
                            BLIO_ReportBootStatus(BL_PROGRAMMED, "BL: Application image has been programmed\r\n");
                            mLED_2_Off();
                            mLED_1_Off();
                            
                            temp_count=0;
                            while(BOOT_FILE_NAME[temp_count]!=0)
                            {
	                            usb_write_buffer[temp_count]= BOOT_FILE_NAME[temp_count];
	                            temp_count++;
	                        } 
	                        	usb_write_buffer[temp_count++]=' ';
	                       
								usb_write_buffer[temp_count++]=(temp_month/10)+0x30;
   								usb_write_buffer[temp_count++]=(temp_month%10)+0x30;
   								usb_write_buffer[temp_count++]='/';
   								usb_write_buffer[temp_count++]=(temp_day/10)+0x30;
   								usb_write_buffer[temp_count++]=(temp_day%10)+0x30;
   								usb_write_buffer[temp_count++]='/';
   								usb_write_buffer[temp_count++]=((temp_yr/1000)%10)+0x30;
   								usb_write_buffer[temp_count++]=((temp_yr/100)%10)+0x30;
   								usb_write_buffer[temp_count++]=((temp_yr/10)%10)+0x30;
   								usb_write_buffer[temp_count++]=(temp_yr%10)+0x30;
   								usb_write_buffer[temp_count++]=' ';
   								usb_write_buffer[temp_count++]=(temp_hrs/10)+0x30;
   								usb_write_buffer[temp_count++]=(temp_hrs%10)+0x30;
   								usb_write_buffer[temp_count++]=':';
   								usb_write_buffer[temp_count++]=(temp_mins/10)+0x30;
   								usb_write_buffer[temp_count++]=(temp_mins%10)+0x30;
   								
   								usb_write_buffer[temp_count++]=' ';
   								
	                        	usb_write_buffer[temp_count++]='O';
	                        	usb_write_buffer[temp_count++]='K';
	                        	
	                            fp=FSfopen( "STATUS.TXT", "w" );
	                            FSfwrite(usb_write_buffer, 1, temp_count, fp);
	                            FSfclose( fp );
	                            
                            
                        }
                        else
                        {
                            // Error reported by lower layer
                            BootImageFileError = TRUE;
                            mLED_1_Off();
                            mLED_4_On();
                            
                            temp_count=0;
                            while(BOOT_FILE_NAME[temp_count]!=0)
                            {
	                            usb_write_buffer[temp_count]= BOOT_FILE_NAME[temp_count];
	                            temp_count++;
	                        } 
                        	usb_write_buffer[temp_count++]=' ';
                        	
                    		usb_write_buffer[temp_count++]=(temp_month/10)+0x30;
							usb_write_buffer[temp_count++]=(temp_month%10)+0x30;
							usb_write_buffer[temp_count++]='/';
							usb_write_buffer[temp_count++]=(temp_day/10)+0x30;
							usb_write_buffer[temp_count++]=(temp_day%10)+0x30;
							usb_write_buffer[temp_count++]='/';
							usb_write_buffer[temp_count++]=((temp_yr/1000)%10)+0x30;
							usb_write_buffer[temp_count++]=((temp_yr/100)%10)+0x30;
							usb_write_buffer[temp_count++]=((temp_yr/10)%10)+0x30;
							usb_write_buffer[temp_count++]=(temp_yr%10)+0x30;
							usb_write_buffer[temp_count++]=' ';
							usb_write_buffer[temp_count++]=(temp_hrs/10)+0x30;
							usb_write_buffer[temp_count++]=(temp_hrs%10)+0x30;
							usb_write_buffer[temp_count++]=':';
							usb_write_buffer[temp_count++]=(temp_mins/10)+0x30;
							usb_write_buffer[temp_count++]=(temp_mins%10)+0x30;
							
							usb_write_buffer[temp_count++]=' ';
   								
                        	usb_write_buffer[temp_count++]='N';
                        	usb_write_buffer[temp_count++]='O';
                        	usb_write_buffer[temp_count++]='T';
                        	usb_write_buffer[temp_count++]=' ';
                        	usb_write_buffer[temp_count++]='O';
                        	usb_write_buffer[temp_count++]='K';
                        	
                            fp=FSfopen( "STATUS.TXT", "w" );
                            FSfwrite(usb_write_buffer, 1, temp_count, fp);
                            FSfclose( fp );
                        }
                    }
                    else
                    {
                        // Count and, if necessary, report the errors locating the file
                        ErrCount++;
                        if (ErrCount > MAX_LOCATE_RETRYS)
                        {
                            ErrCount = 0;
                            BootImageFileError = TRUE;
                            BLIO_ReportBootStatus(BL_FILE_NOT_FOUND, "BL: Application image not found\r\n");
                        }
                    }
                }
            //}
            else
           	{
                BootImageFileError = FALSE;
            }
        }
//        else
//        {
//            // Initialize transport layer used to access the boot image's file system
//            TransportInitialized = BLMedia_InitializeTransport();
//            if (TransportInitialized)
//            {
//                BLIO_ReportBootStatus(BL_TRANSPORT_INIT, "BL: Transport initialized\r\n");
//            }
//        }

        // Watch for user to abort the load
//d        if (BLIO_AbortLoad())
  //d      {
    //d        LoadingApplication = FALSE;
      //d  }
    }

} // LoadApplication


/****************************************************************************
  Function:
    int main(void)

  Description:
    This is the boot loader's main C-language entry point.  It initializes 
    the boot loader's IO, and uses it to determine if the boot loader should
    be invoked.  If so, it attempts to load the application.  After loading
    and programming the boot image (or immediately, if the boot loader is
    not invoked), it checks the to see if the image in Flash is valid and, 
    if so, calls the application's main entry point.

  Precondition:
    The appropriate startup code must have been executed.

  Parameters:
    None

  Returns:
    Integer exit code (0)

  Remarks:
    This routine is executed only once, after a reset.
***************************************************************************/
int main ( void )
{
    #if defined(__PIC32MX__)
//        // Initialize the MCU
//        SYSTEMConfigWaitStatesAndPB( GetSystemClock() );
//        CheKseg0CacheOn();
//        INTEnableSystemMultiVectoredInt();
//	SYSTEMConfig(40000000, SYS_CFG_ALL );
   BOOL    TransportInitialized    = FALSE;
    //BOOL    FileSystemInitialized   = FALSE;
    BOOL    BootMediumAttached      = FALSE;
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
    #endif

	Init_Port();
    // Initialize the boot loader IO
    BLIO_InitializeIO();
    USB_VBUSON_TRIS	 = 0;
	USB_VBUSON = 0;

    BLIO_ReportBootStatus(BL_RESET, "BL: ***** Reset *****\r\n");
	mLED_3_On();
    // Check to see if the user requested loading of a new application
//    if (BLIO_LoaderEnabled())
    {
	    #if defined(PIC24FJ64GB004_PIM) || defined(PIC24FJ256DA210_DEV_BOARD)
    	//On the PIC24FJ64GB004 Family of USB microcontrollers, the PLL will not power up and be enabled
    	//by default, even if a PLL enabled oscillator configuration is selected (such as HS+PLL).
    	//This allows the device to power up at a lower initial operating frequency, which can be
    	//advantageous when powered from a source which is not gauranteed to be adequate for 32MHz
    	//operation.  On these devices, user firmware needs to manually set the CLKDIV<PLLEN> bit to
    	//power up the PLL.
        {
            unsigned int pll_startup_counter = 600;
            CLKDIVbits.PLLEN = 1;
            while(pll_startup_counter--);
        }
    
        //Device switches over automatically to PLL output after PLL is locked and ready.
        #endif

        #if defined(__PIC24F__)
        INTCON2bits.ALTIVT = 1;
        #endif
      	TransportInitialized = BLMedia_InitializeTransport();
	    unsigned int delay,delay1;
        for(delay=0;delay<10000;delay++)
        {
            for(delay1=0;delay1<1000;delay1++)
        	{
	        	Nop();
        	}
            TransportInitialized = BLMedia_MonitorMedia();
        	BootMediumAttached = BLMedia_MediumAttached();
       		if(BootMediumAttached)
       		{
			     mLED_3_On();
       		     LoadApplication();
       		     break;
       		}     
        }	
    }

    // Launch the application if the image in Flash is valid
    if (BL_ApplicationIsValid())
    {
        BLIO_ReportBootStatus(BL_BOOTING, "BL: Launching application\r\n");

        // Must disable all interrupts
          BLMedia_DeinitializeTransport();
        

        #if defined(__PIC32MX__)

        INTDisableInterrupts();

        #else

        U1IE = 0;
        U1IR = 0xFF;
        IEC5 = 0;
        IFS5 = 0;
        INTCON2bits.ALTIVT = 0;

        #endif

        ////////////////////////////
        // Launch the application //
        ////////////////////////////

        BootApplication();
    }

    // Should never get here if a valid application was loaded.
    BLIO_ReportBootStatus(BL_BOOT_FAIL, "BL: Application failed to launch\r\n");
    BL_ApplicationFailedToLaunch();

    // Hang system
    while (1)
        ;

    return 0;

} // main

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
 void Init_Port(void)
 {
	AD1PCFG = 0XFFFF;					// ALL DIGITAL
 		
/* Initialisation of PORTA */
	LATA 	=   0b0000000000000000;	
	TRISA	=	0b1000010000100010;		 		
	
/* Initialisation of PORTB */
	LATB	= 	0b0000000000000000;	
	TRISB	=	0b0010111111000111;  
				
/* Initialisation of PORTC */
	LATC	=	0b0000000000000000;	
	TRISC	=	0b000000000000000;
	
/* Initialisation of PORTD */
	LATD	=	0b0000000000000000;	
	TRISD	=	0b0100100000000000;
	
/* Initialistion of PORTE */
	LATE	=	0b0000000000000000;	
	TRISE	=	0b0000000000000000;			
	
/*Initialisation of PORTF */
	LATF	=	0b0000000000000000;	
	TRISF	=	0b0000000000001100;
		
/*Initialization of PORTG*/	
	LATG	=	0b0000000000000000;	
	TRISG 	 =  0b0000000010000000;
}
//#######################################################################################
/*
*******************************************************************************
EOF
*******************************************************************************
*/

