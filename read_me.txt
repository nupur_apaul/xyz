ver 3.08: 
	a. ashwin sir's handover.
ver 3.09: changes made by Aman
	a. unit no added on default screen.
	b. set temp long key press reduced to 3-4 sec. 
	c. watchdog timer added on 22May2013.
	d. status of LP HP corrected in set temp screen.
	e. comp status in plant screen is corrected.
ver 3.10:
	a. issues related to communication resolved
	b. keypad scaning modified