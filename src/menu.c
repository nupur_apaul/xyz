#include "..\inc\headers.h"
//#include "..\inc\FSIO.h"


//FSFILE *filePointer;

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: select_option
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:
//	rec_option = select_option( disp_super_menu, disp_sup_menu_options, LEN_SUP_MENU);
//	if(rec_option)
//		(*exec_sup_menu_options[rec_option - 1])();

//#######################################################################################
ubyte select_option(ubyte rec_opt_index, urombyte *rec_heading_add, uromptr *rec_options_add, ubyte rec_option_length)
{
	ubyte type_count = rec_opt_index,temp_len_cnt,max_menu_len;
	ubyte shift_count = 0,highlight;
	home_clr();
	key_press = 0;
	while(1)
	{
		WDTCLR
		puts_graphical_lcd_rom_lim(rec_heading_add,0,HEADER_RES_SPACE,1,0,arialNarrow_bld_25pix);
		if(rec_option_length>6)
		  max_menu_len=6;
		 else
		   max_menu_len = rec_option_length;
		for(temp_len_cnt=0;temp_len_cnt<max_menu_len;temp_len_cnt++)
		{
			if(type_count<6)	
				highlight=type_count;
			else
				highlight=5;
	
			if(type_count/6 && (orphan_flags2.bits.shift_up || orphan_flags2.bits.shift_dwn))
			{
				if(orphan_flags2.bits.shift_up)
				shift_count++;
				else if(orphan_flags2.bits.shift_dwn)
				shift_count--;
				
				orphan_flags2.bits.shift_up=0;
				orphan_flags2.bits.shift_dwn=0;
			}
			puts_graphical_lcd_rom_lim(rec_options_add[temp_len_cnt+shift_count],0,((temp_len_cnt+1)*25)+25,!(highlight-temp_len_cnt),0,arialNarrow_bld_25pix);	
		}
		
		key_press = 0;
		menu_timer_count = 40;
		orphan_flags1.bits.menu_timer = 1;
		while(	key_press != 'e' && key_press != 'a' && key_press != 'b')
		{
			WDTCLR
			key_press = scan_num_keypad();
/*			if(!orphan_flags1.bits.menu_timer)
			{
				show_time_out();
				return(0);
			}
			else*/ if( key_press == 'c')
			{
				home_clr();
				return(0);
			}		
				
			else if(key_press == 'b')
			{
				type_count++;
				orphan_flags2.bits.shift_up=1;
				if(type_count == rec_option_length)
				{
					type_count = 0;
					shift_count=0;
				}	
			}
			else if(key_press == 'a')
			{
				if(type_count == 0)
				{
					type_count = rec_option_length;					
					shift_count=0;
				}	
				type_count--;
				orphan_flags2.bits.shift_dwn=1;
			}
			else if(key_press == 'e')
			{
				puts_graphical_lcd_rom_lim(rec_options_add[type_count],0,(highlight+1)*25,0,0,arialNarrow_bld_25pix);
				Delays_Xtal_ms(5);
				puts_graphical_lcd_rom_lim(rec_options_add[type_count],0,(highlight+1)*25,1,0,arialNarrow_bld_25pix);	
				Delays_Xtal_ms(5);
				home_clr();
				orphan_flags1.bits.menu_timer = 0;
				return(++type_count);
			}
		}
		Nop();
	}
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: select_option
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:
//	rec_option = select_option( disp_super_menu, disp_sup_menu_options, LEN_SUP_MENU);
//	if(rec_option)
//		(*exec_sup_menu_options[rec_option - 1])();

ubyte select_option1 (struct menu_struct *menu);

//#######################################################################################
ubyte select_option1 (struct menu_struct *menu)
{
	ubyte *on_off_string[2] = {(ubyte*)("OFF"),(ubyte*)("ON")};
	ubyte on_off  = 0;
	
	ubyte *override_string[2] = {(ubyte*)("NORMAL"),(ubyte*)("BYPASS")};
	ubyte override  = 0;
	ubyte temp_line_num;
	home_clr();
	key_press = 0;
	if(menu->total_size <= menu->window_size)
		menu->window_size = menu->total_size;// - 1;
	while(1)
	{
		ClearWDT();
		WDTCLR
		puts_graphical_lcd_rom_lim( menu->rec_heading_add,0,HEADER_RES_SPACE,0,0,menu->font_index_ptr);
//		TEST_PIN = 1;
		for(temp_line_num = 0; temp_line_num < menu->window_size; temp_line_num++)
		{
			puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->window_st_num+temp_line_num], 0,
			((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
			
			if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU )&& (menu->window_st_num+temp_line_num) <  menu->total_size-1)
			{
				on_off = get_relay_status(menu->window_st_num+temp_line_num);
				puts_graphical_lcd_ram_lim(on_off_string[on_off],23,
				((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
				if(menu_selection == NORMAL_TEST_MENU && message_type)
					puts_graphical_lcd_rom_lim(disp_excl_test_messages[message_type],0,13*9,1,0,arialNarrow_bld_13pix);
			}
			
			if((menu_selection ==  BYPASS_FAULTS)&& (menu->window_st_num+temp_line_num) <  menu->total_size-1)
			{
				override = get_override_status(menu->window_st_num+temp_line_num);
				puts_graphical_lcd_ram_lim(override_string[override],18,
				((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
		
			}
		}		
//		TEST_PIN = 0;
		key_press = 0;
		clear_keys_status();
		menu_timer_count = 500;
		orphan_flags1.bits.menu_timer = 1;
	
		while(	key_press != 'e' )//&& key_press != 'a' && key_press != 'b')
		{	
		ClearWDT();
			WDTCLR
			key_press = scan_num_keypad();
/*			if(!orphan_flags1.bits.menu_timer)
			{
				show_time_out();
				menu->curr_list_sel = menu->total_size-1;
				return(0);
			}

			else*/ if( keys[0].long_press)// 7 set temp 
			{	
				home_clr();
				display_set_temp_selection_screen();				
			}
				
			else if(key_press == 'b')
			{
				puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->window_st_num+menu->hlight_line_num], 0,
				((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE,0, 0,menu->font_index_ptr);
				if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU )&& (menu->window_st_num+menu->hlight_line_num) <  menu->total_size-1)
				{
					on_off = get_relay_status(menu->window_st_num+menu->hlight_line_num);
					puts_graphical_lcd_ram_lim(on_off_string[on_off],23,
					((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE,0, 0,menu->font_index_ptr);
					if(menu_selection == NORMAL_TEST_MENU && message_type)
						puts_graphical_lcd_rom_lim(disp_excl_test_messages[message_type],0,13*9,1,0,arialNarrow_bld_13pix);
				}
				
				if((menu_selection ==  BYPASS_FAULTS)&& (menu->window_st_num+menu->hlight_line_num) <  menu->total_size-1)
				{
					override = get_override_status(menu->window_st_num+menu->hlight_line_num);
					puts_graphical_lcd_ram_lim(override_string[override],18,
					((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE, 0, 0,menu->font_index_ptr);
				}
				menu->hlight_line_num++;
				if(menu->hlight_line_num == menu->window_size)
				{
					menu->window_st_num++;
					menu->hlight_line_num--;
					if(menu->window_st_num > (menu->total_size - menu->window_size))
					{
						menu->window_st_num = 0;
						menu->hlight_line_num = 0;
					}

					for(temp_line_num = 0; temp_line_num < menu->window_size; temp_line_num++)
					{
						puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->window_st_num+temp_line_num], 0,
						((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
						
						if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU )&& (menu->window_st_num+temp_line_num) <  menu->total_size-1)
						{
							on_off = get_relay_status(menu->window_st_num+temp_line_num);
							puts_graphical_lcd_ram_lim(on_off_string[on_off],23,
							((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
							if(menu_selection == NORMAL_TEST_MENU && message_type)
								puts_graphical_lcd_rom_lim(disp_excl_test_messages[message_type],0,13*9,1,0,arialNarrow_bld_13pix);
						}
						
						if((menu_selection ==  BYPASS_FAULTS)&& (menu->window_st_num+temp_line_num) <  menu->total_size-1)
						{
							override = get_override_status(menu->window_st_num+temp_line_num);
							puts_graphical_lcd_ram_lim(override_string[override],18,
							((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
						}
					}		
	
				}
				puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->window_st_num+menu->hlight_line_num], 0,
				((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE,1, 0,menu->font_index_ptr);

				if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU )&& (menu->window_st_num+menu->hlight_line_num) <  menu->total_size-1)
				{
					on_off = get_relay_status(menu->window_st_num+menu->hlight_line_num);
					puts_graphical_lcd_ram_lim(on_off_string[on_off],23,
					((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE,1, 0,menu->font_index_ptr);
					if(menu_selection == NORMAL_TEST_MENU && message_type)
						puts_graphical_lcd_rom_lim(disp_excl_test_messages[message_type],0,13*9,1,0,arialNarrow_bld_13pix);
				}
				
				if((menu_selection ==  BYPASS_FAULTS)&& (menu->window_st_num+menu->hlight_line_num) <  menu->total_size-1)
				{
					override = get_override_status(menu->window_st_num+menu->hlight_line_num);
					puts_graphical_lcd_ram_lim(override_string[override],18,
					((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE, 1, 0,menu->font_index_ptr);
				}

			}
			else if(key_press == 'a')
			{
				puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->window_st_num+menu->hlight_line_num], 0,
				((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE, 0, 0,menu->font_index_ptr);
				if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU )&& (menu->window_st_num+menu->hlight_line_num) <  menu->total_size-1)
				{
					on_off = get_relay_status(menu->window_st_num+menu->hlight_line_num);
					puts_graphical_lcd_ram_lim(on_off_string[on_off],23,
					((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE, 0, 0,menu->font_index_ptr);
				}
				if((menu_selection ==  BYPASS_FAULTS)&& (menu->window_st_num+menu->hlight_line_num) <  menu->total_size-1)
				{
					override = get_override_status(menu->window_st_num+menu->hlight_line_num);
					puts_graphical_lcd_ram_lim(override_string[override],18,
					((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE,0, 0,menu->font_index_ptr);
				}
				if(!menu->hlight_line_num--)
				{
					menu->hlight_line_num++;
					if(!menu->window_st_num--)
					{
						menu->window_st_num = menu->total_size - menu->window_size;
						menu->hlight_line_num = menu->window_size - 1;
					}
					
					for(temp_line_num = 0; temp_line_num < menu->window_size; temp_line_num++)
					{
						puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->window_st_num+temp_line_num], 0,
						((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
						
						if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU )&& (menu->window_st_num+temp_line_num) <  menu->total_size-1)
						{
							on_off = get_relay_status(menu->window_st_num+temp_line_num);
							puts_graphical_lcd_ram_lim(on_off_string[on_off],23,
							((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
							if(menu_selection == NORMAL_TEST_MENU && message_type)
								puts_graphical_lcd_rom_lim(disp_excl_test_messages[message_type],0,13*9,1,0,arialNarrow_bld_13pix);
						}
						
						if((menu_selection ==  BYPASS_FAULTS)&& (menu->window_st_num+temp_line_num) <  menu->total_size-1)
						{
							override = get_override_status(menu->window_st_num+temp_line_num);
							puts_graphical_lcd_ram_lim(override_string[override],18,
							((temp_line_num+1)*menu->font_size)+HEADER_RES_SPACE, !(menu->hlight_line_num - temp_line_num), 0,menu->font_index_ptr);
						}
					}		
					
				}
				puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->window_st_num+menu->hlight_line_num], 0,
				((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE, 1, 0,menu->font_index_ptr);
				if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU )&& (menu->window_st_num+menu->hlight_line_num) <  menu->total_size-1)
				{
					on_off = get_relay_status(menu->window_st_num+menu->hlight_line_num);
					puts_graphical_lcd_ram_lim(on_off_string[on_off],23,
					((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE, 1, 0,menu->font_index_ptr);
					if(menu_selection == NORMAL_TEST_MENU && message_type)
						puts_graphical_lcd_rom_lim(disp_excl_test_messages[message_type],0,13*9,1,0,arialNarrow_bld_13pix);
				}
				
				if((menu_selection ==  BYPASS_FAULTS)&& (menu->window_st_num+menu->hlight_line_num) <  menu->total_size-1)
				{
					override = get_override_status(menu->window_st_num+menu->hlight_line_num);
					puts_graphical_lcd_ram_lim(override_string[override],18,
					((menu->hlight_line_num+1)*menu->font_size)+HEADER_RES_SPACE,1, 0,menu->font_index_ptr);
				}

			}
			else if(key_press == 'e')
			{
				puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->hlight_line_num+menu->window_st_num],0,
											HEADER_RES_SPACE + (menu->hlight_line_num + 1) * menu->font_size, 0, 0, menu->font_index_ptr);
				Delays_Xtal_ms(50);
				puts_graphical_lcd_rom_lim( menu->rec_options_add[menu->hlight_line_num+menu->window_st_num],0,
											HEADER_RES_SPACE + (menu->hlight_line_num + 1) * menu->font_size, 1, 0, menu->font_index_ptr);
				Delays_Xtal_ms(50);
				home_clr();
				orphan_flags1.bits.menu_timer = 0;
				menu->curr_list_sel = menu->window_st_num + menu->hlight_line_num;
				return(0);//lakh 
			}
			if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU || menu_selection == BYPASS_FAULTS ))
			{
				if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU ))
					lhb_comm_fn_code = FC_TEST_MODE;
				else if(menu_selection == BYPASS_FAULTS )
					lhb_comm_fn_code =  FC_FAULT_OVERRIDE;
				new_pkt_rdy_to_be_sent = 1;
				//lhb_comm.flags.bits.rx_enable=1;
				check_fn_code_send_pkt_to_lhb();
				if( test2.bits.t25)
				{
					test2.bits.t25 = 0;
					menu_selection = DEFAULT_SCREEN_MENU;
					lhb_comm_fn_code = FC_DEFAULT;
				}	
			}
			else
			{
				check_fn_code_send_pkt_to_lhb();	
			}
			
			if(key_press == 'r') //lakh
			{
				home_clr();
//				if((menu_selection == NORMAL_TEST_MENU || menu_selection == EXCL_TEST_MENU ))
//					return(0);
//				else
				{
					lhb_comm_fn_code = FC_DEFAULT;
					menu_selection = DEFAULT_SCREEN_MENU;
	                  	return(1);
				}
			}
//			if((key_press == 'a' || key_press == 'b')&&(menu->window_st_num>8))
//			break;
			if(key_press != 'e' && key_press != 0)
				clear_keys_status();			
		}
		Nop();
	}
	return(0);
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: menu_mode_options
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void menu_mode_options(void)
{
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	struct menu_struct menu_mode;
	menu_mode.rec_heading_add = disp_menu_mode;
	menu_mode.rec_options_add = disp_menu_mode_options;
	menu_mode.total_size = LEN_MAIN_MENU;
	menu_mode.window_st_num = 0;
	menu_mode.window_size = 7;
	menu_mode.font_size = 13;
	menu_mode.font_index_ptr = arialNarrow_bld_13pix;
	menu_mode.hlight_line_num = 0;
	menu_mode.curr_list_sel = 0;
	
	key_press = 0;	
	clear_keys_status();
	home_clr();
//??	Delays_Xtal_ms(1000);	
    previous_menu_selection =  menu_selection;
	menu_selection = MAIN_MENU;	
	while(1)
	{
		WDTCLR
		menu_selection = MAIN_MENU;	
		new_pkt_rdy_to_be_sent = 0;	
		if(select_option1(&menu_mode))
			break;
		else if(menu_mode.curr_list_sel == menu_mode.total_size-1)
			break;
		else
			(*exec_menu_mode_func_ptr[menu_mode.curr_list_sel])();
		
	}	
	new_pkt_rdy_to_be_sent = 0;		
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void default_settings_options (void)
{
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	str_copy_rom_cnt( (urombyte *)"1111",fact_pswd,4);
	
	struct menu_struct default_settings_mode;
//	default_settings_mode.rec_heading_add = disp_change_default_settings;
	default_settings_mode.rec_heading_add = menu_disp_change_default_settings;//lakh
	default_settings_mode.rec_options_add = disp_default_settings_options;
	default_settings_mode.total_size = LEN_DEFAULT_MENU;
	default_settings_mode.window_st_num = 0;
	default_settings_mode.window_size = 8;
	default_settings_mode.font_size = 13;
	default_settings_mode.font_index_ptr = arialNarrow_bld_13pix;
	default_settings_mode.hlight_line_num = 0;
	default_settings_mode.curr_list_sel = 0;
	
	key_press = 0;	
	home_clr();
//??	Delays_Xtal_ms(1000);	
previous_menu_selection =  menu_selection;
	menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;	
	
	if(!get_string( temp_pswd, disp_enter_pswd, PASSWORD_DIG_COUNT, NUMERIC))
		return;//break;
	if(!str_comp_ram(fact_pswd, temp_pswd, PASSWORD_DIG_COUNT))
	{
		show_wrong_pswd();
		return;//break;
	}	
	while(1)
	{	
		WDTCLR
//		previous_menu_selection = menu_selection;
		menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;	
		if(select_option1(&default_settings_mode))
			break;
		if(default_settings_mode.curr_list_sel == default_settings_mode.total_size-1)
			break;
		else
			(*exec_default_settings_func_ptr[default_settings_mode.curr_list_sel])();
			
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void operating_parameters_menu_options (void)
//{
//	
//	ubyte rec_option = 0;
//	ubyte rec_option1 = 0;
//	ubyte temp_pswd[4] = {0};
//	
//	struct menu_struct operating_parameters_menu;
//	operating_parameters_menu.rec_heading_add = disp_view_operating_parameters_menu;
//	operating_parameters_menu.rec_options_add = disp_operating_parameters_menu_options;
//	operating_parameters_menu.total_size = 2;
//	operating_parameters_menu.window_st_num = 0;
//	operating_parameters_menu.window_size = 8;
//	operating_parameters_menu.font_size = 13;
//	operating_parameters_menu.font_index_ptr = arialNarrow_bld_13pix;
//	operating_parameters_menu.hlight_line_num = 0;
//	operating_parameters_menu.curr_list_sel = 0;
//	
//	key_press = 0;	
//	home_clr();
//	Delays_Xtal_ms(1000);	
//	while(1)
//	{	
//		WDTCLR
//		if(select_option1(&operating_parameters_menu))
//			break;
//		if(operating_parameters_menu.curr_list_sel == operating_parameters_menu.total_size-1)
//			break;
//		else
//			(*exec_operating_parameters_func_ptr[operating_parameters_menu.curr_list_sel])();
//	}
//}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void test_menu_options(void)
{
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	struct menu_struct test_menu;
//	test_menu.rec_heading_add = disp_view_test_menu;//disp_test_mode
	test_menu.rec_heading_add = disp_test_mode;
	test_menu.rec_options_add = disp_test_menu_options;
	test_menu.total_size = LEN_TEST_MENU;
	test_menu.window_st_num = 0;
	test_menu.window_size = 8;
	test_menu.font_size = 13;
	test_menu.font_index_ptr = arialNarrow_bld_13pix;
	test_menu.hlight_line_num = 0;
	test_menu.curr_list_sel = 0;
	
	key_press = 0;	
	home_clr();
//	Delays_Xtal_ms(1000);	
	while(1)
	{
		WDTCLR
		menu_selection = TEST_MENU;
		if(select_option1(&test_menu))
			break;
		if(test_menu.curr_list_sel == test_menu.total_size-1)
			break;
		else
			(*exec_test_menu_func_ptr[test_menu.curr_list_sel])();
		
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void test_normal_menu_options(void)
{
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	struct menu_struct normal_test_menu;
	//normal_test_menu.rec_heading_add = disp_view_normal_test_menu;//lakh - disp_normal_test_menu
	normal_test_menu.rec_heading_add = disp_normal_test_menu;
	normal_test_menu.rec_options_add = disp_test_normal_menu_options;
	normal_test_menu.total_size = LEN_NORMAL_TEST_MENU;
	normal_test_menu.window_st_num = 0;
	normal_test_menu.window_size = 8;
	normal_test_menu.font_size = 13;
	normal_test_menu.font_index_ptr = arialNarrow_bld_13pix;
	normal_test_menu.hlight_line_num = 0;
	normal_test_menu.curr_list_sel = 0;

	test1.test_int = 0;
	test2.test_int = 0;
	
	key_press = 0;	
	clear_keys_status();
	home_clr();
previous_menu_selection =  menu_selection;
	menu_selection = NORMAL_TEST_MENU;
	while(1)
	{
		WDTCLR
		if(select_option1(&normal_test_menu) )
		{
			break;
		}	
		if((normal_test_menu.curr_list_sel == normal_test_menu.total_size-1))// || (key_press == 'r'))
		{
			test1.test_int = 0;
			test2.test_int = 0;
			test2.bits.t25 = 1;
//			lhb_comm_fn_code = FC_TEST_MODE;
			lhb_comm_fn_code = FC_DEFAULT;
			new_pkt_rdy_to_be_sent = 1;
			in_test_mode = 1;
			while(in_test_mode)
			check_fn_code_send_pkt_to_lhb();
			home_clr();	

			break;
		}
		else
			send_normal_test_cmd(normal_test_menu.curr_list_sel);
	
	}
	

}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void test_exlusive_menu_options(void)
{
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	struct menu_struct exc_test_menu;
//	exc_test_menu.rec_heading_add = disp_view_exlus_test_menu;//lakh
	exc_test_menu.rec_heading_add = disp_exlus_test_menu;
	exc_test_menu.rec_options_add = disp_test_exlusive_menu_options;
	exc_test_menu.total_size = LEN_EXCL_TEST_MENU;
	exc_test_menu.window_st_num = 0;
	exc_test_menu.window_size = 8;
	exc_test_menu.font_size = 13;
	exc_test_menu.font_index_ptr = arialNarrow_bld_13pix;
	exc_test_menu.hlight_line_num = 0;
	exc_test_menu.curr_list_sel = 0;
	test1.test_int = 0;
	test2.test_int = 0;
	
	key_press = 0;	
	home_clr();
//	Delays_Xtal_ms(1000);	
	menu_selection = EXCL_TEST_MENU;
	while(1)
	{
		WDTCLR
		if(select_option1(&exc_test_menu))
			break;
		if((exc_test_menu.curr_list_sel == exc_test_menu.total_size-1))//|| (key_press == 'r'))
		{
//			Delays_Xtal_ms(100);
			test1.test_int = 0;
			test2.test_int = 0;
			test2.bits.t25 = 1;
			//lhb_comm.flags.bits.rx_enable=1;
			lhb_comm_fn_code = FC_DEFAULT;
//			lhb_comm_fn_code = FC_TEST_MODE;
			new_pkt_rdy_to_be_sent = 1;
			in_test_mode = 1;
			while(in_test_mode)
			check_fn_code_send_pkt_to_lhb();
			home_clr();
			break;
		}
		else
		{
			send_exclu_test_cmd(exc_test_menu.curr_list_sel);
		}
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void fault_bypass_mode(void)
{
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	struct menu_struct exc_test_menu;
	exc_test_menu.rec_heading_add = disp_select_faults_to_bypass;
	exc_test_menu.rec_options_add = disp_select_faults_to_bypass_options;
	exc_test_menu.total_size = LEN_FAULT_BYPASS;
	exc_test_menu.window_st_num = 0;
	exc_test_menu.window_size = 8;
	exc_test_menu.font_size = 13;
	exc_test_menu.font_index_ptr = arialNarrow_bld_13pix;
	exc_test_menu.hlight_line_num = 0;
	exc_test_menu.curr_list_sel = 0;
	
	key_press = 0;	
	home_clr();
////////////////////////////////////////////////////lakh
         home_clr();
	puts_graphical_lcd_rom_lim((urombyte *)"  Fault Bypass Mode Disabled.......  ",0,13*5,0,0,arialNarrow_bld_13pix);
	ClearWDT();
	Delays_Xtal_ms(1000);
//	ClearWDT();
//	Delays_Xtal_ms(1000);
	lhb_comm_fn_code = 0;
	Nop();	
	return;	
///////////////////////////////////////////////////	
	
	
	
	
//	Delays_Xtal_ms(1000);	
previous_menu_selection =  menu_selection;
	menu_selection = BYPASS_FAULTS;
	while(1)
	{
		WDTCLR
		if(select_option1(&exc_test_menu))
			break;
		if(exc_test_menu.curr_list_sel == exc_test_menu.total_size-1)
		{
//			//lhb_comm.flags.bits.rx_enable=1;
//			lhb_comm_fn_code = FC_FAULT_OVERRIDE;
//			new_pkt_rdy_to_be_sent = 1;
//			check_fn_code_send_pkt_to_lhb();
//			home_clr();

			break;
		}
		else
		{
			send_override_cmd(exc_test_menu.curr_list_sel);
		}
	}
	
	
	
}	
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void factory_menu_options(void)
{

	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	str_copy_rom_cnt( (urombyte *)"8675",fact_pswd,4);
	
	struct menu_struct factory_menu;
	factory_menu.rec_heading_add = disp_factory_menu;//disp_view_factory_menu;lakh
	factory_menu.rec_options_add = disp_factory_menu_options;
	factory_menu.total_size = LEN_FACTORY_MENU;
	factory_menu.window_st_num = 0;
	factory_menu.window_size = 7;
	factory_menu.font_size = 13;
	factory_menu.font_index_ptr = arialNarrow_bld_13pix;
	factory_menu.hlight_line_num = 0;
	factory_menu.curr_list_sel = 0;
	
	key_press = 0;	
	home_clr();
//	Delays_Xtal_ms(1000);	
previous_menu_selection =  menu_selection;
	menu_selection = FACTORY_MENU;	
	
	if(!get_string( temp_pswd, disp_enter_pswd, PASSWORD_DIG_COUNT, NUMERIC))
		return;//break;
	if(!str_comp_ram(fact_pswd, temp_pswd, PASSWORD_DIG_COUNT))
	{
		show_wrong_pswd();
		return;//break;
	}	
	while(1)
	{
		WDTCLR
		if(select_option1(&factory_menu))
			break;
		if(factory_menu.curr_list_sel == factory_menu.total_size-1)
			break;
		else
			(*exec_factory_menu_func_ptr[factory_menu.curr_list_sel])();
	}	
	
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void usb_menu_options(void)
{	
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	struct menu_struct usb_menu;
	usb_menu.rec_heading_add = disp_usb_menu;
	usb_menu.rec_options_add = disp_usb_menu_options;
	usb_menu.total_size = LEN_USB_MENU;
	usb_menu.window_st_num = 0;
	usb_menu.window_size = 8;
	usb_menu.font_size = 13;
	usb_menu.font_index_ptr = arialNarrow_bld_13pix;
	usb_menu.hlight_line_num = 0;
	usb_menu.curr_list_sel = 0;
	
	key_press = 0;	
	home_clr();
//	Delays_Xtal_ms(1000);
	previous_menu_selection = menu_selection;
	menu_selection = USB_MENU;	
	while(1)
	{			
		if(select_option1(&usb_menu))
	       	break;
		if(usb_menu.curr_list_sel == usb_menu.total_size-1)
	          	break;
		else
			(*exec_usb_menu_func_ptr[usb_menu.curr_list_sel])();		
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void supervisor_menu_options(void)
{	
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	str_copy_rom_cnt( (urombyte *)"9876",fact_pswd,4);
	
	struct menu_struct sup_menu;
	sup_menu.rec_heading_add = disp_supervisor_menu;//lakh
	sup_menu.rec_options_add = disp_sup_menu_options;
	sup_menu.total_size = LEN_SUPERVISOR_MENU;
	sup_menu.window_st_num = 0;
	sup_menu.window_size = 8;
	sup_menu.font_size = 13;
	sup_menu.font_index_ptr = arialNarrow_bld_13pix;
	sup_menu.hlight_line_num = 0;
	sup_menu.curr_list_sel = 0;
	
	key_press = 0;	
	home_clr();
//??	Delays_Xtal_ms(1000);	
	menu_selection = SUPERVISOR_MENU;
	
	if(!get_string( temp_pswd, disp_enter_pswd, PASSWORD_DIG_COUNT, NUMERIC))
		return;//break;
	if(!str_comp_ram(fact_pswd, temp_pswd, PASSWORD_DIG_COUNT))
	{
		show_wrong_pswd();
		return;//break;
	}	
	while(1)
	{
		WDTCLR
		if(select_option1(&sup_menu))
			break;
		if(sup_menu.curr_list_sel == sup_menu.total_size - 1)
			break;
			
			else
			(*exec_sup_menu_func_ptr[sup_menu.curr_list_sel])();
				
//		else
//		{
//			if(sup_menu.curr_list_sel == 0)	
//				setpt_source_select_options();		
//			if(sup_menu.curr_list_sel == 1)		
//				Nop();
//				//item_selection = SELECT_DOWNLOAD_EVENTS_TO_USB;
//			if(sup_menu.curr_list_sel == 2)		
//				Nop();
//				//item_selection = SELECT_UPGRADE_FIRMWARE_THROUGH_BOOTLOADER;
//			if(sup_menu.curr_list_sel == 3)	
//				Nop();
//		}				
//		//lhb_comm.flags.bits.rx_enable=1;
//		check_fn_code_send_pkt_to_lhb();
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void setpt_source_select_options(void)
{	
	ubyte rec_option = 0;
	ubyte rec_option1 = 0;
	ubyte temp_pswd[4] = {0};
	
	struct menu_struct setpt_src_sel_menu;
	setpt_src_sel_menu.rec_heading_add = disp_set_point_sel_source_menu;
	setpt_src_sel_menu.rec_options_add = disp_setpt_src_sel_menu_options;
	setpt_src_sel_menu.total_size = LEN_SP_SOURCE_MENU;
	setpt_src_sel_menu.window_st_num = 0;
	setpt_src_sel_menu.window_size = 8;
	setpt_src_sel_menu.font_size = 13;
	setpt_src_sel_menu.font_index_ptr = arialNarrow_bld_13pix;
	setpt_src_sel_menu.hlight_line_num = 0;
	setpt_src_sel_menu.curr_list_sel = 0;
	
	key_press = 0;	
	home_clr();
//??	Delays_Xtal_ms(1000);	
previous_menu_selection =  menu_selection;
	menu_selection = SETPT_SOURCE_SELECT_MENU;
	
	while(1)
	{
		WDTCLR
		if(select_option1(&setpt_src_sel_menu))
			break;
		if(setpt_src_sel_menu.curr_list_sel == setpt_src_sel_menu.total_size - 1)
			break;
		else
		{
			if(setpt_src_sel_menu.curr_list_sel == 0)
				Nop();
			if(setpt_src_sel_menu.curr_list_sel == 1)	
				Nop();
		}				
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
	}
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_string
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:


//#######################################################################################
unsigned get_string(ubyte *rec_dest_add, urombyte *disp_start_add, ubyte rec_str_len, ubyte string_type)
{
	ubyte *src_ptr;
	while(1)
	{
		WDTCLR
		home_clr();
		puts_lcd_rom_lim(disp_start_add, 0x00);
		scan_string(string_type, rec_str_len);
		if(orphan_flags.bits.default_time_out)
		{
			show_time_out();
			return(0);
		}	
		else if(key_press == 'c' || key_press == 'r')
			return(0);

		src_ptr = scanned_strg;
		while(*src_ptr != LINE_FEED)
		{
		
			if(!_IF_NUM(*src_ptr))
			{
				if(string_type == NUMERIC)
					break;
				if(!_IF_ALPHA(*src_ptr))
					if(!_IF_SPACE(*src_ptr))
						break;
			}
			src_ptr++;
		}
		if(*src_ptr == LINE_FEED)
		{
			str_copy_ram_lim(scanned_strg, rec_dest_add, LINE_FEED);
			return(1);
		}
		show_invalid_entry();
	}
}	
//#######################################################################################
//							End of menu.c
//#######################################################################################	

	



