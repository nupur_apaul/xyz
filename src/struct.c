#include "..\inc\headers.h"			//all prototyype declaration.

union complete_plant_union complete_slave_plant[MAX_NO_OF_SLAVE_LHB];
//struct menu_struct menu_mode;
//struct menu_struct default_settings_mode;
struct struct_key  sw[TOTAL_SW]						__attribute__ ((aligned (2))) = {0};
struct struct_key  last_sw[TOTAL_SW] 				__attribute__ ((aligned (2))) = {0};
struct struct_timer2 timer2 						__attribute__ ((aligned (2))) = {0};
union union_temp_struct temp_struct 				__attribute__ ((aligned (2))) = {0};
union union_system_status system_status 			__attribute__ ((aligned (2))) = {0};

struct server_status_struct server_s;
union dutycycle_union DutyCycleLog;
union dutycycle_union EquipmentLogs;
union concat_union concat							__attribute__ ((aligned (2))) = {0};
union LCD_union LCD  								__attribute__ ((aligned (2))) = {0};
union section_union section 						__attribute__ ((aligned (2))) = {0};
struct sensor_struct sensor[10] 					__attribute__ ((aligned (2))) = {0};
union fault_union fault 							__attribute__ ((aligned (2))) = {0};
union event_union event 							__attribute__ ((aligned (2))) = {0};
//struct unit_id_struct unit_id 						__attribute__ ((aligned (2))) = {0};
struct coach_info_struct coach_info  				__attribute__ ((aligned (2))) = {0};
union rxtx_frame_union rx 							__attribute__ ((aligned (2))) = {0};
union rxtx_frame_union tx 							__attribute__ ((aligned (2))) = {0};
struct comm_struct lhb_comm  						__attribute__ ((aligned (2))) = {0};
struct comm_struct prev_lhb_comm  					__attribute__ ((aligned (2))) = {0};


union set_point_union set 							__attribute__ ((aligned (2))) = {0};
union orphan_flags_union orphan_flags  				__attribute__ ((aligned (2))) = {0};
//struct fault_log_struct log   __attribute__ ((aligned (2))) = {0};
unsigned short dutycycle_screen_putdata_x[30];
unsigned short default_screen_putdata_x[10] 		__attribute__ ((aligned (2))) = {0};
unsigned short double_rmpu_putdata_x[10] 		__attribute__ ((aligned (2))) = {0};
unsigned short plantstatus_screen_putdata_x[45] 	__attribute__ ((aligned (2))) = {0};
unsigned short sevenpt_select_screen_putdata_x[30];
unsigned short fault_screen_putdata_x[10] 			__attribute__ ((aligned (2))) = {0};
unsigned short pressurestatus_screen_putdata_x[10]	__attribute__ ((aligned (2))) = {0};

unsigned short menu_options_putdata_x[25] 			__attribute__ ((aligned (2))) = {0};

unsigned short sevenpt_change_screen_readdata_x[10] __attribute__ ((aligned (2))) = {0};
unsigned short coachno_change_screen_readdata_x[3] 	__attribute__ ((aligned (2))) = {0};
unsigned short rmpuno_change_screen_readdata_x[5] 	__attribute__ ((aligned (2))) = {0};
unsigned short rmpumake_change_screen_readdata_x[5] __attribute__ ((aligned (2))) = {0};
unsigned short datetime_change_screen_readdata_x[5] __attribute__ ((aligned (2))) = {0};
unsigned short sleep_hrs_screen_readdata_x[4]		__attribute__ ((aligned (2))) = {0};


//unsigned short rtc_change_screen_readdata_x[2] __attribute__ ((aligned (2))) = {0};
union orphan_flags2_union orphan_flags2 			__attribute__ ((aligned (2))) = {0};
union orphan_flags1_union orphan_flags1 			__attribute__ ((aligned (2))) = {0};

enum MENUS_ENUM MENUS;
enum ITEMS_ENUM ITEMS;

union menu_bits_union stat;
//#######################################################################################
//				structures for keypad module
//#######################################################################################
union key_num_union key_num 						__attribute__ ((aligned (2))) = {0};
union key_alpha_union key_alpha 					__attribute__ ((aligned (2))) = {0};
struct key_struct keys[7],last_keys[7];
struct key_name_struct key_name						__attribute__ ((aligned (2))) = {0};
union frame_flags_union frame_flags					__attribute__ ((aligned (2))) = {0};	

union test1_union test1								__attribute__ ((aligned (2))) = {0};
union test2_union test2								__attribute__ ((aligned (2))) = {0};

struct network_struct network						__attribute__ ((aligned (2))) = {0};
unsigned char fact_pswd[4]							__attribute__ ((aligned (2))) = {0};
struct lcd_fault_log_struct fault_log[FAULTS_ON_LCD]			__attribute__ ((aligned (2))) = {0};

union trip_status_union trip;

union time_union sleep_hrs_start_bytes;
union time_union sleep_hrs_end_bytes;

union complete_plant_union	complete_plant_tx,complete_plant_rx;
union override_stat_union	override;
struct server_struct server, temp_server;

//struct set_value_struct set_temp[7];// __attribute__ ((aligned (8)));
//struct lcd_dutycycle_log_struct dutycycle_log[FAULTS_ON_LCD]			__attribute__ ((aligned (2))) = {0};
 union dutylogunion DutyLog, lcd_dutylog;
 tF_DATA PowerReadings[MAX_POWER_CREADINGS];
// struct dutylogstruct D_log[FAULTS_ON_LCD];