#include "..\inc\headers.h"

#define  uchar unsigned char
#define  uint unsigned short
#define  xchar unsigned char code

#define A0  rs
#define WRR lcd_wr
#define RDD lcd_rd
#define CS  lcd_en
#define RES lcd_reset

unsigned short line_loc=0,char_loc,lcd_loc=0;

unsigned char circular_buff[100];
unsigned char display_buff[30],width_array[80];
unsigned char temp_var;

unsigned char lcd_cmd,lcd_dat,graph_lcd_data,str_len;
unsigned short i,j,k,status;
unsigned short test_count=0;
unsigned char str_len_counter,str_data_counter;
unsigned char temp,scroll_msg_end;
unsigned char col_value,inter_col_value;
unsigned short pix=0;

const unsigned char *CharBitmaps_ptr;
const unsigned short *CharDescriptors_ptr;
unsigned char char_count,CHAR_HEIGHT_IN_BITS,CHAR_WIDTH_IN_BITS,byte_index_ptr,inter_byte_index_ptr,x,y,pkd_str_len,width_array_ptr;


#ifdef LCD_320x240
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void initGraphicsLCD()
{
	lcd_reset=0;
	Delays_Xtal_ms(4);
	lcd_reset=1;
	Delays_Xtal_ms(9);
	
	
	update_lcd(0xFF);
	CS=1;A0=1;RDD=0;WRR=1;
	Delays_Xtal_ms(4);
	update_lcd(0x00);
	CS=0;A0=0;RDD=0;WRR=1;

	cmd_wr(0x40);	//system set
  	data_wr(0x30);	//P1
	data_wr(0x87);	//P2
	data_wr(0x07);	//P3
	data_wr(0x27);	//P4
  	data_wr(66);	//P5
  	data_wr(240);	//P6
	data_wr(40);	//P7
  	data_wr(0);		//P8
  	Delays_Xtal_ms(0x9);
  	cmd_wr(0x44);	//set display start address
  	data_wr(0x00);	//P1
  	data_wr(0x00);	//P2
  	data_wr(240);	//P3
  	data_wr(0x80);	//P4
  	data_wr(0x25);	//P5
	data_wr(240);	//P6
	data_wr(0x00);	//P7
	data_wr(0x4b);	//P8
	data_wr(0x00);	//P9
	data_wr(0x00);	//P10
	Delays_Xtal_ms(0x9);
	cmd_wr(0x5a);		//set horizontal scroll position
	data_wr(0x00);
	Delays_Xtal_ms(0x9);
	cmd_wr(0x5b);		//set display overlay format
	data_wr(0x1c);
	Delays_Xtal_ms(0x9);
	cmd_wr(0x59);	//enable display
	data_wr(0x04);
	Delays_Xtal_ms(0x9);
	cmd_wr(0x4c);	//set direction of cursor movement
	Delays_Xtal_ms(0x9);
	cmd_wr(0x46);	//set cursor address
	data_wr(0);
	data_wr(0);
	Delays_Xtal_ms(0x9);
	cmd_wr(0x42);	//write to display memory
	for(pix=0;pix< 32760;pix++)
		data_wr(0x00);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void update_lcd(unsigned char temp_byte)
{
	LCD.byte = temp_byte;	
	lcd_d0 = LCD.data.d0;
	lcd_d1 = LCD.data.d1;
	lcd_d2 = LCD.data.d2;
	lcd_d3 = LCD.data.d3;
	lcd_d4 = LCD.data.d4;
	lcd_d5 = LCD.data.d5;
	lcd_d6 = LCD.data.d6;
	lcd_d7 = LCD.data.d7;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void read_lcd(void)
//{
//	LCD.data.d0=PORTFbits.RF1;
//	LCD.data.d1=PORTFbits.RF0;
//	LCD.data.d2=PORTDbits.RD7;
//	LCD.data.d3=PORTDbits.RD6;
//	LCD.data.d4=PORTDbits.RD5;
//	LCD.data.d5=PORTDbits.RD4;
//	LCD.data.d6=PORTDbits.RD13;
//	LCD.data.d7=PORTDbits.RD12;
//	status=LCD.byte ;	
//}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void data_wr(unsigned char data)
{
  busy();
  CS=0;
  Nop();Nop();Nop();
  A0=0;
  Nop();Nop();Nop();
  RDD=1;
  update_lcd(data);
  WRR=0;
  Nop();Nop();Nop();
  WRR=1;
  Nop();Nop();Nop();  
  CS=1;
}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void cmd_rd(void)
//{
//	rs=1;
//	lcd_rd=0;
//	lcd_wr=1;
//	enable();
//}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void cmd_wr(unsigned char command)
{
	busy();
  	CS=0;
  	Nop();Nop();Nop();  	
	A0=1;
  	Nop();Nop();Nop();	
	RDD=1;
  	update_lcd(command);
	WRR=0;
  	Nop();Nop();Nop();	
	WRR=1;
  	Nop();Nop();Nop();	
	CS=1; 
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void busy()
{
	Nop();Nop();Nop();Nop();Nop();
//  CS=0;A0=0 ;
//  do
//  { 
//  data_pin_input();
//  RDD=0;
//  Delays_Xtal_us(20);
//  read_lcd();
//  RDD=1;
//  }
//  while(LCD.data.d7);
//  data_pin_output();
}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void wcode(uint csrl,uint csrh)
{        
  uint temdat1=0,temdat2=0;
  temdat1=csrl+40*csrh;
  temdat2=temdat1/256;
  temdat1=temdat1%256;
  cmd_wr(0x46);		//set cursor address
  data_wr(temdat1);
  data_wr(temdat2);
  cmd_wr(0x42);		//write to display
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void stat_rd(void)
//{
//	rs=1;
//	lcd_rd=0;
//	lcd_wr=1;
//	enable();
//	
//}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void enable(void)
//{
//	lcd_en=0;
//	Nop();
//	Nop();
//	Nop();
//	lcd_en=1;
//	Nop();
//	Nop();
//	Nop();	
//}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void data_pin_input(void)
//{
//	lcd_d0_tris=1;
//	lcd_d1_tris=1;
//	lcd_d2_tris=1;
//	lcd_d3_tris=1;
//	lcd_d4_tris=1;
//	lcd_d5_tris=1;
//	lcd_d6_tris=1;
//	lcd_d7_tris=1;
//}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void data_pin_output(void)
//{
//	lcd_d0_tris=0;
//	lcd_d1_tris=0;
//	lcd_d2_tris=0;
//	lcd_d3_tris=0;
//	lcd_d4_tris=0;
//	lcd_d5_tris=0;
//	lcd_d6_tris=0;
//	lcd_d7_tris=0;
//}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned long shift_coll(unsigned short first_byte, unsigned short sec_byte, unsigned char first_char_width, unsigned char sec_char_width)
{
	union
	{
		unsigned short word;
		unsigned char integer[2];
	}concat;
	
	concat.integer[0] = sec_byte;
	concat.integer[1] = first_byte;
	
	concat.integer[1] = concat.integer[1] >> (8 - (first_char_width));
	concat.word = concat.word << (8 - (first_char_width));
	
	return(concat.word);
}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void bit_shift( unsigned char col_val,unsigned char char_val)
{
	concat.word = shift_coll(section.byte[col_val][char_val], section.byte[col_val][char_val + 1], width_array[char_val], width_array[char_val + 1]);
	section.byte[col_val][char_val] = concat.integer[1];
	section.byte[col_val][char_val + 1] = concat.integer[0];
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_rom_str_to_data_buff(urombyte *str_data,unsigned char *str_buffer,unsigned char string_length,const unsigned char *CharBitmaps_ptr,const unsigned short *CharDescriptors_ptr)
{
	unsigned char *dup_str_buffer,dup_width_array[60];
	dup_str_buffer=str_buffer;
	width_array_ptr=0;
	
	CHAR_HEIGHT_IN_BITS=*(CharDescriptors_ptr+1);

	for(str_len_counter=0;str_len_counter<string_length;str_len_counter++)
	{
		char_loc=*(str_data+str_len_counter)- OFFSET;		 //char_loc is location of the char in the look up of width & no. of chars	
		dup_width_array[str_len_counter]=(*(CharDescriptors_ptr+(char_loc*3))+ SPACE_BW_CHARACTERS);
		
		 if((dup_width_array[str_len_counter]-1)>=48)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+5))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+5);
					if((dup_width_array[str_len_counter]-1)!=48)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+6))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+6);
					j=j+7;
					}
					else
					j=j+6;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=48)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%48)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+7;
				}
				else
				str_buffer=str_buffer+6;
							
				width_array_ptr++;
					
			}
				else if((dup_width_array[str_len_counter]-1)>=40)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					if((dup_width_array[str_len_counter]-1)!=40)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+5))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+5);
					j=j+6;
					}
					else
					j=j+5;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=40)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%40)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+6;
				}
				else
				str_buffer=str_buffer+5;
				width_array_ptr++;
					
			}
			else if((dup_width_array[str_len_counter]-1)>=32)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					if((dup_width_array[str_len_counter]-1)!=32)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					j=j+5;
					}
					else
					j=j+4;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=32)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%32)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+5;	
				}
				else
				str_buffer=str_buffer+4;	
				width_array_ptr++;
				
			}
				else if((dup_width_array[str_len_counter]-1)>=24)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					if((dup_width_array[str_len_counter]-1)!=24)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					j=j+4;
					}
					else
					j=j+3;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=24)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%24)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+4;	
				}
				else
				str_buffer=str_buffer+3;	
				width_array_ptr++;
			
			}
				
			else if((dup_width_array[str_len_counter]-1)>=16)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					if((dup_width_array[str_len_counter]-1)!=16)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					j=j+3;
					}
					else
					j=j+2;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=16)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%16)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+3;	
				}
				else
				str_buffer=str_buffer+2;
			
				width_array_ptr++;
				
			}
			else 	if((dup_width_array[str_len_counter]-1)>=8)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					if((dup_width_array[str_len_counter]-1)!=8)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					j=j+2;
					}
					else
					j=j+1;
				}
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=8)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%8)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+2;
				}
				else
				str_buffer=str_buffer+1;
				width_array_ptr++;
				
			}	
			else
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					j=j+1;
				}
					
				width_array[width_array_ptr]=(dup_width_array[str_len_counter]);
				width_array_ptr++;
					
				str_buffer=str_buffer+1;
			}	
	}
//	pkd_str_len = string_length;	//for debugging
	str_packing(str_data,dup_str_buffer,CharBitmaps_ptr ,CharDescriptors_ptr,string_length);
}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_ram_str_to_data_buff(ubyte *str_data,unsigned char *str_buffer,unsigned char string_length,const unsigned char *CharBitmaps_ptr,const unsigned short *CharDescriptors_ptr)
{
	unsigned char *dup_str_buffer,dup_width_array[60];
	dup_str_buffer=str_buffer;
	width_array_ptr=0;
	
	CHAR_HEIGHT_IN_BITS=*(CharDescriptors_ptr+1);

	for(str_len_counter=0;str_len_counter<string_length;str_len_counter++)
	{
		char_loc=*(str_data+str_len_counter)- OFFSET;		 //char_loc is location of the char in the look up of width & no. of chars	
		dup_width_array[str_len_counter]=(*(CharDescriptors_ptr+(char_loc*3))+ SPACE_BW_CHARACTERS);
		
		 if((dup_width_array[str_len_counter]-1)>=48)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+5))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+5);
					if((dup_width_array[str_len_counter]-1)!=48)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+6))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+6);
					j=j+7;
					}
					else
					j=j+6;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=48)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%48)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+7;
				}
				else
				str_buffer=str_buffer+6;
							
				width_array_ptr++;
					
			}
				else if((dup_width_array[str_len_counter]-1)>=40)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					if((dup_width_array[str_len_counter]-1)!=40)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+5))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+5);
					j=j+6;
					}
					else
					j=j+5;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=40)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%40)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+6;
				}
				else
				str_buffer=str_buffer+5;
				width_array_ptr++;
					
			}
			else if((dup_width_array[str_len_counter]-1)>=32)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					if((dup_width_array[str_len_counter]-1)!=32)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					j=j+5;
					}
					else
					j=j+4;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=32)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%32)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+5;	
				}
				else
				str_buffer=str_buffer+4;	
				width_array_ptr++;
				
			}
				else if((dup_width_array[str_len_counter]-1)>=24)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					if((dup_width_array[str_len_counter]-1)!=24)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					j=j+4;
					}
					else
					j=j+3;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=24)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%24)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+4;	
				}
				else
				str_buffer=str_buffer+3;	
				width_array_ptr++;
			
			}
				
			else if((dup_width_array[str_len_counter]-1)>=16)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					if((dup_width_array[str_len_counter]-1)!=16)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					j=j+3;
					}
					else
					j=j+2;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=16)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%16)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+3;	
				}
				else
				str_buffer=str_buffer+2;
			
				width_array_ptr++;
				
			}
			else 	if((dup_width_array[str_len_counter]-1)>=8)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					if((dup_width_array[str_len_counter]-1)!=8)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					j=j+2;
					}
					else
					j=j+1;
				}
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=8)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%8)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+2;
				}
				else
				str_buffer=str_buffer+1;
				width_array_ptr++;
				
			}	
			else
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					j=j+1;
				}
					
				width_array[width_array_ptr]=(dup_width_array[str_len_counter]);
				width_array_ptr++;
					
				str_buffer=str_buffer+1;
			}	
	}
//	pkd_str_len = string_length;	//for debugging
	str_packing(str_data,dup_str_buffer,CharBitmaps_ptr ,CharDescriptors_ptr,string_length);
}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void str_packing(urombyte *str_data,unsigned char *string_buffer,const unsigned char *CharBitmaps_ptr,const unsigned short *CharDescriptors_ptr,unsigned char string_length)
{
	pkd_str_len=0;
	byte_index_ptr=0;
	char_count=width_array_ptr;		

	while(char_count!=0)
	{
//		wcode(0,0);
//		for(i=0;i<40;i++)
//			for(j=0;j<40;j++)
//				data_wr( section.byte[i][j]);
//		Nop();
		
		
		if(width_array[byte_index_ptr+1]!=0)						// if next_byte !=0 then proceed to next statement
		{
			if(width_array[byte_index_ptr]<8)						// if current byte < 8 then bit shifting required
			{
				//%%%%% bit shifting
				for(col_value = 0; col_value <CHAR_HEIGHT_IN_BITS; col_value++)
					bit_shift(col_value,byte_index_ptr);
				//%%%%% adjusting the  width of all bytes	
				if((width_array[byte_index_ptr] + width_array[byte_index_ptr+1])/8)
				{
					width_array[byte_index_ptr+1]=(width_array[byte_index_ptr]+width_array[byte_index_ptr+1])%8;
					width_array[byte_index_ptr] = 8;
				
				}
				else
				{
					width_array[byte_index_ptr]=(width_array[byte_index_ptr]+width_array[byte_index_ptr+1])%8;
					width_array[byte_index_ptr+1] = 0;
				}
			}
			else
			{
				pkd_str_len++;
				byte_index_ptr++;
				char_count--;
			}	
		}
		else 							// if next_byte==0 then remove the current byte and shift all bytes left in the array
		{
				
			 if(byte_index_ptr< (MAX_CHARS_IN_A_LINE-1))
			 {
				for(inter_byte_index_ptr=byte_index_ptr;inter_byte_index_ptr<byte_index_ptr+char_count-2;inter_byte_index_ptr++)
				{
					width_array[inter_byte_index_ptr+1]=width_array[inter_byte_index_ptr+2];
					for(inter_col_value=0;inter_col_value<CHAR_HEIGHT_IN_BITS;inter_col_value++)
						section.byte[inter_col_value][inter_byte_index_ptr+1]=section.byte[inter_col_value][inter_byte_index_ptr+2];
				}
				for(inter_col_value=0;inter_col_value<CHAR_HEIGHT_IN_BITS;inter_col_value++)
					section.byte[inter_col_value][inter_byte_index_ptr+1]=0x00;
				
				width_array[inter_byte_index_ptr+1]=0;
			}
				char_count--;	
		}	
	}

}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void lcd_byte(ubyte rec_byte, unsigned rec_type)        
{
//	RS = rec_type;
//	lcd_data.bytes = ((rec_byte >> 4) & 0x0F);
//	lcd_port_update();
//	
//	EN=1;	
//	Nop();Nop();Nop();Nop();Nop();
//	EN=0;
//	
//	lcd_data.bytes = rec_byte & 0x0F;
//	lcd_port_update();	
//	
//	EN=1;
//	Nop();Nop();Nop();Nop();Nop();
//	EN=0;	
//	Delays_Xtal_ms(2);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void puts_lcd_srom_lim(srombyte *rec_ram_add , ubyte rec_delim)
//{
//#ifndef MENU_ON_LED
//  	while ((ubyte)*rec_ram_add != rec_delim)
//		lcd_byte((ubyte)*rec_ram_add++, DATA);
//#else
//	str_copy_rom_lim(rec_ram_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
//	transmit_lcd_to_led();
//#endif
//}
//#######################################################################################

//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void home_clr(void)
{
#ifndef MENU_ON_LED
	wcode(0,HEADER_RES_SPACE);
//	str_fill( lcd.graph_buff,9600,0x00);
	k=0;
	while(k++ != (9600-(HEADER_RES_SPACE * NO_OF_BYTES_PER_LINE)))
	data_wr( 0);
	k=0;
#else
//	lcd_line_1();
	led_disp_buff[0][0] = ' ';
	led_disp_buff[0][1] = 0x00;
	led_disp_buff[1][0] = ' ';
	led_disp_buff[1][1] = 0x00;
	transmit_lcd_to_led();
#endif
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void lcd_clr_line_1(unsigned char rec_col_len)
{
//#ifndef MENU_ON_LED
//	lcd_line_1();
//	while(rec_col_len--)
//		lcd_byte(' ', DATA);
//	lcd_line_1();
//#else
//	led_disp_buff[0][0] = ' ';
//	led_disp_buff[0][1] = 0x00;
//	transmit_lcd_to_led();
//#endif
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void lcd_clr_line_2(unsigned char rec_col_len)
{
//#ifndef MENU_ON_LED
//	lcd_line_2();
//	while(rec_col_len--)
//		lcd_byte(' ', DATA);
//	lcd_line_2();
//#else
//	led_disp_buff[1][0] = ' ';
//	led_disp_buff[1][1] = 0x00;
//	transmit_lcd_to_led();
//#endif
}

//#######################################################################################

//GRAPHICAL ROUTINES
//#######################################################################################

//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_ram(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt, ubyte rec_delim,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED
		ubyte temp_str_len = get_str_len(rec_ram_add, rec_delim);
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_ram_str_to_data_buff(rec_ram_add,&section.byte[0][0],(rec_cnt<=temp_str_len)?rec_cnt:temp_str_len,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
		
		(byte_index_ptr > 39) ? byte_index_ptr = 39 : byte_index_ptr;
		(39 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (39 - x);
//		wcode(x,y);
		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
	//		for(j=0;j<40;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
				highlight ? data_wr( 0XFF-section.byte[temp_i][temp_j]) : data_wr( section.byte[temp_i][temp_j]);
			}	
				
		last_x_pos = x + (byte_index_ptr + 1);
#else
//	str_copy_ram_lim(rec_ram_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_rom(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt, ubyte rec_delim,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED
		ubyte temp_str_len = get_str_len((ubyte *)rec_ram_add, rec_delim);
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_rom_str_to_data_buff(rec_ram_add,&section.byte[0][0],(rec_cnt<=temp_str_len)?rec_cnt:temp_str_len,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
		
		(byte_index_ptr > 39) ? byte_index_ptr = 39 : byte_index_ptr;
		(39 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (39 - x);
//		wcode(x,y);
		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
	//		for(j=0;j<40;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
				highlight ? data_wr( 0XFF-section.byte[temp_i][temp_j]) : data_wr( section.byte[temp_i][temp_j]);
			}	
				
		last_x_pos = x + (byte_index_ptr + 1);
#else
//	str_copy_rom_lim(rec_rom_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}

//#######################################################################################


//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_ram_lim(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED

  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_ram_str_to_data_buff(rec_ram_add,&section.byte[0][0],(get_str_len(rec_ram_add, rec_delim)),FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
		
		(byte_index_ptr > 39) ? byte_index_ptr = 39 : byte_index_ptr;
		(39 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (39 - x);
//		wcode(x,y);
		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
	//		for(j=0;j<40;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
				highlight ? data_wr( 0XFF-section.byte[temp_i][temp_j]) : data_wr( section.byte[temp_i][temp_j]);
			}	
				
		last_x_pos = x + (byte_index_ptr + 1);
				
	
#else
//	str_copy_ram_lim(rec_ram_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_rom_lim(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_rom_str_to_data_buff(rec_ram_add,&section.byte[0][0],(get_str_len((ubyte *)rec_ram_add, rec_delim)),FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
		
		(byte_index_ptr > 39) ? byte_index_ptr = 39 : byte_index_ptr;
		(39 - x) > byte_index_ptr ? (byte_index_ptr = byte_index_ptr) : (byte_index_ptr = (39 - x));
//		wcode(x,y);
		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
	//		for(j=0;j<40;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
				highlight ? data_wr( 0XFF-section.byte[temp_i][temp_j]) : data_wr( section.byte[temp_i][temp_j]);
			}	
				
		last_x_pos = x + (byte_index_ptr + 1);
	
		
#else
//	str_copy_rom_lim(rec_rom_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################


//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_ram_cnt(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED

  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_ram_str_to_data_buff(rec_ram_add,&section.byte[0][0],rec_cnt,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
		
		(byte_index_ptr > 39) ? byte_index_ptr = 39 : byte_index_ptr;
		(39 - x) > byte_index_ptr ? (byte_index_ptr = byte_index_ptr) : (byte_index_ptr = (39 - x));
//		wcode(x,y);
		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
	//		for(j=0;j<40;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
				highlight ? data_wr( 0XFF-section.byte[temp_i][temp_j]) : data_wr( section.byte[temp_i][temp_j]);
			}	
				
		last_x_pos = x + (byte_index_ptr + 1);

#else
//	str_copy_ram_lim(rec_ram_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_rom_cnt(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_rom_str_to_data_buff(rec_ram_add,&section.byte[0][0],rec_cnt,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
		
		(byte_index_ptr > 39) ? byte_index_ptr = 39 : byte_index_ptr;
		(39 - x) > byte_index_ptr ? (byte_index_ptr = byte_index_ptr) : (byte_index_ptr = (39 - x));
//		wcode(x,y);
		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
	//		for(j=0;j<40;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
				highlight ? data_wr( 0XFF-section.byte[temp_i][temp_j]) : data_wr( section.byte[temp_i][temp_j]);
			}	
				
		last_x_pos = x + (byte_index_ptr + 1);
		
#else
//	str_copy_rom_lim(rec_rom_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################



#endif
//#################	###################	###################	###############################################################
//#################	###################	###################	##############################################################
//#################	###################	###################	##############################################################
//#################	###################	###################	##############################################################
//#################	###################	###################	##############################################################
//#################	###################	###################	##############################################################
//#################	###################	###################	##############################################################

#ifdef LCD_240x128


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void initGraphicsLCD()
{	

	lcd_reset=1; 				//reset delay
	Delays_Xtal_ms(4);
	lcd_reset=0;
	Delays_Xtal_ms(4);
	lcd_reset=1;
	Delays_Xtal_ms(4);
	
	set_mode();		//OR MODE 
	set_graph_home();  	//ADRESS 0XF00
	set_graph_area();	//40 CHAR/LINE		             
//	set_txt_home();    	//0X0000		   				    
//	set_txt_area();		//40 CHARS/LINE
	
	
	set_cursor_pattern();	//SINGLE LINE CURSOR
	set_cursor_ptr(0x0000);		
//	data_addr_ptr(0x00); 	  
	
	set_disp_mode();    	//TEXT & GRAPHICS ON ..IMP
	

//	update_lcd(0xB0);
//	cmd_wr();

	lcd_font_sel=0;          //6x8 font      
	
	lcd_font_sel=0; 				//8*8 font size
//	data_addr_ptr(0x0000);			//clearing LCD at startup
//	clr_disp(480);
	data_addr_ptr(0x0000);
	clr_disp(3840);       
	return;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void set_mode(void)
{
	chk_stat1();
//    lcd_cmd=0x80;			 //OR with Graphics & Text
    lcd_cmd=0x82;			 //CG ROM Only
    update_lcd(lcd_cmd);
 	cmd_wr();
	return;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void set_disp_mode(void)
{
	chk_stat1();
	lcd_cmd=0x98;		//graphics ON,Text OFF,cursor OFF, blink OFF
//	lcd_cmd=0x9C;			//graphics ON,Text ON,cursor OFF, blink OFF
////	lcd_cmd=0x9E;			//graphics ON,Text ON,cursor ON, blink OFF
	update_lcd(lcd_cmd);
	cmd_wr();
	return;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void set_cursor_pattern(void)
{
	chk_stat1();
	lcd_cmd=0xA0; 				//1 line cursor
	update_lcd(lcd_cmd);
	cmd_wr();
	return;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void set_graph_area(void)
{
	chk_stat1();
	lcd_dat=0x1e;				// 30 char bytes per line
	update_lcd(lcd_dat);
	data_wr();
	chk_stat1();
	lcd_dat=0x00;		//fix
	update_lcd(lcd_dat);
	data_wr();
	chk_stat1();
	lcd_cmd=0x43;
	update_lcd(lcd_cmd);
	cmd_wr();
	return;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void set_graph_home()
{
	chk_stat1();
	lcd_dat=0x00; 
	update_lcd(lcd_dat);
	data_wr();
	chk_stat1();
	lcd_dat=0x00 ;//0x07
	update_lcd(lcd_dat);
	data_wr();
	chk_stat1();
	lcd_cmd=0x42;  //command for setting graph home
	update_lcd(lcd_cmd);
	cmd_wr();
    return; 
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void set_txt_area()
{
	chk_stat1();
	lcd_dat=0x1e;					  // no. of Column 30
	update_lcd(lcd_dat);
	data_wr();
	chk_stat1();
	lcd_dat=0x00;   //0x00h		//fix byte
	update_lcd(lcd_dat);
	data_wr();
	chk_stat1();
	lcd_cmd=0x41;     //command for setting txt area
	update_lcd(lcd_cmd);
	cmd_wr();
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void set_txt_home()
{
	 chk_stat1();
	 lcd_dat=0x00;      
	 update_lcd(lcd_dat);
	 data_wr();
	 chk_stat1();
	 lcd_dat=0x00;
	 update_lcd(lcd_dat);
	 data_wr();
	 chk_stat1();
	 lcd_cmd=0x40;     //command for setting txt home area
	 update_lcd(lcd_cmd);
	 cmd_wr();
	 return;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void clr_disp(unsigned short value)
{
   int i;
     for(i=0;i<value;i++)
     {
		lcd_dat=0x00; //((' ')-0x20)
		update_lcd(lcd_dat);
		data_wr();
		chk_stat1();
		lcd_cmd=0xC0;
		update_lcd(lcd_cmd);
		cmd_wr();
		chk_stat1();
     }
  	 Nop();
  	 Nop();
   return;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void home_clr()
{
//	data_addr_ptr(0x0000);			//clearing LCD 
//	clr_disp(480);
//	data_addr_ptr(0x0700);
//	wcode(0,HEADER_RES_SPACE);
	wcode((ubyte)((0*240)/320),(ubyte)((HEADER_RES_SPACE*128)/240));
	clr_disp(3840);           
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void data_addr_ptr(int addr)
{
   chk_stat1();
   lcd_dat = (unsigned char)(addr & 0xFF);
   update_lcd(lcd_dat);
   data_wr();
   addr = addr>>8;
   chk_stat1();
   lcd_dat=addr;
   update_lcd(lcd_dat);
   data_wr();
   chk_stat1();
   lcd_cmd=0x24;
   update_lcd(lcd_cmd);
   cmd_wr();
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void set_cursor_ptr(int addr)
{
   chk_stat1();
   lcd_dat = addr;
   update_lcd(lcd_dat);
   data_wr();
   addr = addr>>8;
   chk_stat1();
   lcd_dat=addr;
   update_lcd(lcd_dat);
   data_wr();
   chk_stat1();
   lcd_cmd=0x21;
   update_lcd(lcd_cmd);
   cmd_wr();
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
	void update_lcd(unsigned char temp_byte)
{
	
	LCD.byte = temp_byte;	
	lcd_d0 = LCD.data.d0;
	Nop();
	lcd_d1 = LCD.data.d1;
	Nop();
	lcd_d2 = LCD.data.d2;
	Nop();
	lcd_d3 = LCD.data.d3;
	Nop();
	lcd_d4 = LCD.data.d4;
	Nop();
	lcd_d5 = LCD.data.d5;
	Nop();
	lcd_d6 = LCD.data.d6;
	Nop();
	lcd_d7 = LCD.data.d7;

}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void read_lcd(void)
{
	
	LCD.data.d0=PORTDbits.RD1;
	LCD.data.d1=PORTDbits.RD2;
	LCD.data.d2=PORTDbits.RD3;
	LCD.data.d3=PORTDbits.RD4;
	LCD.data.d4=PORTDbits.RD5;
	LCD.data.d5=PORTDbits.RD6;
	LCD.data.d6=PORTDbits.RD7;
	LCD.data.d7=PORTFbits.RF0;
	status=LCD.byte ;	

}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void data_rd(void)
{
	
	rs=0;
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	lcd_rd=0;
	lcd_wr=1;
	
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	enable();
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void data_wr(void)
{
	rs=0;
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	lcd_rd=1;
	lcd_wr=0;
	
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	enable();
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void cmd_rd(void)
{
	
	rs=1;
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	lcd_rd=0;
	lcd_wr=1;
	
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	enable();
	
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void cmd_wr(void)
{
	
	rs=1;
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	lcd_rd=1;
	lcd_wr=0;
	
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	enable();
	
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void stat_rd(void)
{
	rs=1;
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	lcd_rd=0;
	lcd_wr=1;
	
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	enable();
	
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void enable(void)
{
	lcd_en=0;
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();	
	Nop();
	Nop();
	Nop();	
	Nop();
	Nop();
	Nop();	
	Nop();	
	Nop();
	Nop();
	Nop();	
//	Nop();	
//	Nop();
//	Nop();
//	Nop();	
//	Nop();
//	Nop();
//	Nop();	
//	Nop();
//	Nop();
//	Nop();	
//	Nop();
//	Nop();
//	Nop();	

	lcd_wr=1;
	lcd_en=1;

	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();	
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();	
	Nop();
	Nop();
	Nop();	
	Nop();
	Nop();
//	Nop();	
//	Nop();
//	Nop();
//	Nop();	
//	Nop();
//	Nop();
//	Nop();	
	
	
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void data_pin_input(void)
{
	lcd_d0_tris=1;
	lcd_d1_tris=1;
	lcd_d2_tris=1;
	lcd_d3_tris=1;
	lcd_d4_tris=1;
	lcd_d5_tris=1;
	lcd_d6_tris=1;
	lcd_d7_tris=1;
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void data_pin_output(void)
{
	lcd_d0_tris=0;
	lcd_d1_tris=0;
	lcd_d2_tris=0;
	lcd_d3_tris=0;
	lcd_d4_tris=0;
	lcd_d5_tris=0;
	lcd_d6_tris=0;
	lcd_d7_tris=0;
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void chk_stat1(void)
{
	return;
	data_pin_input();
	stat_rd();
	read_lcd();
	while((status & 0x03) != 0x03)		//checking whether STA0 & STA1 are set or not
	{
		read_lcd();
		status=status & 0x03;
	} 
	data_pin_output();
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void chk_stat2(void)
{
	return;
	data_pin_input();
	stat_rd();
	read_lcd();
	while((status & 0x0c) != 0x0c)		//checking whether STA2 & STA3 are set or not
	{
		read_lcd();
		status=status & 0x0c;
	}
	data_pin_output();
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned long shift_coll(unsigned short first_byte, unsigned short sec_byte, unsigned char first_char_width, unsigned char sec_char_width)
{
	union
	{
		unsigned short word;
		unsigned char integer[2];
	}concat;
	
	concat.integer[0] = sec_byte;
	concat.integer[1] = first_byte;
	
	concat.integer[1] = concat.integer[1] >> (8 - (first_char_width));
	concat.word = concat.word << (8 - (first_char_width));
	
	return(concat.word);
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void bit_shift( unsigned char col_val,unsigned char char_val)
{
	concat.word = shift_coll(section.byte[col_val][char_val], section.byte[col_val][char_val + 1], width_array[char_val], width_array[char_val + 1]);
	section.byte[col_val][char_val] = concat.integer[1];
	section.byte[col_val][char_val + 1] = concat.integer[0];
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_rom_str_to_data_buff(urombyte *str_data,unsigned char *str_buffer,unsigned char string_length,const unsigned char *CharBitmaps_ptr,const unsigned short *CharDescriptors_ptr)
{
	unsigned char *dup_str_buffer,dup_width_array[60];
	dup_str_buffer=str_buffer;
	width_array_ptr=0;
	
	CHAR_HEIGHT_IN_BITS=*(CharDescriptors_ptr+1);
	
	str_fill( width_array,80,0x00);

	for(str_len_counter=0;str_len_counter<string_length;str_len_counter++)
	{
		char_loc=*(str_data+str_len_counter)- OFFSET;		 //char_loc is location of the char in the look up of width & no. of chars	
		dup_width_array[str_len_counter]=(*(CharDescriptors_ptr+(char_loc*3))+ SPACE_BW_CHARACTERS);
		
		 if((dup_width_array[str_len_counter]-1)>=48)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+5))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+5);
					if((dup_width_array[str_len_counter]-1)!=48)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+6))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+6);
					j=j+7;
					}
					else
					j=j+6;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=48)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%48)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+7;
				}
				else
				str_buffer=str_buffer+6;
							
				width_array_ptr++;
					
			}
				else if((dup_width_array[str_len_counter]-1)>=40)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					if((dup_width_array[str_len_counter]-1)!=40)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+5))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+5);
					j=j+6;
					}
					else
					j=j+5;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=40)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%40)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+6;
				}
				else
				str_buffer=str_buffer+5;
				width_array_ptr++;
					
			}
			else if((dup_width_array[str_len_counter]-1)>=32)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					if((dup_width_array[str_len_counter]-1)!=32)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					j=j+5;
					}
					else
					j=j+4;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=32)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%32)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+5;	
				}
				else
				str_buffer=str_buffer+4;	
				width_array_ptr++;
				
			}
				else if((dup_width_array[str_len_counter]-1)>=24)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					if((dup_width_array[str_len_counter]-1)!=24)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					j=j+4;
					}
					else
					j=j+3;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=24)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%24)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+4;	
				}
				else
				str_buffer=str_buffer+3;	
				width_array_ptr++;
			
			}
				
			else if((dup_width_array[str_len_counter]-1)>=16)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					if((dup_width_array[str_len_counter]-1)!=16)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					j=j+3;
					}
					else
					j=j+2;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=16)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%16)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+3;	
				}
				else
				str_buffer=str_buffer+2;
			
				width_array_ptr++;
				
			}
			else 	if((dup_width_array[str_len_counter]-1)>=8)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					if((dup_width_array[str_len_counter]-1)!=8)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					j=j+2;
					}
					else
					j=j+1;
				}
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=8)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%8)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+2;
				}
				else
				str_buffer=str_buffer+1;
				width_array_ptr++;
				
			}	
			else
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					j=j+1;
				}
					
				width_array[width_array_ptr]=(dup_width_array[str_len_counter]);
				width_array_ptr++;
					
				str_buffer=str_buffer+1;
			}	
	}
//	pkd_str_len = string_length;	//for debugging
	str_packing(str_data,dup_str_buffer,CharBitmaps_ptr ,CharDescriptors_ptr,string_length);
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_ram_str_to_data_buff(ubyte *str_data,unsigned char *str_buffer,unsigned char string_length,const unsigned char *CharBitmaps_ptr,const unsigned short *CharDescriptors_ptr)
{
	unsigned char *dup_str_buffer,dup_width_array[60];
	dup_str_buffer=str_buffer;
	width_array_ptr=0;
	
	CHAR_HEIGHT_IN_BITS=*(CharDescriptors_ptr+1);

	str_fill( width_array,80,0x00);
	for(str_len_counter=0;str_len_counter<string_length;str_len_counter++)
	{
		char_loc=*(str_data+str_len_counter)- OFFSET;		 //char_loc is location of the char in the look up of width & no. of chars	
		dup_width_array[str_len_counter]=(*(CharDescriptors_ptr+(char_loc*3))+ SPACE_BW_CHARACTERS);
		
		 if((dup_width_array[str_len_counter]-1)>=48)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+5))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+5);
					if((dup_width_array[str_len_counter]-1)!=48)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+6))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+6);
					j=j+7;
					}
					else
					j=j+6;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=48)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%48)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+7;
				}
				else
				str_buffer=str_buffer+6;
							
				width_array_ptr++;
					
			}
				else if((dup_width_array[str_len_counter]-1)>=40)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					if((dup_width_array[str_len_counter]-1)!=40)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+5))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+5);
					j=j+6;
					}
					else
					j=j+5;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=40)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%40)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+6;
				}
				else
				str_buffer=str_buffer+5;
				width_array_ptr++;
					
			}
			else if((dup_width_array[str_len_counter]-1)>=32)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					if((dup_width_array[str_len_counter]-1)!=32)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+4))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+4);
					j=j+5;
					}
					else
					j=j+4;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=32)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%32)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+5;	
				}
				else
				str_buffer=str_buffer+4;	
				width_array_ptr++;
				
			}
				else if((dup_width_array[str_len_counter]-1)>=24)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					if((dup_width_array[str_len_counter]-1)!=24)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+3))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+3);
					j=j+4;
					}
					else
					j=j+3;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=24)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%24)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+4;	
				}
				else
				str_buffer=str_buffer+3;	
				width_array_ptr++;
			
			}
				
			else if((dup_width_array[str_len_counter]-1)>=16)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					if((dup_width_array[str_len_counter]-1)!=16)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+2);
					j=j+3;
					}
					else
					j=j+2;
				}
				width_array[width_array_ptr++]=8;
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=16)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%16)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+3;	
				}
				else
				str_buffer=str_buffer+2;
			
				width_array_ptr++;
				
			}
			else 	if((dup_width_array[str_len_counter]-1)>=8)
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					if((dup_width_array[str_len_counter]-1)!=8)
					{
					*(str_buffer+((str_data_counter*MAX_CHARS_IN_A_LINE*2)+1))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j+1);
					j=j+2;
					}
					else
					j=j+1;
				}
				width_array[width_array_ptr++]=8;
				if((dup_width_array[str_len_counter])!=8)
				{
				width_array[width_array_ptr]=((dup_width_array[str_len_counter]-1)%8)+ SPACE_BW_CHARACTERS;
				str_buffer=str_buffer+2;
				}
				else
				str_buffer=str_buffer+1;
				width_array_ptr++;
				
			}	
			else
			{
				j=0;
				for(str_data_counter=0;str_data_counter<CHAR_HEIGHT_IN_BITS;str_data_counter++)
				{
					*(str_buffer+(str_data_counter*MAX_CHARS_IN_A_LINE*2))=*(CharBitmaps_ptr +  *(CharDescriptors_ptr+(char_loc*3)+2) +j);
					j=j+1;
				}
					
				width_array[width_array_ptr]=(dup_width_array[str_len_counter]);
				width_array_ptr++;
					
				str_buffer=str_buffer+1;
			}	
	}
//	pkd_str_len = string_length;	//for debugging
	str_packing(str_data,dup_str_buffer,CharBitmaps_ptr ,CharDescriptors_ptr,string_length);
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void str_packing(urombyte *str_data,unsigned char *string_buffer,const unsigned char *CharBitmaps_ptr,const unsigned short *CharDescriptors_ptr,unsigned char string_length)
{
	pkd_str_len=0;
	byte_index_ptr=0;
	char_count=width_array_ptr;		

	while(char_count!=0)
	{
//		wcode(0,0);
//		for(i=0;i<40;i++)
//			for(j=0;j<40;j++)
//				data_wr( section.byte[i][j]);
//		Nop();
		
		
		if(width_array[byte_index_ptr+1]!=0)						// if next_byte !=0 then proceed to next statement
		{
			if(width_array[byte_index_ptr]<8)						// if current byte < 8 then bit shifting required
			{
				//%%%%% bit shifting
				for(col_value = 0; col_value <CHAR_HEIGHT_IN_BITS; col_value++)
					bit_shift(col_value,byte_index_ptr);
				//%%%%% adjusting the  width of all bytes	
				if((width_array[byte_index_ptr] + width_array[byte_index_ptr+1])/8)
				{
					width_array[byte_index_ptr+1]=(width_array[byte_index_ptr]+width_array[byte_index_ptr+1])%8;
					width_array[byte_index_ptr] = 8;
				
				}
				else
				{
					width_array[byte_index_ptr]=(width_array[byte_index_ptr]+width_array[byte_index_ptr+1])%8;
					width_array[byte_index_ptr+1] = 0;
				}
			}
			else
			{
				pkd_str_len++;
				byte_index_ptr++;
				char_count--;
			}	
		}
		else 							// if next_byte==0 then remove the current byte and shift all bytes left in the array
		{
				
			 if(byte_index_ptr< (MAX_CHARS_IN_A_LINE-1))
			 {
				for(inter_byte_index_ptr=byte_index_ptr;inter_byte_index_ptr<byte_index_ptr+char_count-2;inter_byte_index_ptr++)
				{
					width_array[inter_byte_index_ptr+1]=width_array[inter_byte_index_ptr+2];
					for(inter_col_value=0;inter_col_value<CHAR_HEIGHT_IN_BITS;inter_col_value++)
						section.byte[inter_col_value][inter_byte_index_ptr+1]=section.byte[inter_col_value][inter_byte_index_ptr+2];
				}
				for(inter_col_value=0;inter_col_value<CHAR_HEIGHT_IN_BITS;inter_col_value++)
					section.byte[inter_col_value][inter_byte_index_ptr+1]=0x00;
				
				width_array[inter_byte_index_ptr+1]=0;
			}
				char_count--;	
		}	
	}
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void graph_lcd_display(unsigned char x_cordinate,unsigned char y_cordinate,unsigned short *pkd_str)
{
//	j=0;
//		for(y=y_cordinate;y<y_cordinate+CHAR_HEIGHT_IN_BITS;y++)
//		{
//			for(x=x_cordinate;x<=x_cordinate+pkd_str_len;x++)
//			{
//				lcd.frame_buff[y][x] = section.integer[y-y_cordinate][x-x_cordinate];//*(pkd_str+j);
//				j=j+2;
//			}
//			
//				data_addr_ptr(0x0700);
//
//			pkd_str=pkd_str+(MAX_CHARS_IN_A_LINE);	
//			j=0;
//		}
}		
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_ram(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt, ubyte rec_delim,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED
		ubyte temp_str_len = get_str_len(rec_ram_add, rec_delim);
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_ram_str_to_data_buff(rec_ram_add,&section.byte[0][0],(rec_cnt<=temp_str_len)?rec_cnt:temp_str_len,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);

		(byte_index_ptr > 29) ? byte_index_ptr = 29 : byte_index_ptr;
		(29 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (29 - x);

		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
//			for(j=0;j<30;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
//				chk_stat1();
				Delays_Xtal_us(6);
				highlight ? update_lcd(0XFF-section.byte[temp_i][temp_j]) : update_lcd(section.byte[temp_i][temp_j]);
				data_wr();
//				chk_stat1();
				update_lcd(0xC0);
				cmd_wr();
			}
	//	last_x_pos = x + (byte_index_ptr ? byte_index_ptr : 1);		
		last_x_pos = x + (byte_index_ptr + 1);		
#else
//	str_copy_ram_lim(rec_ram_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void puts_graphical_lcd_rom(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt, ubyte rec_delim,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED
		ubyte temp_str_len = get_str_len((ubyte *)rec_ram_add, rec_delim);
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_rom_str_to_data_buff(rec_ram_add,&section.byte[0][0],(rec_cnt<=temp_str_len)?rec_cnt:temp_str_len,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);

		(byte_index_ptr > 29) ? byte_index_ptr = 29 : byte_index_ptr;
		(29 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (29 - x);

		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
//			for(j=0;j<30;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
//				chk_stat1();
				Delays_Xtal_us(6);
				highlight ? update_lcd(0XFF-section.byte[temp_i][temp_j]) : update_lcd(section.byte[temp_i][temp_j]);
				data_wr();
//				chk_stat1();
				update_lcd(0xC0);
				cmd_wr();
			}
	//	last_x_pos = x + (byte_index_ptr ? byte_index_ptr : 1);		
		last_x_pos = x + (byte_index_ptr + 1);		
#else
//	str_copy_rom_lim(rec_rom_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void puts_graphical_lcd_ram_lim(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j,rec_cnt;
#ifndef MENU_ON_LED
		rec_cnt = (get_str_len(rec_ram_add, rec_delim));
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_ram_str_to_data_buff(rec_ram_add,&section.byte[0][0],rec_cnt,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
		
		if(((rec_cnt*6)/8)>byte_index_ptr)
			byte_index_ptr += 1;
		(byte_index_ptr > 29) ? byte_index_ptr = 29 : byte_index_ptr;
		(29 - x) > byte_index_ptr ? (byte_index_ptr = byte_index_ptr) : (byte_index_ptr = (29 - x));

		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
//			for(j=0;j<30;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
//				chk_stat1();
				Delays_Xtal_us(6);
				highlight ? update_lcd(0XFF-section.byte[temp_i][temp_j]) : update_lcd(section.byte[temp_i][temp_j]);
				data_wr();
//				chk_stat1();
				update_lcd(0xC0);
				cmd_wr();
			}
	//	last_x_pos = x + (byte_index_ptr ? byte_index_ptr : 1);		
		last_x_pos = x + (byte_index_ptr + 1);		
				
	
#else
//	str_copy_ram_lim(rec_ram_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################void puts_graphical_lcd_ram_lim(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr)
//{
//	ubyte temp_i,temp_j;
//#ifndef MENU_ON_LED
//
//  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
//		load_ram_str_to_data_buff(rec_ram_add,&section.byte[0][0],(get_str_len(rec_ram_add, rec_delim)),FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
//		
//		(byte_index_ptr > 29) ? byte_index_ptr = 29 : byte_index_ptr;
//		(29 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (29 - x);
//
//		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
////			for(j=0;j<30;j++)
//			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
//			{
//				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
////				chk_stat1();
//				Delays_Xtal_us(6);
//				highlight ? update_lcd(0XFF-section.byte[temp_i][temp_j]) : update_lcd(section.byte[temp_i][temp_j]);
//				data_wr();
////				chk_stat1();
//				update_lcd(0xC0);
//				cmd_wr();
//			}
//	//	last_x_pos = x + (byte_index_ptr ? byte_index_ptr : 1);		
//		last_x_pos = x + (byte_index_ptr + 1);		
//				
//	
//#else
////	str_copy_ram_lim(rec_ram_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
//	transmit_lcd_to_led();
//#endif
//}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
//void puts_graphical_lcd_rom_lim(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr)
//{
//	ubyte temp_i,temp_j,rec_cnt;
//#ifndef MENU_ON_LED
//		
// 		rec_cnt = (get_str_len(rec_ram_add, rec_delim));
// 		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
//		load_rom_str_to_data_buff(rec_ram_add,&section.byte[0][0],(get_str_len((ubyte *)rec_ram_add, rec_delim)),FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);
//
//		if(((rec_cnt*6)/8)>byte_index_ptr)
//			byte_index_ptr += 1;
//
//		(byte_index_ptr > 29) ? byte_index_ptr = 29 : byte_index_ptr;
//		(29 - x) > byte_index_ptr ? (byte_index_ptr = byte_index_ptr) : (byte_index_ptr = (29 - x));
//
//		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
////			for(j=0;j<30;j++)
//			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
//			{
//				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
////				chk_stat1();
//				Delays_Xtal_us(6);
//				highlight ? update_lcd(0XFF-section.byte[temp_i][temp_j]) : update_lcd(section.byte[temp_i][temp_j]);
//				data_wr();
////				chk_stat1();
//				update_lcd(0xC0);
//				cmd_wr();
//			}	
//	//	last_x_pos = x + (byte_index_ptr ? byte_index_ptr : 1);		
//		last_x_pos = x + (byte_index_ptr + 1);		
//	
//#else
////	str_copy_rom_lim(rec_rom_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
//	transmit_lcd_to_led();
//#endif
//}
//#######################################################################################
void puts_graphical_lcd_rom_lim(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED
		
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_rom_str_to_data_buff(rec_ram_add,&section.byte[0][0],(get_str_len((ubyte *)rec_ram_add, rec_delim)),FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);

		(byte_index_ptr > 29) ? byte_index_ptr = 29 : byte_index_ptr;
		(29 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (29 - x);

		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
//			for(j=0;j<30;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
//				chk_stat1();
				Delays_Xtal_us(6);
				highlight ? update_lcd(0XFF-section.byte[temp_i][temp_j]) : update_lcd(section.byte[temp_i][temp_j]);
				data_wr();
//				chk_stat1();
				update_lcd(0xC0);
				cmd_wr();
			}	
	//	last_x_pos = x + (byte_index_ptr ? byte_index_ptr : 1);		
		last_x_pos = x + (byte_index_ptr + 1);		
	
#else
//	str_copy_rom_lim(rec_rom_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}

//ubyte graphics_buffer[][3] = {
//        0xFF, 0xFF, 0xFF,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0x80, 0x00, 0x00,
//        0xFF, 0xFF, 0xFF,
//        };
//void puts_graphical_lcd_graphics(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_delim,ubyte font_index_ptr) {
//	ubyte temp_i,temp_j;
//
//
//            int rows = sizeof(graphics_buffer)/sizeof(graphics_buffer[0]);
//            int cols = sizeof(graphics_buffer[0]);
//            
//            for(temp_i=0;temp_i<=cols-1;temp_i++)
//			for(temp_j=0;temp_j<=rows-1;temp_j++)
//			{
//				wcode(temp_i,temp_j);
//
//				Delays_Xtal_us(6);
//                
//				update_lcd(graphics_buffer[temp_j][temp_i]);
//                
//				data_wr();
//
//				update_lcd(0xC0);
//				cmd_wr();
//			}				
//}

//#######################################################################################


//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_ram_cnt(ubyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED

  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_ram_str_to_data_buff(rec_ram_add,&section.byte[0][0],rec_cnt,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);

		(byte_index_ptr > 29) ? byte_index_ptr = 29 : byte_index_ptr;
		(29 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (29 - x);

		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
//			for(j=0;j<30;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
//				chk_stat1();
				Delays_Xtal_us(6);
				highlight ? update_lcd(0XFF-section.byte[temp_i][temp_j]) : update_lcd(section.byte[temp_i][temp_j]);
				data_wr();
//				chk_stat1();
				update_lcd(0xC0);
				cmd_wr();
			}
//	last_x_pos = x + (byte_index_ptr ? byte_index_ptr : 1);		
		last_x_pos = x + (byte_index_ptr + 1);		

#else
//	str_copy_ram_lim(rec_ram_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void puts_graphical_lcd_rom_cnt(urombyte *rec_ram_add,ubyte x,ubyte y,ubyte highlight, ubyte rec_cnt,ubyte font_index_ptr)
{
	ubyte temp_i,temp_j;
#ifndef MENU_ON_LED
  		str_fill( section.single_byte,MAX_CHAR_HEIGHT*MAX_CHARS_IN_A_LINE*2,0x00);	
		load_rom_str_to_data_buff(rec_ram_add,&section.byte[0][0],rec_cnt,FontCharBitmapsPtr[font_index_ptr] ,FontCharDescriptorsPtr[font_index_ptr]);

		(byte_index_ptr > 29) ? byte_index_ptr = 29 : byte_index_ptr;
		(29 - x) > byte_index_ptr ? byte_index_ptr = byte_index_ptr : (29 - x);

		for(temp_i=0;temp_i<*(FontCharDescriptorsPtr[font_index_ptr]+1);temp_i++)
//			for(j=0;j<30;j++)
			for(temp_j=0;temp_j<=byte_index_ptr;temp_j++)
			{
				wcode((ubyte)(x+temp_j),(ubyte)(temp_i+y));
//				chk_stat1();
				//Delays_Xtal_us(6);
				highlight ? update_lcd(0XFF-section.byte[temp_i][temp_j]) : update_lcd(section.byte[temp_i][temp_j]);
				data_wr();
//				chk_stat1();
				update_lcd(0xC0);
				cmd_wr();
			}
	//	last_x_pos = x + (byte_index_ptr ? byte_index_ptr : 1);		
		last_x_pos = x + (byte_index_ptr + 1);		
#else
//	str_copy_rom_lim(rec_rom_add, &led_disp_buff[orphan_flags2.bits.led_line_ptr][0], rec_delim);
	transmit_lcd_to_led();
#endif
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void lcd_byte(ubyte rec_byte, unsigned rec_type)        
{
	//just defined for PIS compatibility
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void wcode(ubyte x,ubyte y)
{
   uinteger location, home; 
   uinteger line; 
   
   
   home = 0x0000; 
   line = 0x001e; 
   location = home + (((uinteger)y) * line) + (x); 
   
   data_addr_ptr(location); 
}
//#######################################################################################
#endif
