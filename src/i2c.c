#include "..\inc\headers.h"
//#include "..\inc\i2c.h"


#define 	RTC_ADDR							0x00D0			// device address of RTC
//#define		SYS_CLOCK							32000000
//#define		GetSystemClock()        		   (SYS_CLOCK)
//#define 	GetPeripheralClock()     		   (SYS_CLOCK/2)
//#define 	GetInstructionClock()     		   (SYS_CLOCK)
#define		I2C_CLOCK_FREQ					    100000


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void init_i2c(void)
{	
//	unsigned short config2, config1;
//
//	config2 = 0x130;//0x11;									/* Baud rate is set for 100 Khz */
////	config1 = (I2C_ON & I2C_IDLE_CON & I2C_CLK_HLD &		/* Configure I2C for 7 bit address mode */
////			   I2C_IPMI_DIS & I2C_7BIT_ADD & I2C_SLW_EN &
////			   I2C_SM_DIS & I2C_GCALL_DIS & I2C_STR_DIS & 
////			   I2C_NACK & I2C_ACK_DIS & I2C_RCV_DIS &			//i2c_nack
////			   I2C_STOP_DIS & I2C_RESTART_DIS & I2C_START_DIS);
//	config1 = (I2C_ON & I2C_IDLE_CON  &		/* Configure I2C for 7 bit address mode */
//			   I2C_7BIT_ADD & I2C_SLW_EN &
//			   I2C_SM_DIS & I2C_STR_DIS & 
//			   I2C_NACK & I2C_ACK_DIS & I2C_RCV_DIS &			//i2c_nack
//			   I2C_STOP_DIS & I2C_RESTART_DIS & I2C_START_DIS);
//			   			   
// 	i2c2_open(config1,config2);
//	i2c2_eebyte_write(RTC_ADDR,0x00,0x00);
//	Delays_Xtal_ms(15);
//
//// Configure Various I2C Options
////I2CConfigure(I2C2, I2C_STOP_IN_IDLE );
////// Set Desired Operation Frequency
//I2CSetFrequency(I2C2, GetPeripheralClock(), I2C_CLOCK_FREQ);
////I2CSetSlaveAddress(I2C2, RTC_ADDR, 0, I2C_USE_7BIT_ADDRESS);
////I2CEnable(I2C2, TRUE);
//
////I2C2BRG=0X130;
////I2C2CONbits.ACKDT=1;
////I2C2CONbits.SCLREL=1;
////I2C2CONbits.RCEN=1;
////I2C2CONbits.GCEN=1;
////I2C2CONbits.FRZ=0;
//I2C2CONbits.ON=1;
////


}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned char i2c2_eebyte_write(unsigned char control, unsigned char address, unsigned char data)
{
	i2c2_idle();                      // ensure module is idle
	i2c2_start();                     // initiate START condition
	while(I2C2CONbits.SEN);
	if(I2C2STATbits.BCL)           // test for bus collision
		return(-1);                // return with Bus Collision error 
	else if(i2c2_mast_write(control))			// write 1 byte 
		return(-3);							// set error for write collision
		
	i2c2_idle();									// ensure module is idle
	if(!I2C2STATbits.ACKSTAT)					// test for ACK condition, if received
	{
		if(i2c2_mast_write(address))		// WRITE word address to EEPROM/RTC
			return(-3);						// return with write collision error
		i2c2_idle();								// ensure module is idle
		if(!I2C2STATbits.ACKSTAT)					// test for ACK condition, if received
		{
			if(i2c2_mast_write(data))		// WRITE data to EEPROM/RTC
				return(-3);						// return with write collision error
			i2c2_idle();								// ensure module is idle
			if (!I2C2STATbits.ACKSTAT)				// test for ACK condition, if received
			{
				i2c2_stop();              			// send STOP condition
				while(I2C2CONbits.PEN);			// wait until stop condition is over 
				if (I2C2STATbits.BCL)				// test for bus collision
					return(-1);					// return with Bus Collision error 
			}
			else
				return (-2);				// return with Not Ack error of data sent
		}
		else
			return (-2);				// return with Not Ack error of address sent
	}
	else
		return (-2);				// return with Not Ack error of control word (device ID) sent
	return(0);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void i2c2_idle(void)
{
	/* Wait until I2C Bus is Inactive */
	while(I2C2CONbits.SEN || I2C2CONbits.PEN || I2C2CONbits.RCEN || I2C2CONbits.ACKEN || I2C2STATbits.TRSTAT);	
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
	Nop();
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void i2c2_start(void)
{
     I2C2CONbits.SEN = 1;	/* initiate Start on SDA and SCL pins */
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
char i2c2_mast_write(unsigned char data_out)
{
	I2C2STATbits.IWCOL = 0;
	while(I2C2STATbits.TRSTAT);	
	I2C2TRN = data_out;
	if(I2C2STATbits.IWCOL)        /* If write collision occurs,return -1 */
		return (-1);
	else
	{
		while(I2C2STATbits.TBF); // new wait until write cycle is complete   
		return 0;		// if WCOL bit is not set return non-negative 
	}
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void i2c2_stop(void)
{
	I2C2CONbits.PEN = 1;	/* initiate Stop on SDA and SCL pins */
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned char i2c2_mast_read(void)
{
	I2C2CONbits.RCEN = 1;
	while(I2C2CONbits.RCEN);
	I2C2STATbits.I2COV = 0;
	return(I2C2RCV);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void i2c2_not_ack(void)
{
	I2C2CONbits.ACKDT = 1;
	I2C2CONbits.ACKEN = 1;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned char i2c2_get_str(unsigned char *rdptr, unsigned char length)
{
	while (length--)           // perform getcI2C() for 'length' number of bytes
	{
		*rdptr++ = i2c2_mast_read();       // save byte received
		while (I2C2CONbits.RCEN); // check that receive sequence is over    
		
		if(I2C2STATbits.BCL)       // test for bus collision
			return (-1);            // return with Bus Collision error 
		
		if(length)               // test if 'length' bytes have been read
		{
			I2C2CONbits.ACKDT = 0;    // set acknowledge bit state for ACK        
			I2C2CONbits.ACKEN = 1;    // initiate bus acknowledge sequence
			while(I2C2CONbits.ACKEN); // wait until ACK sequence is over 
		} 
	}
	return (0);                 // last byte received so don't send ACK      
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void i2c2_restart(void)
{
	I2C2CONbits.RSEN = 1;	/* initiate restart on SDA and SCL pins	*/
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void i2c2_open(unsigned short config1,unsigned short config2)
{
	I2C2BRG = config2;
	I2C2CON = config1;
}
//#######################################################################################


////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: main
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################
//void i2c2_close(void)
//{
//	/* clear the I2CEN bit */
//	I2C2CONbits.I2CEN = 0;
//	
//	/* clear the SI2C & MI2C Interrupt enable bits */
//	_SI2C2IE = 0;
//	_MI2C2IE = 0;
//	
//	/* clear the SI2C & MI2C Interrupt flag bits */
//	_SI2C2IF = 0;
//	_MI2C2IF = 0;
//}
////#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
char i2c2_data_ready(void)
{
	return I2C2STATbits.RBF;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: main
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned char i2c2_ee_seq_read(unsigned char control, unsigned char address, unsigned char *rdptr, unsigned char length)
{
//	unsigned char i2c_data;
	i2c2_idle();                      // ensure module is idle
	i2c2_start();                     // initiate START condition
	while(I2C2CONbits.SEN);      // wait until start condition is over 
	if (I2C2STATbits.BCL)           // test for bus collision
		return (-1);                // return with Bus Collision error 
	else
	{
		if (i2c2_mast_write(control))    // write 1 byte 
			return (-3);              // set error for write collision

		i2c2_idle();                    // ensure module is idle
		if (!I2C2STATbits.ACKSTAT)   // test for ACK condition, if received
		{
			if (i2c2_mast_write(address))  // WRITE word address to EEPROM	
				return (-3);            // return with write collision error

			i2c2_idle();                  // ensure module is idle
			if (!I2C2STATbits.ACKSTAT) // test for ACK condition received
			{
				i2c2_restart();             // generate I2C bus restart condition
				while (I2C2CONbits.RSEN);  // wait until re-start condition is over 
				if (i2c2_mast_write(control +1))// WRITE 1 byte - R/W bit should be 1 for read
					return (-3);          // set error for write collision
				i2c2_idle();                // ensure module is idle
				if (!I2C2STATbits.ACKSTAT)// test for ACK condition received
				{
					if (i2c2_get_str(rdptr,length))// read in multiple bytes
						return (-1);        // return with Bus Collision error

					i2c2_not_ack();            // send not ACK condition
					while (I2C2CONbits.ACKEN);// wait until ACK sequence is over 
					i2c2_stop();              // send STOP condition
					while (I2C2CONbits.PEN);// wait until stop condition is over 
					if (I2C2STATbits.BCL)   // test for bus collision
						return (-1);        // return with Bus Collision error 
				}
				else
					return (-2);          // return with Not Ack error
			}
			else
				return (-2);            // return with Not Ack error
		}
		else
			return (-2);              // return with Not Ack error
	}
	return (0);                   // return with no error
}


















