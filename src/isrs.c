#include "..\inc\headers.h"

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: _DMACH0
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void __ISR(_DMA_0_VECTOR,ipl5) DMA_lhb_tx_handler(void)
{
	int dmaFlags=DCH0INT&0xff; 				// read the interrupt flags
	DCH0INTCLR=0x000000ff; 					// clear the DMA channel interrupt flags	
	IFS1CLR = 0x00010000; 					// Be sure to clear the DMA0 interrupt flags
	lhb_comm.flags.bits.pkt_transmiting = 0;
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: _T1Interrupt
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//timer 1 interrupt timed for 100us delay
//#######################################################################################
void __ISR(_TIMER_1_VECTOR, ipl5) IntTimer1Handler(void)
{
	static ubyte play = 0;
	IFS0bits.T1IF = 0;
 //   PR1   = 8000;     //100us*80Mhz = 8000
	if(lhb_comm.stx_timer_cnt && lhb_comm.flags.bits.rx_enable)
	{
		lhb_comm.stx_timer_cnt--;
		if(!lhb_comm.stx_timer_cnt )
		{
			lhb_comm.flags.word = 0;
			lhb_comm.rx_cnt = 0;
			lhb_comm.stx_timer_cnt = 0;
			lhb_comm.flags.bits.rx_enable = 1;
		}
	}		
	if(!key_num.bits.key_pressed)
	key_scan();
}
//#######################################################################################
//#######################################################################################
//Function Name: Timer2 Interrupt Routine
//attribute keyword (attribute specifier)
//for declaring function as interrupt handler tag function with interrupt attribute 

//50ms
//#######################################################################################
void __ISR(_TIMER_2_VECTOR, ipl5) IntTimer2Handler(void)
{	
	IFS0bits.T2IF = 0;			/* Reset Timer 1 interrupt flag */
//	PR2=0x09C4; //PR2=0x00FA;					//5msec  period timer
	TMR2=0x0000;
	
	for (i=0 ; i<TOTAL_KEYS;i++)
	{
		if (keys[i].timer_on)
			keys[i].timer_count--;	
		if (keys[i].timer_count==0 && (keys[i].timer_on))
		{
			keys[i].timer_on=0;
			keys[i].timer_complete=1;
			break;
		}
	}
	
//	if(timer2.msec_50.timer)
//		timer2.msec_50.timer--;

//	else
	{
//		timer2.msec_50.timer = 10; 
//		timer2.msec_50.status.tick = 1;

		if(key_num.bits.key_debounce)
		{
			if(!--key_debounce_count)
				key_num.bits.key_debounce = 0;
		}
		
		if(orphan_flags1.bits.menu_timer)
		{
			if(!--menu_timer_count)
				orphan_flags1.bits.menu_timer = 0;
		}
		

		if(orphan_flags.bits.delay_timer)
		{
			if(!--delay_timer_count)
				orphan_flags.bits.delay_timer = 0;
		}
		

	}
		
	if(timer2.sec.timer)
	{		 
		timer2.sec.timer--;
		
	}
	else
	{
		timer2.sec.timer = 10; 
		timer2.sec.status.tick = 1;
		timer2.stat.bits.blinker_status = !timer2.stat.bits.blinker_status;
		
	}
	
	if( timer2.display_timeout_5sec.timer)
	{		 
		timer2.display_timeout_5sec.timer--;
		
	}

	if( timer2.display_timeout_10sec.timer)
	{		 
		timer2.display_timeout_10sec.timer--;
		
	}
	

}// interrupt loop closes.
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: _U2RXInterrupt
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void __ISR(_UART_1B_VECTOR, ipl5) IntUart1BHandler(void)
{
	ubyte stx_etx_dig_len;
	u2_rx_byte =(unsigned char) U1BRXREG;
	IFS2bits.U1BRXIF = 0;	// Clear the Recieve Interrupt Flag
	orphan_flags.bits.u2_rx_byte = 1;
	timer2.display_timeout_5sec.timer = 100;
	if(lhb_comm.flags.bits.rx_enable)
	{
		if( lhb_comm.rx_cnt >= RXTX_FRAME_STRUCT)
			lhb_comm.rx_cnt = 0;
		if(!lhb_comm.flags.bits.rx_stx)
		{
			stx_etx_dig_len = L_STX;
			rx.data.stx[stx_etx_dig_len - 1] = u2_rx_byte;
			lhb_comm.flags.bits.rx_stx = str_comp_rom( (urombyte *)STX_STR, rx.data.stx, L_STX);
			if(!lhb_comm.flags.bits.rx_stx)
				while(--stx_etx_dig_len)
					rx.data.stx[L_STX - stx_etx_dig_len - 1] = rx.data.stx[L_STX - stx_etx_dig_len];
			else
			{
				lhb_comm.stx_timer_cnt = 20000;
				lhb_comm.rx_cnt = L_STX;
			}
		}
		else
		{
			rx.bytes[ lhb_comm.rx_cnt++] = u2_rx_byte;
			if(lhb_comm.rx_cnt == L_FRAME_INIT)
				lhb_comm.flags.bits.rx_stx = str_comp_rom( (urombyte *)ID_SRC, rx.data.dest_id, L_SRC_ID);
			if(str_comp_rom( (urombyte *)ETX_STR, &rx.bytes[ lhb_comm.rx_cnt - L_ETX], L_ETX))
			{
				lhb_comm.flags.bits.rx_comp = 1;
				lhb_comm.flags.bits.rx_stx=0;
				lhb_comm.flags.bits.rx_enable = 0;
				if(rx.data.func_code[0] != FC_PEN_DRIVE_DETECTED_WAIT)
					pendrive_detected = 0;
				else
					Nop();	
			}
			if(lhb_comm.rx_cnt > 990)
				Nop();
		}
	}		

		
	if(U1BSTAbits.OERR)
		U1BSTAbits.OERR = 0;
}



//*****************************************************************************
// Exception Variable Declaration
//*****************************************************************************

// These are the available exception code values that may appear in the 'Cause'
// register's ExcCode field which gets read during a general exception error.  
// Refer to the MIPS 32 M4K processor Core Software User's Manual for more 
// information on this.  'static' is used in this case because an exception 
// condition might stop 'auto' variables from being created
static enum {
EXCEP_IRQ = 0, // interrupt
EXCEP_AdEL = 4, // address error exception (load or ifetch)
EXCEP_AdES, // address error exception (store)
EXCEP_IBE, // bus error (ifetch)
EXCEP_DBE, // bus error (load/store)
EXCEP_Sys, // syscall
EXCEP_Bp, // breakpoint
EXCEP_RI, // reserved instruction
EXCEP_CpU, // coprocessor unusable
EXCEP_Overflow, // arithmetic overflow
EXCEP_Trap, // trap (possible divide by zero)
EXCEP_IS1 = 16, // implementation specfic 1
EXCEP_CEU, // CorExtend Unuseable
EXCEP_C2E // coprocessor 2
} _excep_code;

// static in case exception condition would stop auto variable being created
static unsigned int _epc_code;
static unsigned int _excep_addr;



//*****************************************************************************
// Exception Handling
//*****************************************************************************

// This function overrides the normal _weak_ _generic_exception_handler which
// is defined in the C32 User's Guide.  The _weak_ _generic_exception_handler 
// just does an infinite loop.  
void _general_exception_handler(void)
{
//	while(1);
// Mask off Mask of the ExcCode Field from the Cause Register
// Refer to the MIPs M4K Software User's manual 
_excep_code=_CP0_GET_CAUSE() & 0x0000007C >> 2; 
_excep_addr=_CP0_GET_EPC();
//exception_handling_bkup_rst();
while (1) 
{
// Examine _excep_code to identify the type of exception
switch(_excep_code)
{
case EXCEP_AdEL: // address error exception (load or ifetch)
{
SoftReset();//while(1);
}
break;
case EXCEP_AdES: // address error exception (store)
{
SoftReset();//while(1);
}
break;
case EXCEP_IBE: // bus error (ifetch)
{
SoftReset();//while(1);
}
break;
case EXCEP_DBE:     // bus error (load/store)
{
SoftReset();//while(1);
}
break;
case EXCEP_Overflow: // arithmetic overflow
{
SoftReset();//while(1);
}
break;
case EXCEP_Trap: // trap (like divide by 0)
{
SoftReset();//while(1);
}
break;
default:
{
//The rest of the exception types are probably less common
SoftReset();//while(1);
}
}
}
} 

///////////////////////
////*****************************************************************************
//// Exception Variable Declaration
////*****************************************************************************
//
//// These are the available exception code values that may appear in the 'Cause'
//// register's ExcCode field which gets read during a general exception error.  
//// Refer to the MIPS 32 M4K processor Core Software User's Manual for more 
//// information on this.  'static' is used in this case because an exception 
//// condition might stop 'auto' variables from being created
//static enum {
//EXCEP_IRQ = 0, // interrupt
//EXCEP_AdEL = 4, // address error exception (load or ifetch)
//EXCEP_AdES, // address error exception (store)
//EXCEP_IBE, // bus error (ifetch)
//EXCEP_DBE, // bus error (load/store)
//EXCEP_Sys, // syscall
//EXCEP_Bp, // breakpoint
//EXCEP_RI, // reserved instruction
//EXCEP_CpU, // coprocessor unusable
//EXCEP_Overflow, // arithmetic overflow
//EXCEP_Trap, // trap (possible divide by zero)
//EXCEP_IS1 = 16, // implementation specfic 1
//EXCEP_CEU, // CorExtend Unuseable
//EXCEP_C2E // coprocessor 2
//} _excep_code;
//
//// static in case exception condition would stop auto variable being created
//static unsigned int _epc_code;
//static unsigned int _excep_addr;
//
//
//
////*****************************************************************************
//// Exception Handling
////*****************************************************************************
//
//// This function overrides the normal _weak_ _generic_exception_handler which
//// is defined in the C32 User's Guide.  The _weak_ _generic_exception_handler 
//// just does an infinite loop.  
//void _general_exception_handler(void)
//{
////	while(1);
//// Mask off Mask of the ExcCode Field from the Cause Register
//// Refer to the MIPs M4K Software User's manual 
//_excep_code=_CP0_GET_CAUSE() & 0x0000007C >> 2; 
//_excep_addr=_CP0_GET_EPC();
////exception_handling_bkup_rst();
//while (1) 
//{
//// Examine _excep_code to identify the type of exception
//switch(_excep_code)
//{
//case EXCEP_AdEL: // address error exception (load or ifetch)
//{
//				SoftReset();//while(1);
//
//}
//break;
//case EXCEP_AdES: // address error exception (store)
//{
//				SoftReset();//while(1);
//
//}
//break;
//case EXCEP_IBE: // bus error (ifetch)
//{
//				SoftReset();//while(1);
//
//}
//break;
//case EXCEP_DBE:     // bus error (load/store)
//{
//				SoftReset();//while(1);
//
//}
//break;
//case EXCEP_Overflow: // arithmetic overflow
//{
//				SoftReset();//while(1);
//
//}
//break;
//case EXCEP_Trap: // trap (like divide by 0)
//{
//				SoftReset();//while(1);
//
//}
//break;
//default:
//{
////The rest of the exception types are probably less common
//				SoftReset();//while(1);
//
//}
//}
//}
//} 
//

