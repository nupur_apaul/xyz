#include "..\inc\headers.h" 

#define TOTAL_NO_OF_FONTS			1
ubyte last_x_pos;

#ifdef LCD_320x240

const uint_8 *FontCharBitmapsPtr[TOTAL_NO_OF_FONTS]=
		{
//			&arialNarrow19ptCharBitmaps[0],
//			&arialNarrow23ptCharBitmaps[0],
//			&arialNarrow30ptCharBitmaps[0],
//			&arialNarrow45ptCharBitmaps[0],
//			&arialNarrow6ptCharBitmaps[0],
//			&arialNarrow10ptCharBitmaps[0],
			&arialNarrow46ptCharBitmaps[0],
			&arialNarrow31ptCharBitmaps[0],
			&arialNarrowBld10ptCharBitmaps[0],
			&arialNarrowBld19ptCharBitmaps[0],
			&arialNarrow16ptCharBitmaps[0]
		};
													
const FONT_CHAR_INFO *FontCharDescriptorsPtr[TOTAL_NO_OF_FONTS] =
		{
//			&arialNarrow19ptCharDescriptors[0][0],
//			&arialNarrow23ptCharDescriptors[0][0],
//			&arialNarrow30ptCharDescriptors[0][0],
//			&arialNarrow45ptCharDescriptors[0][0],
//			&arialNarrow6ptCharDescriptors[0][0],
//			&arialNarrow10ptCharDescriptors[0][0],
//			&arialNarrow46ptCharDescriptors[0][0],
//			&arialNarrow31ptCharDescriptors[0][0],

			&arialNarrowBld10ptCharDescriptors[0][0],
			&arialNarrowBld19ptCharDescriptors[0][0],
			&arialNarrow16ptCharDescriptors[0][0]
		};

#endif

#ifdef LCD_240x128

const uint_8 *FontCharBitmapsPtr[TOTAL_NO_OF_FONTS]=
		{
////			&arialNarrow19ptCharBitmaps[0],
////			&arialNarrow23ptCharBitmaps[0],
////			&arialNarrow30ptCharBitmaps[0],
////			&arialNarrow45ptCharBitmaps[0],
////			&arialNarrow6ptCharBitmaps[0],
////			&arialNarrow10ptCharBitmaps[0],
			//&arialNarrow23ptCharBitmaps[0],
			&arialNarrowBld10ptCharBitmaps[0]
//			&arialNarrow5ptCharBitmaps[0]
//			&arialNarrow10ptCharBitmaps[0],
		
			
//			&arialNarrow16ptCharBitmaps[0]
//			&arialNarrowBld10ptCharBitmaps[0],
//			&arialNarrow19ptCharBitmaps[0]
		};
													
const FONT_CHAR_INFO *FontCharDescriptorsPtr[TOTAL_NO_OF_FONTS] =
		{
////			&arialNarrow19ptCharDescriptors[0][0],
////			&arialNarrow23ptCharDescriptors[0][0],
////			&arialNarrow30ptCharDescriptors[0][0],
////			&arialNarrow45ptCharDescriptors[0][0],
////			&arialNarrow6ptCharDescriptors[0][0],
////			&arialNarrow10ptCharDescriptors[0][0],
			//&arialNarrow23ptCharDescriptors[0][0],
			
		//	&arialNarrow10ptCharDescriptors[0][0],
			&arialNarrowBld10ptCharDescriptors[0][0]
//			&arialNarrow5ptCharDescriptors[0][0]
		
			
//			&arialNarrow16ptCharDescriptors[0][0]
//			&arialNarrowBld10ptCharDescriptors[0][0],
//			&arialNarrow19ptCharDescriptors[0][0]
		};

#endif











enum  FONT_ENUM FONT;
