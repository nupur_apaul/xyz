//#include "p24HJ256GP206.h"
//#include "common.h"
//#include "def1.h"
//#include "struct.h"
#include "..\inc\headers.h"

/*********************************************************************************
Function Name			:	Init_Port()	
Input Parameters		:	VOID
Output	Parameters		:   VOID
Function Description	:	Initailze all required port of LED Display Card.
**********************************************************************************/
void Init_Port()
{
	AD1PCFG = 0XFFFF;					// ALL DIGITAL
	

	TRISB=0x0000;
	TRISC=0x0000;
	TRISD=0x0000;
	TRISF=0x0000;
	TRISG=0x0000;
	
	BUTTON1_TRIS  = 1;
    BUTTON2_TRIS  = 1; 
    BUTTON3_TRIS   = 1;
    BUTTON4_TRIS   = 1;
    BUTTON5_TRIS   = 1;
    BUTTON6_TRIS   = 1;
    BUTTON7_TRIS   = 1;
	
	
//	TRISGbits.TRISG7=1;
//	TRISFbits.TRISF4=1;
//	TRISFbits.TRISF2=1;
//	TRISDbits.TRISD10=1;
//	TRISDbits.TRISD0=1;	

	TRISDbits.TRISD3 =0;
	TRISDbits.TRISD12 =0;
	TRISDbits.TRISD13 =0;
	
	TRISDbits.TRISD4  =0;
	TRISDbits.TRISD5 =0;
	TRISGbits.TRISG13 =0;
	TRISFbits.TRISF0 =0;
	TRISFbits.TRISF1 =0;
	TRISGbits.TRISG1 =0;
	TRISGbits.TRISG0 =0;
	TRISGbits.TRISG14 =0;
	
	TRISGbits.TRISG12 =0;
	TRISDbits.TRISD2 =0;
	TRISDbits.TRISD1 =0;
	TRISGbits.TRISG15 =0;
	
	
	

	ODCDbits.ODCD3 =1;
	ODCDbits.ODCD12 =1;
	ODCDbits.ODCD13 =1;
	
	ODCDbits.ODCD4  =1;
	ODCDbits.ODCD5 =1;
	ODCGbits.ODCG13 =1;
	ODCFbits.ODCF0 =1;
	ODCFbits.ODCF1 =1;
	ODCGbits.ODCG1 =1;
	ODCGbits.ODCG0 =1;
	ODCGbits.ODCG14 =1;
	
	ODCGbits.ODCG12 =1;
	ODCDbits.ODCD2 =1;
	ODCDbits.ODCD1 =1;
//ODCGbits.ODCG15 =1;
	
	BKLT_CTRL = 1;

  	INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
	lhb_comm.flags.bits.rx_enable=1;	
//	RE_DE_TRIS = 0;
//	SEL_232_485_TRIS = 0;
	EN_SDFC_TRIS = 0;
//	DTR_PIN_TRIS = 1;
//
//	RE_DE = 0;
//	SEL_232_485 = 0;//SELECT RS232 IN MUX
	EN_SDFC = 0;//ENABLE ADM3307
	
//	analog_pin_tris = 1;
//	TEST_PIN_TRIS = 0;
//	TEST_PIN = 0;
}	
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void timer1_open(void)
{
    TMR1  = 0;          /* Reset Timer1 to 0x0000 */
	T1CONbits.TON = 0;
	T1CONbits.TCS=0;
	T1CONbits.TSYNC=0;
	T1CONbits.TCKPS=0;
	T1CONbits.TGATE=0;
	T1CONbits.TWIP=0;
	T1CONbits.TWDIS=0;
	T1CONbits.SIDL=0;
	T1CONbits.FRZ=0;
    PR1   = 32625;     //100us*80Mhz = 8000
	T1CONbits.TON = 1;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void timer1_int(void)
{
    IFS0bits.T1IF=0;
    IPC1bits.T1IP = 5;    /* assigning Interrupt Priority */
    IEC0bits.T1IE = 1; /* Interrupt Enable /Disable */
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void timer2_open(void)
{
    TMR2  = 0;
	T2CONbits.TON = 0;
	T2CONbits.TCS=0;
	T2CONbits.TCKPS=7;
	T2CONbits.TGATE=0;
	T2CONbits.SIDL=0;
//	T2CONbits.FRZ=0;
	T2CONbits.T32=0;
    PR2 = 15625;     //50ms*80Mhz = 4000000 / 256 = 15625 (prescalar enabled 1:256)
	T2CONbits.TON = 1;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void timer2_int(void)
{
    IFS0bits.T2IF=0;
    IPC2bits.T2IP = 3;
    IEC0bits.T2IE = 1;
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void init_uart1B(void) 
{
// configure U1BMODE
	U1BMODEbits.UARTEN = 0;	// Bit15 TX, RX DISABLED, ENABLE at end of func
	U1BMODEbits.USIDL = 0;	// Bit13 Continue in Idle
	U1BMODEbits.IREN = 0;	// Bit12 No IR translation
	U1BMODEbits.WAKE = 0;	// Bit7 No Wake up (since we don't sleep here)
	U1BMODEbits.LPBACK = 0;	// Bit6 No Loop Back
	U1BMODEbits.ABAUD = 0;	// Bit5 No Autobaud (would require sending '55')
	U1BMODEbits.RXINV = 0;	// Bit4 IdleState = 1  (for dsPIC)
	U1BMODEbits.BRGH = 1;	// Bit3 16 clocks per bit period
	U1BMODEbits.PDSEL = 0;	// Bits1,2 8bit, No Parity
	U1BMODEbits.STSEL = 0;	// Bit0 One Stop Bit

// Load all values in for U1BSTA SFR
	U1BSTAbits.UTXISEL1 = 0;	//Bit15 Int when Char is transferred (1/2 config!)
	U1BSTAbits.UTXINV = 0;	//Bit14 N/A, IRDA config
	U1BSTAbits.UTXISEL0 = 1;	//Bit13 Other half of Bit15				changed later to chk interrupt flag of tx
	U1BSTAbits.UTXBRK = 0;	//Bit11 Disabled
	U1BSTAbits.URXEN = 0;	//Bit12 RX pins controlled by periph
	U1BSTAbits.UTXEN = 0;	//Bit10 TX pins controlled by periph
	U1BSTAbits.UTXBF = 0;	//Bit9 *Read Only Bit*
	U1BSTAbits.TRMT = 0;	//Bit8 *Read Only bit*
	U1BSTAbits.URXISEL = 0;	//Bits6,7 Int. on character recieved
	U1BSTAbits.ADDEN = 0;	//Bit5 Address Detect Disabled
	U1BSTAbits.RIDLE = 0;	//Bit4 *Read Only Bit*
	U1BSTAbits.PERR = 0;		//Bit3 *Read Only Bit*
	U1BSTAbits.FERR = 0;		//Bit2 *Read Only Bit*
	U1BSTAbits.OERR = 0;		//Bit1 *Read Only Bit*
	U1BSTAbits.URXDA = 0;	//Bit0 *Read Only Bit*

	INTSetVectorPriority(INT_UART_1B_VECTOR, INT_PRIORITY_LEVEL_5);
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);

	IFS2bits.U1BTXIF = 0;	// Clear the Transmit Interrupt Flag
	IEC2bits.U1BTXIE = 0;	// Enable Transmit Interrupts
	IFS2bits.U1BRXIF = 0;	// Clear the Recieve Interrupt Flag
	IEC2bits.U1BRXIE = 1;	// Enable Recieve Interrupts
}	

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void usart1B_set_baud(ubyte rec_baud_rate)
{
	U1BMODEbits.UARTEN = 0;
	U1BSTAbits.OERR = 0;
	Nop();
	U1BBRG = baud_rate_lookup[rec_baud_rate];
	Nop();
	U1BMODEbits.UARTEN = 1;
	U1BSTAbits.URXEN = 1;	//Bit12 RX pins controlled by periph
	U1BSTAbits.UTXEN = 1;
	Nop();
}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void usart1B_write(unsigned char data)
{
	IFS2bits.U1BTXIF = 0;
	U1BTXREG = data;      // Write the data byte to the USART2
	while(!IFS2bits.U1BTXIF);
	IFS2bits.U1BTXIF = 0;
}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void usart1B_write_str(ubyte *rec_ram_add, uinteger rec_str_len, uinteger rec_ms_delay)
{
	while(rec_str_len--)
	{
		usart1B_write(*rec_ram_add++);
		Delays_Xtal_ms(rec_ms_delay);
	}
}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void usart1B_close(void)
{
	U1BMODEbits.UARTEN = 0;
}

//#######################################################################################
//
//if ( ReadEventWDT() )
//	{
//		// A WDT event did occur
//		 DisableWDT();
//		 ClearEventWDT();	// clear the WDT event flag so a subsequent event can set the event bit
//
//
//		// Blink all LEDs ON/OFF forever to indicate that we have seen WDT timeout reset.
//		while(1)
//		{
//			int i;
//
//			mPORTAToggleBits(BIT_7 | BIT_6 | BIT_5 | BIT_5 | BIT_4 | \
//								 BIT_3 | BIT_2 | BIT_1 | BIT_0 );
//
//			// Insert some delay
//			i = 1024*1024;
//			while(i--);
//		}
//	}
//




