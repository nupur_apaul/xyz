#include "..\inc\headers.h"


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//Function Name 			:Delay_ms()
//Input Parameters		:Void
//Output Parameters 		:Void
//Functional Description 	:This function provide a delay 		
//						of 1ms when crystal frequency sets to 64 MHz.	
//#######################################################################################
void Delay_ms(void)
{
	unsigned short delay_count = 1598;
	while(delay_count--);
}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//Function Name			:		Delay_Xtal_ms(freq,multiplier_ms)
//Input Parameters  		:		unsigned char freq,unsigned char multiplier_ms
//Output Parameters		:		Void
//Functional Description	:		
//#######################################################################################
void Delays_Xtal_ms(unsigned short multiplier_ms)
{
	while(multiplier_ms--)
	Delay_ms();
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//Function Name 			:Delay_us()
//Input Parameters		:Void
//Output Parameters 		:Void
//Functional Description 	:This function provide a delay 		
//						of 1us when crystal frequency sets to 4 MHz.	
//#######################################################################################
void Delay_us(void)
{
//	ubyte delay_temp = 3;
//	while(delay_temp--);
//	Nop();
//	Nop();
//	Nop();
}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//Function Name 			:Delay_us()
//Input Parameters		:Void
//Output Parameters 		:Void
//Functional Description 	:This function provide a delay of x ms when crystal 		
//						:frequency sets to 64 MHz.	
//#######################################################################################
void Delays_Xtal_us(unsigned short multiplier_us)
{
	while(multiplier_us--)
	{
		Nop();
		Nop();
		Nop();
	}
}


/*******************************************************************
						End of Delay.c
*******************************************************************/
