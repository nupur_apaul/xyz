#include "..\inc\headers.h"



#define		ROW_LENGTH				4
#define		COL_LENGTH				4
#define		KEYPAD_CNT				5



//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: unsigned char scan_keypad( urombyte *rec_keypad_ptr)
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned char scan_keypad(urombyte *rec_keypad_ptr)
{
	unsigned char row_num;
	unsigned char col_num = 3;
	unsigned char col_lookup[] = {0xFE,0xFD,0xFB,0xF7};

//	if ( ir_flag.bits.cmplt)
//	{
//		ir_flag.bits.cmplt=0;
//		key_num.bits.key_pressed = 1;
//		if(ir_key)
//			return(ir_key);
//	}
//

	key_num.bits.key_pressed = 0;
	key_num.bits.keyreleased = 0;
	
	key_num.bytes.port = 0xF0;
	keypad_update();
	
	if(!key_num.bits.row_1)		//detecting the row in which key is pressed
		row_num = 0;
	else if(!key_num.bits.row_2)
		row_num = 1;
	else if(!key_num.bits.row_3)
		row_num = 2;
	else if(!key_num.bits.row_4)
		row_num = 3;
	else 
		return(0);

	while(1)
	{
		WDTCLR   
		key_num.bytes.port = col_lookup[col_num];
		keypad_update();
		if(!key_num.bits.row_1)		//detecting the row in which key is pressed
			break;
		else if(!key_num.bits.row_2)
			break;
		else if(!key_num.bits.row_3)
			break;
		else if(!key_num.bits.row_4)
			break;
		
		else if(!col_num--)
			return(0);
	}
	key_num.bits.long_key_press = 0;
	key_long_press_timer = T3_DEL_1SEC;
	key_num.bits.key_pressed = 1;
	
	key_num.bytes.port = 0xF0;
	keypad_update();
	while(!key_num.bits.keyreleased);
	return(*(rec_keypad_ptr + (row_num * COL_LENGTH) + col_num));
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: keypad_update
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void keypad_update(void)
{
//	KEY5 = key_num.bits.col_1;
//	KEY6 = key_num.bits.col_2;
//	KEY7 = key_num.bits.col_3;
//	KEY8 = key_num.bits.col_4;
//	
//	Delays_Xtal_us(5);
//	
//	key_num.bits.row_4 = KEY1;
//	key_num.bits.row_3 = KEY2;
//	key_num.bits.row_2 = KEY3;
//	key_num.bits.row_1 = KEY4;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: scan_num_keypad
//	ARGUMENTS	: none
//	RETURN TYPE	: return the pressed key
//	DESCRIPTION	:

//#######################################################################################
ubyte scan_num_keypad(void)
{	
	unsigned char ret_val = 0;
	key_press = 0;
//	key_scan();

	if (keys[0].released)
		ret_val = 'U';				// 7 SET00 TEMP KEY
	else if (keys[1].released)
		ret_val = 'a';				// UP KEY
	else if (keys[2].released)
		ret_val = 'b';				// DN KEY
	else if (keys[3].released)
		ret_val = 'L';				// RIGHT SHIFT
	else if (keys[4].released)
		ret_val = 'e';				// ENTER/FUNCTION KEY
	else if (keys[5].released)
		ret_val = 'c';				// ACK KEY
	else if(keys[6].released)       //BACK KEY
	      ret_val = 'r';
	else
		ret_val = 0;
		
		
//	key_debounce_count = 5;
//	key_num.bits.key_debounce = 1;
//	key_num.bits.key_pressed = 0;
//	
//	
//	
//	if (!BUTTON1 && !key_num.bits.key_pressed) 
//	{
//		while(key_num.bits.key_debounce);
//		if(!BUTTON1 ) 	
//		{
//			ret_val = 'U';//7 SET TEMP KEY
//		}
//			
//	}		
// 	else if (!BUTTON2 && !key_num.bits.key_pressed) 
//	{
//		while(key_num.bits.key_debounce);
//		if(!BUTTON2)
// 			ret_val = 'a';//UP KEY
//	}		
//	else if (!BUTTON3 && !key_num.bits.key_pressed) 
//	{
//		while(key_num.bits.key_debounce);
//		if(!BUTTON3)
//			ret_val = 'b';//DN KEY
//	}		
//	else if (!BUTTON4 && !key_num.bits.key_pressed) 
//	{
//		while(key_num.bits.key_debounce);
//		if(!BUTTON4) 
//			ret_val = 'L';//RIGHT SHIFT
//	}		
//	else if (!BUTTON5 && !key_num.bits.key_pressed) 
//	{
//		while(key_num.bits.key_debounce);
//		if(!BUTTON5) 
//			ret_val = 'e';//ENTER/FUNCTION KEY
//	}		
//	else if (!BUTTON6 && !key_num.bits.key_pressed) 
//	{
//		while(key_num.bits.key_debounce);
//		if(!BUTTON6) 
//			ret_val = 'c';//ACK KEY
//	}		
//	else	
//		ret_val = 0;
//
	if(ret_val)
	{
		keys[0].released = 0;
		keys[1].released = 0;
		keys[2].released = 0;
		keys[3].released = 0;
		keys[4].released = 0;
		keys[5].released = 0;
		keys[6].released = 0;
		key_num.bits.key_pressed = 0;	
	}	
	
//	if( BUTTON1 && BUTTON2 && BUTTON3 && BUTTON4 && BUTTON5 && BUTTON6)
//		key_num.bits.key_pressed = 0;	
		
	return (ret_val);
//		return(scan_keypad(&alpha_keys_lookup[0][0][0]));
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: scan_alpha_keypad
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte scan_alpha_keypad(void)
{
	ubyte keypad_no = 0;
	ubyte temp_key_press, last_key_pressed;
	key_alpha.bits.key_pressed = 0;
	key_alpha.bits.timer_on = 0;
	temp_key_press = scan_keypad(&alpha_keys_lookup[0][0][0]);
	
	if(!key_num.bits.key_pressed)
		return(0);
	
	switch (temp_key_press)
	{
		case '1':
		{
//			if(key_alpha.bits.lcd_disp_on)
//				lcd_byte(temp_key_press, DATA);
			key_alpha.bits.key_pressed = 1;
			return(temp_key_press);
		}
		
		case 'a': case 'b': case 'c': case 'e': case '*': case '#':
		{
			key_alpha.bits.key_pressed = 1;
			return(temp_key_press);
		}
		
		case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': case '0':
		{
//			lcd_byte(temp_key_press, DATA);
			lcd_cursor_blink_off();
			alpha_key_timer = 10;
			key_alpha.bits.timer_on = 1;
			last_key_pressed = temp_key_press;
			temp_key_press = 0;
			keypad_no++;
			while(key_alpha.bits.timer_on)
			{
				temp_key_press = scan_keypad(&alpha_keys_lookup[keypad_no][0][0]);
				if(temp_key_press == 0 || temp_key_press == '$')
					continue;
				else if(temp_key_press == '*')
				{
					key_alpha.bits.timer_on = 0;
					break;
				}	
				else if(temp_key_press == 'C' || temp_key_press == 'F' || temp_key_press == 'I' || 
						temp_key_press == 'L' || temp_key_press == 'O' || temp_key_press == 'S' || 
						temp_key_press == 'Z' || temp_key_press == ' ')
					keypad_no = 0;
				last_key_pressed = temp_key_press;
				lcd_cursor_left();
//				lcd_byte(last_key_pressed, DATA);
				alpha_key_timer = 10;
				key_alpha.bits.timer_on = 1;
				keypad_no++;
			}
			lcd_cursor_blink_on();
			key_alpha.bits.key_pressed = 1;
			return(last_key_pressed);
		}
		default:
			return(0);
	}
	return(last_key_pressed);
}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: scan_string
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void scan_string(ubyte rec_key_type, ubyte rec_str_len)
{
	ubyte str_dig_counter = 0,temp_index=0;
	unsigned string_scan_status = 0;

	default_timer_count = T3_DEL_10SEC;
	orphan_flags.bits.default_timer = 1;
	orphan_flags.bits.default_time_out = 0;
	str_fill(scanned_strg, 16, LINE_FEED);
	key_press = 0;
	clear_keys_status();
	str_fill(scanned_strg,rec_str_len,'X');
	puts_graphical_lcd_ram_cnt(scanned_strg,0,HEADER_RES_SPACE+50,0,rec_str_len,arialNarrow_bld_25pix);
	
//	lcd_line_2();
//	lcd_cursor_blink_on();
	
	while(orphan_flags.bits.default_time_out == 0 && key_press!= 'c')
	{
		ClearWDT();
		if(!string_scan_status)
		{
			puts_graphical_lcd_ram_cnt(scanned_strg,0,HEADER_RES_SPACE+50,0,rec_str_len,arialNarrow_bld_25pix);
			string_scan_status = 1;
			clear_keys_status();
		}	

//		lcd_byte(0xC0 + str_dig_counter, COMMAND);

		default_timer_count = T3_DEL_10SEC;
		orphan_flags.bits.default_timer = 1;
		orphan_flags.bits.default_time_out = 0;
		key_alpha.bits.key_pressed = 0;
		key_press = 0;
		if(rec_key_type == ALPHANUMERIC)
			key_press = scan_alpha_keypad();
		else if(rec_key_type == NUMERIC)
		{
	
			key_press = scan_num_keypad();
			if(key_press == 'a')
				str_dig_counter++;
			else if(key_press == 'b')	
				str_dig_counter--;
		//	else if(key_press == 'e')//lakh
			else if(key_press == 'L')
			{
				if(scanned_strg[temp_index]!='X' )
				{
					str_dig_counter=0;
					temp_index++;
				}	
				if(temp_index == 4)//lakh
				{
				temp_index = 0;
				}
			}	
			else if(key_press == 'r')
				break;
			if(str_dig_counter==255)
				str_dig_counter=9;
			if(str_dig_counter>9)
				str_dig_counter=0;	
			if(key_press)
				string_scan_status = 0;	
		}
			
		if(!string_scan_status)
		{
//			if(key_press == '#')
//			{
//				str_dig_counter++;
//				if(str_dig_counter == rec_str_len)
//					str_dig_counter = 0;
//			}
//					
//			else if(key_press == '*')
//			{
//				if(str_dig_counter == 0)
//					str_dig_counter = rec_str_len;					
//				str_dig_counter--;
//			}
//			else if(key_press == 'e')
//			{
//				if(scanned_strg[0] != LINE_FEED)
//					break;
//			}
//			else if(key_press == 'c')
//			{
//				break;
//			}
//			else
			{
				scanned_strg[temp_index]=str_dig_counter+0x30;		
			//	if(temp_index == rec_str_len)
					if(key_press == 'e')
					break;
			}
			Nop();
		}
		Nop();
		
	}
	if(orphan_flags.bits.default_time_out)
		disp_time_out();
	Nop();
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: disp_time_out
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void disp_time_out(void)
{
#ifndef MENU_ON_LED
	home_clr();
	puts_lcd_rom_lim(disp_sorry_time_out, 0x00);
#endif
	delay_key_chk_ms(2000);	
}
//#######################################################################################
	

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_ir_key
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
//unsigned char get_ir_key(void)
//{	
//	int i = 0;
//	
//	for(i=0;i<=15;i++)
//		{
//			tmrir=code2.data[i].dbitl;
//			if(tmrir>55 && tmrir<88)
//			{
//				ircode = ircode << 1;
//				ircode = ircode & 0xfffe;
//				irdata = ircode;
//			}	
//			else if(tmrir>192 && tmrir<220)
//			{
//				ircode = ircode << 1;
//				ircode = ircode | 0x0001;
//				irdata = ircode;
//			}
//		}	
//	//ir_val=get_ir_ascii();
//	return(ir_ascii());
//}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: ir_ascii
//	ARGUMENTS	: 
//	RETURN TYPE	: unsigned char 
//	DESCRIPTION	: Return the ASCII value of the IR key pressed
//#######################################################################################
//unsigned char ir_ascii(void)
//{
//	switch(irdata)
//	  { 
//		case ZERO:
//			return('0');
//			//lcd_byte('0', DATA);
//		break;
//		case ONE:
//			return('1');
//			//lcd_byte('1', DATA);
//		break;
//		case TWO:
//			return('2');
//			//lcd_byte('2', DATA);
//		break;
//		case THREE:
//			return('3');
//			//lcd_byte('3', DATA);
//		break;
//		case FOUR:
//			return('4');
//			//lcd_byte('4', DATA);
//		break;
//		case FIVE:
//			return('5');
//			//lcd_byte('5', DATA);
//		break;
//		case SIX:
//			return('6');
//			//lcd_byte('6', DATA);
//		break;
//		case SEVEN:
//			return('7');
//			//lcd_byte('7', DATA);
//		break;
//		case EIGHT:
//			return('8');
//			//lcd_byte('8', DATA);
//		break;
//		case NINE:
//			return('9');
//			//lcd_byte('9', DATA);
//		break;
//		case ENTER:
//			return('e');
//			//lcd_byte('9', DATA);
//		break;
//		case LEFT:
//			return('*');
//			//lcd_byte('9', DATA);
//		break;
//		case RIGHT:
//			return('#');
//			//lcd_byte('9', DATA);
//		break;
//		case CANCEL:
//			return('c');
//			//lcd_byte('9', DATA);
//		break;
//		case UP:
//			return('a');
//			//lcd_byte('9', DATA);
//		break;
//		case DOWN:
//			return('b');
//			//lcd_byte('9', DATA);
//		break;
//	}	
////	key_count_byte++;
////	if(key_count_byte == 16)
////	{
////		lcd_clr_line_2(16);
////		key_count_byte = 0;
////		
////	}
//	return(0);
//}
//#######################################################################################

void clear_keys_status(void)
{

	unsigned char cnt = 0;
		
	for(cnt=0;cnt<7;cnt++)
	{
		keys[cnt].cont_press_pulse =0;
		keys[cnt].cont_press_timer =0;
		keys[cnt].continued_press =0;
		keys[cnt].long_press =0;
		keys[cnt].pressed =0;
		keys[cnt].released =0;
		keys[cnt].state =0;
		keys[cnt].timer_complete =0;
		keys[cnt].timer_count =0;
		keys[cnt].timer_on =0;
	}	
	key_num.bits.key_pressed = 0;	
	
}





