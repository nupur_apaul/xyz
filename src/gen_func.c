#include "..\inc\headers.h"


	urombyte crc_high_lookup[] = 
	{
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
	0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
	0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
	0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
	0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
	0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
	0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
	0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
	0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
	0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
	0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
	0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
	0x40
	};

urombyte crc_low_lookup[] = 
	{
	0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
	0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
	0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
	0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
	0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
	0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
	0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
	0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
	0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
	0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
	0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
	0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
	0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
	0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
	0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
	0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
	0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
	0x40
	};
//unsigned q_create(struct q_struct *rec_q, uword rec_flash_start_page, uinteger rec_pkt_size, uword rec_q_size, uinteger rec_flash_pg_size);
//unsigned q_chk_empty(struct q_struct *rec_q);
//void q_dispose(struct q_struct *rec_q);
//unsigned q_chk_full(struct q_struct *rec_q);
//unsigned q_enqueue(struct q_struct *rec_q, ubyte *rec_ram_add);
//unsigned q_dequeue(struct q_struct *rec_q, ubyte *rec_ram_add);

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: show_time_out
//	ARGUMENTS	: 
//	RETURN TYPE	:
//	DESCRIPTION	:


//#######################################################################################
void show_time_out(void)
{
	home_clr();
	puts_lcd_rom_lim(disp_sorry_time_out, 0x00);
	delay_key_chk_ms(2000);	
}
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: test_queue
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: delay_key_chk_ms
//	ARGUMENTS	: Key pad entries
//	RETURN TYPE	: None
//	DESCRIPTION	: 

//#######################################################################################
unsigned delay_key_chk_ms(uinteger rec_delay_ms)
{
	delay_timer_count = rec_delay_ms / 50;
	delay_timer_count++;
	orphan_flags.bits.delay_timer = 1;
	
	key_press = 0;
	while(orphan_flags.bits.delay_timer && !key_press)
		key_press = scan_num_keypad();
	
	orphan_flags.bits.delay_timer = 0;
	delay_timer_count = 0;
	if(key_press == 'c')
	{
		key_press = 0;
		return(0);
	}
	else
		key_press = 0;
	return(1);
}
//void test_queue(void)
//{
//////ubyte temp_array[FLASH_PAGE_SIZE_64MB];
////	ubyte temp_array[10];
////	uinteger temp_byte_fill;
////	uinteger temp_byte_pkt;
////	struct q_struct fifo 						__attribute__ ((aligned (4))) = {0};
////	uinteger q_start_add = 0;
////	uinteger pkt_size = 128;
////	uinteger flash_page_size = FLASH_PAGE_SIZE_64MB;
////	uword q_size = 25;
////	Nop();
////	Nop();
////	q_create(&fifo, q_start_add, pkt_size, q_size, flash_page_size);
////	
////	if(!q_chk_empty(&fifo))
////		while(1);
////	if(q_chk_full(&fifo))
////		while(1);
////	
////	for(temp_byte_pkt = 0; temp_byte_pkt < q_size; temp_byte_pkt++)
////	{
////		for(temp_byte_fill = temp_byte_pkt; temp_byte_fill < temp_byte_pkt + pkt_size; temp_byte_fill++)
////			temp_array[temp_byte_fill - temp_byte_pkt] = temp_byte_fill;
////		q_enqueue(&fifo, temp_array);
////		str_fill(temp_array, FLASH_PAGE_SIZE_64MB, 0x00);
////		read_page_64mb_flash(temp_array, 0);
////		read_page_64mb_flash(temp_array, 1);
////		read_page_64mb_flash(temp_array, 2);
////		Nop();
////		Nop();
////	}
////	for(temp_byte_pkt = 0; temp_byte_pkt < 5; temp_byte_pkt++)
////	{
////		q_dequeue(&fifo, temp_array);
////		str_fill(temp_array, FLASH_PAGE_SIZE_64MB, 0x00);
////	}
////	for(temp_byte_pkt = 0; temp_byte_pkt < 7; temp_byte_pkt++)
////	{
////		for(temp_byte_fill = temp_byte_pkt; temp_byte_fill < temp_byte_pkt + pkt_size; temp_byte_fill++)
////			temp_array[temp_byte_fill - temp_byte_pkt] = temp_byte_fill;
////		q_enqueue(&fifo, temp_array);
////		str_fill(temp_array, FLASH_PAGE_SIZE_64MB, 0x00);
////		read_page_64mb_flash(temp_array, 0);
////		read_page_64mb_flash(temp_array, 1);
////		read_page_64mb_flash(temp_array, 2);
////		read_page_64mb_flash(temp_array, 3);
////		Nop();
////		Nop();
////	}
////	for(temp_byte_pkt = 0; temp_byte_pkt < 5; temp_byte_pkt++)
////	{
////		q_dequeue(&fifo, temp_array);
////		str_fill(temp_array, FLASH_PAGE_SIZE_64MB, 0x00);
////	}
////	for(temp_byte_pkt = 0; temp_byte_pkt < 10; temp_byte_pkt++)
////	{
////		for(temp_byte_fill = temp_byte_pkt; temp_byte_fill < temp_byte_pkt + pkt_size; temp_byte_fill++)
////			temp_array[temp_byte_fill - temp_byte_pkt] = temp_byte_fill;
////		q_enqueue(&fifo, temp_array);
////		str_fill(temp_array, FLASH_PAGE_SIZE_64MB, 0x00);
////		read_page_64mb_flash(temp_array, 0);
////		read_page_64mb_flash(temp_array, 1);
////		read_page_64mb_flash(temp_array, 2);
////		read_page_64mb_flash(temp_array, 3);
////		Nop();
////		Nop();
////	}
////	for(temp_byte_pkt = 0; temp_byte_pkt < 30; temp_byte_pkt++)
////	{
////		q_dequeue(&fifo, temp_array);
////		str_fill(temp_array, FLASH_PAGE_SIZE_64MB, 0x00);
////	}
////	Nop();
////	Nop();
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: create_q
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################
//unsigned q_create(struct q_struct *rec_q, uword rec_flash_start_page, uinteger rec_pkt_size, uword rec_q_size, uinteger rec_flash_pg_size)
//{
////	rec_q->start_page = rec_flash_start_page;
////	rec_q->pkt_size = rec_pkt_size;
////	rec_q->qlimit = rec_q_size + 1;
////	q_dispose(rec_q);
////	rec_q->flash_page_size = rec_flash_pg_size;
//	return(1);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: q_dispose
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################
//void q_dispose(struct q_struct *rec_q)
//{
////	rec_q->qsize = 0;
////	rec_q->qrear = 0;
////	rec_q->qfront = 0;
//}	
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: q_empty
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################
//unsigned q_chk_empty(struct q_struct *rec_q)
//{
//	return(rec_q->qsize == 0);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: q_empty
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################
//unsigned q_chk_full(struct q_struct *rec_q)
//{
//	return(rec_q->qsize == (rec_q->qlimit - 1));
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: q_enqueue
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################
//unsigned q_enqueue(struct q_struct *rec_q, ubyte *rec_ram_add)
//{
////	SEL_FLASH_1
////
////	if(write_pkt_64mb_flash(rec_ram_add, rec_q->start_page, rec_q->qrear, rec_q->pkt_size))
////		disp_flash_error();
////
////	if(++rec_q->qrear == rec_q->qlimit)
////		rec_q->qrear = 0;
////		
////	if(!q_chk_full(rec_q))
////		rec_q->qsize++;
////		
////	else if(++rec_q->qfront == rec_q->qlimit)
////		rec_q->qfront = 0;
////	
//	return(1);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: q_dequeue
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################
//unsigned q_dequeue(struct q_struct *rec_q, ubyte *rec_ram_add)
//{
////	if(q_chk_empty(rec_q))
////		return(0);
////	else
////	{
////		SEL_FLASH_1
////		if(read_pkt_64mb_flash(rec_ram_add, rec_q->start_page, rec_q->qfront, rec_q->pkt_size))
////			return(0);
////			
////		rec_q->qsize--;
////		if(++rec_q->qfront == rec_q->qlimit)
////			rec_q->qfront = 0;
////	}
//	return(1);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: q_get_front
////	ARGUMENTS	: 
////	RETURN TYPE	:
////	DESCRIPTION	:
//
////#######################################################################################
//unsigned q_get_front(struct q_struct *rec_q, ubyte *rec_ram_add)
//{
////	if(q_chk_empty(rec_q))
////		return(0);
////
////	SEL_FLASH_1
////	if(read_pkt_64mb_flash(rec_ram_add, rec_q->start_page, rec_q->qfront, rec_q->pkt_size))
////		return(0);
//	
//	return(1);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: str_validate_delim
////	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
////	RETURN TYPE	:
////	DESCRIPTION	:
//
//#define		NOT_VALID			(unsigned)0
//#define		VALID				(unsigned)1
////#######################################################################################
//unsigned str_validate_delim(ubyte *res_ram_add, uinteger rec_count, ubyte rec_delim)
//{
//	while(--rec_count)
//	{
//		res_ram_add++;
//		if(*res_ram_add == rec_delim)
//			return(VALID);
//	}
//	return(NOT_VALID);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: str_validate_alpha
////	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
////	RETURN TYPE	:
////	DESCRIPTION	:
//
//#define		NOT_ALPHABET		(unsigned)0
//#define		ALPHABET			(unsigned)1
////#######################################################################################
//unsigned str_validate_alpha(ubyte *res_ram_add, ubyte rec_count, ubyte rec_delim)
//{
//	if(_IF_SPACE(*res_ram_add) || _IF_DASH(*res_ram_add) || *res_ram_add == rec_delim)
//		return(NOT_ALPHABET);
//
//	while(--rec_count)
//	{
//		res_ram_add++;
//		if(*res_ram_add == rec_delim)
//		{
//			if(!_IF_SPACE(*--res_ram_add))
//				return(ALPHABET);
//			return(NOT_ALPHABET);
//		}
//		if(!_IF_ALPHA(*res_ram_add))
//			return(NOT_ALPHABET);
//	}
//	return(NOT_ALPHABET);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: str_validate_num
////	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
////	RETURN TYPE	:
////	DESCRIPTION	:
//
//#define		NOT_NUMBER			(unsigned)0
//#define		NUMBER				(unsigned)1
////#######################################################################################
//unsigned str_validate_num(ubyte *res_ram_add, ubyte rec_count, ubyte rec_delim)
//{
//	if(_IF_SPACE(*res_ram_add) || _IF_DASH(*res_ram_add) || *res_ram_add == rec_delim)
//		return(NOT_NUMBER);
//
//	while(--rec_count)
//	{
//		res_ram_add++;
//		if(*res_ram_add == rec_delim)
//		{
//			if(!_IF_SPACE(*--res_ram_add))
//				return(NUMBER);
//			return(NOT_NUMBER);
//		}
//		if(!_IF_NUM(*res_ram_add))
//			return(NOT_NUMBER);
//	}
//	return(NOT_NUMBER);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: str_validate_alpha_num
////	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
////	RETURN TYPE	:
////	DESCRIPTION	:
//
//#define		NOT_ALPHANUM			(unsigned)0
//#define		ALPHANUM				(unsigned)1
////#######################################################################################
//unsigned str_validate_alpha_num(ubyte *res_ram_add, ubyte rec_count, ubyte rec_delim)
//{
//	if(_IF_SPACE(*res_ram_add) || _IF_DASH(*res_ram_add) || *res_ram_add == rec_delim)
//		return(NOT_ALPHABET);
//
//	while(--rec_count)
//	{
//		res_ram_add++;
//		if(*res_ram_add == rec_delim)
//		{
//			if(!_IF_SPACE(*--res_ram_add))
//				return(ALPHANUM);
//			return(NOT_ALPHANUM);
//		}
//		if(!_IF_ALPHA_NUM(*res_ram_add))
//			return(NOT_ALPHANUM);
//	}
//	return(NOT_ALPHANUM);
//}
////#######################################################################################
//
//
////#######################################################################################
////								FUNCTION DETAILS
////#######################################################################################
////	NAME		: str_validate_names
////	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
////	RETURN TYPE	:
////	DESCRIPTION	:
//
//#define		NOT_VALID			(unsigned)0
//#define		VALID				(unsigned)1
////#######################################################################################
//unsigned str_validate_names(ubyte *res_ram_add, ubyte rec_count, ubyte rec_delim)
//{
//	if(_IF_SPACE(*res_ram_add) || _IF_DASH(*res_ram_add) || *res_ram_add == rec_delim)
//		return(NOT_VALID);
//	
//	while(--rec_count)
//	{
//		res_ram_add++;
//		if(*res_ram_add == rec_delim)
//		{
//			if(!_IF_SPACE(*--res_ram_add))
//				return(VALID);
//			return(NOT_VALID);
//		}	
////		if(!_IF_SPACE(*res_ram_add) && !_IF_DASH(*res_ram_add) && !_IF_ALPHA_NUM(*res_ram_add) && _IF_SYMBOL(*res_ram_add))
//		if(!(_IF_SPACE(*res_ram_add) || _IF_DASH(*res_ram_add) || _IF_ALPHA_NUM(*res_ram_add) || _IF_COLON(*res_ram_add)))
//			return(NOT_VALID);
//	}
//	return(NOT_VALID);
//}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: fill_hash_wt_line_feed
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void fill_hash_wt_line_feed(ubyte *res_ram_add, ubyte rec_count)
{
	while(!rec_count)
	{
		if(*res_ram_add == '#')
			*res_ram_add = LINE_FEED;
		rec_count--;
		res_ram_add++;
	}	
	
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: dec_long_to_char
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte dec_long_to_char(uword *rec_src_ram_add, ubyte *rec_dest_ram_add)
{
	ubyte ascii_buff[10] = {0};
	ubyte ascii_buff_count = 0;
	uword rec_byte = 0;
	if(!*rec_src_ram_add)
	{
		*rec_dest_ram_add = 0x30;
		return(1);
	}
	rec_byte = *rec_src_ram_add;
	while(rec_byte)
	{
		ascii_buff[ascii_buff_count++] = (rec_byte % 10) + 0x30;
		rec_byte = rec_byte / 10;
	}
	rec_byte = ascii_buff_count;
	
	while(ascii_buff_count)
		*rec_dest_ram_add++ = ascii_buff[--ascii_buff_count];
	return(rec_byte);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: dec_int_to_char
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte dec_int_to_char(uinteger *rec_src_ram_add, ubyte *rec_dest_ram_add)
{
	ubyte ascii_buff[5] = {0};
	ubyte ascii_buff_count = 0;
	uinteger rec_byte = 0;
	if(!*rec_src_ram_add)
	{
		*rec_dest_ram_add = 0x30;
		return(1);
	}
	rec_byte = *rec_src_ram_add;
	while(rec_byte)
	{
		ascii_buff[ascii_buff_count++] = (rec_byte % 10) + 0x30;
		rec_byte = rec_byte / 10;
	}
	rec_byte = ascii_buff_count;
	
	while(ascii_buff_count)
		*rec_dest_ram_add++ = ascii_buff[--ascii_buff_count];
	return(rec_byte);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: dec_byte_to_char
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte dec_byte_to_char (ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add)
{
	ubyte ascii_buff[3] = {0};
	ubyte rec_byte;
	ubyte ascii_buff_count = 0;
	
	rec_byte = *rec_src_ram_add;
	if(!rec_byte)
	{
		*rec_dest_ram_add = 0x30;
		return(1);
	}
	while(rec_byte)
	{
		ascii_buff[ascii_buff_count++] = (rec_byte % 10) + 0x30;
		rec_byte = rec_byte / 10;
	}
	rec_byte = ascii_buff_count;
	
	while(ascii_buff_count)
		*rec_dest_ram_add++ = ascii_buff[--ascii_buff_count];
	return(rec_byte);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: char_to_dec_byte
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte dec_to_bcd(ubyte rec_byte)
{
	if(rec_byte > 99)
		return(255);
	return((rec_byte / 10) * 0x10) + (rec_byte % 10);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: char_to_dec_byte
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte bcd_to_dec(ubyte rec_byte)
{
	return((rec_byte / 0x10) * 10) + (rec_byte % 0x10);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: hex_byte_to_char
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void hex_byte_to_char(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add)
{
	rec_dest_ram_add[0] = *rec_src_ram_add / 0x10;
	rec_dest_ram_add[1] = *rec_src_ram_add % 0x10;

	if(rec_dest_ram_add[0] <= 0x09)
		rec_dest_ram_add[0] += 0x30;
	else
		rec_dest_ram_add[0] += 0x37;
		
	if(rec_dest_ram_add[1] <= 0x09)
		rec_dest_ram_add[1] += 0x30;
	else
		rec_dest_ram_add[1] += 0x37;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: hex_int_to_char
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void hex_int_to_char(uinteger *rec_src_ram_add, ubyte *rec_dest_ram_add)
{
	ubyte temp_byte;
	
	rec_dest_ram_add[0] = (ubyte)((*rec_src_ram_add / 0x1000));
	rec_dest_ram_add[1] = (ubyte)((*rec_src_ram_add / 0x0100) % 0x0010);
	rec_dest_ram_add[2] = (ubyte)((*rec_src_ram_add / 0x0010) % 0x0010);
	rec_dest_ram_add[3] = (ubyte)(*rec_src_ram_add % 0x0010);

	for(temp_byte = 0; temp_byte < 4; temp_byte++)
	{
		if(rec_dest_ram_add[temp_byte] <= 0x09)
			rec_dest_ram_add[temp_byte] += 0x30;
		else
			rec_dest_ram_add[temp_byte] += 0x37;
	}	
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: hex_int_to_char
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void hex_word_to_char(uword *rec_src_ram_add, ubyte *rec_dest_ram_add)
{
	ubyte temp_byte;
	
	rec_dest_ram_add[0] = (ubyte)((*rec_src_ram_add / 0x10000000));
	rec_dest_ram_add[1] = (ubyte)((*rec_src_ram_add / 0x01000000) % 0x00000010);
	rec_dest_ram_add[2] = (ubyte)((*rec_src_ram_add / 0x00100000) % 0x00000010);
	rec_dest_ram_add[3] = (ubyte)((*rec_src_ram_add / 0x00010000) % 0x00000010);
	rec_dest_ram_add[4] = (ubyte)((*rec_src_ram_add / 0x00001000) % 0x00000010);
	rec_dest_ram_add[5] = (ubyte)((*rec_src_ram_add / 0x00000100) % 0x00000010);
	rec_dest_ram_add[6] = (ubyte)((*rec_src_ram_add / 0x00000010) % 0x00000010);
	rec_dest_ram_add[7] = (ubyte)( *rec_src_ram_add % 0x00000010);

	for(temp_byte = 0; temp_byte < 8; temp_byte++)
	{
		if(rec_dest_ram_add[temp_byte] <= 0x09)
			rec_dest_ram_add[temp_byte] += 0x30;
		else
			rec_dest_ram_add[temp_byte] += 0x37;
	}	
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: char_to_hex_byte
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte char_to_hex_byte(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_delim)
{
	ubyte ascii_buff[2] = {0};
	ubyte ascii_buff_count = 0;
	ubyte dig_length = 0;
	
	while(*rec_src_ram_add != rec_delim)
	{
		*rec_src_ram_add++;
		ascii_buff_count++;
		if(ascii_buff_count > sizeof(ascii_buff))
		{
			*rec_dest_ram_add = 0;
			return(0);
		}	
	}
	
	for(dig_length = 0; dig_length < ascii_buff_count; dig_length++)
	{
		rec_src_ram_add--;
		if('0' <= *rec_src_ram_add && *rec_src_ram_add <= '9')	
			ascii_buff[(sizeof(ascii_buff) - 1) - dig_length] = (*rec_src_ram_add - '0');
		else
			ascii_buff[(sizeof(ascii_buff) - 1) - dig_length] = (*rec_src_ram_add - '7');
	}

	*rec_dest_ram_add = ((ascii_buff[0] << 4) | ascii_buff[1]);
	return(++dig_length);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: char_to_hex_int
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte char_to_hex_int(ubyte *rec_src_ram_add, uinteger *rec_dest_ram_add, ubyte rec_delim)
{
	ubyte ascii_buff[4] = {0};
	ubyte ascii_buff_count = 0;
	ubyte dig_length = 0;
	
	while(*rec_src_ram_add != rec_delim)
	{
		*rec_src_ram_add++;
		ascii_buff_count++;
		if(ascii_buff_count > sizeof(ascii_buff))
		{
			*rec_dest_ram_add = 0;
			return(0);
		}	
	}
	
	for(dig_length = 0; dig_length < ascii_buff_count; dig_length++)
	{
		rec_src_ram_add--;
		if('0' <= *rec_src_ram_add && *rec_src_ram_add <= '9')	
			ascii_buff[(sizeof(ascii_buff) - 1) - dig_length] = (*rec_src_ram_add - '0');
		else
			ascii_buff[(sizeof(ascii_buff) - 1) - dig_length] = (*rec_src_ram_add - '7');
	}


	*rec_dest_ram_add = (((uinteger)ascii_buff[0] << 12) |
						 ((uinteger)ascii_buff[1] << 8) |
						 ((uinteger)ascii_buff[2] << 4) |
						 ((uinteger)ascii_buff[3]));
	return(++dig_length);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: char_to_dec_byte
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte char_to_dec_byte(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_delim)
{
	ubyte ascii_buff[3] = {0};
	ubyte ascii_buff_count = 0;
	ubyte temp_count = 0;
	
	while(*rec_src_ram_add != rec_delim)
	{
		*rec_src_ram_add++;
		ascii_buff_count++;
		if(ascii_buff_count > sizeof(ascii_buff))
		{
			*rec_dest_ram_add = 0;
			return(0);
		}	
	}

	for(temp_count = 0; temp_count < ascii_buff_count; temp_count++)
		ascii_buff[(sizeof(ascii_buff) - 1) - temp_count] = (*--rec_src_ram_add - 48);
		
	rec_src_ram_add += ascii_buff_count + 1;
	*rec_dest_ram_add++ = (ascii_buff[0] * 100) + (ascii_buff[1] * 10) + ascii_buff[2];
	return(ascii_buff_count + 1);
}

ubyte char_to_dec_byte_cnt(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_cnt)
{
	ubyte ascii_buff[3] = {0};
	ubyte ascii_buff_count = 0;
	ubyte temp_count = 0;
	
	while(rec_cnt--)
	{
		*rec_src_ram_add++;
		ascii_buff_count++;
		if(ascii_buff_count > sizeof(ascii_buff))
		{
			*rec_dest_ram_add = 0;
			return(0);
		}	
	}

	for(temp_count = 0; temp_count < ascii_buff_count; temp_count++)
		ascii_buff[(sizeof(ascii_buff) - 1) - temp_count] = (*--rec_src_ram_add - 48);
		
	rec_src_ram_add += ascii_buff_count + 1;
	*rec_dest_ram_add++ = (ascii_buff[0] * 100) + (ascii_buff[1] * 10) + ascii_buff[2];
	return(ascii_buff_count + 1);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: char_to_dec_int
//	ARGUMENTS	: ubyte *rec_src_ram_add, uinteger *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte char_to_dec_int(ubyte *rec_src_ram_add, uinteger *rec_dest_ram_add, ubyte rec_delim)
{
	ubyte ascii_buff[5] = {0};
	ubyte ascii_buff_count = 0;
	ubyte temp_count = 0;
	
	while(*rec_src_ram_add != rec_delim)
	{
		*rec_src_ram_add++;
		ascii_buff_count++;
		if(ascii_buff_count > sizeof(ascii_buff))
		{
			*rec_dest_ram_add = 0;
			return(0);
		}	
	}
		
	for(temp_count = 0; temp_count < ascii_buff_count; temp_count++)
		ascii_buff[(sizeof(ascii_buff) - 1) - temp_count] = (*--rec_src_ram_add - 48);
			
	*rec_dest_ram_add = ((uinteger)ascii_buff[0] * 10000) + 
						((uinteger)ascii_buff[1] * 1000) + 
						((uinteger)ascii_buff[2] * 100) + 
						((uinteger)ascii_buff[3] * 10) + 
						((uinteger)ascii_buff[4]);
	return(ascii_buff_count + 1);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: char_to_dec_int
//	ARGUMENTS	: ubyte *rec_src_ram_add, uinteger *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte char_to_dec_long(ubyte *rec_src_ram_add, uword *rec_dest_ram_add, ubyte rec_delim)
{
	ubyte ascii_buff[10] = {0};
	ubyte ascii_buff_count = 0;
	ubyte temp_count = 0;
	uword char_to_long = 0;
	
	while(*rec_src_ram_add != rec_delim)
	{
		*rec_src_ram_add++;
		ascii_buff_count++;
		if(ascii_buff_count > sizeof(ascii_buff))
		{
			*rec_dest_ram_add = 0;
			return(0);
		}	
	}
		
	for(temp_count = 0; temp_count < ascii_buff_count; temp_count++)
		ascii_buff[(sizeof(ascii_buff) - 1) - temp_count] = (*--rec_src_ram_add - 48);
	
	char_to_long = char_to_long + ((uword)ascii_buff[0] * 1000000000);
	char_to_long = char_to_long + ((uword)ascii_buff[1] * 100000000);
	char_to_long = char_to_long + ((uword)ascii_buff[2] * 10000000);
	char_to_long = char_to_long + ((uword)ascii_buff[3] * 1000000);
	char_to_long = char_to_long + ((uword)ascii_buff[4] * 100000);
	char_to_long = char_to_long + ((uword)ascii_buff[5] * 10000);
	char_to_long = char_to_long + ((uword)ascii_buff[6] * 1000);
	char_to_long = char_to_long + ((uword)ascii_buff[7] * 100);
	char_to_long = char_to_long + ((uword)ascii_buff[8] * 10);
	char_to_long = char_to_long + ((uword)ascii_buff[9]);
	
	*rec_dest_ram_add = char_to_long;
	
	return(ascii_buff_count + 1);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: concat_byte_to_int
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
uinteger concat_byte_to_int(ubyte rec_byte_1, ubyte rec_byte_2 )
{
	uinteger int_1;
	int_1 = (uinteger)rec_byte_1;
	int_1 = int_1 * 256;
	int_1 = int_1 + (uinteger)rec_byte_2;
	return(int_1);
}	
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: conv_integer_to_bytes
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void conv_integer_to_bytes(uinteger int_1, ubyte *rec_byte_add)
{
	*(rec_byte_add + 1) = (ubyte) (int_1 % 256);
	int_1 = int_1 / 256;	
	*rec_byte_add = (ubyte) (int_1 % 256);
}
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_byte
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte get_byte (uinteger rec_delay_count)
{
//	unsigned long time_delay_count = (unsigned long)(rec_delay_count);
//	IFS0bits.U1RXIF = 0;
//	u1_rx_byte = 0;
//	time_delay_count *= (1000 / 7);
//	while(time_delay_count--)
//	{
//		if(IFS0bits.U1RXIF)
//		{
//			u1_rx_byte = U1RXREG;
//			IFS0bits.U1RXIF = 0;
//			return(u1_rx_byte);
//		}
//	Nop();
//	}
	return(0);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_stx_cmd
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned get_stx_cmd(uinteger rec_delay_ms)
{
//	unsigned short time_delay_count = 0;
//	handshake_timer_count = rec_delay_ms / 50;
//	db_upload.bits.handshake_timer = 1;
//	while(u1_rx_byte != STX && db_upload.bits.handshake_timer == 1);
//	Nop();
//	Nop();
//	Nop();
//	
//	
//	{
//		u1_rx_byte = 0;
//		Delays_Xtal_ms(1);
//		if(IFS0bits.U1RXIF)
//		{
//			u1_rx_byte = U1RXREG;
//			IFS0bits.U1RXIF = 0;
//		}
//		else
//			Nop();
//	}
//	if(u1_rx_byte == STX)
//		return(1);
//	else
//		return(0);
//		
//		
//		
//	IFS0bits.U1RXIF = 0;
//	u1_rx_byte = 0;
//	while(u1_rx_byte != STX && time_delay_count++ != rec_delay_ms)
//	{
//		u1_rx_byte = 0;
//		Delays_Xtal_ms(1);
//		if(IFS0bits.U1RXIF)
//		{
//			u1_rx_byte = U1RXREG;
//			IFS0bits.U1RXIF = 0;
//		}
//		else
//			Nop();
//	}
//	if(u1_rx_byte == STX)
//		return(1);
//	else
		return(0);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_ack_cmd
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte get_ack_cmd(uinteger rec_delay_ms)
{
//	unsigned short time_delay_count = 0;
//	IFS0bits.U1RXIF = 0;
//	u1_rx_byte = 0;
//	while(u1_rx_byte != ACK && u1_rx_byte != NAK && time_delay_count++ != rec_delay_ms)
//	{
//		u1_rx_byte = 0;
//		Delays_Xtal_ms(1);
//		if(IFS0bits.U1RXIF)
//		{
//			u1_rx_byte = U1RXREG;
//			IFS0bits.U1RXIF = 0;
//		}
//		else
//			Nop();
//	}
//	if(u1_rx_byte == ACK)
//		return(ACK);
//	else if(u1_rx_byte == NAK)
//		return(NAK);
//	else
		return(0);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_dle_cmd
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned get_dle_cmd(uinteger rec_delay_ms)
{
//	unsigned short time_delay_count = 0;
//	IFS0bits.U1RXIF = 0;
//	u1_rx_byte = 0;
//	while(u1_rx_byte != DLE && time_delay_count++ != rec_delay_ms)
//	{
//		u1_rx_byte = 0;
//		Delays_Xtal_ms(1);
//		if(IFS0bits.U1RXIF)
//		{
//			u1_rx_byte = U1RXREG;
//			IFS0bits.U1RXIF = 0;
//		}	
//	}
//	if(u1_rx_byte == DLE)
//		return(1);
//	else
		return(0);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: stx_hex_dle_cmd
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned stx_hex_dle_cmd(uinteger rec_delay_ms)
{
//	unsigned short time_delay_count = 0;
//	IFS0bits.U1RXIF = 0;
//	u1_rx_byte = 0;
//	while(u1_rx_byte != DLE && time_delay_count++ != rec_delay_ms)
//	{
//		u1_rx_byte = 0;
//		send_stx_usart3A();
//		Delays_Xtal_ms(1);
//		if(IFS0bits.U1RXIF)
//		{
//			u1_rx_byte = U1RXREG;
//			IFS0bits.U1RXIF = 0;
//		}
//	}
//	if(u1_rx_byte == DLE)
//		return(1);
//	else
		return(0);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: str_copy_ram_cnt
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
uinteger str_search_ram(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, uinteger rec_count, uinteger rec_dest_lim)
{
	uinteger dup_rec_dest_lim = rec_dest_lim;
	while(rec_dest_lim)
	{
		if(str_comp_ram(rec_src_ram_add, rec_dest_ram_add, rec_count))
			return(dup_rec_dest_lim - rec_dest_lim);
		while(--rec_dest_lim && (*rec_src_ram_add != *++rec_dest_ram_add));
	}
	return(0xFFFF);	
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: str_copy_ram_cnt
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void str_copy_ram_cnt(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, uinteger rec_count)
{
	while(rec_count--)
		*rec_dest_ram_add++ = *rec_src_ram_add++;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: str_copy_ram_int
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void str_copy_ram_int(uinteger *rec_src_ram_add, uinteger *rec_dest_ram_add, uinteger rec_count)
{
	while(rec_count--)
		*rec_dest_ram_add++ = *rec_src_ram_add++;
}
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_copy_rom_cnt
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void str_copy_rom_cnt(urombyte *rec_src_rom_add, ubyte *rec_dest_ram_add, uinteger rec_count)
{
	while(rec_count--)
		*rec_dest_ram_add++ = *rec_src_rom_add++;
}
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_copy_ram_lim
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void str_copy_ram_lim(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_delimitter)
{
	while(*rec_src_ram_add != rec_delimitter)
		*rec_dest_ram_add++ = *rec_src_ram_add++;
}
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_copy_ram_lim
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
uinteger str_copy_ram_lim_ret(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_delimitter)
{
	ubyte *rec_src_add = rec_src_ram_add;
	while(*rec_src_ram_add != rec_delimitter)
		*rec_dest_ram_add++ = *rec_src_ram_add++;
	return (rec_src_ram_add - rec_src_add + 1);
}
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_copy_rom_lim
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
uinteger str_copy_rom_lim_ret(urombyte *rec_src_rom_add, ubyte *rec_dest_ram_add, ubyte rec_delimitter)
{
	urombyte *rec_src_add = rec_src_rom_add;
	while(*rec_src_rom_add != rec_delimitter)
		*rec_dest_ram_add++ = *rec_src_rom_add++;
	return (rec_src_rom_add - rec_src_add + 1);
}
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_copy_rom_lim
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void str_copy_rom_lim(urombyte *rec_src_rom_add, ubyte *rec_dest_ram_add, ubyte rec_delimitter)
{
	while(*rec_src_rom_add != rec_delimitter)
		*rec_dest_ram_add++ = *rec_src_rom_add++;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_comp_ram_lim
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned str_comp_ram_lim(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, uinteger rec_delim)
{
	while(*rec_src_ram_add != rec_delim)
	{
		if(*rec_dest_ram_add++ != *rec_src_ram_add++)
			return(0);
	}
	return(1);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_comp_ram
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned str_comp_ram(ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add, uinteger rec_count)
{
	while(rec_count--)
	{
		if(*rec_dest_ram_add++ != *rec_src_ram_add++)
			return(0);
	}
	return(1);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_comp_rom
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned str_comp_rom(urombyte *rec_src_rom_add, ubyte *rec_dest_ram_add, uinteger rec_count)
{
	while(rec_count--)
	{
		if(*rec_dest_ram_add++ != *rec_src_rom_add++)
			return(0);
	}
	return(1);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_fill_int
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void str_fill_int(uinteger *rec_ram_add, uinteger rec_count, uinteger rec_char)
{
	while(rec_count--)
		*rec_ram_add++ = rec_char;
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_fill_int
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void str_fill_word(uword *rec_ram_add, uinteger rec_count, uword rec_char)
{
	while(rec_count--)
		*rec_ram_add++ = rec_char;
}
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:str_fill
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void str_fill(ubyte *rec_ram_add, uinteger rec_count, ubyte rec_char)
{
	while(rec_count--)
		*rec_ram_add++ = rec_char;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_int_str_len
//	ARGUMENTS	: Key pad entries
//	RETURN TYPE	: None
//	DESCRIPTION	: 

//#######################################################################################
uinteger get_int_str_len(ubyte *rec_ram_add, ubyte rec_delim)	
{
	ubyte *temp_ram_add = rec_ram_add;
	while(*rec_ram_add++ != rec_delim);
	return(--rec_ram_add - temp_ram_add);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_train_dig_count
//	ARGUMENTS	: Key pad entries
//	RETURN TYPE	: None
//	DESCRIPTION	: 

//#######################################################################################
ubyte get_str_len(ubyte *rec_ram_add, ubyte rec_delim)	
{
	ubyte *temp_ram_add = rec_ram_add;
	while(*rec_ram_add++ != rec_delim);
	return(--rec_ram_add - temp_ram_add);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:send_stx_usart3A
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_stx_usart3A(void)
	{	
//	usart3A_write(STX);		// transmitting data ubyte	
	}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:send_dle_usart1
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_dle_usart1 (void)
	{	
//	usart3A_write(DLE);		// transmitting data ubyte	
	}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:send_stx_usart2
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_etx_usart3A (void)
	{	
//	usart3A_write(ETX);		// transmitting data byte	
	}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:send_nak_usart3A
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_nak_usart3A (void)
	{	
//	usart3A_write(NAK);		// transmitting data byte	
	}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:send_stx_usart2
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_stx_usart2A (void)
	{	
//	usart2A_write(STX);		// transmitting data ubyte	
	}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:send_dle_usart2A
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_dle_usart2A (void)
	{	
//	usart2A_write(DLE);		// transmitting data ubyte	
	}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:send_etx_usart2A
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_etx_usart2A (void)
	{	
//	usart2A_write(ETX);		// transmitting data byte	
	}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:send_nak_usart2A
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_nak_usart2A (void)
{	
//	usart2A_write(NAK);		// transmitting data byte	
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: verify_crc
//	ARGUMENTS	: Key pad entries
//	RETURN TYPE	: None
//	DESCRIPTION	: 

//#######################################################################################
unsigned verify_crc(ubyte *rec_buff_address, uinteger rec_buff_length)
{
	ubyte dup_crc_low,dup_crc_high;
	
	dup_crc_high = *(rec_buff_address + rec_buff_length);
	dup_crc_low = *(rec_buff_address + rec_buff_length + 1);
	crc_generate_modbus(rec_buff_address, rec_buff_length);
	if((dup_crc_high == *(rec_buff_address + rec_buff_length)) && (dup_crc_low == *(rec_buff_address + rec_buff_length + 1)))
	{
		if((prev_dup_crc_high != dup_crc_high) && (prev_dup_crc_low != dup_crc_low))
			rx.data.new_pkt_rcvd = 1;	
		
		prev_dup_crc_high = dup_crc_high;
		prev_dup_crc_low = dup_crc_low;
		return (1);
	}	
	else 
	{
		prev_dup_crc_high = dup_crc_high;
		prev_dup_crc_low = dup_crc_low;
		return (0);
	}	
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:crc_generate
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void crc_generate(ubyte *rec_buff_address, uinteger rec_buff_length)
{															// CRC_initialization
	ubyte dup_crc_byte = 0;
	ubyte crc_high = 0;
	ubyte crc_low = 0;
	ubyte crc_byte = 0;
	ubyte temp = 0;                  							// rec_buff_length 
	
	if(rec_buff_length >= 3)										// if data bytes > 2 no need to append zero, here
	{
		dup_crc_byte = crc_byte  = *rec_buff_address++;				// CRC buff is initialized by first data byte
		crc_high =	*rec_buff_address++;							// CRC HIGH is initialized by second data byte
		crc_low   =	*rec_buff_address;							// CRC LOW is initialized by third data byte
		rec_buff_length = rec_buff_length - 2;	
		temp = 0x02;
	}
	else if(rec_buff_length == 2)									// if data bytes = 2 append one zero
	{
		dup_crc_byte = crc_byte = *rec_buff_address++;				// CRC buff is initialized by first data byte
		crc_high = *rec_buff_address++;							// CRC HIGH is initialized by second data byte
		crc_low = 0x00;									// CRC LOW is initialized by ZERO
		rec_buff_length = 1;				
		temp=0x02;
	}
	else if(rec_buff_length==1)									// if data bytes = 1 append two zeros
	{
		dup_crc_byte = crc_byte = *rec_buff_address++;				// CRC buff is initialized by the data byte
		crc_high =	0x00;									// CRC HIGH is initialized by ZERO
		*rec_buff_address++;	
		crc_low = 0x00;									// CRC LOW is initialized by ZERO
		rec_buff_length = 0x00;					
		temp=0x01;
	}
// Check if last crc_byte points to last element in table .These elements
// cannot  be read from the lookup table,because they are beyond the program memory page.
append_zero:
	while(rec_buff_length != 0x00)
	{
		dup_crc_byte = dup_crc_byte ^ 0xFF;					// checking if the byte is 0xFF
		if(dup_crc_byte == 0x00)
		{
			crc_high = crc_high ^ LAST_TABLE_ELEMENT_HIGH; 	// yes ,get the last element for high byte ,XOR  with high byte.
			crc_low = crc_low ^ LAST_TABLE_ELEMENT_LOW;		// get last element for low byte  ,XOR with low byte.	
			rec_buff_length--;
			dup_crc_byte = crc_byte = crc_high;		 		// copy high byte to crc_byte.
			crc_high = crc_low;								// copy low byte to crc_high.
			*rec_buff_address++;											// point to the next data byte.
			crc_low = *rec_buff_address;							// offset value is loaded.	
			goto append_zero;
		}
		crc_high = look_table_high[crc_byte] ^ crc_high; 	// get value for high byte
		// XOR table element with high byte.
		crc_low	 = look_table_low[crc_byte] ^ crc_low; 		// get value for low byte												
		rec_buff_length--;										// decrement data byte.
		dup_crc_byte = crc_byte = crc_high;					// copy high byte to crc_byte.
		crc_high = crc_low;								// copy low byte to crc_high.
		*rec_buff_address++;												// point to the next data byte.
		crc_low = *rec_buff_address;							// offset value is loaded.
	}
	while(temp != 0x00)										// ie 3-1=2 append two bytes with zeros.
	{
		crc_low = 0x00;										// appending zero after the transmit_buff bytes
		rec_buff_length++;										// increment for additional iteration
		temp--;	
		goto append_zero;
	}
	crc_low	= crc_high;
	crc_high = crc_byte;				//to be appended before transmitting the transmit_buff.
	*rec_buff_address--;
	*rec_buff_address--;
	*rec_buff_address = crc_high;				//append at th last of buffer.
	*rec_buff_address++;
	*rec_buff_address = crc_low;
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: get_chk_sum
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
unsigned get_chk_sum(ubyte *rec_start_add, uinteger rec_byte_count)
{
	ubyte chk_sum = 0;
	while(rec_byte_count--)
		chk_sum = chk_sum ^ *rec_start_add++;
	return(chk_sum);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: add_chk_sum
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void add_chk_sum(ubyte *rec_start_add, uinteger rec_byte_count)
{
	ubyte chk_sum = 0;
	while(rec_byte_count--)
		chk_sum = chk_sum ^ *rec_start_add++;
	*rec_start_add = chk_sum;
}
//#######################################################################################

//#######################################################################################




//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: show_invalid_entry
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void show_invalid_entry(void)
{
	home_clr();
	puts_lcd_rom_lim(disp_invalid_entry, 0x00);
//	lcd_line_2();
	puts_lcd_rom_lim(disp_try_again, 0x00);
	delay_key_chk_ms(1000);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: show_invalid_filename
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void show_invalid_filename(void)
{
//	home_clr();
//	puts_lcd_rom_lim((urombyte *)"INVALID FILENAME", 0x00);
//	lcd_line_2();
//	puts_lcd_rom_lim(disp_try_again, 0x00);
//	delay_key_chk_ms(1000);
//	while(1);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: show_wrong_pswd
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void show_wrong_pswd(void)
{
	home_clr();
	puts_lcd_rom_lim(disp_pswd_not_ok, 0x00);
//	lcd_line_2();
	puts_lcd_rom_lim(disp_try_again, 0x00);
	delay_key_chk_ms(1000);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: show_file_open_error
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void show_file_open_error(ubyte rec_file_num)
{
//	ubyte temp_num_char[3];
//	home_clr();
//	puts_graphical_lcd_ram_cnt( temp_num_char,0,HEADER_RES_SPACE,0,dec_byte_to_char( &rec_file_num, temp_num_char),arialNarrow_bld_25pix);
//	puts_graphical_lcd_rom_lim(disp_file_open_error,0,HEADER_RES_SPACE+35,0,0x00,arialNarrow_bld_25pix);
//	delay_key_chk_ms(1000);
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: show_usb_error
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void show_usb_error(ubyte rec_error_num)
{
//	home_clr();
//	puts_graphical_lcd_rom_lim( (urombyte *)(usb_errors[rec_error_num]),0,HEADER_RES_SPACE,0,0x00,arialNarrow_bld_25pix);
//	puts_graphical_lcd_rom_lim(disp_try_again,0,HEADER_RES_SPACE+35,0,0x00,arialNarrow_bld_25pix);
//	delay_key_chk_ms(10000);
//	SoftReset();
}
//#######################################################################################
//#######################################################################################
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: verify_crc
//	ARGUMENTS	: Key pad entries
//	RETURN TYPE	: None
//	DESCRIPTION	: 

//#######################################################################################
unsigned verify_crc_modbus(ubyte *rec_buff_address, uinteger rec_buff_length)
{
	static ubyte dup_crc_low,dup_crc_high;
	
	dup_crc_high = *(rec_buff_address + rec_buff_length);
	dup_crc_low = *(rec_buff_address + rec_buff_length + 1);
	crc_generate_modbus(rec_buff_address, rec_buff_length);
	if((dup_crc_high == *(rec_buff_address + rec_buff_length)) && (dup_crc_low == *(rec_buff_address + rec_buff_length + 1)))
	{
		if((prev_dup_crc_high != dup_crc_high) && (prev_dup_crc_low != dup_crc_low))
			rx.data.new_pkt_rcvd = 1;	
		
		prev_dup_crc_high = dup_crc_high;
		prev_dup_crc_low = dup_crc_low;
		return (1);
	}	
	else 
	{
		prev_dup_crc_high = dup_crc_high;
		prev_dup_crc_low = dup_crc_low;
		return (0);
	}	
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:crc_generate
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void crc_generate_modbus(ubyte *rec_buff_address, uinteger rec_buff_length)
{
	ubyte crc_high = 0xFF;
	ubyte crc_low = 0xFF;
	ubyte crc_index;

	while(rec_buff_length--)
	{
		crc_index = crc_low ^ *rec_buff_address++;
		crc_low = crc_high ^ crc_high_lookup[crc_index];
		crc_high = crc_low_lookup[crc_index];
	}
	*rec_buff_address++ = crc_low;
	*rec_buff_address = crc_high;
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		: dec_int_to_char_fixed
//	ARGUMENTS	: ubyte *rec_src_ram_add, ubyte *rec_dest_ram_add
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte dec_int_to_char_fixed(uinteger *rec_src_ram_add, ubyte *rec_dest_ram_add, ubyte rec_dig_count)
{
	ubyte ascii_buff[rec_dig_count];
	ubyte ascii_buff_count = 0;
	uinteger rec_byte;

	str_fill( ascii_buff, rec_dig_count, '0');
	rec_byte = *rec_src_ram_add;
	while(rec_byte)
	{
		ascii_buff[ascii_buff_count++] = (rec_byte % 10) + 0x30;
		rec_byte = rec_byte / 10;
	}
	rec_byte = rec_dig_count;
	while(rec_dig_count)
		*rec_dest_ram_add++ = ascii_buff[--rec_dig_count];
	return(rec_dig_count);
}







