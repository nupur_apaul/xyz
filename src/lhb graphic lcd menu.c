#include "..\inc\headers.h"

#pragma config UPLLEN   = ON            // USB PLL Enabled
#pragma config FPLLMUL  = MUL_20        // PLL Multiplier
#pragma config UPLLIDIV = DIV_4         // USB PLL Input Divider
#pragma config FPLLIDIV = DIV_4         // PLL Input Divider
#pragma config FPLLODIV = DIV_1         // PLL Output Divider
#pragma config FPBDIV   = DIV_1         // Peripheral Clock divisor
#pragma config FWDTEN   = OFF           // Watchdog Timer 
#pragma config WDTPS    = PS4096        // Watchdog Timer Postscale
#pragma config FCKSM    = CSDCMD        // Clock Switching & Fail Safe Clock Monitor
#pragma config OSCIOFNC = OFF           // CLKO Enable
#pragma config POSCMOD  = HS            // Primary Oscillator
#pragma config IESO     = OFF           // Internal/External Switch-over
#pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable
#pragma config FNOSC    = PRIPLL        // Oscillator Selection
#pragma config CP       = OFF           // Code Protect
#pragma config BWP      = OFF           // Boot Flash Write Protect
#pragma config PWP      = OFF           // Program Flash Write Protect
#pragma config ICESEL   = ICS_PGx2      // ICE/ICD Comm Channel Select
#pragma config DEBUG    = OFF           // Debugger Disabled for Starter Kit
#pragma config FVBUSONIO    = OFF       // VBUSON not controlled by USB module,used as IO

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
uinteger size_of_menu;
ubyte *address_of_menu;
ubyte string_index;	                                                                   
int main(void)
{
	init_peripheral();	
	IEC0bits.T2IE = 0;
	initGraphicsLCD();
	home_clr();
	init_dma_tx_lhb();
	IEC0bits.T2IE = 1;
	system_status.bits.power_up = 1;
	stat.bits.refresh_display = 1;
	stat.bits.use_up_dwn_keys = 1;
//	display_set_temp_values_screen();
//	change_server_settings();
	while(1)
	{
		WDTCLR
		key_press = 0;
		clear_keys_status();
		home_clr();
//		Delays_Xtal_ms(1000);
		load_default_screen_data();
		if( keys[0].long_press )// 7 set temp 
		{	
			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
		}
		else if(key_press == 'e')// enter
		{			
			//home_clr();	
            //display_apaul_graphics();
            menu_mode_options();
		}
		if(key_press == 'U')
			display_set_temp_selection_screen();
		new_pkt_rdy_to_be_sent = 0;
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################

void init_peripheral(void)
{
	Init_Port();
	init_uart1B();// This does not enables the UART module. Enable through function usart3_set_baud();
	usart1B_set_baud(BAUD_RATE_115200);
	timer2_open();
	timer2_int();
	timer1_open();
	timer1_int();

}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
/* menu actions that do nothing call this function */
void not_handled(void)
{
	
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void (*const menu_function[])(void) = 
{
	not_handled
};
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_default_screen_data(void)
{	ubyte total_chars = 0;
	ubyte temp_cnt  = 0;
	ubyte slash_char_cnt = 0;
previous_menu_selection = menu_selection;
	menu_selection = DEFAULT_SCREEN_MENU;
	IEC0bits.T2IE = 1;
	
	while(1)
	{
		WDTCLR
		if( frame_flags.bits.default_frame_rcvd)
		{
			puts_graphical_lcd_rom_lim(disp_date,0,0,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[0]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.date.byte,default_screen_putdata_x[0],0,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_time,16,0,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[1]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.time.byte,default_screen_putdata_x[1],0,0,',',arialNarrow_bld_13pix);

			puts_graphical_lcd_rom_lim(disp_coach_no,0,13,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[2]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.coach_no,default_screen_putdata_x[2],13,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("UNIT NO:",13,13,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[10]= last_x_pos;
			puts_graphical_lcd_ram_cnt(coach_info.unit_no,default_screen_putdata_x[10],13,0,12,arialNarrow_bld_13pix);

//			puts_graphical_lcd_rom_lim(disp_coach_no,0,13,0,0,arialNarrow_bld_13pix);
//			default_screen_putdata_x[2]= last_x_pos;
//			puts_graphical_lcd_ram_lim(coach_info.coach_no,default_screen_putdata_x[2],13,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_rmpu_make,0,13*2,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[3]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.rmpu_make_pp_npp,default_screen_putdata_x[3]-1,13*2,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_rmpu_no_pp,0,13*3,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[4]= last_x_pos;

			puts_graphical_lcd_ram_cnt(&coach_info.rmpu_no_pp_npp[0],14,13*3,0,12,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_rmpu_no_npp,0,13*4,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[5]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&coach_info.rmpu_no_pp_npp[13],14,13*4,0,12,arialNarrow_bld_13pix);
			
			puts_graphical_lcd_rom_lim(disp_lhb_sw_ver,0,13*5,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[6]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.sw_ver,default_screen_putdata_x[6],13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim((urombyte *)"LCD Ver:",last_x_pos+1,13*5,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim((urombyte *)disp_sw_vrs,last_x_pos,13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_operational_mode,0,13*6,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[7]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.plant_opr_mode,default_screen_putdata_x[7],13*6,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_curr_flt1,0,13*7,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[8]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.fault[0],default_screen_putdata_x[8],13*7,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_curr_flt2,0,13*8,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[9]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.fault[1],default_screen_putdata_x[9],13*8,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_press_menu_for_options,0,13*9,0,0,arialNarrow_bld_13pix);
	
			IEC0bits.T2IE = 1;
			stat.bits.data_received = 0;
			frame_flags.integer = 0;
			break;	
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
		
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;
		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			load_default_screen_data();
		}	
	}	

	while( !keys[0].long_press && key_press != 'e' )
	{
		WDTCLR

		key_press = 0;
		key_press = scan_num_keypad();

		if(key_press == 'U')
			display_set_temp_selection_screen();

		if( frame_flags.bits.default_frame_rcvd)
		{
			IEC0bits.T2IE = 0;
			puts_graphical_lcd_ram_lim(coach_info.date.byte,default_screen_putdata_x[0],0,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(coach_info.time.byte,default_screen_putdata_x[1],0,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(coach_info.coach_no,default_screen_putdata_x[2],13,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(coach_info.unit_no,default_screen_putdata_x[10],13,0,12,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_coach_no,0,13,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(coach_info.rmpu_make_pp_npp,default_screen_putdata_x[3]-1,13*2,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&coach_info.rmpu_no_pp_npp[0],14,13*3,0,12,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&coach_info.rmpu_no_pp_npp[13],14,13*4,0,12,arialNarrow_bld_13pix);
			
			puts_graphical_lcd_ram_lim(coach_info.sw_ver,default_screen_putdata_x[6],13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(coach_info.plant_opr_mode,default_screen_putdata_x[7],13*6,0,',',arialNarrow_bld_13pix);
		
			if(coach_info.fault[0][0]!=' ')
			{
				if(timer2.stat.bits.blinker_status)
				puts_graphical_lcd_ram_lim(coach_info.fault[0],default_screen_putdata_x[8],13*7,0,',',arialNarrow_bld_13pix);
				else
				puts_graphical_lcd_ram_lim(coach_info.fault[0],default_screen_putdata_x[8],13*7,1,',',arialNarrow_bld_13pix);
				
			}
			else
			puts_graphical_lcd_ram_lim(coach_info.fault[0],default_screen_putdata_x[8],13*7,0,',',arialNarrow_bld_13pix);
			if(coach_info.fault[1][0]!=' ')
			{
				if(timer2.stat.bits.blinker_status)
				puts_graphical_lcd_ram_lim(coach_info.fault[1],default_screen_putdata_x[9],13*8,0,',',arialNarrow_bld_13pix);
				else
				puts_graphical_lcd_ram_lim(coach_info.fault[1],default_screen_putdata_x[9],13*8,1,',',arialNarrow_bld_13pix);
				
			}
			else	
			puts_graphical_lcd_ram_lim(coach_info.fault[1],default_screen_putdata_x[9],13*8,0,',',arialNarrow_bld_13pix);

			IEC0bits.T2IE = 1;
			stat.bits.data_received = 0;
			frame_flags.integer = 0;	
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;
		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			load_default_screen_data();
		}	
				
	}
}
void display_apaul_graphics(void) {
    
//    ubyte total_chars = 0;
//    
//	ubyte temp_cnt  = 0;
//    
//	ubyte slash_char_cnt = 0;
//    
//    previous_menu_selection = menu_selection;
//    
//	menu_selection = DEFAULT_SCREEN_MENU;
//    
//    while(1) {
//        
//      WDTCLR
//      
//      puts_graphical_lcd_graphics(0,0,0,0,15,0);
//      
//        key_press = 0;
//		key_press = scan_num_keypad();
//        
//        
//      if(key_press == 'c' || key_press =='r')
//		{
//			home_clr();	
//			load_default_screen_data();
//		}	
//		if(keys[5].long_press)
//			load_pressure_status_screen();
//		if(keys[0].long_press)
//		{
//			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
//		}
//        
//        check_fn_code_send_pkt_to_lhb();	
//    }	
    
    
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void display_set_temp_selection_screen(void)
{
	ubyte temp_byte = 0;
	ubyte first_packet;
previous_menu_selection =  menu_selection;
	menu_selection = SEVEN_POINT_SELECT_MENU;

	IEC0bits.T2IE = 0;	
	key_press = 0;	
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	IEC0bits.T2IE = 1;
	temp_byte = '1';
	first_packet = 0;
	
	while(1)
	{
		WDTCLR
		if(frame_flags.bits.view_set_point_frame_rcvd  && rx.data.new_pkt_rcvd)
		{
//			if(stat.bits.use_up_dwn_keys)
//			{
//				puts_graphical_lcd_rom_lim(disp_temp_set_pt,0,0,0,0,arialNarrow_bld_13pix);
//				sevenpt_select_screen_putdata_x[0]= last_x_pos;
//			}	
	
//			puts_graphical_lcd_rom_lim(disp_set_pt,0,13*1,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_set_pt,0,0,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[1]= last_x_pos;
//			puts_graphical_lcd_ram_cnt(&set.temp.curr_set_point_no,sevenpt_select_screen_putdata_x[1],13*1,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&set.temp.curr_set_point_no,sevenpt_select_screen_putdata_x[1],0,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_coolsp,0,13*1,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[2]= 0;
			puts_graphical_lcd_ram_lim(set.temp.curr_cool,sevenpt_select_screen_putdata_x[2],13*2,0,',',arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[3]= 10;
			puts_graphical_lcd_ram_lim(set.temp.curr_heat,sevenpt_select_screen_putdata_x[3],13*2,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,15,13*4,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[8]= last_x_pos+1;
			puts_graphical_lcd_ram_lim(&set.temp.loop[0].byte[0],sevenpt_select_screen_putdata_x[8],13*4,0,',',arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[9]= last_x_pos+2;	
			puts_graphical_lcd_ram_lim(&set.temp.loop[4].byte[0],25,13*4,0,',',arialNarrow_bld_13pix);		
//			puts_graphical_lcd_ram_lim(&set.temp.loop[4].byte[0],sevenpt_select_screen_putdata_x[9],13*4,0,',',arialNarrow_bld_13pix);		
			puts_graphical_lcd_rom_lim(disp_RA1,0,13*4,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[10]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[0].byte[0],sevenpt_select_screen_putdata_x[10]-2,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_RA2,7+1,13*4,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[11]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[1].byte[0],sevenpt_select_screen_putdata_x[11]-2,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim((urombyte *)"HP    LP",sevenpt_select_screen_putdata_x[8],13*3,0,0,arialNarrow_bld_13pix);//AMAN 23May2013
//AMAN		puts_graphical_lcd_rom_lim((urombyte *)"LP     HP",sevenpt_select_screen_putdata_x[8],13*3,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_SA1,0,13*5,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[12]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[4].byte[0],sevenpt_select_screen_putdata_x[12]-1,13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_SA2,7+1,13*5,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[13]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[5].byte[0],sevenpt_select_screen_putdata_x[13]-2,13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_2,15,13*5,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[14]= last_x_pos+1;
			puts_graphical_lcd_ram_lim(&set.temp.loop[1].byte[0],sevenpt_select_screen_putdata_x[14],13*5,0,',',arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[15]= last_x_pos+2;	
			puts_graphical_lcd_ram_lim(&set.temp.loop[5].byte[0],25,13*5,0,',',arialNarrow_bld_13pix);		
			puts_graphical_lcd_rom_lim(disp_RH1,0,13*6,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[16]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[6].byte[0],sevenpt_select_screen_putdata_x[16]-1,13*6,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_RH2,7+1,13*6,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[17]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[8].byte[0],sevenpt_select_screen_putdata_x[17]-2,13*6,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_1,15,13*6,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[18]= last_x_pos+1;
			puts_graphical_lcd_ram_lim(&set.temp.loop[2].byte[0],sevenpt_select_screen_putdata_x[18],13*6,0,',',arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[19]= last_x_pos+2;	
			puts_graphical_lcd_ram_lim(&set.temp.loop[6].byte[0],25,13*6,0,',',arialNarrow_bld_13pix);		
			puts_graphical_lcd_rom_lim(disp_AT1,0,13*7,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[20]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[2].byte[0],sevenpt_select_screen_putdata_x[20]-1,13*7,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_AT2,7+1,13*7,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[21]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[3].byte[0],sevenpt_select_screen_putdata_x[21]-2,13*7,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_2,15,13*7,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[22]= last_x_pos+1;
			puts_graphical_lcd_ram_lim(&set.temp.loop[3].byte[0],sevenpt_select_screen_putdata_x[22],13*7,0,',',arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[23]= last_x_pos+2;	
			puts_graphical_lcd_ram_lim(&set.temp.loop[7].byte[0],25,13*7,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim((urombyte *)"Hold 7 SET TEMP key for 5sec",0,13*8,0,0,arialNarrow_bld_13pix);//AMAN 23May2013
			puts_graphical_lcd_rom_lim((urombyte *)"to select/change Set point  ",0,13*9,0,0,arialNarrow_bld_13pix);//AMAN 23May2013
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;		
			IEC0bits.T2IE = 1;
			break;	
		
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			display_set_temp_selection_screen();
		}		
	}	


	while(key_press != 'e' && key_press !='r') //lakh
	{
		WDTCLR
		if(frame_flags.bits.view_set_point_frame_rcvd  && rx.data.new_pkt_rcvd)
		{

			IEC0bits.T2IE = 0;	
			if(!first_packet)
			{
				if(set.temp.curr_set_point_no == 'D')
					temp_byte = '1';
				else
					temp_byte = set.temp.curr_set_point_no;
					stat.bits.refresh_display = 1;

			}
			puts_graphical_lcd_ram_cnt(&set.temp.curr_set_point_no,sevenpt_select_screen_putdata_x[1],0,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(set.temp.curr_cool,sevenpt_select_screen_putdata_x[2],13*2,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(set.temp.curr_heat,sevenpt_select_screen_putdata_x[3],13*2,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[8],13*4,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.loop[0].byte[0],sevenpt_select_screen_putdata_x[8],13*4,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",25,13*4,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.loop[4].byte[0],25,13*4,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[10]-2,13*4,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[0].byte[0],sevenpt_select_screen_putdata_x[10]-2,13*4,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[11]-2,13*4,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[1].byte[0],sevenpt_select_screen_putdata_x[11]-2,13*4,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[12]-1,13*5,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[4].byte[0],sevenpt_select_screen_putdata_x[12]-1,13*5,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[13]-2,13*5,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[5].byte[0],sevenpt_select_screen_putdata_x[13]-2,13*5,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[14],13*5,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.loop[1].byte[0],sevenpt_select_screen_putdata_x[14],13*5,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",25,13*5,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.loop[5].byte[0],25,13*5,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[16]-1,13*6,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[6].byte[0],sevenpt_select_screen_putdata_x[16]-1,13*6,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[17]-2,13*6,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[8].byte[0],sevenpt_select_screen_putdata_x[17]-2,13*6,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[18],13*6,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.loop[2].byte[0],sevenpt_select_screen_putdata_x[18],13*6,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",25,13*6,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.loop[6].byte[0],25,13*6,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[20]-1,13*7,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[2].byte[0],sevenpt_select_screen_putdata_x[20]-1,13*7,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[21]-2,13*7,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[3].byte[0],sevenpt_select_screen_putdata_x[21]-2,13*7,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",sevenpt_select_screen_putdata_x[22],13*7,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.loop[3].byte[0],sevenpt_select_screen_putdata_x[22],13*7,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("   ",25,13*7,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.loop[7].byte[0],25,13*7,0,',',arialNarrow_bld_13pix);
			
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;		
			IEC0bits.T2IE = 1;
		}	
		key_press = 0;
		key_press = scan_num_keypad();
		if(stat.bits.use_up_dwn_keys)
		{
//			if(key_press == 'a')
//			{
//				if(temp_byte < '7')
//					temp_byte++;
//				else
//					temp_byte = '1';
//				stat.bits.refresh_display = 1;
//			}
//			if(key_press == 'b')
//			{
//				if(temp_byte > '1')
//					temp_byte--;
//				else
//					temp_byte = '7';
//				stat.bits.refresh_display = 1;
//			}
		}		
		if(key_press == 'c' || key_press =='r')
		{
			home_clr();	
			load_default_screen_data();
		}	
		if(keys[5].long_press)
			load_pressure_status_screen();
		if(keys[0].long_press)
		{
			display_set_temp_values_screen();
			display_set_temp_selection_screen();
		}	
//		if(key_press == 'c')//ack
//		{
//			set.temp.curr_set_point_no = temp_byte;
//			new_pkt_rdy_to_be_sent = 1;
//			first_packet = 0;
//			
//			lhb_comm_fn_code = FC_SET_TEMP_SETPOINT;
//		}
//		if(stat.bits.refresh_display)
//		{
//			if(stat.bits.use_up_dwn_keys)
//				puts_graphical_lcd_ram_cnt(&temp_byte,sevenpt_select_screen_putdata_x[0],0,0,1,arialNarrow_bld_13pix);
//			stat.bits.refresh_display = 0;
//		}			
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			display_set_temp_selection_screen();
		}	
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void display_set_temp_values_screen(void)
{
	ubyte temp_byte = 0,prev_set_pt = '1';
	ubyte first_packet;
previous_menu_selection =  menu_selection;
	menu_selection = SEVEN_POINT_SELECT_MENU;

	IEC0bits.T2IE = 0;	
	key_press = 0;	
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	IEC0bits.T2IE = 1;
	temp_byte = '1';
	first_packet = 0;
	ubyte v =0;
	while(1)
	{
		WDTCLR
		if(frame_flags.bits.view_set_point_frame_rcvd  && rx.data.new_pkt_rcvd)
		{
			puts_graphical_lcd_rom_lim("Current selected set point: ",0,0,0,0,arialNarrow_bld_13pix);
			sevenpt_select_screen_putdata_x[1]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&set.temp.curr_set_point_no,sevenpt_select_screen_putdata_x[1],0,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("Positions",1,13*1,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(" | ",8,13*1,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("Cooling",12,13*1,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(" | ",18,13*1,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("Heating",22,13*1,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("Press ENTER to save. ACK to return",0,13*9,0,0,arialNarrow_bld_13pix);
			for(v=0;v<7;v++)
			{
				puts_graphical_lcd_rom_cnt(&set_values_arr[v][0],0,13*(v+2),0,30,arialNarrow_bld_13pix);
			}	
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;		
			IEC0bits.T2IE = 1;
			break;	
		
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			display_set_temp_values_screen();
		}		
	}	

	while(key_press != 'c' && key_press !='r') //lakh
	{
		WDTCLR
		if(frame_flags.bits.view_set_point_frame_rcvd  && rx.data.new_pkt_rcvd)
		{

			IEC0bits.T2IE = 0;	
			if(!first_packet)
			{
				if(set.temp.curr_set_point_no == 'D')
					temp_byte = '1';
				else
					temp_byte = set.temp.curr_set_point_no;
					stat.bits.refresh_display = 1;

			}
			puts_graphical_lcd_ram_cnt(&set.temp.curr_set_point_no,sevenpt_select_screen_putdata_x[1],0,0,1,arialNarrow_bld_13pix);
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;		
			IEC0bits.T2IE = 1;
		}	
		key_press = 0;
		//key_scan();
		key_press = scan_num_keypad();
		if(stat.bits.use_up_dwn_keys)
		{
			if(key_press == 'b')
			{
				if(temp_byte < '7')
					temp_byte++;
				else
					temp_byte = '1';
				stat.bits.refresh_display = 1;
			}
			if(key_press == 'a')
			{
				if(temp_byte > '1')
					temp_byte--;
				else
					temp_byte = '7';
				stat.bits.refresh_display = 1;
			}
		}		
//		if(keys[5].long_press)
//			load_pressure_status_screen();
		if(key_press == 'e')//ack
		{
			set.temp.curr_set_point_no = temp_byte;
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			
			lhb_comm_fn_code = FC_SET_TEMP_SETPOINT;
		}
		if(prev_set_pt != set.temp.curr_set_point_no)
		{
			prev_set_pt = set.temp.curr_set_point_no;
			temp_byte = set.temp.curr_set_point_no;
		}
		if(stat.bits.refresh_display)
		{
			if(stat.bits.use_up_dwn_keys)
			{
					for(v=0;v<7;v++)
					{
						puts_graphical_lcd_rom_cnt(&set_values_arr[v][0],0,13*(v+2),((temp_byte-49)==v),30,arialNarrow_bld_13pix);
					}	
					stat.bits.refresh_display = 0;
			}
		}			
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			display_set_temp_values_screen();
		}	
	}
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_status_of_plant_screen_data(void)
{
	ubyte *on_off[2] = {(ubyte*)("OFF,"),(ubyte*)("ON ,")};
	ubyte *ok_flt[3] = {(ubyte*)("FLT,"),(ubyte*)("OK ,"),(ubyte*)("BLK,")};
	uinteger room_1 = 0,room_2 = 0,room_avg = 0;
	ubyte room_temp[5] = {"    ,"};
	previous_menu_selection =  menu_selection;
	menu_selection = STATUS_OF_PLANT_MENU;
	//lhb_comm.flags.bits.rx_enable=1;
	check_fn_code_send_pkt_to_lhb();	
	
	IEC0bits.T2IE = 0;	
	key_press = 0;	
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);

	while(1)
	{
		WDTCLR
		if(frame_flags.bits.plant_status_frame_rcvd)
		{
			puts_graphical_lcd_rom_lim(disp_local,0,0,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[0]= last_x_pos;
			puts_graphical_lcd_ram_lim(coach_info.coach_no,plantstatus_screen_putdata_x[0],0,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_hvac,10,0,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[1]= last_x_pos;
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.di1.bits.hvac],plantstatus_screen_putdata_x[1],0,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_mode,18,0,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[2]= last_x_pos;
			puts_graphical_lcd_ram_cnt( coach_info.plant_opr_mode,plantstatus_screen_putdata_x[2],0,0,7,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_400V,0,13,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[3]= last_x_pos;
			puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.v_ok],plantstatus_screen_putdata_x[3],13,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim(disp_Sensors,7,13,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("SEN:",7,13,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[4]= last_x_pos;
//			puts_graphical_lcd_ram_lim(ok_flt[!(fault.logs.status.open.byte | fault.logs.status.shrt.byte)],plantstatus_screen_putdata_x[4],13,0,',',arialNarrow_bld_13pix);
			fault.logs.status.open.byte = fault.logs.status.open.byte & 0xBF;
			fault.logs.status.shrt.byte = fault.logs.status.shrt.byte & 0xBF;
			puts_graphical_lcd_ram_lim(ok_flt[!(fault.logs.status.open.byte | fault.logs.status.shrt.byte)],plantstatus_screen_putdata_x[4],13,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_sp,13,13,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[39]= last_x_pos;
			puts_graphical_lcd_ram_lim(&set.temp.temp[7].byte[0],plantstatus_screen_putdata_x[39]-1,13*1,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("RA:",last_x_pos+1,13,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[40]= last_x_pos;
//			puts_graphical_lcd_ram_lim(&set.temp.temp[0].byte[0],plantstatus_screen_putdata_x[40]-1,13*1,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Blowers,0,13*2,1,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,7,13*2,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[5]= last_x_pos;
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.spfn11],plantstatus_screen_putdata_x[5]-1,13*2,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_2,12,13*2,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[6]= last_x_pos;
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel2.bits.spfn12],plantstatus_screen_putdata_x[6]-1,13*2,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_1,17,13*2,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[7]= last_x_pos;
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.spfn21],plantstatus_screen_putdata_x[7]-1,13*2,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_2,22,13*2,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[8]= last_x_pos;
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel2.bits.spfn22],plantstatus_screen_putdata_x[8]-1,13*2,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim((urombyte *)"      ",0,13*3,1,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Th,7,13*3,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[9]= last_x_pos;
			if(fault.logs.status.blk1.bits.spfn11)
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[9]-1,13*3,1,',',arialNarrow_bld_13pix);
			else
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.spfn11],plantstatus_screen_putdata_x[9]-1,13*3,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Th,12,13*3,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[10]= last_x_pos;
			if(fault.logs.status.blk1.bits.spfn12)
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[10]-1,13*3,1,',',arialNarrow_bld_13pix);
			else
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.spfn12],plantstatus_screen_putdata_x[10]-1,13*3,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Th,17,13*3,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[11]= last_x_pos;
			if(fault.logs.status.blk1.bits.spfn21)
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[11]-1,13*3,1,',',arialNarrow_bld_13pix);
			else
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.spfn21],plantstatus_screen_putdata_x[11]-1,13*3,1,',',arialNarrow_bld_13pix);	
			puts_graphical_lcd_rom_lim(disp_Th,22,13*3,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[12]= last_x_pos;
			if(fault.logs.status.blk2.bits.spfn22)
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[12]-1,13*3,1,',',arialNarrow_bld_13pix);
			else
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.spfn22],plantstatus_screen_putdata_x[12]-1,13*3,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Cfans,0,13*4,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,7,13*4,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[13]= last_x_pos;		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.cnd11],plantstatus_screen_putdata_x[13]-1,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_2,12,13*4,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[14]= last_x_pos;		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.cnd12],plantstatus_screen_putdata_x[14]-1,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_1,17,13*4,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[15]= last_x_pos;		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel2.bits.cnd21],plantstatus_screen_putdata_x[15]-1,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_2,22,13*4,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[16]= last_x_pos;		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel3.bits.cnd22],plantstatus_screen_putdata_x[16]-1,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Th,7,13*5,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[17]= last_x_pos;
			if(fault.logs.status.blk1.bits.cnd11)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[17]-1,13*5,0,',',arialNarrow_bld_13pix);
			else	
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.cnd11],plantstatus_screen_putdata_x[17]-1,13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Th,12,13*5,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[18]= last_x_pos;
			if(fault.logs.status.blk1.bits.cnd12)	
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[18]-1,13*5,0,',',arialNarrow_bld_13pix);
			else	
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.cnd12],plantstatus_screen_putdata_x[18]-1,13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Th,17,13*5,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[19]= last_x_pos;
			if(fault.logs.status.blk1.bits.cnd21)	
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[19]-1,13*5,0,',',arialNarrow_bld_13pix);
			else	
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.cnd21],plantstatus_screen_putdata_x[19]-1,13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Th,22,13*5,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[20]= last_x_pos;
			if(fault.logs.status.blk1.bits.cnd22)	
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[20]-1,13*5,0,',',arialNarrow_bld_13pix);
			else	
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.cnd22],plantstatus_screen_putdata_x[20]-1,13*5,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Comp,0,13*6,1,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,7,13*6,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[21]= last_x_pos;		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.cmp11],plantstatus_screen_putdata_x[21]-1,13*6,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_2,12,13*6,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[22]= last_x_pos;		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.cmp12],plantstatus_screen_putdata_x[22]-1,13*6,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_1,17,13*6,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[23]= last_x_pos;		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel3.bits.cmp21],plantstatus_screen_putdata_x[23]-1,13*6,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_2,22,13*6,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[24]= last_x_pos;		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel3.bits.cmp22],plantstatus_screen_putdata_x[24]-1,13*6,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Cpr1,0,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[25]= last_x_pos;		
//			puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.cp1],plantstatus_screen_putdata_x[25]-1,13*7,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[!fault.logs.status.di2.bits.cp1],plantstatus_screen_putdata_x[25]-1,13*7,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_LP,7,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[26]= last_x_pos;
			if(fault.logs.status.blk2.bits.cmp11)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[26],13*7,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.lp11],plantstatus_screen_putdata_x[26],13*7,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_LP,12,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[27]= last_x_pos;
			if(fault.logs.status.blk2.bits.cmp12)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[27],13*7,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.lp12],plantstatus_screen_putdata_x[27],13*7,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_LP,17,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[28]= last_x_pos;
			if(fault.logs.status.blk2.bits.cmp21)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[28],13*7,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.lp21],plantstatus_screen_putdata_x[28],13*7,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_LP,22,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[29]= last_x_pos;
			if(fault.logs.status.blk2.bits.cmp22)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[29],13*7,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.lp22],plantstatus_screen_putdata_x[29],13*7,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Cpr2,0,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[30]= last_x_pos;
//			puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.cp2],plantstatus_screen_putdata_x[30]-1,13*8,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[!fault.logs.status.di3.bits.cp2],plantstatus_screen_putdata_x[30]-1,13*8,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_HP,7,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[31]= last_x_pos;
			if(fault.logs.status.blk2.bits.cmp11)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[31]-1,13*8,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.hp11],plantstatus_screen_putdata_x[31]-1,13*8,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_HP,12,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[32]= last_x_pos;
			if(fault.logs.status.blk2.bits.cmp12)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[32]-1,13*8,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.hp12],plantstatus_screen_putdata_x[32]-1,13*8,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_HP,17,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[33]= last_x_pos;
			if(fault.logs.status.blk2.bits.cmp21)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[33]-1,13*8,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di4.bits.hp21],plantstatus_screen_putdata_x[33]-1,13*8,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_HP,22,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[34]= last_x_pos;
			if(fault.logs.status.blk2.bits.cmp22)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[34]-1,13*8,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di4.bits.hp22],plantstatus_screen_putdata_x[34]-1,13*8,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Heater,0,13*9,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,7,13*9,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[35]= last_x_pos;
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel2.bits.htr1],plantstatus_screen_putdata_x[35]-1,13*9,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_OHP,12,13*9,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[36]= last_x_pos;
			if( fault.logs.status.blk2.bits.htr1)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[36]-1,13*9,0,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.htr1],plantstatus_screen_putdata_x[36]-1,13*9,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_2,17,13*9,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[37]= last_x_pos;
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel3.bits.htr2],plantstatus_screen_putdata_x[37]-1,13*9,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_OHP,22,13*9,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[38]= last_x_pos;
			if( fault.logs.status.blk2.bits.htr2)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[38]-1,13*9,0,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.htr2],plantstatus_screen_putdata_x[38]-1,13*9,0,',',arialNarrow_bld_13pix);
				
			IEC0bits.T2IE = 1;
	//		stat.bits.data_received = 0;	
			frame_flags.integer = 0;	
			break;	
		}

		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
	
		if(keys[0].long_press)
		{	
			stat.bits.use_up_dwn_keys = 0;
			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
			stat.bits.use_up_dwn_keys = 1;
		}
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			load_status_of_plant_screen_data();
		}	
	}		
	
	IEC0bits.T2IE = 1;
	while(key_press != 'e' && key_press !='r')
	{
		WDTCLR
		key_press = 0;
		key_press = scan_num_keypad();
		
		if(frame_flags.bits.plant_status_frame_rcvd)
		{
			IEC0bits.T2IE = 0;
			if(!(fault.logs.status.open.bits.rm1 || fault.logs.status.shrt.bits.rm1))
			{
				room_1 = (set.temp.temp[0].byte[0]-48)*100;
				room_1 = room_1+(set.temp.temp[0].byte[1]-48)*10;
				room_1 = room_1+(set.temp.temp[0].byte[3]-48);
			}
			else
			room_1 = 0;

			if(!(fault.logs.status.open.bits.rm2 || fault.logs.status.shrt.bits.rm2))
			{
				room_2 = (set.temp.temp[1].byte[0]-48)*100;
				room_2 = room_2+(set.temp.temp[1].byte[1]-48)*10;
				room_2 = room_2+(set.temp.temp[1].byte[3]-48);
			}
			else
			room_2 = 0;

			if(room_1 & room_2)
				room_avg = (room_1 + room_2)/2;
			if(!room_1 & !room_2)
				room_avg = 0;
			else if(!room_1)
				room_avg = room_2;
			else if(!room_2)
				room_avg = room_1;
	
			room_temp[0] = 48+(room_avg/100);
			room_temp[1] = 48+((room_avg%100)/10);
			room_temp[2] = '.';
			room_temp[3] = 48+(room_avg%10);
			room_temp[4] = ',';
			if(fault.logs.status.shrt.bits.rm1&&fault.logs.status.shrt.bits.rm2)
			{
				room_temp[0] = 'S';
				room_temp[1] = 'H';
				room_temp[2] = 'R';
				room_temp[3] = 'T';
				room_temp[4] = ',';
			}
			if(fault.logs.status.open.bits.rm1&&fault.logs.status.open.bits.rm2)
			{
				room_temp[0] = 'O';
				room_temp[1] = 'P';
				room_temp[2] = 'E';
				room_temp[3] = 'N';
				room_temp[4] = ',';
			}
			puts_graphical_lcd_ram_lim(coach_info.coach_no,plantstatus_screen_putdata_x[0],0,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.di1.bits.hvac],plantstatus_screen_putdata_x[1],0,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( coach_info.plant_opr_mode,plantstatus_screen_putdata_x[2],0,0,7,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.v_ok],plantstatus_screen_putdata_x[3],13,0,',',arialNarrow_bld_13pix);
			fault.logs.status.open.byte = fault.logs.status.open.byte & 0xBF;
			fault.logs.status.shrt.byte = fault.logs.status.shrt.byte & 0xBF;
			puts_graphical_lcd_ram_lim(ok_flt[!(fault.logs.status.open.byte | fault.logs.status.shrt.byte)],plantstatus_screen_putdata_x[4],13,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[7].byte[0],plantstatus_screen_putdata_x[39]-1,13*1,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_ram_lim(&set.temp.temp[0].byte[0],plantstatus_screen_putdata_x[40]-2,13*1,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("   ",plantstatus_screen_putdata_x[40],13*1,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&room_temp[0],plantstatus_screen_putdata_x[40],13*1,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(&set.temp.temp[6].byte[0],26,13*1,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.spfn11],plantstatus_screen_putdata_x[5]-1,13*2,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel2.bits.spfn12],plantstatus_screen_putdata_x[6]-1,13*2,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.spfn21],plantstatus_screen_putdata_x[7]-1,13*2,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel2.bits.spfn22],plantstatus_screen_putdata_x[8]-1,13*2,1,',',arialNarrow_bld_13pix);
						
			if(fault.logs.status.blk1.bits.spfn11)
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[9]-1,13*3,1,',',arialNarrow_bld_13pix);
			else
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.spfn11],plantstatus_screen_putdata_x[9]-1,13*3,1,',',arialNarrow_bld_13pix);
		
			if(fault.logs.status.blk1.bits.spfn12)
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[10]-1,13*3,1,',',arialNarrow_bld_13pix);
			else
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.spfn12],plantstatus_screen_putdata_x[10]-1,13*3,1,',',arialNarrow_bld_13pix);
				
			if(fault.logs.status.blk1.bits.spfn21)
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[11]-1,13*3,1,',',arialNarrow_bld_13pix);
			else
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.spfn21],plantstatus_screen_putdata_x[11]-1,13*3,1,',',arialNarrow_bld_13pix);
				
			if(fault.logs.status.blk2.bits.spfn22)
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[12]-1,13*3,1,',',arialNarrow_bld_13pix);
			else
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.spfn22],plantstatus_screen_putdata_x[12]-1,13*3,1,',',arialNarrow_bld_13pix);
			
					
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.cnd11],plantstatus_screen_putdata_x[13]-1,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.cnd12],plantstatus_screen_putdata_x[14]-1,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel2.bits.cnd21],plantstatus_screen_putdata_x[15]-1,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel3.bits.cnd22],plantstatus_screen_putdata_x[16]-1,13*4,0,',',arialNarrow_bld_13pix);
				
	
			if(fault.logs.status.blk1.bits.cnd11)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[17]-1,13*5,0,',',arialNarrow_bld_13pix);
			else	
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.cnd11],plantstatus_screen_putdata_x[17]-1,13*5,0,',',arialNarrow_bld_13pix);
		
			if(fault.logs.status.blk1.bits.cnd12)	
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[18]-1,13*5,0,',',arialNarrow_bld_13pix);
			else	
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.cnd12],plantstatus_screen_putdata_x[18]-1,13*5,0,',',arialNarrow_bld_13pix);
	
			if(fault.logs.status.blk1.bits.cnd21)	
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[19]-1,13*5,0,',',arialNarrow_bld_13pix);
			else	
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.cnd21],plantstatus_screen_putdata_x[19]-1,13*5,0,',',arialNarrow_bld_13pix);
		
			if(fault.logs.status.blk1.bits.cnd22)	
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[20]-1,13*5,0,',',arialNarrow_bld_13pix);
			else	
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.cnd22],plantstatus_screen_putdata_x[20]-1,13*5,0,',',arialNarrow_bld_13pix);
					
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.cmp11],plantstatus_screen_putdata_x[21]-1,13*6,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel1.bits.cmp12],plantstatus_screen_putdata_x[22]-1,13*6,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel3.bits.cmp21],plantstatus_screen_putdata_x[23]-1,13*6,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel3.bits.cmp22],plantstatus_screen_putdata_x[24]-1,13*6,1,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.cp1],plantstatus_screen_putdata_x[25]-1,13*7,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[!fault.logs.status.di2.bits.cp1],plantstatus_screen_putdata_x[25]-1,13*7,1,',',arialNarrow_bld_13pix);
						
			if(fault.logs.status.blk2.bits.cmp11)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[26],13*7,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.lp11],plantstatus_screen_putdata_x[26],13*7,1,',',arialNarrow_bld_13pix);
	
			if(fault.logs.status.blk2.bits.cmp12)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[27],13*7,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.lp12],plantstatus_screen_putdata_x[27],13*7,1,',',arialNarrow_bld_13pix);
	
			if(fault.logs.status.blk2.bits.cmp21)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[28],13*7,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.lp21],plantstatus_screen_putdata_x[28],13*7,1,',',arialNarrow_bld_13pix);
			
			if(fault.logs.status.blk2.bits.cmp22)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[29],13*7,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.lp22],plantstatus_screen_putdata_x[29],13*7,1,',',arialNarrow_bld_13pix);
			
//			puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.cp2],plantstatus_screen_putdata_x[30]-1,13*8,1,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(on_off[!fault.logs.status.di3.bits.cp2],plantstatus_screen_putdata_x[30]-1,13*8,1,',',arialNarrow_bld_13pix);
					
			if(fault.logs.status.blk2.bits.cmp11)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[31]-1,13*8,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.hp11],plantstatus_screen_putdata_x[31]-1,13*8,1,',',arialNarrow_bld_13pix);
				
			if(fault.logs.status.blk2.bits.cmp12)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[32]-1,13*8,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di2.bits.hp12],plantstatus_screen_putdata_x[32]-1,13*8,1,',',arialNarrow_bld_13pix);
				
			if(fault.logs.status.blk2.bits.cmp21)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[33]-1,13*8,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di4.bits.hp21],plantstatus_screen_putdata_x[33]-1,13*8,1,',',arialNarrow_bld_13pix);
	
			if(fault.logs.status.blk2.bits.cmp22)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[34]-1,13*8,1,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di4.bits.hp22],plantstatus_screen_putdata_x[34]-1,13*8,1,',',arialNarrow_bld_13pix);
		
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel2.bits.htr1],plantstatus_screen_putdata_x[35]-1,13*9,0,',',arialNarrow_bld_13pix);
			
			if( fault.logs.status.blk2.bits.htr1)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[36]-1,13*9,0,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di1.bits.htr1],plantstatus_screen_putdata_x[36]-1,13*9,0,',',arialNarrow_bld_13pix);
				
			puts_graphical_lcd_ram_lim(on_off[fault.logs.status.rel3.bits.htr2],plantstatus_screen_putdata_x[37]-1,13*9,0,',',arialNarrow_bld_13pix);
			
			if( fault.logs.status.blk2.bits.htr2)		
				puts_graphical_lcd_ram_lim(ok_flt[2],plantstatus_screen_putdata_x[38]-1,13*9,0,',',arialNarrow_bld_13pix);
			else		
				puts_graphical_lcd_ram_lim(ok_flt[fault.logs.status.di3.bits.htr2],plantstatus_screen_putdata_x[38]-1,13*9,0,',',arialNarrow_bld_13pix);
				
				IEC0bits.T2IE = 1;
	//			stat.bits.data_received = 0;	
				frame_flags.integer = 0;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(keys[0].long_press)
		{	
			stat.bits.use_up_dwn_keys = 0;
			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
			stat.bits.use_up_dwn_keys = 1;
		}
	
		if(key_press == 'b')
			load_dutycycle_data();//Dutycycle_trip_log();
		if(key_press == 'r')         
		{
		home_clr();
	//	menu_mode_options();
		break;
		} 
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			load_status_of_plant_screen_data();
		}	
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void display_trip_count(void)
{
	ubyte *on_off[2] = {(ubyte*)("OFF,"),(ubyte*)("ON ,")};
	ubyte *ok_flt[3] = {(ubyte*)("FLT,"),(ubyte*)("OK ,"),(ubyte*)("BLK,")};
previous_menu_selection =  menu_selection;
	menu_selection = VIEW_TRIP_COUNT;
	//lhb_comm.flags.bits.rx_enable=1;
	check_fn_code_send_pkt_to_lhb();	
	
	IEC0bits.T2IE = 0;	
	key_press = 0;	
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);

	while(1)
	{
		WDTCLR
		if( frame_flags.bits.view_trip_count_frame_rcvd)
		{
			puts_graphical_lcd_rom_lim( (urombyte *)"TRIPS IN LAST 1 HOUR   ",0,0,1,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Blowers,0,13*2,1,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,7,13*2,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[5]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.BLOWER[0][0].byte,plantstatus_screen_putdata_x[5],13*2,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_2,12,13*2,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[6]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.BLOWER[0][1].byte,plantstatus_screen_putdata_x[6],13*2,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_1,17,13*2,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[7]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.BLOWER[1][0].byte,plantstatus_screen_putdata_x[7],13*2,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_2,22,13*2,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[8]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.BLOWER[1][1].byte,plantstatus_screen_putdata_x[8],13*2,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Cfans,0,13*4,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,7,13*4,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[13]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.COND[0][0].byte,plantstatus_screen_putdata_x[13],13*4,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_2,12,13*4,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[14]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.COND[0][1].byte,plantstatus_screen_putdata_x[14],13*4,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_1,17,13*4,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[15]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.COND[1][0].byte,plantstatus_screen_putdata_x[15],13*4,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_2_2,22,13*4,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[16]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.COND[1][1].byte,plantstatus_screen_putdata_x[16],13*4,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Comp,0,13*6,1,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,7,13*6,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[21]= last_x_pos;
			puts_graphical_lcd_rom_lim(disp_1_2,12,13*6,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[22]= last_x_pos;
			puts_graphical_lcd_rom_lim(disp_2_1,17,13*6,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[23]= last_x_pos;
			puts_graphical_lcd_rom_lim(disp_2_2,22,13*6,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[24]= last_x_pos;
			puts_graphical_lcd_rom_lim(disp_LP,7,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[26]= last_x_pos;
			puts_graphical_lcd_ram_cnt( (ubyte *)"  ",plantstatus_screen_putdata_x[26],13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.LP[0][0].byte,plantstatus_screen_putdata_x[26]+1,13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_LP,12,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[27]= last_x_pos;
			puts_graphical_lcd_ram_cnt( (ubyte *)"  ",plantstatus_screen_putdata_x[27],13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.LP[0][1].byte,plantstatus_screen_putdata_x[27]+1,13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_LP,17,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[28]= last_x_pos;
			puts_graphical_lcd_ram_cnt( (ubyte *)"  ",plantstatus_screen_putdata_x[28],13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.LP[1][0].byte,plantstatus_screen_putdata_x[28]+1,13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_LP,22,13*7,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[29]= last_x_pos;
			puts_graphical_lcd_ram_cnt( (ubyte *)"  ",plantstatus_screen_putdata_x[29],13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.LP[1][1].byte,plantstatus_screen_putdata_x[29]+1,13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_HP,7,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[31]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.HP[0][0].byte,plantstatus_screen_putdata_x[31],13*8,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_HP,12,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[32]= last_x_pos;
			puts_graphical_lcd_ram_cnt(&trip.count.HP[0][1].byte,plantstatus_screen_putdata_x[32],13*8,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_HP,17,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[33]= last_x_pos;
			puts_graphical_lcd_ram_cnt( &trip.count.HP[1][0].byte,plantstatus_screen_putdata_x[33],13*8,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_HP,22,13*8,1,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[34]= last_x_pos;
			puts_graphical_lcd_ram_cnt( &trip.count.HP[1][1].byte,plantstatus_screen_putdata_x[34],13*8,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_Heater,0,13*9,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_1,7,13*9,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[35]= last_x_pos;
			puts_graphical_lcd_ram_cnt( &trip.count.HEATER[0].byte,plantstatus_screen_putdata_x[35],13*9,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_1_2,17,13*9,0,0,arialNarrow_bld_13pix);
			plantstatus_screen_putdata_x[37]= last_x_pos;
			puts_graphical_lcd_ram_cnt( &trip.count.HEATER[1].byte,plantstatus_screen_putdata_x[37],13*9,0,1,arialNarrow_bld_13pix);		
			
			IEC0bits.T2IE = 1;
	//		stat.bits.data_received = 0;	
			frame_flags.integer = 0;
			break;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
			
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			display_trip_count();
		}
	}			
	
	IEC0bits.T2IE = 1;
	
	while(key_press != 'e' && key_press !='r')
	{
		WDTCLR
		key_press = 0;
		key_press = scan_num_keypad();
		
		if( frame_flags.bits.view_trip_count_frame_rcvd)
		{
			IEC0bits.T2IE = 0;
			puts_graphical_lcd_ram_cnt(&trip.count.BLOWER[0][0].byte,plantstatus_screen_putdata_x[5],13*2,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.BLOWER[0][1].byte,plantstatus_screen_putdata_x[6],13*2,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.BLOWER[1][0].byte,plantstatus_screen_putdata_x[7],13*2,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.BLOWER[1][1].byte,plantstatus_screen_putdata_x[8],13*2,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.COND[0][0].byte,plantstatus_screen_putdata_x[13],13*4,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.COND[0][1].byte,plantstatus_screen_putdata_x[14],13*4,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.COND[1][0].byte,plantstatus_screen_putdata_x[15],13*4,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.COND[1][1].byte,plantstatus_screen_putdata_x[16],13*4,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( (ubyte *)"  ",plantstatus_screen_putdata_x[26],13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.LP[0][0].byte,plantstatus_screen_putdata_x[26]+1,13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( (ubyte *)"  ",plantstatus_screen_putdata_x[27],13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.LP[0][1].byte,plantstatus_screen_putdata_x[27]+1,13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( (ubyte *)"  ",plantstatus_screen_putdata_x[28],13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.LP[1][0].byte,plantstatus_screen_putdata_x[28]+1,13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( (ubyte *)"  ",plantstatus_screen_putdata_x[29],13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.LP[1][1].byte,plantstatus_screen_putdata_x[29]+1,13*7,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.HP[0][0].byte,plantstatus_screen_putdata_x[31],13*8,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&trip.count.HP[0][1].byte,plantstatus_screen_putdata_x[32],13*8,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.HP[1][0].byte,plantstatus_screen_putdata_x[33],13*8,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.HP[1][1].byte,plantstatus_screen_putdata_x[34],13*8,1,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.HEATER[0].byte,plantstatus_screen_putdata_x[35],13*9,0,1,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt( &trip.count.HEATER[1].byte,plantstatus_screen_putdata_x[37],13*9,0,1,arialNarrow_bld_13pix);

			
			IEC0bits.T2IE = 1;
	//		stat.bits.data_received = 0;	
			frame_flags.integer = 0;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
			
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			display_trip_count();
		}	

	}
}


void Dutycycle_trip_log(void)
{
    ubyte temp[5]={0,0,0,0,'\0'};
    ubyte src_temp1[2]={0};
    ubyte time_date_str[9]={0};
	ubyte temp_count, start_ptr = 0,end_ptr;
	previous_menu_selection =  menu_selection;
	menu_selection = DUTYCYCLE_TRIP_MENU;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
    
//	Delays_Xtal_ms(1000);

	new_pkt_rdy_to_be_sent = 1;
	lhb_comm_fn_code =  FC_VIEW_DUTYCYCLE_TRIP_LOG;
	while(key_press != 'r')
	{
		ClearWDT();
		WDTCLR
		
        puts_graphical_lcd_rom_lim(disp_strt_date_time,0,0,0,0,arialNarrow_bld_13pix);
       // puts_graphical_lcd_rom_lim(disp_curr_date_time,15,0,0,0,arialNarrow_bld_13pix);
		if((frame_flags.bits.view_dutycycle_trip_log && rx.data.new_pkt_rcvd) | key_press)
		{
            memcpy(time_date_str,EquipmentLogs.data.DutyCycles.data.TripClock.StartTimeStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,16,13*0,0,0,arialNarrow_bld_13pix);
                
            memcpy(time_date_str,EquipmentLogs.data.DutyCycles.data.TripClock.StartDateStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,23,13*0,0,0,arialNarrow_bld_13pix);
//            memcpy(time_date_str,lcd_dutylog.log.CurrTimeStamp.byte,8);
//            puts_graphical_lcd_rom_lim(time_date_str,16,13*1,0,0,arialNarrow_bld_13pix);
//            memcpy(time_date_str,lcd_dutylog[0].log.CurrDateStamp.byte,8);
//            puts_graphical_lcd_rom_lim(time_date_str,24,13*1,0,0,arialNarrow_bld_13pix);  
              puts_graphical_lcd_rom_lim(disp_dutycycle_trip_log,0,13*2,0,0,arialNarrow_bld_13pix);
                //puts_graphical_lcd_ram_lim(&fault_log[temp_count].fault_str[0],0,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_rom_lim( (urombyte *)"      11    12   21   22       ",0,13*3,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"BLR",0,13*4,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"LP ",0,13*5,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"HP ",0,13*6,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"CDF",0,13*7,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"HTR",0,13*8,1,0,arialNarrow_bld_13pix);

//                strncpy(temp,event.log.,4); 
////               // temp[3]='\0';
//                puts_graphical_lcd_rom_lim(temp,0,13*4,0,0,arialNarrow_bld_13pix);
                memcpy(temp, EquipmentLogs.data.trip.count.BLOWER,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,7,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,17,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,22,13*4,0,0,arialNarrow_bld_13pix);
               // src_temp= atoi(dest_ptr);
                
                memcpy(temp, EquipmentLogs.data.trip.count.LP,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,7,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,17,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,22,13*5,0,0,arialNarrow_bld_13pix);
                
//                src_ptr= lcd_dutylog[0].log.Compressors;
                memcpy(temp, EquipmentLogs.data.trip.count.HP,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,7,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,17,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,22,13*6,0,0,arialNarrow_bld_13pix);
               
                memcpy(temp, EquipmentLogs.data.trip.count.COND,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,7,13*7,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*7,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,17,13*7,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,22,13*7,0,0,arialNarrow_bld_13pix);
          
                

                memcpy(temp, EquipmentLogs.data.trip.count.HEATER,2);

                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,7,13*8,0,0,arialNarrow_bld_13pix);

                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*8,0,0,arialNarrow_bld_13pix);
               
              
              
			if(!no_of_logs_to_disp)
			puts_graphical_lcd_rom_lim("NO Data Available......",0,13*1,0,0,arialNarrow_bld_13pix);

			frame_flags.bits.view_dutycycle_trip_log = 0;
			rx.data.new_pkt_rcvd = 0;
			frame_flags.integer = 0;	
		}
		
		key_press = 0;
		key_press = scan_num_keypad();		
		if(keys[0].long_press)// 7 set temp 
			display_set_temp_values_screen();
        if(key_press == 'a')
        {
            
            home_clr();
            load_dutycycle_data();
        }
//			display_set_temp_selection_screen();
//           if(key_press == 'r')
//          {
//      	     menu_mode_options();  //lakh
//           }

		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
	}
	//str_fill( &lcd_dutylog[0].log.serial_num.byte[0], 117,' ');
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_fault_data(void)
{
	ubyte temp_count, start_ptr = 0,end_ptr;
	previous_menu_selection =  menu_selection;
	menu_selection = FAULT_DATA_MENU;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
//	puts_graphical_lcd_rom_lim(disp_fault_data,0,0,0,0,arialNarrow_bld_13pix);
	
	new_pkt_rdy_to_be_sent = 1;
	lhb_comm_fn_code = FC_VIEW_STORED_FAULTS;
	while(key_press != 'r')
	{
		ClearWDT();
		WDTCLR
		puts_graphical_lcd_rom_lim(disp_fault_data,0,0,0,0,arialNarrow_bld_13pix);
		if((frame_flags.bits.stored_fault_frame_rcvd && rx.data.new_pkt_rcvd) | key_press)
		{
			if((start_ptr+8) < temp_fault_count)
				end_ptr=start_ptr + 8;
			else
				end_ptr = temp_fault_count;	

			for(temp_count = start_ptr;temp_count < end_ptr;temp_count++ )
			{
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].fault_str[0],0,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].date.byte[0],16,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].time.byte[0],23,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
			}
			if(!no_of_logs_to_disp)
			puts_graphical_lcd_rom_lim("NO Data Available......",0,13*1,0,0,arialNarrow_bld_13pix);

			frame_flags.bits.stored_fault_frame_rcvd = 0;
			rx.data.new_pkt_rcvd = 0;
			frame_flags.integer = 0;	
		}
		
		key_press = 0;
		key_press = scan_num_keypad();
		
		if(key_press == 'b')
		{	
			home_clr();
			if((start_ptr+8) < temp_fault_count)
				start_ptr += 8;
			else
			{
				temp_fault_count = 0;
				new_pkt_rdy_to_be_sent = 1;
				lhb_comm_fn_code = FC_VIEW_STORED_FAULTS;
				str_fill( &fault_log[0].fault_str[0], 117,' ');
				stat.bits.request_nxt_fault_frame = 1;
			}
		}
		
		if(key_press == 'a')
		{	
			home_clr();
			if((start_ptr-8) >= 0)
				start_ptr -= 8;
			else
			{
				temp_fault_count = 0;
				new_pkt_rdy_to_be_sent = 1;
				lhb_comm_fn_code = FC_VIEW_PREV_STORED_FAULTS;
				str_fill( &fault_log[0].fault_str[0], 117,' ');
				stat.bits.request_nxt_fault_frame = 1;
			}	
		
		}
		
			
		if(keys[0].long_press)// 7 set temp 
			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
//           if(key_press == 'r')
//          {
//      	     menu_mode_options();  //lakh
//           }

		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
		
//		if(!timer2.display_timeout_10sec.timer)
//		{
//			puts_graphical_lcd_rom_lim("TIME OUT",10,13*1,0,0,arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("TIME OUT",10,13*2,0,0,arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("TIME OUT",10,13*3,0,0,arialNarrow_bld_13pix);
//			puts_graphical_lcd_rom_lim("TIME OUT",10,13*4,0,0,arialNarrow_bld_13pix);
//			Delays_Xtal_ms(2000);
//			timer2.display_timeout_10sec.status.tick = 1;
//		}
//		if(timer2.display_timeout_10sec.status.tick)
//		{
//			timer2.display_timeout_10sec.status.tick = 0;
//			home_clr();	
//			load_default_screen_data();
////			load_fault_data();
//		}	
	}
	str_fill( &fault_log[0].fault_str[0], 117,' ');
}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################

void load_dutycycle_data(void)
{
    ubyte temp[5]={0,0,0,0,'\0'};
    ubyte src_temp1[2]={0};
    ubyte time_date_str[9]={0};
	ubyte temp_count, start_ptr = 0,end_ptr;
	previous_menu_selection =  menu_selection;
	menu_selection = DUTY_CYCLE_DATA_MENU;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
    time_date_str[8]='\0';
	

	new_pkt_rdy_to_be_sent = 1;
	lhb_comm_fn_code = FC_VIEW_STORED_DUTYCYCLE;
	while(key_press != 'r')
	{
		ClearWDT();
		WDTCLR
		if((frame_flags.bits.stored_dutycycle_frame_rcvd && rx.data.new_pkt_rcvd && !no_of_logs_to_disp))
        {
            puts_graphical_lcd_rom_lim("NO Data Available......",0,13*1,0,0,arialNarrow_bld_13pix);
		
        }
        else if((frame_flags.bits.stored_dutycycle_frame_rcvd && rx.data.new_pkt_rcvd && no_of_logs_to_disp) | key_press)
		{
             puts_graphical_lcd_rom_lim(disp_strt_date_time,0,0,0,0,arialNarrow_bld_13pix);
             puts_graphical_lcd_rom_lim(disp_curr_date_time,15,0,0,0,arialNarrow_bld_13pix);
//            if((start_ptr +1) < temp_duty_count)
//				end_ptr=start_ptr + 1;
//			else
//				end_ptr = temp_duty_count;	
//            for(temp_count = start_ptr;temp_count < end_ptr;temp_count++ )
//            {    
            memcpy(time_date_str,lcd_dutylog.log.StartTimeStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,0,13*1,0,0,arialNarrow_bld_13pix);
                
            memcpy(time_date_str,lcd_dutylog.log.StartDateStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,7,13*1,0,0,arialNarrow_bld_13pix);
            memcpy(time_date_str,lcd_dutylog.log.CurrTimeStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,16,13*1,0,0,arialNarrow_bld_13pix);
            memcpy(time_date_str,lcd_dutylog.log.CurrDateStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,24,13*1,0,0,arialNarrow_bld_13pix);  
            puts_graphical_lcd_rom_lim(disp_dutycycle_data,0,13*2,0,0,arialNarrow_bld_13pix);
                //puts_graphical_lcd_ram_lim(&fault_log[temp_count].fault_str[0],0,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_rom_lim( (urombyte *)"SERIAL NO.    11  12   21  22   ",0,13*3,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"BLR",8,13*4,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"CDF",8,13*5,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"CMP",8,13*6,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"HTR",8,13*7,1,0,arialNarrow_bld_13pix);
//                str_copy_ram_cnt (lcd_dutylog[0].log.serial_num.byte, lcddest_ptr , 4);
//	            lcddest_ptr += 4;
                strncpy(temp,lcd_dutylog.log.serial_num.byte,4); 
               // temp[3]='\0';
                puts_graphical_lcd_rom_lim(temp,0,13*4,0,0,arialNarrow_bld_13pix);
              
                //src_ptr= lcd_dutylog[0].log.Blowers;
                memcpy(temp, lcd_dutylog.log.Blowers,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,16,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,20,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,24,13*4,0,0,arialNarrow_bld_13pix);
               // src_temp= atoi(dest_ptr);

                memcpy(temp, lcd_dutylog.log.Condenser,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,16,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,20,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,24,13*5,0,0,arialNarrow_bld_13pix);
          
//                src_ptr= lcd_dutylog[0].log.Compressors;
                memcpy(temp, lcd_dutylog.log.Compressors,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,16,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,20,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,24,13*6,0,0,arialNarrow_bld_13pix);
           

                memcpy(temp, lcd_dutylog.log.Heaters,2);

                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*7,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,16,13*7,0,0,arialNarrow_bld_13pix);
           // }
              
//			if(!no_of_logs_to_disp)
//			puts_graphical_lcd_rom_lim("NO Data Available......",0,13*1,0,0,arialNarrow_bld_13pix);

			frame_flags.bits.stored_dutycycle_frame_rcvd = 0;
			rx.data.new_pkt_rcvd = 0;
			frame_flags.integer = 0;	
		}
       
		
		key_press = 0;
		key_press = scan_num_keypad();
		
		if(key_press == 'b')//down
		{	
			home_clr();
            Dutycycle_trip_log();
           
//			if((start_ptr+1) < temp_duty_count)
//				start_ptr += 1;
//			else
		//	{
				//temp_duty_count = 0;
		//		new_pkt_rdy_to_be_sent = 1;
		//		lhb_comm_fn_code = FC_VIEW_NEXT_STORED_DUTYCYCLE;
				//str_fill( &lcd_dutylog[0].log.serial_num.byte[0], 117,' ');
		//		stat.bits.request_nxt_fault_frame = 1;
		//	}
		}
		
		if(key_press == 'a')
		{	
			home_clr();
            load_status_of_plant_screen_data(); 
//			if((start_ptr-1) >= 0)
//				start_ptr -= 1;
//			else
			//{
			//	temp_duty_count = 0;
			//	new_pkt_rdy_to_be_sent = 1;
			//	lhb_comm_fn_code = FC_VIEW_PREV_STORED_DUTYCYCLE;
				//str_fill( &fault_log[0].fault_str[0], 117,' ');
			//	stat.bits.request_nxt_fault_frame = 1;
			//}	
		
		}
		
			
		if(keys[0].long_press)// 7 set temp 
			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
//           if(key_press == 'r')
//          {
//      	     menu_mode_options();  //lakh
//           }

		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
	}
	//str_fill( &lcd_dutylog[0].log.serial_num.byte[0], 117,' ');
}






/*

void load_dutycycle_data(void)
{
    ubyte temp[5]={0,0,0,0,'\0'};
    ubyte src_temp1[2]={0};
    ubyte time_date_str[8]={0};
	ubyte temp_count, start_ptr = 0,end_ptr;
	previous_menu_selection =  menu_selection;
	menu_selection = DUTY_CYCLE_DATA_MENU;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
    
//	Delays_Xtal_ms(1000);
//	puts_graphical_lcd_rom_lim(disp_fault_data,0,0,0,0,arialNarrow_bld_13pix);
	//ubyte *lcddest_ptr= &lcd_dutylog[FAULTS_ON_LCD];
//    ubyte *dest_ptr=&temp;
//    ubyte *src_ptr=&lcd_dutylog[0].log.serial_num.byte;
	new_pkt_rdy_to_be_sent = 1;
	lhb_comm_fn_code = FC_VIEW_STORED_DUTYCYCLE;
	while(key_press != 'r')
	{
		ClearWDT();
		WDTCLR
		
        puts_graphical_lcd_rom_lim(disp_strt_date_time,0,0,0,0,arialNarrow_bld_13pix);
        puts_graphical_lcd_rom_lim(disp_curr_date_time,15,0,0,0,arialNarrow_bld_13pix);
		if((frame_flags.bits.stored_dutycycle_frame_rcvd && rx.data.new_pkt_rcvd) | key_press)
		{
//            if((start_ptr+1) < temp_duty_count)
//				end_ptr=start_ptr + 1;
//			else
//				end_ptr = temp_duty_count;	
       //     for(temp_count = start_ptr;temp_count < end_ptr;temp_count++ )
         //   {    
            memcpy(time_date_str,lcd_dutylog[0].log.StartTimeStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,0,13*1,0,0,arialNarrow_bld_13pix);
                
            memcpy(time_date_str,lcd_dutylog[0].log.StartDateStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,7,13*1,0,0,arialNarrow_bld_13pix);
            memcpy(time_date_str,lcd_dutylog[0].log.CurrTimeStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,16,13*1,0,0,arialNarrow_bld_13pix);
            memcpy(time_date_str,lcd_dutylog[0].log.CurrDateStamp.byte,8);
            puts_graphical_lcd_rom_lim(time_date_str,24,13*1,0,0,arialNarrow_bld_13pix);  
            puts_graphical_lcd_rom_lim(disp_dutycycle_data,0,13*2,0,0,arialNarrow_bld_13pix);
                //puts_graphical_lcd_ram_lim(&fault_log[temp_count].fault_str[0],0,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_rom_lim( (urombyte *)"SERIAL NO.    11  12  21  22    ",0,13*3,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"BLR",8,13*4,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"CDF",8,13*5,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"CMP",8,13*6,1,0,arialNarrow_bld_13pix);
                puts_graphical_lcd_rom_lim( (urombyte *)"HTR",8,13*7,1,0,arialNarrow_bld_13pix);
//                str_copy_ram_cnt (lcd_dutylog[0].log.serial_num.byte, lcddest_ptr , 4);
//	            lcddest_ptr += 4;
                strncpy(temp,lcd_dutylog[0].log.serial_num.byte,4); 
               // temp[3]='\0';
                puts_graphical_lcd_rom_lim(temp,0,13*4,0,0,arialNarrow_bld_13pix);
              
                //src_ptr= lcd_dutylog[0].log.Blowers;
                memcpy(temp, lcd_dutylog[0].log.Blowers,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,16,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,20,13*4,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,24,13*4,0,0,arialNarrow_bld_13pix);
               // src_temp= atoi(dest_ptr);

                memcpy(temp, lcd_dutylog[0].log.Condenser,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,16,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,20,13*5,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,24,13*5,0,0,arialNarrow_bld_13pix);
          
//                src_ptr= lcd_dutylog[0].log.Compressors;
                memcpy(temp, lcd_dutylog[0].log.Compressors,4);
                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,16,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[2]);
                puts_graphical_lcd_rom_lim(src_temp1,20,13*6,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[3]);
                puts_graphical_lcd_rom_lim(src_temp1,24,13*6,0,0,arialNarrow_bld_13pix);
           

                memcpy(temp, lcd_dutylog[0].log.Heaters,2);

                sprintf(src_temp1,"%d",temp[0]);
                puts_graphical_lcd_rom_lim(src_temp1,12,13*7,0,0,arialNarrow_bld_13pix);
                sprintf(src_temp1,"%d",temp[1]);
                puts_graphical_lcd_rom_lim(src_temp1,16,13*7,0,0,arialNarrow_bld_13pix);
           // }
              
			if(!no_of_logs_to_disp)
			puts_graphical_lcd_rom_lim("NO Data Available......",0,13*1,0,0,arialNarrow_bld_13pix);

			frame_flags.bits.stored_dutycycle_frame_rcvd = 0;
			rx.data.new_pkt_rcvd = 0;
			frame_flags.integer = 0;	
		}
		
		key_press = 0;
		key_press = scan_num_keypad();
		
		if(key_press == 'b')//down
		{	
			home_clr();
//			if((start_ptr+1) < temp_duty_count)
//				start_ptr += 1;
//			else
			{
				temp_duty_count = 0;
				new_pkt_rdy_to_be_sent = 1;
				lhb_comm_fn_code = FC_VIEW_STORED_DUTYCYCLE;
				//str_fill( &lcd_dutylog[0].log.serial_num.byte[0], 117,' ');
				stat.bits.request_nxt_fault_frame = 1;
			}
		}
		
		if(key_press == 'a')
		{	
			home_clr();
//			if((start_ptr-1) >= 0)
//				start_ptr -= 1;
//			else
			{
				temp_duty_count = 0;
				new_pkt_rdy_to_be_sent = 1;
				lhb_comm_fn_code = FC_VIEW_PREV_STORED_DUTYCYCLE;
				//str_fill( &fault_log[0].fault_str[0], 117,' ');
				stat.bits.request_nxt_fault_frame = 1;
			}	
		
		}
		
			
		if(keys[0].long_press)// 7 set temp 
			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
//           if(key_press == 'r')
//          {
//      	     menu_mode_options();  //lakh
//           }

		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
	}
	//str_fill( &lcd_dutylog[0].log.serial_num.byte[0], 117,' ');
}

*/
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void change_date_and_time(void)
{
	//	#define	TOTAL_DATA_FIELDS 16
	ubyte TOTAL_DATA_FIELDS = 16;	
	ubyte cnt;
	ubyte arr_cnt;
	ubyte last_blink_status = 0;
	ubyte blink_char_arr[16+1];
	ubyte char_field_cnt = 0;
	ubyte date,month,year,hrs,min,sec;

	ubyte currval_of_char_fields[16+1];
	ubyte first_packet = 0;
	uinteger total_chars = 0;
	ubyte temp_cnt = 0;
	uinteger temp_total_chars = 0;
    previous_menu_selection =  menu_selection;
	menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;
	item_selection = SELECT_CHANGE_DATE_TIME;
	currval_of_char_fields[0] = '0';	
	currval_of_char_fields[1] = '0';	
	currval_of_char_fields[2] = '/';	
	currval_of_char_fields[3] = '0';	
	currval_of_char_fields[4] = '0';	
	currval_of_char_fields[5] = '/';	
	currval_of_char_fields[6] = '0';	
	currval_of_char_fields[7] = '0';
		
	currval_of_char_fields[8] = '0';	
	currval_of_char_fields[9] = '0';	
	currval_of_char_fields[10] = ':';	
	currval_of_char_fields[11] = '0';	
	currval_of_char_fields[12] = '0';	
	currval_of_char_fields[13] = ':';	
	currval_of_char_fields[14] = '0';	
	currval_of_char_fields[15] = '0';	

	arr_cnt = 0;
	currval_of_char_fields[TOTAL_DATA_FIELDS] = 0;
	for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
		blink_char_arr[cnt] = currval_of_char_fields[cnt];
	date = 0;
	month = 0;
	year = 0;
	hrs = 0;
	min = 0;
	sec = 0;
	stat.bits.refresh_display = 1;

	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim(disp_change_date_and_time,0,0,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim(disp_date,0,13,0,0,arialNarrow_bld_13pix);
	datetime_change_screen_readdata_x[0] = last_x_pos; 
	
	puts_graphical_lcd_rom_lim(disp_time,15,13,0,0,arialNarrow_bld_13pix);
	datetime_change_screen_readdata_x[1] = last_x_pos; 
	
	puts_graphical_lcd_rom_lim(disp_date_and_time_from_lhb,0,13*2,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim(disp_date,0,13*3,0,0,arialNarrow_bld_13pix);
	datetime_change_screen_readdata_x[2] = last_x_pos; 
	
	puts_graphical_lcd_rom_lim(disp_time,15,13*3,0,0,arialNarrow_bld_13pix);
	datetime_change_screen_readdata_x[3] = last_x_pos; 
	
	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*4,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim(disp_Press_ack_to_save,0,13*5,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*6,0,0,arialNarrow_bld_13pix);
	
	IEC0bits.T2IE = 1;
	while(key_press != 'e' && key_press !='r')  //lakh
	{	
		WDTCLR
		if(frame_flags.bits.date_time_frame_rcvd && rx.data.new_pkt_rcvd)
		{
			if(!first_packet)
			{
					temp_total_chars = str_copy_ram_lim_ret(coach_info.date.byte,&currval_of_char_fields[0],',');
					char_to_dec_byte_cnt( coach_info.date.value.dd, &date, 2);
					char_to_dec_byte_cnt( coach_info.date.value.mm, &month, 2);
					char_to_dec_byte_cnt( coach_info.date.value.yy, &year, 2);
		
					total_chars = str_copy_ram_lim_ret(coach_info.time.byte,&currval_of_char_fields[8],',');
					char_to_dec_byte_cnt( coach_info.time.value.hh, &hrs, 2);
					char_to_dec_byte_cnt( coach_info.time.value.mm, &min, 2);
					char_to_dec_byte_cnt( coach_info.time.value.ss, &sec, 2);
	
					if(temp_total_chars)
						temp_total_chars--;
					if(total_chars)
						total_chars--;
					temp_cnt  =0; 
			}	
			if(temp_total_chars)
			{
				temp_cnt = 0;
				puts_graphical_lcd_ram_cnt(&coach_info.date.byte[0],datetime_change_screen_readdata_x[2],13*3,0,1,arialNarrow_bld_13pix);
				temp_cnt++;
				for(;temp_cnt  < 8  ;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&coach_info.date.byte[temp_cnt],last_x_pos,13*3,0,1,arialNarrow_bld_13pix);
				}
			}	
			if(total_chars)
			{
				temp_cnt = 0;
				puts_graphical_lcd_ram_cnt(&coach_info.time.byte[temp_cnt],datetime_change_screen_readdata_x[3],13*3,0,1,arialNarrow_bld_13pix);
				temp_cnt++;
				for(;temp_cnt < 8;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&coach_info.time.byte[temp_cnt],last_x_pos,13*3,0,1,arialNarrow_bld_13pix);
				}
			}
		
			stat.bits.refresh_display = 1;
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;
		}
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'b' && temp_total_chars && total_chars)//dwn to decrease data
		{
			switch(char_field_cnt)
			{
				case 0:
						if(date > 1)
							date--;
						else
							date = 31;
						currval_of_char_fields[0] = date/10+ 0x30;
						currval_of_char_fields[1] = date%10+ 0x30;	
				break;
				case 3:		
					if(month > 1)
						month--;
					else
						month = 12;	
					currval_of_char_fields[3] = month/10+ 0x30;
					currval_of_char_fields[4] = month%10+ 0x30;	
				break;
				case 6:
					if(year > 0)
						year--;
					else
						year = 99;
					currval_of_char_fields[6] = year/10+ 0x30;
					currval_of_char_fields[7] = year%10+ 0x30;	
				break;	
				case 8:
					if(hrs > 0)
						hrs--;
					else
						hrs = 23;
					currval_of_char_fields[8] = hrs/10+ 0x30;
					currval_of_char_fields[9] = hrs%10+ 0x30;	
				break;
				case 11:
						if(min > 0)
							min--;
						else
							min = 59;
						currval_of_char_fields[11] = min/10+ 0x30;
						currval_of_char_fields[12] = min%10+ 0x30;	
				break;
				case 14:					
						if(sec > 0)
							sec--;
						else
							sec = 59;
						currval_of_char_fields[14] = sec/10+ 0x30;
						currval_of_char_fields[15] = sec%10+ 0x30;
				break;
				
			}				
		
		 	stat.bits.refresh_display = 1;
		}

		if(key_press == 'a' && temp_total_chars && total_chars)//up to increase data
		{
			switch(char_field_cnt)
			{
				case 0:
						if(date < 31)
							date++;
						else
							date = 1;
						currval_of_char_fields[0] = date/10+ 0x30;
						currval_of_char_fields[1] = date%10+ 0x30;	
				break;
				case 3:		
					if(month < 12)
						month++;
					else
						month = 1;	
					currval_of_char_fields[3] = month/10+ 0x30;
					currval_of_char_fields[4] = month%10+ 0x30;	
				break;
				case 6:
					if(year < 99)
						year++;
					else
						year = 0;
					currval_of_char_fields[6] = year/10+ 0x30;
					currval_of_char_fields[7] = year%10+ 0x30;	
				break;	
				case 8:
					if(hrs < 23)
						hrs++;
					else
						hrs = 0;
					currval_of_char_fields[8] = hrs/10+ 0x30;
					currval_of_char_fields[9] = hrs%10+ 0x30;	
				break;
				case 11:
						if(min < 59)
							min++;
						else
							min = 0;
						currval_of_char_fields[11] = min/10+ 0x30;
						currval_of_char_fields[12] = min%10+ 0x30;	
				break;
				case 14:					
						if(sec < 59)
							sec++;
						else
							sec = 0;
						currval_of_char_fields[14] = sec/10+ 0x30;
						currval_of_char_fields[15] = sec%10+ 0x30;
				break;
				
			}	
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'c' && temp_total_chars && total_chars)//ack
		{
			str_copy_ram_cnt(&currval_of_char_fields[0],coach_info.date.byte,temp_total_chars);		
			str_copy_ram_cnt(&currval_of_char_fields[8],coach_info.time.byte,total_chars);			
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			
			lhb_comm_fn_code = FC_DATE_TIME;
		 //put curr value into data struct
		}
		if(key_press == 'L' && temp_total_chars && total_chars)//right
		{
			if(char_field_cnt < TOTAL_DATA_FIELDS-1)
			{
				switch(char_field_cnt)
				{
					case 0:char_field_cnt= 3;
					break;
					case 3:char_field_cnt= 6;
					break;
					case 6:char_field_cnt= 8;
					break;
					case 8:char_field_cnt= 11;
					break;
					case 11:char_field_cnt= 14;
					break;
					case 14:char_field_cnt= 0;
					break;
				}
			}
		 	else
		 		char_field_cnt = 0;
		 	arr_cnt = 0;
		 	stat.bits.refresh_display = 1;
		}
		if(last_blink_status != timer2.stat.bits.blinker_status)
		{
			last_blink_status = timer2.stat.bits.blinker_status;
			stat.bits.refresh_display = 1;
		}
		
		if(stat.bits.refresh_display)
		{
			IEC0bits.T2IE = 0;
			for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
				blink_char_arr[cnt] = currval_of_char_fields[cnt];
			blink_char_arr[char_field_cnt] = '_';
			blink_char_arr[char_field_cnt+1] = '_';
			if(timer2.stat.bits.blinker_status)
			{	
				if(temp_total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],datetime_change_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;temp_cnt  < 8  ;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
					}
				}	
				if(total_chars)
				{
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],datetime_change_screen_readdata_x[1],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;temp_cnt < 16;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
					}
				}

			}				
			else
			{	
				if(temp_total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],datetime_change_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;temp_cnt  < 8  ;temp_cnt++)
						puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
				}	
				if(total_chars)
				{
	
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],datetime_change_screen_readdata_x[1],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;temp_cnt < 16;temp_cnt++)
						puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
				}
			}
			
			IEC0bits.T2IE = 1;
			stat.bits.refresh_display = 0;
		}
		
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			change_date_and_time();
		}		
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void change_Coach_no(void)
{
	ubyte TOTAL_DATA_FIELDS = 12;
	ubyte cnt = 0;
	ubyte arr_cnt = 0;
	ubyte last_blink_status = 0;
	ubyte blink_char_arr[TOTAL_DATA_FIELDS+1];
	ubyte char_field_cnt = 0;
	ubyte x_pos_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte minval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte maxval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte currval_of_char_fields[TOTAL_DATA_FIELDS+1];
	ubyte first_packet = 0;
	uinteger total_chars = 0;;
	ubyte temp_cnt = 0;
previous_menu_selection =  menu_selection;
	menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;
	item_selection = SELECT_CHANGE_COACH_NO;
	
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		minval_of_char_fields[cnt] = '0';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		maxval_of_char_fields[cnt] = 'Z';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		currval_of_char_fields[cnt] = '0';
	arr_cnt = 2;
	currval_of_char_fields[TOTAL_DATA_FIELDS] = ',';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
		blink_char_arr[cnt] = currval_of_char_fields[cnt];
	stat.bits.refresh_display = 1;


	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim(disp_change_Coach_no,0,0,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_temp_coach_no,0,13*1,0,0,arialNarrow_bld_13pix);
	coachno_change_screen_readdata_x[0] = last_x_pos; 
	
	puts_graphical_lcd_rom_lim(disp_coach_no,0,13*2,0,0,arialNarrow_bld_13pix);
	coachno_change_screen_readdata_x[1] = last_x_pos; 
	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*3,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_ack_to_save,0,13*4,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*5,0,0,arialNarrow_bld_13pix);
	
	IEC0bits.T2IE = 1;
	first_packet = 0;
	while(key_press != 'e' && key_press !='r')//lakh
	{
		WDTCLR
		if(frame_flags.bits.coach_no_frame_rcvd && rx.data.new_pkt_rcvd)
		{
			if(!first_packet)
			{
				total_chars = str_copy_ram_lim_ret(coach_info.coach_no,currval_of_char_fields,',') - 1;
				temp_cnt  =0; 
				arr_cnt = str_search_ram( &coach_info.coach_no[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
			}	
			if(total_chars)
			{
				temp_cnt = 0;
				puts_graphical_lcd_ram_cnt(&coach_info.coach_no[0],coachno_change_screen_readdata_x[1],13*2,0,1,arialNarrow_bld_13pix);
				temp_cnt++;
			}
			for(;temp_cnt < total_chars;temp_cnt++)
			{
				puts_graphical_lcd_ram_cnt(&coach_info.coach_no[temp_cnt],last_x_pos,13*2,0,1,arialNarrow_bld_13pix);
			}
			stat.bits.refresh_display = 1;
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;	
		}
		
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'b' && total_chars)//dwn to decrease data
		{
			if(arr_cnt > 2)
				arr_cnt--;
			else
				arr_cnt = sizeof(char_value_arr);
			if(currval_of_char_fields[char_field_cnt] > minval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = maxval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'a'&& total_chars)//up to increase data
		{
			if(arr_cnt < sizeof(char_value_arr))
				arr_cnt++;
			else
				arr_cnt = 2;
			if(currval_of_char_fields[char_field_cnt] < maxval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = minval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'c'&& total_chars)//ack
		{
			str_copy_ram_cnt(currval_of_char_fields,coach_info.coach_no,total_chars);			
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			lhb_comm_fn_code = FC_COACH_NO;
		 //put curr value into data struct
		}
		if(key_press == 'L'&& total_chars)//right
		{
			if(char_field_cnt < total_chars)
		 		char_field_cnt++;
		 	else
		 		char_field_cnt = 0;
		 	arr_cnt = str_search_ram( &coach_info.coach_no[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
		 	stat.bits.refresh_display = 1;
		}
		if(last_blink_status != timer2.stat.bits.blinker_status)
		{
			last_blink_status = timer2.stat.bits.blinker_status;
			stat.bits.refresh_display = 1;
		}
		
		if(stat.bits.refresh_display)
		{
			IEC0bits.T2IE = 0;
			for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
				blink_char_arr[cnt] = currval_of_char_fields[cnt];
			blink_char_arr[char_field_cnt] = '_';
			if(timer2.stat.bits.blinker_status)
			{					
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[0],coachno_change_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
				}
				for(;temp_cnt < total_chars;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
				}				
			}				
			else
			{									
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[0],coachno_change_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
				}
				for(;temp_cnt < total_chars;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
				}		
			}
			
			IEC0bits.T2IE = 1;
			stat.bits.refresh_display = 0;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			change_Coach_no();
		}		
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void change_Unit_no(void)
{
	ubyte TOTAL_DATA_FIELDS = 12;
	ubyte cnt = 0;
	ubyte arr_cnt = 0;
	ubyte last_blink_status = 0;
	ubyte blink_char_arr[TOTAL_DATA_FIELDS+1];
	ubyte char_field_cnt = 3;
	ubyte x_pos_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte minval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte maxval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte currval_of_char_fields[TOTAL_DATA_FIELDS+1];
	ubyte first_packet = 0;
	uinteger total_chars = 0;;
	ubyte temp_cnt = 0;
previous_menu_selection =  menu_selection;
	menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;
	item_selection = SELECT_CHANGE_UNIT_NO;
	
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		minval_of_char_fields[cnt] = '0';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		maxval_of_char_fields[cnt] = 'Z';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		currval_of_char_fields[cnt] = '0';
	arr_cnt = 2;
	currval_of_char_fields[TOTAL_DATA_FIELDS] = ',';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
		blink_char_arr[cnt] = currval_of_char_fields[cnt];
	stat.bits.refresh_display = 1;


	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim(disp_change_Unit_no,0,0,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_temp_unit_no,0,13*1,0,0,arialNarrow_bld_13pix);
	coachno_change_screen_readdata_x[0] = last_x_pos; 
	
	puts_graphical_lcd_rom_lim("UNIT NO:",0,13*2,0,0,arialNarrow_bld_13pix);
	coachno_change_screen_readdata_x[1] = last_x_pos; 
	puts_graphical_lcd_rom_lim("Unit id format: ",0,13*5,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim("APLXXYYMMSSS",14,13*5,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*7,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_ack_to_save,0,13*8,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*9,0,0,arialNarrow_bld_13pix);
	
	IEC0bits.T2IE = 1;
	first_packet = 0;
	while(key_press != 'e' && key_press !='r') //lakh
	{
		if(frame_flags.bits.unit_no_frame_rcvd && rx.data.new_pkt_rcvd)
		{
			if(!first_packet)
			{
				total_chars = str_copy_ram_lim_ret(coach_info.unit_no,currval_of_char_fields,',') - 1;
				temp_cnt  =0; 
				arr_cnt = str_search_ram( &coach_info.unit_no[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
			}	
			if(total_chars)
			{
				temp_cnt = 0;
				puts_graphical_lcd_ram_cnt(&coach_info.unit_no[0],coachno_change_screen_readdata_x[1],13*2,0,1,arialNarrow_bld_13pix);
				temp_cnt++;
			}
			for(;temp_cnt < total_chars;temp_cnt++)
			{
				puts_graphical_lcd_ram_cnt(&coach_info.unit_no[temp_cnt],last_x_pos,13*2,0,1,arialNarrow_bld_13pix);
			}
			stat.bits.refresh_display = 1;
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;	
		}
		
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'b' && total_chars)//dwn to decrease data
		{
			if(arr_cnt > 2)
				arr_cnt--;
			else
				arr_cnt = sizeof(char_value_arr);
			if(currval_of_char_fields[char_field_cnt] > minval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = maxval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'a'&& total_chars)//up to increase data
		{
			if(arr_cnt < sizeof(char_value_arr))
				arr_cnt++;
			else
				arr_cnt = 2;
			if(currval_of_char_fields[char_field_cnt] < maxval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = minval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'c'&& total_chars)//ack
		{
			str_copy_ram_cnt(currval_of_char_fields,coach_info.unit_no,total_chars);			
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			lhb_comm_fn_code = FC_UNIT_NO;
		 //put curr value into data struct
		}
		if(key_press == 'L'&& total_chars)//right
		{
			if(char_field_cnt < total_chars)
		 		char_field_cnt++;
		 	else
		 		char_field_cnt = 3;
		 	arr_cnt = str_search_ram( &coach_info.unit_no[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
		 	stat.bits.refresh_display = 1;
		}
		if(last_blink_status != timer2.stat.bits.blinker_status)
		{
			last_blink_status = timer2.stat.bits.blinker_status;
			stat.bits.refresh_display = 1;
		}
		
		if(stat.bits.refresh_display)
		{
			IEC0bits.T2IE = 0;
			for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
				blink_char_arr[cnt] = currval_of_char_fields[cnt];
			blink_char_arr[char_field_cnt] = '_';
			if(timer2.stat.bits.blinker_status)
			{					
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[0],coachno_change_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
				}
				for(;temp_cnt < total_chars;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
				}				
			}				
			else
			{									
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[0],coachno_change_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
				}
				for(;temp_cnt < total_chars;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
				}		
			}
			
			IEC0bits.T2IE = 1;
			stat.bits.refresh_display = 0;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			change_Unit_no();
		}		
	}
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void change_RMPU_no(void)
{
	//	#define	TOTAL_DATA_FIELDS 25
	ubyte TOTAL_DATA_FIELDS = 25;
	ubyte cnt;
	ubyte arr_cnt;
	ubyte last_blink_status = 0;
	ubyte blink_char_arr[TOTAL_DATA_FIELDS+1];
	ubyte char_field_cnt = 0;
	ubyte x_pos_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte minval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte maxval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte currval_of_char_fields[TOTAL_DATA_FIELDS+1];
	ubyte first_packet = 0;
	uinteger total_chars = 0;
	ubyte temp_cnt = 0;
	ubyte slash_char_cnt = 0;
previous_menu_selection =  menu_selection;
	menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;
	item_selection = SELECT_CHANGE_RMPU_NO;
	
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		minval_of_char_fields[cnt] = '0';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		maxval_of_char_fields[cnt] = 'Z';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		currval_of_char_fields[cnt] = '0';
	arr_cnt = 2;
	currval_of_char_fields[TOTAL_DATA_FIELDS] = ',';
	currval_of_char_fields[12] = '/';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
		blink_char_arr[cnt] = currval_of_char_fields[cnt];
	stat.bits.refresh_display = 1;

	IEC0bits.T2IE = 0;	
	key_press = 0;
		clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim(disp_change_RMPU_no,0,0,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim(disp_temp_rmpu_no,0,13,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_rmpu_no,0,13*4,0,0,arialNarrow_bld_13pix);
	rmpuno_change_screen_readdata_x[0] = 0; 
	rmpuno_change_screen_readdata_x[1] = 0; 
	rmpuno_change_screen_readdata_x[2] = 0; //change to 15
	rmpuno_change_screen_readdata_x[3] = 0; //change to 15
	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*7,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_ack_to_save,0,13*8,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*9,0,0,arialNarrow_bld_13pix);
	
	IEC0bits.T2IE = 1;
	while(key_press != 'e' &&  key_press !='r')//lakh
	{
		if(frame_flags.bits.rmpu_no_frame_rcvd && rx.data.new_pkt_rcvd)
		{
			if(!first_packet)
			{
					total_chars = str_copy_ram_lim_ret(coach_info.rmpu_no_pp_npp,currval_of_char_fields,',');
					if(total_chars)
						total_chars--;
					temp_cnt  =0; 
					arr_cnt = str_search_ram( &coach_info.rmpu_no_pp_npp[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
			}
			if(total_chars)
			{
				temp_cnt = 0;
				puts_graphical_lcd_ram_cnt(&coach_info.rmpu_no_pp_npp[temp_cnt],rmpuno_change_screen_readdata_x[2],13*5,0,1,arialNarrow_bld_13pix);
				temp_cnt++;				
				for(;(temp_cnt  < total_chars) && coach_info.rmpu_no_pp_npp[temp_cnt] != '/'   ;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&coach_info.rmpu_no_pp_npp[temp_cnt],last_x_pos,13*5,0,1,arialNarrow_bld_13pix);
				}
				slash_char_cnt = temp_cnt;
				temp_cnt++;
				puts_graphical_lcd_ram_cnt(&coach_info.rmpu_no_pp_npp[temp_cnt],rmpuno_change_screen_readdata_x[3],13*6,0,1,arialNarrow_bld_13pix);
				temp_cnt++;
				for(;(temp_cnt  < total_chars) && coach_info.rmpu_no_pp_npp[temp_cnt] != ',' ;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&coach_info.rmpu_no_pp_npp[temp_cnt],last_x_pos,13*6,0,1,arialNarrow_bld_13pix);
				}
			}	

			stat.bits.refresh_display = 1;
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;	
		}
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'b' && total_chars)//dwn to decrease data
		{
			if(arr_cnt > 2)
				arr_cnt--;
			else
				arr_cnt = sizeof(char_value_arr);
			if(currval_of_char_fields[char_field_cnt] > minval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = maxval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'a'&& total_chars)//up to increase data
		{
			if(arr_cnt < sizeof(char_value_arr))
				arr_cnt++;
			else
				arr_cnt = 2;
			if(currval_of_char_fields[char_field_cnt] < maxval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = minval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'c'&& total_chars)//ack
		{			
			str_copy_ram_lim(currval_of_char_fields,coach_info.rmpu_no_pp_npp,',');		
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			
			lhb_comm_fn_code = FC_RMPU_NO;
		}
		if(key_press == 'L'&& total_chars)//right
		{
			if(char_field_cnt < total_chars-1)
			{
		 		char_field_cnt++;
	 		if(char_field_cnt == slash_char_cnt)//final
	 			char_field_cnt++;
		 	}	
		 	else
		 		char_field_cnt = 0;
		 	arr_cnt = str_search_ram( &coach_info.rmpu_no_pp_npp[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
		 	stat.bits.refresh_display = 1;
		}
		if(last_blink_status != timer2.stat.bits.blinker_status)
		{
			last_blink_status = timer2.stat.bits.blinker_status;
			stat.bits.refresh_display = 1;
		}		
			
		if(stat.bits.refresh_display)
		{
			IEC0bits.T2IE = 0;
			for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
				blink_char_arr[cnt] = currval_of_char_fields[cnt];
			blink_char_arr[char_field_cnt] = '_';
			if(timer2.stat.bits.blinker_status)
			{				
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],rmpuno_change_screen_readdata_x[0],13*2,(0),1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;(temp_cnt  < total_chars) && blink_char_arr[temp_cnt] != '/'   ;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13*2,(0),1,arialNarrow_bld_13pix);
					}
					temp_cnt++;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],rmpuno_change_screen_readdata_x[1],13*3,(0),1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;(temp_cnt  < total_chars) && blink_char_arr[temp_cnt] != ',' ;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13*3,(0),1,arialNarrow_bld_13pix);
					}
				}	

			}				
			else
			{				
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt++],rmpuno_change_screen_readdata_x[0],13*2,0,1,arialNarrow_bld_13pix);
					for(;(temp_cnt  < total_chars) && currval_of_char_fields[temp_cnt] != '/'   ;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13*2,0,1,arialNarrow_bld_13pix);
					}
					temp_cnt++;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt++],rmpuno_change_screen_readdata_x[1],13*3,0,1,arialNarrow_bld_13pix);
					for(;(temp_cnt  < total_chars) && currval_of_char_fields[temp_cnt] != ',' ;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13*3,0,1,arialNarrow_bld_13pix);
					}
				}	

			}
			
			IEC0bits.T2IE = 1;
			stat.bits.refresh_display = 0;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
		
		if(!timer2.display_timeout_5sec.timer)
		{
			home_clr();
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			change_RMPU_no();
		}	
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void change_server_settings(void)
{
	//	#define	TOTAL_DATA_FIELDS 25
	ubyte TOTAL_DATA_FIELDS = 25;
	ubyte cnt;
	ubyte pos;
	ubyte arr_cnt;
	ubyte last_blink_status = 0;
	ubyte blink_char_arr[TOTAL_DATA_FIELDS+1];
	ubyte char_field_cnt = 0;
	ubyte x_pos_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte minval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte maxval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte currval_of_char_fields[TOTAL_DATA_FIELDS+1];
	ubyte first_packet = 0;
	uinteger total_chars = 0;
	ubyte temp_cnt = 0;
	ubyte slash_char_cnt = 0;
	ubyte dig_count = 0;
	ubyte index = 0;
	ubyte line_no = 0;
	previous_menu_selection =  menu_selection;
	menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;
	item_selection = SELECT_CHANGE_RMPU_NO;
	ubyte max_no_of_chars[3] = {15,3,3};
//	ubyte max_no_of_chars[3] = {16,4,4};
	ubyte char_arr[12] ={'0','1','2','3','4','5','6','7','8','9','.','X'} ;
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		minval_of_char_fields[cnt] = '0';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		maxval_of_char_fields[cnt] = 'Z';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		currval_of_char_fields[cnt] = '0';
	arr_cnt = 2;
	currval_of_char_fields[TOTAL_DATA_FIELDS] = ',';
	currval_of_char_fields[12] = '/';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
		blink_char_arr[cnt] = currval_of_char_fields[cnt];
//	stat.bits.refresh_display = 1;
	struct server_struct temp2_server;

	IEC0bits.T2IE = 0;	
	key_press = 0;
		clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim("Change IP Address and Ports:",0,0,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim("IP   : ",0,13,0,0,arialNarrow_bld_13pix);
	rmpuno_change_screen_readdata_x[0] = last_x_pos; 
	puts_graphical_lcd_rom_lim("Async: ",0,13*2,0,0,arialNarrow_bld_13pix);
	rmpuno_change_screen_readdata_x[1] = last_x_pos; 
	puts_graphical_lcd_rom_lim("Sync : ",0,13*3,0,0,arialNarrow_bld_13pix);
	rmpuno_change_screen_readdata_x[2] = last_x_pos; 

	puts_graphical_lcd_rom_lim("Press RIGHT to move Cursor",0,13*7,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*7,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_ack_to_save,0,13*8,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*9,0,0,arialNarrow_bld_13pix);
	
	IEC0bits.T2IE = 1;
	stat.bits.refresh_display = 0;
	ubyte blink_counter=4,blink=0;	
	while(key_press != 'e' && key_press !='r') //lakh
	{
		if(blink_counter==0)
		{
			blink_counter = 5;
			blink = !blink;
		}
		blink_counter--;
		if((frame_flags.bits.rmpu_no_frame_rcvd && rx.data.new_pkt_rcvd)||(stat.bits.refresh_display)||(!blink_counter && first_packet ))
		{
			if(!first_packet)
			{
				temp_server=server;
				str_fill(temp_server.ip_add,16,'X');
				str_copy_ram_lim(server.ip_add,temp_server.ip_add,0);
				temp2_server = temp_server;
				dig_count = temp_server.ip_add[index] -48;
			}
				temp2_server = temp_server;
		
			if(blink)
			{
				if(line_no == 0)
					temp2_server.ip_add[index] = '_';
				else if(line_no == 1)
					temp2_server.port_no[0][index] = '_';
				else if(line_no == 2)
					temp2_server.port_no[1][index] = '_';
			}
			puts_graphical_lcd_ram_cnt(&temp2_server.ip_add[0],5,13*1,0,16,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&temp2_server.port_no[0][0],5,13*2,0,4,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&temp2_server.port_no[1][0],5,13*3,0,4,arialNarrow_bld_13pix);


			puts_graphical_lcd_ram_lim(&server.ip_add[0],17,13*1,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server.port_no[0][0],17,13*2,0,4,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server.port_no[1][0],17,13*3,0,4,arialNarrow_bld_13pix);

			stat.bits.refresh_display = 1;
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;	
		}
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'a')//dwn to decrease data
		{
			dig_count++;
			if(dig_count>11)
				dig_count = 0;
			if(line_no == 0)
				temp_server.ip_add[index] = char_arr[dig_count];
			else if(line_no == 1)
				temp_server.port_no[0][index] = char_arr[dig_count];
			else if(line_no == 2)
				temp_server.port_no[1][index] = char_arr[dig_count];
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'b')//dwn to decrease data
		{
			if(dig_count==0)
				dig_count = 12;
			dig_count--;
			if(line_no == 0)
				temp_server.ip_add[index] = char_arr[dig_count];
			else if(line_no == 1)
				temp_server.port_no[0][index] = char_arr[dig_count];
			else if(line_no == 2)
				temp_server.port_no[1][index] = char_arr[dig_count];
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'c')
		{
			str_fill( server.ip_add, 16, 0);
			str_copy_ram_lim(temp_server.ip_add,server.ip_add,'X');		
			str_copy_ram_cnt(temp_server.port_no[0],server.port_no[0],4);		
			str_copy_ram_cnt(temp_server.port_no[1],server.port_no[1],4);		
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			lhb_comm_fn_code = FC_SERVER;
		}
//		if(key_press == 'e')
//		{
//			line_no++;
//			index = 0;
//			if(line_no==3)
//				line_no = 0;
//			if(line_no == 0)
//				dig_count = temp_server.ip_add[index]-48;
//			else if(line_no == 1)
//				dig_count = temp_server.port_no[0][index]-48;
//			else if(line_no == 2)
//				dig_count = temp_server.port_no[1][index]-48;
//		}
		if(key_press == 'L')//right
		{
			index++;
			if(index>max_no_of_chars[line_no])
			{
				index = 0;
				line_no++;
			}
			if(line_no==3)
				line_no = 0;
			if(line_no == 0)
				dig_count = temp_server.ip_add[index]-48;
			else if(line_no == 1)
				dig_count = temp_server.port_no[0][index]-48;
			else if(line_no == 2)
				dig_count = temp_server.port_no[1][index]-48;				
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();	
	}
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void change_RMPU_make(void)
{
	ubyte TOTAL_DATA_FIELDS = 17;
	ubyte cnt;
	ubyte arr_cnt;
	ubyte last_blink_status = 0;
	ubyte blink_char_arr[TOTAL_DATA_FIELDS+1];
	ubyte char_field_cnt = 0;
	ubyte x_pos_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte minval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte maxval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte currval_of_char_fields[TOTAL_DATA_FIELDS+1];
	ubyte first_packet = 0;
	uinteger total_chars = 0;
	ubyte temp_cnt = 0;
//ubyte slash_char_cnt = 0;
previous_menu_selection =  menu_selection;
	menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;
	item_selection = SELECT_CHANGE_RMPU_MAKE;
	
	
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		minval_of_char_fields[cnt] = ' ';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		maxval_of_char_fields[cnt] = 'Z';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		currval_of_char_fields[cnt] = ' ';
	arr_cnt = 0;
	currval_of_char_fields[TOTAL_DATA_FIELDS] = ',';
	currval_of_char_fields[12] = '/';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
		blink_char_arr[cnt] = currval_of_char_fields[cnt];
	stat.bits.refresh_display = 1;

	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim(disp_change_RMPU_make,0,0,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim(disp_temp_rmpu_make,0,13,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_rmpu_make,0,13*4,0,0,arialNarrow_bld_13pix);
	rmpumake_change_screen_readdata_x[0] = 0; 
	rmpumake_change_screen_readdata_x[1] = 0; 
	rmpumake_change_screen_readdata_x[2] = 0; 
	rmpumake_change_screen_readdata_x[3] = 0; 
	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*7,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_ack_to_save,0,13*8,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*9,0,0,arialNarrow_bld_13pix);
	
	IEC0bits.T2IE = 1;
	while(key_press != 'e' && key_press !='r')//lakh
	{
		if(frame_flags.bits.rmpu_make_frame_rcvd && rx.data.new_pkt_rcvd)
		{
			if(!first_packet)
			{
					total_chars = str_copy_ram_lim_ret(coach_info.rmpu_make_pp_npp,currval_of_char_fields,',');
					temp_cnt  =0; 
					arr_cnt = str_search_ram( &coach_info.rmpu_make_pp_npp[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
			}	
			if(total_chars)
			{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&coach_info.rmpu_make_pp_npp[temp_cnt++],rmpumake_change_screen_readdata_x[2],13*5,0,1,arialNarrow_bld_13pix);
					for(;(temp_cnt  < total_chars) && coach_info.rmpu_make_pp_npp[temp_cnt] != ','   ;temp_cnt++)
						puts_graphical_lcd_ram_cnt(&coach_info.rmpu_make_pp_npp[temp_cnt],last_x_pos,13*5,0,1,arialNarrow_bld_13pix);
	
			}
		
			stat.bits.refresh_display = 1;
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;	
		}
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'b' && total_chars)//dwn to decrease data
		{
			if(arr_cnt > 0)
				arr_cnt--;
			else
				arr_cnt = sizeof(char_value_arr);
			if(currval_of_char_fields[char_field_cnt] > minval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = maxval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'a'&& total_chars)//up to increase data
		{
			if(arr_cnt < sizeof(char_value_arr))
				arr_cnt++;
			else
				arr_cnt = 0;
			if(currval_of_char_fields[char_field_cnt] < maxval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = minval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'c'&& total_chars)//ack
		{			
			str_copy_ram_lim(currval_of_char_fields,coach_info.rmpu_make_pp_npp,',');		
			coach_info.rmpu_make_pp_npp[16] = ',';
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			
			lhb_comm_fn_code = FC_RMPU_MAKE;
		}
		if(key_press == 'L'&& total_chars)//right
		{
			if(char_field_cnt < total_chars-1)
		 		char_field_cnt++;
		 	else
		 		char_field_cnt = 0;
		 		
		 	arr_cnt = str_search_ram( &coach_info.rmpu_make_pp_npp[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));	
	//	 	arr_cnt = 0;
		 	stat.bits.refresh_display = 1;
		}
		if(last_blink_status != timer2.stat.bits.blinker_status)
		{
			last_blink_status = timer2.stat.bits.blinker_status;
			stat.bits.refresh_display = 1;
		}		
			
		if(stat.bits.refresh_display)
		{
			IEC0bits.T2IE = 0;
			for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
				blink_char_arr[cnt] = currval_of_char_fields[cnt];
			blink_char_arr[char_field_cnt] = '_';
			if(timer2.stat.bits.blinker_status)
			{	
				if(total_chars)
				{				
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],rmpumake_change_screen_readdata_x[0],13*2,(0),1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;(temp_cnt  < total_chars) && blink_char_arr[temp_cnt] != ','   ;temp_cnt++)
						puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13*2,(0),1,arialNarrow_bld_13pix);
				}

			}				
			else
			{	
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt++],rmpumake_change_screen_readdata_x[0],13*2,0,1,arialNarrow_bld_13pix);
					for(;(temp_cnt  < total_chars) && currval_of_char_fields[temp_cnt] != ','   ;temp_cnt++)
						puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13*2,0,1,arialNarrow_bld_13pix);

				}

			}
	
			IEC0bits.T2IE = 1;
			stat.bits.refresh_display = 0;
		}
	
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			change_RMPU_make();
		}		
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_exclu_test_cmd(ubyte relay_type)
{	

	ubyte temp_byte = 0;

	switch(relay_type)
	{
		case  0: temp_byte = test1.bits.t1; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				 test1.bits.t1 = !temp_byte;
				 //on_off = test1.bits.t1;	
		break;
		case  1:temp_byte = test1.bits.t2; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t2 = !temp_byte;
				 //on_off = test1.bits.t2;		
		break;
		case  2:temp_byte = test1.bits.t3; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				 test1.bits.t3 = !temp_byte;
				 //on_off = test1.bits.t3;		
		break;
		case  3:temp_byte = test1.bits.t4; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				 test1.bits.t4 = !temp_byte;
				 //on_off = test1.bits.t4;		
		break;
		case  4: temp_byte = test1.bits.t5; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t5 = !temp_byte;	
				 //on_off = test1.bits.t5;	
		break;
		case  5: temp_byte = test1.bits.t6; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t6 = !temp_byte;	
				 //on_off = test1.bits.t6;	
		break;
		case  6: temp_byte = test1.bits.t7; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t7 = !temp_byte;
				 //on_off = test1.bits.t7;		
		break;
		case  7: temp_byte = test1.bits.t9; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t9 = !temp_byte;
				 //on_off = test1.bits.t9;		
		break;
		case  8: temp_byte = test1.bits.t10; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t10 = !temp_byte;
				 //on_off = test1.bits.t10;		
		break;
		case  9: temp_byte = test1.bits.t11; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t11 = !temp_byte;
				 //on_off = test1.bits.t11;		
		break;
		case  10: temp_byte = test1.bits.t12; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t12 = !temp_byte;
				 //on_off = test1.bits.t12;		
		break;
		case  11: temp_byte = test1.bits.t13; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t13 = !temp_byte;
				 //on_off = test1.bits.t13;		
		break; 
		case  12: temp_byte = test1.bits.t14; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t14 = !temp_byte;
				 //on_off = test1.bits.t14;		
		break; 
		case  13: temp_byte = test1.bits.t15; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test1.bits.t15 = !temp_byte;
				 //on_off = test1.bits.t15;		
		break;		
		case  14: temp_byte = test2.bits.t17; //byte3
				 test1.test_int = 0;
				 test2.test_int = 0;
				test2.bits.t17 = !temp_byte;
				 //on_off = test2.bits.t17;			
		break;
		case  15: temp_byte = test2.bits.t18; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test2.bits.t18 = !temp_byte;
				 //on_off = test2.bits.t18;		
		break;
		case  16:temp_byte = test2.bits.t19; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test2.bits.t19 = !temp_byte;
				 //on_off = test2.bits.t19;		
		break;
		case  17: temp_byte = test2.bits.t20; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test2.bits.t20 = !temp_byte;
				 //on_off = test2.bits.t20;		
		break;
		case  18:temp_byte = test2.bits.t21; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test2.bits.t21 = !temp_byte;
				 //on_off = test2.bits.t21;		
		break;
		case  19:temp_byte = test2.bits.t22; 
				 test1.test_int = 0;
				 test2.test_int = 0;
				test2.bits.t22 = !temp_byte;
				 //on_off = test2.bits.t22;		
		break;
	}	

}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_override_cmd(ubyte feedback_type)
{	

	ubyte temp_byte = 0;

	switch(feedback_type)
	{
		case  0: override.stat.blower_fan_00_fault_override = !override.stat.blower_fan_00_fault_override;
		break;
		case  1: override.stat.blower_fan_01_fault_override = !override.stat.blower_fan_01_fault_override;
		break;
		case  2: override.stat.blower_fan_10_fault_override = !override.stat.blower_fan_10_fault_override;
		break;
		case  3: override.stat.blower_fan_11_fault_override = !override.stat.blower_fan_11_fault_override;
		break;
		case  4: override.stat.heater_0_fault_override = !override.stat.heater_0_fault_override;
		break;
		case  5: override.stat.heater_1_fault_override = !override.stat.heater_1_fault_override;
		break;
		case  6: override.stat.cond_00_fault_override =	!override.stat.cond_00_fault_override;
		break;
		case  7: override.stat.cond_01_fault_override =	!override.stat.cond_01_fault_override;
		break;
		case  8: override.stat.cond_10_fault_override =	!override.stat.cond_10_fault_override;
		break;
		case  9: override.stat.cond_11_fault_override =	!override.stat.cond_11_fault_override;
		break;
		case  10: override.stat.LP_00_fault_override = !override.stat.LP_00_fault_override;
		break;
		case  11: override.stat.LP_01_fault_override =	!override.stat.LP_01_fault_override;
		break; 
		case  12: override.stat.LP_10_fault_override =	!override.stat.LP_10_fault_override;
		break; 
		case  13: override.stat.LP_11_fault_override =	!override.stat.LP_11_fault_override;
		break;		
		case  14: override.stat.HP_00_fault_override = !override.stat.HP_00_fault_override;
		break;
		case  15: override.stat.HP_01_fault_override =	!override.stat.HP_01_fault_override;
		break;
		case  16: override.stat.HP_10_fault_override =	!override.stat.HP_10_fault_override;
		break;
		case  17: override.stat.HP_11_fault_override =	!override.stat.HP_11_fault_override;
		break;
//		case  18:test2.bits.t21 =
//		break;
//		case  19:test2.bits.t22 =	
//		break;
	}

}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void send_normal_test_cmd(ubyte relay_type)
{	
	ubyte temp_byte = 0;
	message_type  = 0;// no message display
	
	switch(relay_type)
	{
		case  0: temp_byte = test1.bits.t1; //computer ok
				 test1.bits.t1 = !temp_byte;
				 if( !test1.bits.t1)
				 {
					 test1.test_int=0;
					 test2.test_int=0;
				 } 
				 //on_off = test1.bits.t1;	
		break;
		case  1: temp_byte = test1.bits.t2; //spare
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test1.bits.t2 = test1.bits.t1 && !temp_byte;
				
		break;
		case  2: temp_byte = test1.bits.t3; //sf1
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test1.bits.t3 = test1.bits.t1 && !temp_byte;
				 	
				 if(!test1.bits.t3 && !test1.bits.t4)
				 {
					 test1.bits.t5 = 0;
					 test1.bits.t6 = 0;
					 test1.bits.t7 = 0;
					 test1.bits.t9 = 0;
					 test1.bits.t10 = 0;
					 test1.bits.t11 = 0;
					 
				 }
				 ///on_off = test1.bits.t3;	
		break;
		case  3:temp_byte = test1.bits.t4;  //sf2
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test1.bits.t4 = test1.bits.t1 && !temp_byte;
				 	
				 if(!test1.bits.t3 && !test1.bits.t4)
				 {
					 test1.bits.t5 = 0;
					 test1.bits.t6 = 0;
					 test1.bits.t7 = 0;
					 test1.bits.t9 = 0;
					 test1.bits.t10 = 0;
					 test1.bits.t11 = 0;
				 }
				 //on_off = test1.bits.t4;	
		break;
		case  4: temp_byte = test1.bits.t5; //spare
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test1.bits.t5 = test1.bits.t1 && !temp_byte;
				 	
				 	
		break;
		case  5: temp_byte = test1.bits.t6;  //cd1
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t3 | test1.bits.t4) && !temp_byte)
					message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else	
					test1.bits.t6 = ((test1.bits.t3 | test1.bits.t4) && test1.bits.t1) && !temp_byte;
					
				if( !test1.bits.t6 && !test1.bits.t7)
				{
					test1.bits.t9 = 0;
					test1.bits.t10 = 0;
				}	
		
		break;
		
		case  6: temp_byte = test1.bits.t7;  //cd2
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t3 | test1.bits.t4) && !temp_byte)
					message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else	
					test1.bits.t7 = ((test1.bits.t3 | test1.bits.t4) && test1.bits.t1) && !temp_byte;
					
				if( !test1.bits.t6 && !test1.bits.t7)
				{
					test1.bits.t9 = 0;
					test1.bits.t10 = 0;
				}	
		break;
		/*******************/
		case  7:temp_byte = test1.bits.t9;  //cmp1
				if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t3 | test1.bits.t4) && !temp_byte)
				 	message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else if(!(test1.bits.t6 | test1.bits.t7) && !temp_byte)
				 	message_type  = 4;//switch_on_condensor _relay11/condensor _relay12
				else	
					test1.bits.t9 = (test1.bits.t6 | test1.bits.t7) && test1.bits.t1 && !temp_byte;
				 //on_off = test1.bits.t7;	
		break;
		case  8: temp_byte = test1.bits.t10;  //cmp2
				if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t3 | test1.bits.t4) && !temp_byte)
				 	message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else if(!(test1.bits.t6 | test1.bits.t7) && !temp_byte)
				 	message_type  = 4;//switch_on_condensor _relay11/condensor _relay12
				else 	
					test1.bits.t10 = (test1.bits.t3 | test1.bits.t4) && (test1.bits.t6 | test1.bits.t7) && test1.bits.t1 && !temp_byte;
				 //on_off = test1.bits.t7;	
		break;
		case  9: temp_byte = test1.bits.t11;  //htr1
				if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t3 | test1.bits.t4) && !temp_byte)
				 	message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else	
					test1.bits.t11 = (test1.bits.t3 | test1.bits.t4) && test1.bits.t1 && !temp_byte;
				 //on_off = test1.bits.t11;	
		break;
		
		
		
		case  10: temp_byte = test1.bits.t12; //computer ok
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test1.bits.t12 = test1.bits.t1 && !temp_byte;
				 //on_off = test1.bits.t1;	
		break;
		case  11: temp_byte = test1.bits.t13; //spare
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test1.bits.t13 = test1.bits.t1 && !temp_byte;
				
		break;
		case  12: temp_byte = test1.bits.t14; //sf21
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test1.bits.t14 = test1.bits.t1 && !temp_byte;
				 	
				 if(!test1.bits.t14 && !test1.bits.t15)
				 {
					 test2.bits.t17 = 0;
					 test2.bits.t18 = 0;
					 test2.bits.t19 = 0;
					 test2.bits.t20 = 0;
					 test2.bits.t21 = 0;
					 test2.bits.t22 = 0;
					 
				 }
				 ///on_off = test1.bits.t3;	
		break;
		case  13:temp_byte = test1.bits.t15;  //sf22
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test1.bits.t15 = test1.bits.t1 && !temp_byte;
				 	
				 if(!test1.bits.t14 && !test1.bits.t15)
				 {
					 test2.bits.t17 = 0;
					 test2.bits.t18 = 0;
					 test2.bits.t19 = 0;
					 test2.bits.t20 = 0;
					 test2.bits.t21 = 0;
					 test2.bits.t22 = 0;
				 }
				 //on_off = test1.bits.t4;	
		break;
		case  14: temp_byte = test2.bits.t17; //spare
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				 else	
				 	test2.bits.t17 = test1.bits.t1 && !temp_byte;
					 	
		break;
		case  15: temp_byte = test2.bits.t18;  //cd21
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t14 | test1.bits.t15) && !temp_byte)
					message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else	
					test2.bits.t18 = ((test1.bits.t14 | test1.bits.t15) && test1.bits.t1) && !temp_byte;
					
				if( !test2.bits.t18 && !test2.bits.t19)	
				{
					test2.bits.t18 = 0;
					test2.bits.t19 = 0;
				}	

		break;
		
		case  16: temp_byte = test2.bits.t19;  //cd2
				 if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t14 && test1.bits.t15) && !temp_byte)
					message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else	
					test2.bits.t19 = ((test1.bits.t14 | test1.bits.t15) && test1.bits.t1) && !temp_byte;
					
				if( !test2.bits.t18 && !test2.bits.t19)	
				{
					test2.bits.t18 = 0;
					test2.bits.t19 = 0;
				}	
		break;
		/*******************/
		case  17:temp_byte = test2.bits.t20;  //cmp21
				if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t14 | test1.bits.t15) && !temp_byte)
				 	message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else if(!(test2.bits.t18 | test2.bits.t19) && !temp_byte)
				 	message_type  = 4;//switch_on_condensor _relay11/condensor _relay12
				else	
					test2.bits.t20 = (test1.bits.t14 | test1.bits.t15) && (test2.bits.t18 | test2.bits.t19) && test1.bits.t1 && !temp_byte;
				 //on_off = test1.bits.t7;	
		break;
		case  18: temp_byte = test2.bits.t21;  //cmp22
				if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t14 | test1.bits.t15) && !temp_byte)
				 	message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else if(!(test2.bits.t18 | test2.bits.t19) && !temp_byte)
				 	message_type  = 4;//switch_on_condensor _relay11/condensor _relay12
				else 	
					test2.bits.t21 = (test1.bits.t14 | test1.bits.t15) && (test2.bits.t18 | test2.bits.t19) && test1.bits.t1 && !temp_byte;
				 //on_off = test1.bits.t7;	
		break;
		case  19: temp_byte = test2.bits.t22;  //htr1
				if(!test1.bits.t1 && !temp_byte)
				 	message_type  = 1;//switch_on_computer_ok_relay
				else if(!(test1.bits.t14 && test1.bits.t15) && !temp_byte)
				 	message_type  = 2;//switch_on_supply_fan11/supply fan12_relay
				else	
					test2.bits.t22 = test1.bits.t1 && !temp_byte;
				 //on_off = test1.bits.t11;	
		break;
	}	
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte get_relay_status(ubyte relay_type)
{

	switch(relay_type)
	{
		case  0: return(test1.bits.t1);
		break;
		case  1:return(test1.bits.t2); 	
		break;
		case  2:return(test1.bits.t3);	
		break;
		case  3:return(test1.bits.t4);	
		break;
		case  4: return(test1.bits.t5);
		break;
		case  5: return(test1.bits.t6);
		break;
		case  6:return(test1.bits.t7);	
		break;
		case  7: return(test1.bits.t9);	
		break;
		case  8: return(test1.bits.t10);	
		break;
		case  9: return(test1.bits.t11);	
		break;
		case  10: return(test1.bits.t12);
		break;
		case  11: return(test1.bits.t13);	
		break; 
		case  12: return(test1.bits.t14);	
		break; 
		case  13: return(test1.bits.t15);	
		break;		
		case  14: return(test2.bits.t17);//byte3		
		break;
		case  15: return(test2.bits.t18);	
		break;
		case  16:return(test2.bits.t19);	
		break;
		case  17:return(test2.bits.t20);	
		break;
		case  18:return(test2.bits.t21);
		break;
		case  19:return(test2.bits.t22);	
		break;
	}
	return(1);
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
ubyte get_override_status(ubyte feedback_type)
{
	switch(feedback_type)
	{
		case  0: return( override.stat.blower_fan_00_fault_override);
		break;
		case  1:return( override.stat.blower_fan_01_fault_override); 	
		break;
		case  2:return( override.stat.blower_fan_10_fault_override);	
		break;
		case  3:return( override.stat.blower_fan_11_fault_override);	
		break;
		case  4: return( override.stat.heater_0_fault_override);
		break;
		case  5: return( override.stat.heater_1_fault_override);
		break;
		case  6:return( override.stat.cond_00_fault_override);	
		break;
		case  7: return( override.stat.cond_01_fault_override);	
		break;
		case  8: return( override.stat.cond_10_fault_override);	
		break;
		case  9: return( override.stat.cond_11_fault_override);	
		break;
		case  10: return( override.stat.LP_00_fault_override);
		break;
		case  11: return( override.stat.LP_01_fault_override);	
		break; 
		case  12: return( override.stat.LP_10_fault_override);	
		break; 
		case  13: return( override.stat.LP_11_fault_override);	
		break;		
		case  14: return( override.stat.HP_00_fault_override);
		break;
		case  15: return( override.stat.HP_01_fault_override);	
		break;
		case  16:return( override.stat.HP_10_fault_override);	
		break;
		case  17:return( override.stat.HP_11_fault_override);	
		break;
//		case  18:return(test2.bits.t21);
//		break;
//		case  19:return(test2.bits.t22);	
//		break;
	}
	return(1);
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void slave_selection(void)
{
	ubyte temp_count = 0,highlight_no = 0, start_count = 0;
	home_clr();
	
	puts_graphical_lcd_rom_lim( (urombyte *)"Configured as Master",0,0,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim( (urombyte *)"Please Select Slave",0,(13*1),0,0,arialNarrow_bld_13pix);
	
	while(1)
	{
		WDTCLR
		key_press = 0;
		key_press = scan_num_keypad();
		for(temp_count = start_count; temp_count < 6; temp_count++)
		{
			if( (temp_count + 2) != (highlight_no+start_count + 2))
			puts_graphical_lcd_rom_lim( complete_slave_plant[temp_count].info.coach_info.coach_no,0,(13*(temp_count + 2)),0,',',arialNarrow_bld_13pix);
		}
		temp_count = 0;
		puts_graphical_lcd_rom_lim( complete_slave_plant[temp_count].info.coach_info.coach_no,0,(13*(highlight_no+start_count + 2)),1,',',arialNarrow_bld_13pix);
		if(key_press == 'b')
		{
			highlight_no++;
			if(highlight_no == 6)
			{
				highlight_no--;
				start_count++;
				if(start_count == 25)
				{
					highlight_no = 0;
					start_count = 0;
				}	
			}
		}
		if(key_press == 'a')
		{
			if(highlight_no == 0)
			{

			}
			highlight_no--;
		}
		if(key_press == 'e')
		{
			selected_slave = start_count + highlight_no;
			break;
		}
	}

}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void start_networking_mode(void)
{
	previous_menu_selection =  menu_selection;
	menu_selection = NETWORK_MENU;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
	

	puts_graphical_lcd_rom_lim((urombyte *)" Networking Mode Disabled.......  ",0,13*5,0,0,arialNarrow_bld_13pix);
	ClearWDT();
	Delays_Xtal_ms(1000);
	ClearWDT();
	lhb_comm_fn_code = 0;
	Nop();	
	return;	
	
	puts_graphical_lcd_rom_lim( (urombyte *)"Attempting to be Master",0,0,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim( (urombyte *)"Please Wait....",0,(13*1),0,0,arialNarrow_bld_13pix);
	
				
	new_pkt_rdy_to_be_sent = 1;
	while(key_press != 'e')
	{
		lhb_comm_fn_code = FC_NETWORKING;
		key_press = 0;
		key_press = scan_num_keypad();
		check_fn_code_send_pkt_to_lhb();
		if(frame_flags.bits.default_frame_rcvd)
		{
			new_pkt_rdy_to_be_sent = 0;
			if(frame_flags.bits.networking_frame_rcvd)
			{
			
				puts_graphical_lcd_rom_lim( (urombyte *)"Netwrork configured",0,0,0,0,arialNarrow_bld_13pix);
				puts_graphical_lcd_rom_lim( (urombyte *)"waiting for data....",0,(13*1),0,0,arialNarrow_bld_13pix);
				network.flags.bits.start_nw_mode = 1;		//R
				frame_flags.bits.networking_frame_rcvd = 0;
			
			}	
			else 
				slave_selection();
			frame_flags.integer = 0;	
			//lhb_comm.flags.bits.rx_enable=1;
			
		}
//			IEC0bits.T2IE = 0;
//			for(temp_cnt = 0; temp_cnt < 8; temp_cnt++)
//			{
//				//puts_graphical_lcd_ram_cnt(pressure[temp_cnt],pressurestatus_screen_putdata_x[temp_cnt],13*(temp_cnt+1),0,',',arialNarrow_bld_13pix);
//				
//			}					
//			IEC0bits.T2IE = 1;
//			frame_flags.integer = 0;
//		}
//		check_fn_code_send_pkt_to_lhb();
//		if(keys[5].long_press)
//			load_status_of_plant_screen_data();
//		 if(key_press == 'r')
//             {
//             menu_mode_options();  //lakh
//              }
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			start_networking_mode();
		}	
	}
	
	
	
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_pressure_status_screen(void)
{

	ubyte temp_cnt = 0;
	ubyte *on_off[2] = {(ubyte*)("OFF,"),(ubyte*)("ON,")};
	ubyte *ok_flt[3] = {(ubyte*)("FLT,"),(ubyte*)("OK,"),(ubyte*)("BLK,")};
previous_menu_selection =  menu_selection;
	menu_selection = STATUS_OF_PRESSURE_SENSOR_MENU;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim(disp_view_press_sens_stat,0,0,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim(disp_HP_11,0,13,0,0,arialNarrow_bld_13pix);
		pressurestatus_screen_putdata_x[0]= last_x_pos;
	
	puts_graphical_lcd_rom_lim(disp_HP_12,15,13,0,0,arialNarrow_bld_13pix);
		pressurestatus_screen_putdata_x[1]= last_x_pos;
	
	puts_graphical_lcd_rom_lim(disp_HP_21,0,13*2,0,0,arialNarrow_bld_13pix);
		pressurestatus_screen_putdata_x[2]= last_x_pos;
	
	puts_graphical_lcd_rom_lim(disp_HP_22,15,13*2,0,0,arialNarrow_bld_13pix);
		pressurestatus_screen_putdata_x[3]= last_x_pos;
	
	puts_graphical_lcd_rom_lim(disp_LP_11,0,13*3,0,0,arialNarrow_bld_13pix);
		pressurestatus_screen_putdata_x[4]= last_x_pos;
	
	puts_graphical_lcd_rom_lim(disp_LP_12,15,13*3,0,0,arialNarrow_bld_13pix);
		pressurestatus_screen_putdata_x[5]= last_x_pos;
	
	puts_graphical_lcd_rom_lim(disp_LP_21,0,13*4,0,0,arialNarrow_bld_13pix);
		pressurestatus_screen_putdata_x[6]= last_x_pos;
	
	puts_graphical_lcd_rom_lim(disp_LP_22,15,13*4,0,0,arialNarrow_bld_13pix);
		pressurestatus_screen_putdata_x[7]= last_x_pos;
	
	IEC0bits.T2IE = 1;
	
	while(key_press != 'e')
	{
		key_press = 0;
		key_press = scan_num_keypad();
		if(frame_flags.bits.plant_status_frame_rcvd)
		{
			IEC0bits.T2IE = 0;
			for(temp_cnt = 0; temp_cnt < 8; temp_cnt++)
			{
				//puts_graphical_lcd_ram_cnt(pressure[temp_cnt],pressurestatus_screen_putdata_x[temp_cnt],13*(temp_cnt+1),0,',',arialNarrow_bld_13pix);
				
			}					
			IEC0bits.T2IE = 1;
			frame_flags.integer = 0;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		if(keys[5].long_press)
			load_status_of_plant_screen_data();
			
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			load_pressure_status_screen();
		}	
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################
void change_sleep_hrs(void)
{
	ubyte TOTAL_DATA_FIELDS = 16;	
	ubyte cnt;
	ubyte arr_cnt;
	ubyte last_blink_status = 0;
	ubyte blink_char_arr[16+1];
	ubyte char_field_cnt = 0;
	ubyte start_hrs,start_min,start_sec,end_hrs,end_min,end_sec;
	ubyte currval_of_char_fields[16+1];
	ubyte first_packet = 0;
	uinteger total_chars = 0;
	ubyte temp_cnt = 0;
	uinteger temp_total_chars = 0;
previous_menu_selection =  menu_selection;
	menu_selection = SUPERVISOR_MENU;
	item_selection = SELECT_SLEEP_HRS_ENTRY;
	currval_of_char_fields[0] = '0';	
	currval_of_char_fields[1] = '0';	
	currval_of_char_fields[2] = '/';	
	currval_of_char_fields[3] = '0';	
	currval_of_char_fields[4] = '0';	
	currval_of_char_fields[5] = '/';	
	currval_of_char_fields[6] = '0';	
	currval_of_char_fields[7] = '0';
		
	currval_of_char_fields[8] = '0';	
	currval_of_char_fields[9] = '0';	
	currval_of_char_fields[10] = ':';	
	currval_of_char_fields[11] = '0';	
	currval_of_char_fields[12] = '0';	
	currval_of_char_fields[13] = ':';	
	currval_of_char_fields[14] = '0';	
	currval_of_char_fields[15] = '0';	
//////////////////////////////////////////////////////
     home_clr();
	puts_graphical_lcd_rom_lim((urombyte *)"  Sleep Mode Disabled.......  ",0,13*5,0,0,arialNarrow_bld_13pix);
	ClearWDT();
	Delays_Xtal_ms(1000);
//	ClearWDT();
//	Delays_Xtal_ms(1000);
	lhb_comm_fn_code = 0;
	Nop();	
	return;	

//////////////////////////////////////////////////////





	arr_cnt = 0;
	currval_of_char_fields[TOTAL_DATA_FIELDS] = 0;
	for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
		blink_char_arr[cnt] = currval_of_char_fields[cnt];

	start_hrs = 0;
	start_min = 0;
	start_sec = 0;
		
	end_hrs = 0;
	end_min = 0;
	end_sec = 0;
			
	stat.bits.refresh_display = 1;

	IEC0bits.T2IE = 0;	
	key_press = 0;
		clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim( (urombyte *)"ENTER SLEEP START/END HOURS",0,0,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim((urombyte *)"START:",0,13,0,0,arialNarrow_bld_13pix);
	sleep_hrs_screen_readdata_x[0] = last_x_pos; 
	puts_graphical_lcd_rom_lim((urombyte *)"END:",15,13,0,0,arialNarrow_bld_13pix);
	sleep_hrs_screen_readdata_x[1] = last_x_pos; 
	puts_graphical_lcd_rom_lim((urombyte *)"SLEEP START/END HOURS FROM LHB",0,13*2,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim((urombyte *)"START:",0,13*3,0,0,arialNarrow_bld_13pix);
	sleep_hrs_screen_readdata_x[2] = last_x_pos; 
	puts_graphical_lcd_rom_lim((urombyte *)"END:",15,13*3,0,0,arialNarrow_bld_13pix);
	sleep_hrs_screen_readdata_x[3] = last_x_pos; 
	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*4,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_ack_to_save,0,13*5,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*6,0,0,arialNarrow_bld_13pix);
	IEC0bits.T2IE = 1;
	while(key_press != 'e' )
	{	
		if( frame_flags.bits.sleep_hrs_frame_rcvd && rx.data.new_pkt_rcvd)
		{
			if(!first_packet)
			{
					str_copy_ram_cnt(sleep_hrs_start_bytes.byte,&currval_of_char_fields[0],8);
					temp_total_chars = 8;
					str_copy_ram_cnt(sleep_hrs_end_bytes.byte,&currval_of_char_fields[8],8);
					total_chars = 8;
					if(temp_total_chars)
						temp_total_chars--;
					if(total_chars)
						total_chars--;
					temp_cnt  =0; 
			}	
			if(temp_total_chars)
			{
				temp_cnt = 0;
				puts_graphical_lcd_ram_cnt(&sleep_hrs_start_bytes.byte[0],sleep_hrs_screen_readdata_x[2],13*3,0,1,arialNarrow_bld_13pix);
				temp_cnt++;
				for(;temp_cnt  < 8  ;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&sleep_hrs_start_bytes.byte[temp_cnt],last_x_pos,13*3,0,1,arialNarrow_bld_13pix);
				}
			}	
			if(total_chars)
			{
				temp_cnt = 0;
				puts_graphical_lcd_ram_cnt(&sleep_hrs_end_bytes.byte[temp_cnt],sleep_hrs_screen_readdata_x[3],13*3,0,1,arialNarrow_bld_13pix);
				temp_cnt++;
				for(;temp_cnt < 8;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&sleep_hrs_end_bytes.byte[temp_cnt],last_x_pos,13*3,0,1,arialNarrow_bld_13pix);
				}
			}
		
			stat.bits.refresh_display = 1;
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;
		}
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'b' && temp_total_chars && total_chars)//dwn to decrease data
		{
			switch(char_field_cnt)
			{
				case 0:
						if(start_hrs > 0)
							start_hrs--;
						else
							start_hrs = 23;
						currval_of_char_fields[0] = start_hrs/10+ 0x30;
						currval_of_char_fields[1] = start_hrs%10+ 0x30;	
				break;
				case 3:		
					if(start_min > 0)
						start_min--;
					else
						start_min = 59;	
					currval_of_char_fields[3] = start_min/10+ 0x30;
					currval_of_char_fields[4] = start_min%10+ 0x30;	
				break;
				case 6:
					if(start_sec > 0)
						start_sec--;
					else
						start_sec = 99;
					currval_of_char_fields[6] = start_sec/10+ 0x30;
					currval_of_char_fields[7] = start_sec%10+ 0x30;	
				break;	
				case 8:
					if(end_hrs > 0)
						end_hrs--;
					else
						end_hrs = 23;
					currval_of_char_fields[8] = end_hrs/10+ 0x30;
					currval_of_char_fields[9] = end_hrs%10+ 0x30;	
				break;
				case 11:
						if(end_min > 0)
							end_min--;
						else
							end_min = 59;
						currval_of_char_fields[11] = end_min/10+ 0x30;
						currval_of_char_fields[12] = end_min%10+ 0x30;	
				break;
				case 14:					
						if(end_sec > 0)
							end_sec--;
						else
							end_sec = 59;
						currval_of_char_fields[14] = end_sec/10+ 0x30;
						currval_of_char_fields[15] = end_sec%10+ 0x30;
				break;
				
			}				
		
		 	stat.bits.refresh_display = 1;
		}

		if(key_press == 'a' && temp_total_chars && total_chars)//up to increase data
		{
			switch(char_field_cnt)
			{
				case 0:
						if(start_hrs < 23)
							start_hrs++;
						else
							start_hrs = 0;
						currval_of_char_fields[0] = start_hrs/10+ 0x30;
						currval_of_char_fields[1] = start_hrs%10+ 0x30;	
				break;
				case 3:		
					if(start_min < 59)
						start_min++;
					else
						start_min = 0;	
					currval_of_char_fields[3] = start_min/10+ 0x30;
					currval_of_char_fields[4] = start_min%10+ 0x30;	
				break;
				case 6:
					if(start_sec < 59)
						start_sec++;
					else
						start_sec = 0;
					currval_of_char_fields[6] = start_sec/10+ 0x30;
					currval_of_char_fields[7] = start_sec%10+ 0x30;	
				break;	
				case 8:
					if(end_hrs < 23)
						end_hrs++;
					else
						end_hrs = 0;
					currval_of_char_fields[8] = end_hrs/10+ 0x30;
					currval_of_char_fields[9] = end_hrs%10+ 0x30;	
				break;
				case 11:
						if(end_min < 59)
							end_min++;
						else
							end_min = 0;
						currval_of_char_fields[11] = end_min/10+ 0x30;
						currval_of_char_fields[12] = end_min%10+ 0x30;	
				break;
				case 14:					
						if(end_sec < 59)
							end_sec++;
						else
							end_sec = 0;
						currval_of_char_fields[14] = end_sec/10+ 0x30;
						currval_of_char_fields[15] = end_sec%10+ 0x30;
				break;
				
			}	
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'c' && temp_total_chars && total_chars)//ack
		{
			str_copy_ram_cnt(&currval_of_char_fields[0],sleep_hrs_start_bytes.byte,temp_total_chars);		
			str_copy_ram_cnt(&currval_of_char_fields[8],sleep_hrs_end_bytes.byte,total_chars);			
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			
			lhb_comm_fn_code = FC_SLEEP_HRS;
		 //put curr value into data struct
		}
		if(key_press == 'L' && temp_total_chars && total_chars)//right
		{
			if(char_field_cnt < TOTAL_DATA_FIELDS-1)
			{
				switch(char_field_cnt)
				{
					case 0:char_field_cnt= 3;
					break;
					case 3:char_field_cnt= 6;
					break;
					case 6:char_field_cnt= 8;
					break;
					case 8:char_field_cnt= 11;
					break;
					case 11:char_field_cnt= 14;
					break;
					case 14:char_field_cnt= 0;
					break;
				}
			}
		 	else
		 		char_field_cnt = 0;
		 	arr_cnt = 0;
		 	stat.bits.refresh_display = 1;
		}
		if(last_blink_status != timer2.stat.bits.blinker_status)
		{
			last_blink_status = timer2.stat.bits.blinker_status;
			stat.bits.refresh_display = 1;
		}
		
		if(stat.bits.refresh_display)
		{
			IEC0bits.T2IE = 0;
			for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
				blink_char_arr[cnt] = currval_of_char_fields[cnt];
			blink_char_arr[char_field_cnt] = '_';
			blink_char_arr[char_field_cnt+1] = '_';
			if(timer2.stat.bits.blinker_status)
			{	
				if(temp_total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],sleep_hrs_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;temp_cnt  < 8  ;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
					}
				}	
				if(total_chars)
				{
					//temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],sleep_hrs_screen_readdata_x[1],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;temp_cnt < 16;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
					}
				}
	
			}				
			else
			{	
				if(temp_total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],sleep_hrs_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;temp_cnt  < 8  ;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
					}
				}	
				if(total_chars)
				{
					//temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],sleep_hrs_screen_readdata_x[1],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
					for(;temp_cnt < 16;temp_cnt++)
					{
						puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
					}
				}

			}
			
			IEC0bits.T2IE = 1;
			stat.bits.refresh_display = 0;
		}
		
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			change_sleep_hrs();
		}		
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################	
void restore_dafault_settings(void)
{
	ubyte temp_cnt = 0;
previous_menu_selection =  menu_selection;
	menu_selection = SUPERVISOR_MENU;
	item_selection = SELECT_RESTORE_FACTORY_SETTINGS;
	
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);

	puts_graphical_lcd_rom_lim((urombyte *)"Press ACK to Restore Factory Settings",0,13*2,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*6,0,0,arialNarrow_bld_13pix);

	IEC0bits.T2IE = 1;
	while(key_press != 'e' && key_press !='r')//lakh
	{
		key_press = 0;
		key_press = scan_num_keypad();
		if( frame_flags.bits.restore_default_frame_rcvd & (frame_flags.bits.default_frame_rcvd))
		{
			IEC0bits.T2IE = 0;
//			if( str_comp_rom( (urombyte *)"RESTORE DONE" , rx.data.data_crc_bytes , 12))
				puts_graphical_lcd_rom_lim((urombyte *)"Factory Settings Restored",0,13*4,0,0,arialNarrow_bld_13pix);
			
			IEC0bits.T2IE = 1;
			frame_flags.integer = 0;
			
			
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(key_press == 'c')//ack
		{
			lhb_comm_fn_code = FC_RESTORE_DEFAULT;
			str_copy_rom_lim( (urombyte *)"RESTORE",tx.data.data_crc_bytes,0);
			new_pkt_rdy_to_be_sent = 1;
	
		}

		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			restore_dafault_settings();
		}	
	}
}
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################	
//void sel_single_double_rmpu(void)
//{
//
//	ubyte temp_cnt = 0;
//
//	menu_selection = SUPERVISOR_MENU;
//	item_selection = SELECT_SINGLE_DOUBLE_RMPU;
//	
//	IEC0bits.T2IE = 0;	
//	key_press = 0;
//	clear_keys_status();
//	home_clr();
////	Delays_Xtal_ms(1000);
//
//	puts_graphical_lcd_rom_lim((urombyte *)"PRESS UP for SINGLE RMPU",0,13*2,0,0,arialNarrow_bld_13pix);
//	puts_graphical_lcd_rom_lim((urombyte *)"PRESS DOWN for DOUBLE RMPU",0,13*2,0,0,arialNarrow_bld_13pix);
//	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*6,0,0,arialNarrow_bld_13pix);
//
//	IEC0bits.T2IE = 1;
//
//	while(key_press != 'e')
//	{
//		key_press = 0;
//		key_press = scan_num_keypad();
//		if( frame_flags.bits.restore_default_frame_rcvd)
//		{
//			IEC0bits.T2IE = 0;
//			if( str_comp_rom( (urombyte *)"RESTORE DONE" , rx.data.data_crc_bytes , 12))
//				puts_graphical_lcd_rom_lim((urombyte *)"FACTORY SETTINGS RESTORED",0,13*4,0,0,arialNarrow_bld_13pix);
//			
//			IEC0bits.T2IE = 1;
//			frame_flags.integer = 0;
//			
//			
//		}
//		//lhb_comm.flags.bits.rx_enable=1;
//		check_fn_code_send_pkt_to_lhb();
//		
//		if(key_press == 'c')//ack
//		{
//			str_copy_rom_lim( (urombyte *)"RESTORE",tx.data.data_crc_bytes,0);
//			new_pkt_rdy_to_be_sent = 1;
//	
//		}
//
//		if(!timer2.display_timeout_5sec.timer)
//		{
//			display_timeout_message();
//			timer2.display_timeout_5sec.status.tick = 1;
//
//		}
//		else if(timer2.display_timeout_5sec.status.tick)
//		{
//			timer2.display_timeout_5sec.status.tick = 0;
//			home_clr();	
//			restore_dafault_settings();
//		}	
//	}	
//}	
void sel_single_double_rmpu(void)
{

	unsigned char last_blink_status = 0;
	unsigned char blink_char_arr[16+1];
	unsigned char first_packet = 0;
	unsigned char temp_byte ;
previous_menu_selection =  menu_selection;
	menu_selection = SUPERVISOR_MENU;
	item_selection = SELECT_SINGLE_DOUBLE_RMPU;
			
	stat.bits.refresh_display = 1;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim( (urombyte *)"RMPU Mode: ",0,0,0,0,arialNarrow_bld_13pix);//lakh
	double_rmpu_putdata_x[0] = last_x_pos; 

	puts_graphical_lcd_rom_lim((urombyte *)"Select RMPU Mode: ",0,13*2,0,0,arialNarrow_bld_13pix);//lakh
	double_rmpu_putdata_x[1] = last_x_pos; 

//	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*4,0,0,arialNarrow_bld_13pix);
	//puts_graphical_lcd_rom_lim("Press ACK to CHANGE",0,13*5,0,0,arialNarrow_bld_13pix);
	
	puts_graphical_lcd_rom_lim("Press ACK to Change & Save Changes",0,13*5,0,0,arialNarrow_bld_13pix);//lakh
	puts_graphical_lcd_rom_lim("Press ENTER to exit",0,13*6,0,0,arialNarrow_bld_13pix);
	
//	puts_graphical_lcd_rom_lim(disp_Press_left_for_default_menu,0,13*6,0,0,arialNarrow_bld_13pix);
	IEC0bits.T2IE = 1;
	while(key_press != 'e' && key_press !='r')//lakh
	{	
		if( frame_flags.bits.sleep_hrs_frame_rcvd && rx.data.new_pkt_rcvd)
		{
			orphan_flags.bits.both_rmpu_sel = complete_plant_rx.info.both_rmpu_sel-48;
			temp_byte = !orphan_flags.bits.both_rmpu_sel;
			stat.bits.refresh_display = 1;
			
			if(orphan_flags.bits.both_rmpu_sel)
			puts_graphical_lcd_rom_lim( (urombyte *)"DOUBLE MODE SELECTED  ",double_rmpu_putdata_x[0],0,0,0,arialNarrow_bld_13pix);
			else
			puts_graphical_lcd_rom_lim( (urombyte *)"SINGLE MODE SELECTED  ",double_rmpu_putdata_x[0],0,0,0,arialNarrow_bld_13pix);

			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;
		}
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'c' )//ack
		{
			orphan_flags.bits.both_rmpu_sel=temp_byte;
//		 	stat.bits.refresh_display = 1;
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			
			lhb_comm_fn_code = FC_SEL_BOTH_RMPU;
		 //put curr value into data struct
		}
		if(timer2.sec.status.tick)
		{
			timer2.stat.bits.blinker_status^=1;
			timer2.sec.status.tick = 0;
			timer2.sec.timer = 9;
		}
		if(last_blink_status != timer2.stat.bits.blinker_status)
		{
			last_blink_status = timer2.stat.bits.blinker_status;
			stat.bits.refresh_display = 1;
		}
		
//		if(stat.bits.refresh_display)
		{
			IEC0bits.T2IE = 0;
			if(timer2.stat.bits.blinker_status)
			{
				puts_graphical_lcd_rom_lim( (urombyte *)"        ",double_rmpu_putdata_x[1],13*2,0,0,arialNarrow_bld_13pix);
				
			}		
			else
			{	
				if(temp_byte)
				puts_graphical_lcd_rom_lim( (urombyte *)"DOUBLE  ",double_rmpu_putdata_x[1],13*2,0,0,arialNarrow_bld_13pix);
				else
				puts_graphical_lcd_rom_lim( (urombyte *)"SINGLE  ",double_rmpu_putdata_x[1],13*2,0,0,arialNarrow_bld_13pix);

			}
			IEC0bits.T2IE = 1;
			stat.bits.refresh_display = 0;
		}
		
		
		check_fn_code_send_pkt_to_lhb();
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			sel_single_double_rmpu();
		}		
	}
}	
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################	
void sel_single_double_decker(void)
{
	
	
	
	
}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void change_ip_and_ports(void)
{
	ubyte TOTAL_DATA_FIELDS = 12;
	ubyte cnt = 0;
	ubyte arr_cnt = 0;
	ubyte last_blink_status = 0;
	ubyte blink_char_arr[TOTAL_DATA_FIELDS+1];
	ubyte char_field_cnt = 0;
	ubyte x_pos_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte minval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte maxval_of_char_fields[TOTAL_DATA_FIELDS];
	ubyte currval_of_char_fields[TOTAL_DATA_FIELDS+1];
	ubyte first_packet = 0;
	uinteger total_chars = 0;;
	ubyte temp_cnt = 0;
previous_menu_selection =  menu_selection;
	menu_selection = SUPERVISOR_MENU;
	item_selection = SELECT_CHANGE_SERVER_SETTINGS;
	
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		minval_of_char_fields[cnt] = '0';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		maxval_of_char_fields[cnt] = 'Z';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS;cnt++)
		currval_of_char_fields[cnt] = '0';
	arr_cnt = 2;
	currval_of_char_fields[TOTAL_DATA_FIELDS] = ',';
	for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
		blink_char_arr[cnt] = currval_of_char_fields[cnt];
	stat.bits.refresh_display = 1;


	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);
	puts_graphical_lcd_rom_lim(disp_change_Unit_no,0,0,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_temp_unit_no,0,13*1,0,0,arialNarrow_bld_13pix);
	coachno_change_screen_readdata_x[0] = last_x_pos; 
	
	puts_graphical_lcd_rom_lim(disp_unit_no,0,13*2,0,0,arialNarrow_bld_13pix);
	coachno_change_screen_readdata_x[1] = last_x_pos; 
	puts_graphical_lcd_rom_lim(disp_Press_updown_to_chng_data,0,13*3,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_ack_to_save,0,13*4,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim(disp_Press_enter_for_default_menu,0,13*5,0,0,arialNarrow_bld_13pix);
	
	IEC0bits.T2IE = 1;
	first_packet = 0;
	while(key_press != 'e')
	{
		if(frame_flags.bits.unit_no_frame_rcvd && rx.data.new_pkt_rcvd)
		{
			if(!first_packet)
			{
				total_chars = str_copy_ram_lim_ret(coach_info.unit_no,currval_of_char_fields,',') - 1;
				temp_cnt  =0; 
				arr_cnt = str_search_ram( &coach_info.unit_no[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
			}	
			if(total_chars)
			{
				temp_cnt = 0;
				puts_graphical_lcd_ram_cnt(&coach_info.unit_no[0],coachno_change_screen_readdata_x[1],13*2,0,1,arialNarrow_bld_13pix);
				temp_cnt++;
			}
			for(;temp_cnt < total_chars;temp_cnt++)
			{
				puts_graphical_lcd_ram_cnt(&coach_info.unit_no[temp_cnt],last_x_pos,13*2,0,1,arialNarrow_bld_13pix);
			}
			stat.bits.refresh_display = 1;
			
			if(!first_packet)
				first_packet = 1;
			
			frame_flags.integer = 0;
			rx.data.new_pkt_rcvd = 0;	
		}
		
		key_press = 0;
		key_press = scan_num_keypad();
		if(key_press == 'b' && total_chars)//dwn to decrease data
		{
			if(arr_cnt > 2)
				arr_cnt--;
			else
				arr_cnt = sizeof(char_value_arr);
			if(currval_of_char_fields[char_field_cnt] > minval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = maxval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'a'&& total_chars)//up to increase data
		{
			if(arr_cnt < sizeof(char_value_arr))
				arr_cnt++;
			else
				arr_cnt = 2;
			if(currval_of_char_fields[char_field_cnt] < maxval_of_char_fields[char_field_cnt])
				currval_of_char_fields[char_field_cnt] = char_value_arr[arr_cnt];
			else
				currval_of_char_fields[char_field_cnt] = minval_of_char_fields[char_field_cnt]; 
		 	stat.bits.refresh_display = 1;
		}
		if(key_press == 'c'&& total_chars)//ack
		{
			str_copy_ram_cnt(currval_of_char_fields,coach_info.unit_no,total_chars);			
			new_pkt_rdy_to_be_sent = 1;
			first_packet = 0;
			lhb_comm_fn_code = FC_UNIT_NO;
		 //put curr value into data struct
		}
		if(key_press == 'L'&& total_chars)//right
		{
			if(char_field_cnt < total_chars)
		 		char_field_cnt++;
		 	else
		 		char_field_cnt = 0;
		 	arr_cnt = str_search_ram( &coach_info.unit_no[char_field_cnt],(ubyte *)char_value_arr, 1, sizeof(char_value_arr));
		 	stat.bits.refresh_display = 1;
		}
		if(last_blink_status != timer2.stat.bits.blinker_status)
		{
			last_blink_status = timer2.stat.bits.blinker_status;
			stat.bits.refresh_display = 1;
		}
		
		if(stat.bits.refresh_display)
		{
			IEC0bits.T2IE = 0;
			for(cnt =0; cnt < TOTAL_DATA_FIELDS+1;cnt++)
				blink_char_arr[cnt] = currval_of_char_fields[cnt];
			blink_char_arr[char_field_cnt] = '_';
			if(timer2.stat.bits.blinker_status)
			{					
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&blink_char_arr[0],coachno_change_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
				}
				for(;temp_cnt < total_chars;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&blink_char_arr[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
				}				
			}				
			else
			{									
				if(total_chars)
				{
					temp_cnt = 0;
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[0],coachno_change_screen_readdata_x[0],13,0,1,arialNarrow_bld_13pix);
					temp_cnt++;
				}
				for(;temp_cnt < total_chars;temp_cnt++)
				{
					puts_graphical_lcd_ram_cnt(&currval_of_char_fields[temp_cnt],last_x_pos,13,0,1,arialNarrow_bld_13pix);
				}		
			}
			
			IEC0bits.T2IE = 1;
			stat.bits.refresh_display = 0;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			change_Unit_no();
		}		
	}
}
//#######################################################################################


//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################	
void display_timeout_message(void)
{
	home_clr();
	puts_graphical_lcd_rom_lim( (urombyte *)"If controller ok led is ON         ",0,13*3,1,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim( (urombyte *)"then check wiring                       ",0,13*4,1,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim( (urombyte *)"If controller ok led is OFF        ",0,13*5,1,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim( (urombyte *)"then controller not ok             ",0,13*6,1,0,arialNarrow_bld_13pix);
//	Delays_Xtal_ms(500);	
	puts_graphical_lcd_rom_lim( (urombyte *)"If controller ok led is ON         ",0,13*3,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim( (urombyte *)"then check wiring                       ",0,13*4,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim( (urombyte *)"If controller ok led is OFF        ",0,13*5,0,0,arialNarrow_bld_13pix);
	puts_graphical_lcd_rom_lim( (urombyte *)"then controller not ok             ",0,13*6,0,0,arialNarrow_bld_13pix);
	Delays_Xtal_ms(500);
}	
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################	
void view_server_status(void)
{
	previous_menu_selection =  menu_selection;
	menu_selection = STATUS_OF_PLANT_MENU;
	//lhb_comm.flags.bits.rx_enable=1;
	check_fn_code_send_pkt_to_lhb();	
	
	IEC0bits.T2IE = 0;	
	key_press = 0;	
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);

	while(1)
	{
		WDTCLR
		if(frame_flags.bits.plant_status_frame_rcvd)
		{
			puts_graphical_lcd_rom_lim("        SERVER STATUS ",0,0,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("Last ACK received:",0,13,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server_s.last_ack_date.byte[0],15,13,0,8,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server_s.last_ack_time.byte[0],23,13,0,8,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("Last Attampt:",0,13*2,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server_s.last_attmpt_date.byte[0],15,13*2,0,8,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server_s.last_attmpt_time.byte[0],23,13*2,0,8,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("STATUS:",0,13*4,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("Events Pending:",0,13*6,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim("Faults Pending:",0,13*7,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&event_pkt[0],15,13*6,0,4,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&fault_pkt[0],15,13*7,0,4,arialNarrow_bld_13pix);
		
			switch(server_s.fault)
			{
				case NO_NETWORK:
				puts_graphical_lcd_rom_lim("NO NETWORK AVAILABLE   ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case NO_RESPONSE_FROM_MODULE:
				puts_graphical_lcd_rom_lim("NO RESPONSE FROM MODULE",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case NO_GRPS_CONNECTIVITY:
				puts_graphical_lcd_rom_lim("NO GRPS CONNECTIVITY   ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case ASYNC_SOCKET_FAILED:
				puts_graphical_lcd_rom_lim("ASYNC SOCKET FAILED    ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case SYNC_SOCKET_FAILED:
				puts_graphical_lcd_rom_lim("SYNC SOCKET FAILED     ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case NO_SERVER_REPONSE:
				puts_graphical_lcd_rom_lim("NO SERVER REPONSE      ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				default:
				puts_graphical_lcd_rom_lim("CDS STATUS HEALTHY     ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
			}

			IEC0bits.T2IE = 1;
			frame_flags.integer = 0;	
			break;	
		}

		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
	
		if(keys[0].long_press)
		{	
			stat.bits.use_up_dwn_keys = 0;
			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
			stat.bits.use_up_dwn_keys = 1;
		}
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			load_status_of_plant_screen_data();
		}	
	}		
	
	IEC0bits.T2IE = 1;
	while(key_press != 'e' && key_press !='r')//lakh
	{
		WDTCLR
		key_press = 0;
		key_press = scan_num_keypad();
		
		if(frame_flags.bits.plant_status_frame_rcvd)
		{
			IEC0bits.T2IE = 0;
			puts_graphical_lcd_ram_cnt(&server_s.last_ack_date.byte[0],15,13,0,8,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server_s.last_ack_time.byte[0],23,13,0,8,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server_s.last_attmpt_date.byte[0],15,13*2,0,8,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&server_s.last_attmpt_time.byte[0],23,13*2,0,8,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&event_pkt[0],15,13*6,0,4,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_cnt(&fault_pkt[0],15,13*7,0,4,arialNarrow_bld_13pix);
			switch(server_s.fault)
			{
				case NO_NETWORK:
				puts_graphical_lcd_rom_lim("NO NETWORK AVAILABLE   ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case NO_RESPONSE_FROM_MODULE:
				puts_graphical_lcd_rom_lim("NO RESPONSE FROM MODULE",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case NO_GRPS_CONNECTIVITY:
				puts_graphical_lcd_rom_lim("NO GRPS CONNECTIVITY   ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case ASYNC_SOCKET_FAILED:
				puts_graphical_lcd_rom_lim("ASYNC SOCKET FAILED    ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case SYNC_SOCKET_FAILED:
				puts_graphical_lcd_rom_lim("SYNC SOCKET FAILED     ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				case NO_SERVER_REPONSE:
				puts_graphical_lcd_rom_lim("NO SERVER REPONSE      ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
				default:
				puts_graphical_lcd_rom_lim("CDS STATUS HEALTHY     ",7,13*4,0,0,arialNarrow_bld_13pix);
				break;
			}
			IEC0bits.T2IE = 1;
			frame_flags.integer = 0;
		}
		//lhb_comm.flags.bits.rx_enable=1;
		check_fn_code_send_pkt_to_lhb();
		
		if(keys[0].long_press)
		{	
			stat.bits.use_up_dwn_keys = 0;
			display_set_temp_values_screen();
//			display_set_temp_selection_screen();
			stat.bits.use_up_dwn_keys = 1;
		}
	
		 
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			view_server_status();
		}	
	}
}	
//#######################################################################################
//EOF
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################	

void download_system_log_to_usb(void)
{
	unsigned char temp_cnt = 0;
previous_menu_selection =  menu_selection;
	menu_selection = USB_MENU;
	item_selection = SELECT_DOWNLOAD_SYSTEM_LOG_TO_USB;
	
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();

	puts_graphical_lcd_rom_lim((urombyte *)"Press ENTER to Download System Log",0,13*2,0,0,arialNarrow_bld_13pix);
	stat.bits.data_received = 0;
	IEC0bits.T2IE = 1;
	while(key_press != 'c' && key_press !='r') //lakh
	{
		ClearWDT();
		key_press = 0;
		key_press = scan_num_keypad();
		if(stat.bits.data_received)
		{
			if( frame_flags.bits.system_pen_drive_frame_rcvd)
			{
				IEC0bits.T2IE = 0;
	//			if( str_comp_rom( (urombyte *)"RESTORE DONE" , rx.data.data_crc_bytes , 12))
				puts_graphical_lcd_rom_lim((urombyte *)"Download Complete..........",0,13*5,0,0,arialNarrow_bld_13pix);
				puts_graphical_lcd_rom_lim("Press ACK to return",0,13*6,0,0,arialNarrow_bld_13pix);			
				IEC0bits.T2IE = 1;
				frame_flags.integer = 0;
				Delays_Xtal_ms(200);
				lhb_comm_fn_code = 0;
				Nop();	
				return;					
				
			}
			else if( frame_flags.bits.pen_drive_detected_frame_rcvd)
			{
				IEC0bits.T2IE = 0;
				lhb_comm_fn_code = FC_SYSTEM_LOG_TO_PEN_DRIVE;
	//			if( str_comp_rom( (urombyte *)"RESTORE DONE" , rx.data.data_crc_bytes , 12))
				puts_graphical_lcd_rom_lim((urombyte *)"Pendrive Detected.........",0,13*4,0,0,arialNarrow_bld_13pix);
				puts_graphical_lcd_rom_lim((urombyte *)"Please Wait...,Downloading System Log",0,13*5,0,0,arialNarrow_bld_13pix);
				new_pkt_rdy_to_be_sent = 1;
				IEC0bits.T2IE = 1;
				frame_flags.integer = 0;
				
				
			}	
			else if(lhb_comm_fn_code == FC_SYSTEM_LOG_TO_PEN_DRIVE)
			{
				puts_graphical_lcd_rom_lim((urombyte *)"Downloading Error............",0,13*5,0,0,arialNarrow_bld_13pix);
				Delays_Xtal_ms(200);
				lhb_comm_fn_code = 0;
				Nop();	
				return;	
			}
		}
		stat.bits.data_received = 0;
		
		check_fn_code_send_pkt_to_lhb();
		
		if(key_press == 'e')//ack
		{
			puts_graphical_lcd_rom_lim((urombyte *)"                                ",0,13*2,0,0,arialNarrow_bld_13pix);
			lhb_comm_fn_code = FC_SYSTEM_LOG_TO_PEN_DRIVE;
			puts_graphical_lcd_rom_lim((urombyte *)"Insert Pendrive.................",0,13*4,0,0,arialNarrow_bld_13pix);
			new_pkt_rdy_to_be_sent = 1;
	
		}
	}
}	
//#######################################################################################

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:
//#######################################################################################	

void download_analysis_log_to_usb(void)
{
	unsigned char temp_cnt = 0;
previous_menu_selection =  menu_selection;
	menu_selection = USB_MENU;
	item_selection = SELECT_DOWNLOAD_ANALYSIS_LOG_TO_USB;
	
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();

	puts_graphical_lcd_rom_lim((urombyte *)"Press ENTER to Download Analysis Log",0,13*2,0,0,arialNarrow_bld_13pix);
	stat.bits.data_received = 0;
	IEC0bits.T2IE = 1;
	while(key_press != 'c' && key_press !='r')//lakh
	{
		ClearWDT();
		key_press = 0;
		key_press = scan_num_keypad();
		if(stat.bits.data_received)
		{
			if( frame_flags.bits.analysis_pen_drive_frame_rcvd)
			{
				IEC0bits.T2IE = 0;
	//			if( str_comp_rom( (urombyte *)"RESTORE DONE" , rx.data.data_crc_bytes , 12))
				puts_graphical_lcd_rom_lim((urombyte *)"DOWNLOAD COMPLETE..........",0,13*5,0,0,arialNarrow_bld_13pix);
				puts_graphical_lcd_rom_lim("Press ACK to return",0,13*6,0,0,arialNarrow_bld_13pix);			
				IEC0bits.T2IE = 1;
				frame_flags.integer = 0;
				Delays_Xtal_ms(200);
				lhb_comm_fn_code = 0;
				Nop();	
				return;					
				
			}
			else if( frame_flags.bits.pen_drive_detected_frame_rcvd)
			{
				IEC0bits.T2IE = 0;
				lhb_comm_fn_code = FC_ANALYSIS_LOG_TO_PEN_DRIVE;
	//			if( str_comp_rom( (urombyte *)"RESTORE DONE" , rx.data.data_crc_bytes , 12))
				puts_graphical_lcd_rom_lim((urombyte *)"PENDRIVE DETECTED.........",0,13*4,0,0,arialNarrow_bld_13pix);
				puts_graphical_lcd_rom_lim((urombyte *)"Please Wait...,Downloading AnalysisLog",0,13*5,0,0,arialNarrow_bld_13pix);
				new_pkt_rdy_to_be_sent = 1;
				IEC0bits.T2IE = 1;
				frame_flags.integer = 0;
				
				
			}	
			else if(lhb_comm_fn_code == FC_ANALYSIS_LOG_TO_PEN_DRIVE)
			{
				puts_graphical_lcd_rom_lim((urombyte *)"Downloading Error............",0,13*5,0,0,arialNarrow_bld_13pix);
				Delays_Xtal_ms(200);
				lhb_comm_fn_code = 0;
				Nop();	
				return;	
			}
		}
		stat.bits.data_received = 0;
		
		check_fn_code_send_pkt_to_lhb();
		
		if(key_press == 'e')//ack
		{
			puts_graphical_lcd_rom_lim((urombyte *)"                                ",0,13*2,0,0,arialNarrow_bld_13pix);
			lhb_comm_fn_code = FC_ANALYSIS_LOG_TO_PEN_DRIVE;
			puts_graphical_lcd_rom_lim((urombyte *)"INSERT PENDRIVE.................",0,13*4,0,0,arialNarrow_bld_13pix);
			new_pkt_rdy_to_be_sent = 1;
	
		}
	}
}	
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_system_log(void)
{
	ubyte temp_count, start_ptr = 0,end_ptr;
	previous_menu_selection =  menu_selection;
	menu_selection = FAULT_DATA_MENU;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
	
	new_pkt_rdy_to_be_sent = 1;
	lhb_comm_fn_code = FC_VIEW_STORED_SYSTEM_LOG;
	while(key_press != 'e' && key_press !='r') //lakh
	{
		ClearWDT();
		WDTCLR
		puts_graphical_lcd_rom_lim("SYSTEM LOG",0,0,0,0,arialNarrow_bld_13pix);
		if((frame_flags.bits.stored_fault_frame_rcvd && rx.data.new_pkt_rcvd) | key_press)
		{
			if((start_ptr+8) < temp_fault_count)
				end_ptr=start_ptr + 8;
			else
				end_ptr = temp_fault_count;	

			for(temp_count = start_ptr;temp_count < end_ptr;temp_count++ )
			{
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].fault_str[0],0,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].date.byte[0],16,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].time.byte[0],23,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
			}
			if(!temp_fault_count)
			puts_graphical_lcd_rom_lim("NO Data Available......",0,13*1,0,0,arialNarrow_bld_13pix);

			frame_flags.bits.stored_fault_frame_rcvd = 0;
			rx.data.new_pkt_rcvd = 0;
			frame_flags.integer = 0;	
		}
		
		key_press = 0;
		key_press = scan_num_keypad();
		
		if(key_press == 'b')
		{	
			home_clr();
			if((start_ptr+8) < temp_fault_count)
				start_ptr += 8;
			else
			{
				temp_fault_count = 0;
				new_pkt_rdy_to_be_sent = 1;
				lhb_comm_fn_code = FC_VIEW_STORED_SYSTEM_LOG;
				str_fill( &fault_log[0].fault_str[0], 117,' ');
				stat.bits.request_nxt_fault_frame = 1;
			}
		}
		
		if(key_press == 'a')
		{	
			home_clr();
			if((start_ptr-8) >= 0)
				start_ptr -= 8;
			else
			{
				temp_fault_count = 0;
				new_pkt_rdy_to_be_sent = 1;
				lhb_comm_fn_code = FC_VIEW_PREV_STORED_SYSTEM_LOG;
				str_fill( &fault_log[0].fault_str[0], 117,' ');
				stat.bits.request_nxt_fault_frame = 1;
			}	
		
		}
		
			
		if(keys[0].long_press)// 7 set temp 
			display_set_temp_values_screen();
		check_fn_code_send_pkt_to_lhb();	
		

	}
	str_fill( &fault_log[0].fault_str[0], 117,' ');
}

void load_power_useses(void) {
    
   previous_menu_selection =  menu_selection;
	menu_selection = POWER_USES_MENU;
	check_fn_code_send_pkt_to_lhb();	
	
	IEC0bits.T2IE = 0;	
	key_press = 0;	
	clear_keys_status();
	home_clr();
    
	ubyte t_buffer[7] = {0};
        
	
    
    puts_graphical_lcd_rom_lim("Power Consumption",0,0,0,0,arialNarrow_bld_13pix);
    
	while(key_press != 'r') 
	{
        ClearWDT();
		WDTCLR
        key_press = 0;
        key_press = scan_num_keypad();
        
        if(frame_flags.bits.update_power_data)
        {
            frame_flags.bits.power_uses_frame_rcvd = 0;
            puts_graphical_lcd_rom_lim("BLWR11 : ",0,13*2,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[0].num);
            puts_graphical_lcd_rom_lim(t_buffer,9,13*2,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("BLWR12 : ",15,13*2,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[1].num);
            puts_graphical_lcd_rom_lim(t_buffer,24,13*2,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("BLWR21 : ",0,13*3,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[2].num);
            puts_graphical_lcd_rom_lim(t_buffer,9,13*3,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("BLWR22 : ",15,13*3,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[3].num);
            puts_graphical_lcd_rom_lim(t_buffer,24,13*3,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("COND11 : ",0,13*4,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[4].num);
            puts_graphical_lcd_rom_lim(t_buffer,9,13*4,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("COND12 : ",15,13*4,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[5].num);
            puts_graphical_lcd_rom_lim(t_buffer,24,13*4,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("COND21 : ",0,13*5,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[6].num);
            puts_graphical_lcd_rom_lim(t_buffer,9,13*5,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("COND22 : ",15,13*5,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[7].num);
            puts_graphical_lcd_rom_lim(t_buffer,24,13*5,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("COMP11 : ",0,13*6,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[8].num);
            puts_graphical_lcd_rom_lim(t_buffer,9,13*6,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("COMP12 : ",15,13*6,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[9].num);
            puts_graphical_lcd_rom_lim(t_buffer,24,13*6,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("COMP21 : ",0,13*7,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[10].num);
            puts_graphical_lcd_rom_lim(t_buffer,9,13*7,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("COMP22 : ",15,13*7,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[11].num);
            puts_graphical_lcd_rom_lim(t_buffer,24,13*7,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("HEATER1: ",0,13*8,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[12].num);
            puts_graphical_lcd_rom_lim(t_buffer,9,13*8,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("HEATER2: ",15,13*8,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[13].num);
            puts_graphical_lcd_rom_lim(t_buffer,24,13*8,0,0,arialNarrow_bld_13pix);

            puts_graphical_lcd_rom_lim("ENERGY METER : ",0,13*9,0,0,arialNarrow_bld_13pix);
            sprintf(t_buffer,"%0.3fA",PowerReadings[14].num);
            puts_graphical_lcd_rom_lim(t_buffer,15,13*9,0,0,arialNarrow_bld_13pix);
            
            frame_flags.bits.update_power_data = 0;
        }
        

        if(keys[0].long_press)// 7 set temp 
                display_set_temp_values_screen();
        
       
        check_fn_code_send_pkt_to_lhb();
        
       if(frame_flags.bits.power_uses_frame_rcvd)
        {
            lhb_comm_fn_code = FC_VIEW_POWER_USESE;
            new_pkt_rdy_to_be_sent = 1;
            frame_flags.bits.power_uses_frame_rcvd = 0;
        }
	}
}

//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void load_analysis_log(void)
{
	ubyte temp_count, start_ptr = 0,end_ptr;
	previous_menu_selection =  menu_selection;
	menu_selection = FAULT_DATA_MENU;
	IEC0bits.T2IE = 0;	
	key_press = 0;
	clear_keys_status();
	home_clr();
	
	new_pkt_rdy_to_be_sent = 1;
	lhb_comm_fn_code = FC_VIEW_STORED_ANALYSIS_LOG;
	while(key_press != 'e' && key_press !='r')//lakh
	{
		ClearWDT();
		WDTCLR
		puts_graphical_lcd_rom_lim("ANALYSIS LOG",0,0,0,0,arialNarrow_bld_13pix);
		if((frame_flags.bits.stored_fault_frame_rcvd && rx.data.new_pkt_rcvd) | key_press)
		{
			if((start_ptr+8) < temp_fault_count)
				end_ptr=start_ptr + 8;
			else
				end_ptr = temp_fault_count;	

			for(temp_count = start_ptr;temp_count < end_ptr;temp_count++ )
			{
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].fault_str[0],0,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].date.byte[0],16,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
				puts_graphical_lcd_ram_lim(&fault_log[temp_count].time.byte[0],23,13*(temp_count-start_ptr+1),0,',',arialNarrow_bld_13pix);
			}
			if(!temp_fault_count)
			puts_graphical_lcd_rom_lim("NO Data Available......",0,13*1,0,0,arialNarrow_bld_13pix);

			frame_flags.bits.stored_fault_frame_rcvd = 0;
			rx.data.new_pkt_rcvd = 0;
			frame_flags.integer = 0;	
		}
		
		key_press = 0;
		key_press = scan_num_keypad();
		
		if(key_press == 'b')
		{	
			home_clr();
			if((start_ptr+8) < temp_fault_count)
				start_ptr += 8;
			else
			{
				temp_fault_count = 0;
				new_pkt_rdy_to_be_sent = 1;
				lhb_comm_fn_code = FC_VIEW_STORED_ANALYSIS_LOG;
				str_fill( &fault_log[0].fault_str[0], 117,' ');
				stat.bits.request_nxt_fault_frame = 1;
			}
		}
		
		if(key_press == 'a')
		{	
			home_clr();
			if((start_ptr-8) >= 0)
				start_ptr -= 8;
			else
			{
				temp_fault_count = 0;
				new_pkt_rdy_to_be_sent = 1;
				lhb_comm_fn_code = FC_VIEW_PREV_STORED_ANALYSIS_LOG;
				str_fill( &fault_log[0].fault_str[0], 117,' ');
				stat.bits.request_nxt_fault_frame = 1;
			}	
		
		}
		
			
		if(keys[0].long_press)// 7 set temp 
			display_set_temp_values_screen();
		check_fn_code_send_pkt_to_lhb();	
		

	}
	str_fill( &fault_log[0].fault_str[0], 117,' ');
}
//#######################################################################################
//#######################################################################################
//								FUNCTION DETAILS
//#######################################################################################
//	NAME		:
//	ARGUMENTS	:
//	RETURN TYPE	:
//	DESCRIPTION	:

//#######################################################################################
void view_version_nos(void)
{
	previous_menu_selection =  menu_selection;
	menu_selection = CHANGE_DEFAULT_SETTINGS_MENU;
	
	check_fn_code_send_pkt_to_lhb();	
	
	IEC0bits.T2IE = 0;	
	key_press = 0;	
	clear_keys_status();
	home_clr();
//	Delays_Xtal_ms(1000);

	while(1)
	{
		if( frame_flags.bits.default_frame_rcvd)
		{
			puts_graphical_lcd_rom_lim(disp_local,0,0,0,0,arialNarrow_bld_13pix);
			default_screen_putdata_x[0]= last_x_pos;

			puts_graphical_lcd_rom_lim(disp_lhb_sw_ver,0,13*2,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(coach_info.sw_ver,18,13*2,0,',',arialNarrow_bld_13pix);
				
			puts_graphical_lcd_rom_lim("Display SW Ver:",0,13*3,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_sw_vrs,18,13*3,0,',',arialNarrow_bld_13pix);
				
//			puts_graphical_lcd_rom_lim(disp_fm_ver,0,13*4,0,0,arialNarrow_bld_13pix);
//			puts_graphical_lcd_ram_lim(energy.energy.fm_vrs_arr,18,13*4,0,',',arialNarrow_bld_13pix);
				
			puts_graphical_lcd_rom_lim("GPRS SW Ver:",0,13*5,0,0,arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(gprs_sw_ver,18,13*5,0,',',arialNarrow_bld_13pix);

			IEC0bits.T2IE = 1;
	//		stat.bits.data_received = 0;	
			frame_flags.integer = 0;	
			break;	
		}

		
		check_fn_code_send_pkt_to_lhb();
	
		if(keys[0].long_press)
		{	
			stat.bits.use_up_dwn_keys = 0;
			display_set_temp_selection_screen();
			stat.bits.use_up_dwn_keys = 1;
		}
		
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			view_version_nos();
		}	
	}		
	
	IEC0bits.T2IE = 1;
	while(key_press != 'e' && key_press !='r')//lakh
	{
		key_press = 0;
		key_press = scan_num_keypad();
		
		if(frame_flags.bits.default_frame_rcvd)
		{
			IEC0bits.T2IE = 0;
			puts_graphical_lcd_ram_lim(coach_info.coach_no,default_screen_putdata_x[0],0,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_ram_lim(coach_info.sw_ver,18,13*2,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(disp_sw_vrs,18,13*3,0,',',arialNarrow_bld_13pix);
//			puts_graphical_lcd_ram_lim(energy.energy.fm_vrs_arr,18,13*4,0,',',arialNarrow_bld_13pix);
			puts_graphical_lcd_rom_lim(gprs_sw_ver,18,13*5,0,',',arialNarrow_bld_13pix);

	
				IEC0bits.T2IE = 1;
	//			stat.bits.data_received = 0;	
				frame_flags.integer = 0;
		}
		
		check_fn_code_send_pkt_to_lhb();
		
		if(keys[0].long_press)
		{	
			stat.bits.use_up_dwn_keys = 0;
			display_set_temp_selection_screen();
			stat.bits.use_up_dwn_keys = 1;
		}
	
//		if(key_press == 'b')
//			display_trip_count();
		 
		if(!timer2.display_timeout_5sec.timer)
		{
			display_timeout_message();
			timer2.display_timeout_5sec.status.tick = 1;

		}
		else if(timer2.display_timeout_5sec.status.tick)
		{
			timer2.display_timeout_5sec.status.tick = 0;
			home_clr();	
			view_version_nos();
		}	
	}
}
//#######################################################################################
