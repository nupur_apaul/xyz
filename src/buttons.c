/*****************************************************************************
 *  Buttons
 * 	Modified for PIC24FJ64GA004 family with PPS.
 *****************************************************************************
 * FileName:        button.c
 * Dependencies:    system.h
 * Processor:       PIC24
 * Compiler:       	MPLAB C30
 * Linker:          MPLAB LINK30
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES, 
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT, 
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR 
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *
 * Buttons processing
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * XXX                  XXX  
 * Brant Ivey			 3/14/06	Modified for PIC24FJ64GA004 family with PPS.
 *****************************************************************************/
#include "..\inc\headers.h"
#include "..\inc\glob_var.h"

/*****************************************************************************
 * Structure: BUTTONS _button_press
 *
 * Overview: the structure provides a n access to button bit indicators.
 *
 *****************************************************************************/
unsigned char _b1_cnt;
unsigned char _b2_cnt;
unsigned char _b3_cnt;
unsigned char _b4_cnt;
unsigned char _b5_cnt;
unsigned char _b6_cnt;
unsigned char _b7_cnt;
//unsigned int _b1_cnt;
//unsigned int _b2_cnt;
//unsigned int _b3_cnt;
//unsigned int _b4_cnt;
//unsigned int _b5_cnt;
//unsigned int _b6_cnt;
//unsigned int _b7_cnt;
//


BUTTON _button_press;

//store old tris values for button I/O pins
unsigned short b1tris;
unsigned short b2tris;
unsigned short b3tris;
unsigned short b4tris;
unsigned short b5tris;
unsigned short b6tris;
unsigned short b7tris;

/*****************************************************************************
 * Function: BtnInit
 *
 * Preconditon: None.
 *
 * Overview: Setup debounce.
 *
 * Input: None.
 *
 * Output: None.
 *
 *****************************************************************************/
void BtnInit(void)
{
	_b1_cnt = 128;
	_b2_cnt = 128;
	_b3_cnt = 128;
	_b4_cnt = 128;
	_b5_cnt = 128;
	_b6_cnt = 128;
	_b7_cnt = 128;
}

/*****************************************************************************
 * Function: BtnProcessEvents
 *
 * Preconditon: None.
 *
 * Overview: Must be called periodically to proccess buttons.
 *
 * Input: None.
 *
 * Output: None.
 *
 *****************************************************************************/
void BtnProcessEvents(void)
{

	//Store old tris values and set buttons as inputs
    	b1tris = BUTTON1_TRIS;
    	b2tris = BUTTON2_TRIS;
    	b3tris = BUTTON3_TRIS;
    	b4tris = BUTTON4_TRIS;
    	b5tris = BUTTON5_TRIS;
    	b6tris = BUTTON6_TRIS;
  	b7tris = BUTTON7_TRIS;
    
    	BUTTON1_TRIS = 1;	
    	BUTTON2_TRIS = 1;
    	BUTTON3_TRIS = 1;
    	BUTTON4_TRIS = 1;
    	BUTTON5_TRIS = 1;
    	BUTTON6_TRIS = 1;
	BUTTON7_TRIS = 1;

	if (!BUTTON1) 
	{
		_b1_cnt++;
		if (_b1_cnt > (128 + BUTTON_MAX_DEBOUCE))
		{
			_b1_cnt = (128 + BUTTON_MAX_DEBOUCE);
			keys[0].state = 1;
			Nop();
			Nop();
			Nop();
			Nop();
			
		}		
	}
	else 
	{
		_b1_cnt--;
		if (_b1_cnt < (128 - BUTTON_MAX_DEBOUCE))
		{
			_b1_cnt = (128 - BUTTON_MAX_DEBOUCE);
			keys[0].state = 0;		
		}
	}
 

	if (!BUTTON2) 
	{
		_b2_cnt++;
		if (_b2_cnt > (128 + BUTTON_MAX_DEBOUCE))
		{
			_b2_cnt = (128 + BUTTON_MAX_DEBOUCE);
			keys[1].state = 1;		
		}
	}
	else
 	{
		_b2_cnt--;
		if (_b2_cnt < (128 - BUTTON_MAX_DEBOUCE))
		{
			_b2_cnt = (128 - BUTTON_MAX_DEBOUCE);
			keys[1].state = 0;		
		}
	}


	if (!BUTTON3) 
	{
		_b3_cnt++;
		if (_b3_cnt > (128 + BUTTON_MAX_DEBOUCE))
	      {
			_b3_cnt = (128 + BUTTON_MAX_DEBOUCE);
			keys[2].state = 1;		
		}
	}
	else 
	{
		_b3_cnt--;
		if (_b3_cnt < (128 - BUTTON_MAX_DEBOUCE)){
			_b3_cnt = (128 - BUTTON_MAX_DEBOUCE);
			keys[2].state = 0;		
		}
	}

	if (!BUTTON4) 
	{
		_b4_cnt++;
		if (_b4_cnt > (128 + BUTTON_MAX_DEBOUCE))
		{
			_b4_cnt = (128 + BUTTON_MAX_DEBOUCE);
			keys[3].state = 1;		
		}
	}
	else 
	{
		_b4_cnt--;
		if (_b4_cnt < (128 - BUTTON_MAX_DEBOUCE))
		{
			_b4_cnt = (128 - BUTTON_MAX_DEBOUCE);
			keys[3].state = 0;		
		}
	}
	
	if (!BUTTON5) 
	{
		_b5_cnt++;
		if (_b5_cnt > (128 + BUTTON_MAX_DEBOUCE))
		{
			_b5_cnt = (128 + BUTTON_MAX_DEBOUCE);
			keys[4].state = 1;		
		}
	}
	else 
	{
		_b5_cnt--;
		if (_b5_cnt < (128 - BUTTON_MAX_DEBOUCE))
		{
			_b5_cnt = (128 - BUTTON_MAX_DEBOUCE);
			keys[4].state = 0;		
		}
	}
	
	if (!BUTTON6) 
	{
		_b6_cnt++;
		if (_b6_cnt > (128 + BUTTON_MAX_DEBOUCE))
		{
			_b6_cnt = (128 + BUTTON_MAX_DEBOUCE);
			keys[5].state = 1;		
		}
	}
	else 
	{
		_b6_cnt--;
		if (_b6_cnt < (128 - BUTTON_MAX_DEBOUCE))
		{
			_b6_cnt = (128 - BUTTON_MAX_DEBOUCE);
			keys[5].state = 0;		
		}
	}	
		
		
	if (!BUTTON7) 
	{
		_b7_cnt++;
		if (_b7_cnt > (128 + BUTTON_MAX_DEBOUCE))
		{
			_b7_cnt = (128 + BUTTON_MAX_DEBOUCE);
			keys[6].state = 1;		
		}
	}
	else 
	{
		_b7_cnt--;
		if (_b7_cnt < (128 - BUTTON_MAX_DEBOUCE))
		{
			_b7_cnt = (128 - BUTTON_MAX_DEBOUCE);
			keys[6].state = 0;		
		}
					
	}
	

    #if defined(BUTTON1_TRIS)
    	//Restore tris values for button I/O pins 
    	BUTTON1_TRIS = b1tris;
    	BUTTON2_TRIS = b2tris;
    	BUTTON3_TRIS = b3tris;
    	BUTTON4_TRIS = b4tris;
    	BUTTON5_TRIS = b5tris;
    	BUTTON6_TRIS = b6tris;
  	BUTTON7_TRIS = b7tris;
    #endif


}

/*****************************************************************************************************************************
FUNCTION DESCRIPTION: KEYPAD ROUTINE (4X4)
#define key1,key2,key3,key4 as their respective port pins
all buttons are inputs i.e TRIS is 1

take common GND in hardware

on a key press the port pins gets shorted with GND
detecting a LOW at port pin is equivalent of a key press
key[i].state = 1 --- key is pressed
key[i].state = 0 --- key is not pressed
*****************************************************************************************************************************/
void key_scan(void)
/*5th button is a virtual button that stores the state of any of the button operated*/
{
//	unsigned char last_key[5];
	unsigned char i;	
	
	BtnProcessEvents();
	for (i=0;i<TOTAL_KEYS;i++)
	{
		keys[i].pressed=keys[i].state & !last_keys[i].state;
		keys[i].released=!keys[i].state & last_keys[i].state;
 		keys[i].continued_press=keys[i].state & last_keys[i].state;
		key_num.bits.key_pressed |= keys[i].released;

		if (keys[i].continued_press && !(keys[i].timer_on))
		{
			keys[i].timer_on=1; 
			keys[i].timer_count=60;//120;
//			if (keys[i].cont_press_timer==0); 
//				keys[i].cont_press_timer=pulse_time_4_cont_press; //#define
		}
		if (keys[i].released) 
		{
			keys[i].timer_on=0; 
			keys[i].timer_complete=0;
			keys[i].long_press=0;
			keys[i].continued_press=0;
			keys[i].pressed=0;
		}
		
		if ((keys[i].timer_complete) && (keys[i].continued_press)) 
		{
			keys[i].long_press=1;
			keys[i].timer_complete=0;
		//	keys[i].timer_count = 100;
			keys[i].continued_press = 0;
		}
		if (keys[i].continued_press && keys[i].cont_press_timer==0)
			keys[i].cont_press_pulse=1;
	
	}
	for (i=0;i<7;i++)
		last_keys[i]=keys[i]; // saving last button state to check for change of button state
}


/*****************************************************************************
 * EOF
 *****************************************************************************/

