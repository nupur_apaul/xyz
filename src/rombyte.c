#include "..\inc\headers.h"
ubyte gprs_sw_ver[6] = "00.00,";
ubyte disp_sw_vrs[6] = "05.00,";

const struct set_value_struct set_temp[7] = 
{
	{{"1"},{"20.0"},{"17.0"}},
	{{"2"},{"20.5"},{"17.5"}},
	{{"3"},{"21.2"},{"18.2"}},
	{{"4"},{"21.9"},{"18.9"}},
	{{"5"},{"22.6"},{"19.6"}},
	{{"6"},{"24.3"},{"20.3"}},
	{{"7"},{"25.0"},{"21.0"}}
};	

urombyte set_values_arr[7][30]= 
	{
		{"    1       20.0      17.0    "},
		{"    2       20.5      17.5    "},
		{"    3       21.2      18.2    "},
		{"    4       21.9      18.9    "},
		{"    5       22.6      19.6    "},
		{"    6       24.3      20.3    "},
		{"    7       25.0      21.0    "}
	};
	
urombyte alpha_keys_lookup[5][4][4] 	= 
	{
	'1','2','3','a',
	'4','5','6','b',
	'7','8','9','e',
	'*','0','#','c',
	
 	'$','A','D','$',
 	'G','J','M','$',
 	'P','T','W','$',
 	'*',' ','#','$',
 	
 	'$','B','E','$',
 	'H','K','N','$',
 	'Q','U','X','$',
 	'$','$','$','$',
 	
 	'$','C','F','$',
 	'I','L','O','$',
 	'R','V','Y','$',
 	'$','$','$','$',
 	
 	'$','$','$','$',
 	'$','$','$','$',
 	'S','$','Z','$',
 	'$','$','$','$'
	};
	
uromint baud_rate_lookup[] = 
	{
	UBRG_1200, UBRG_2400, UBRG_4800, UBRG_9600, 
	UBRG_19200, UBRG_38400, UBRG_57600, UBRG_115200
	};
////#######################################################################################
////				Constant Strings required for display of pressure sensor status
////#######################################################################################
urombyte disp_view_press_sens_stat[] = "PRESSURE SENSOR STATUS";
urombyte disp_HP_11[] =  "HP 11: ";
urombyte disp_HP_12[] =  "HP 12: ";
urombyte disp_HP_21[] =  "HP 21: ";
urombyte disp_HP_22[] =  "HP 22: ";

urombyte disp_LP_11[] =  "LP 11: ";
urombyte disp_LP_12[] =  "LP 12: ";
urombyte disp_LP_21[] =  "LP 21: ";
urombyte disp_LP_22[] =  "LP 22: ";
//
////#######################################################################################
////				Constant Strings required for exclusive test mode
////#######################################################################################
urombyte disp_no_message[] =  						"                                     ";
urombyte disp_switch_on_computer_ok_relay[] =  		"SWITCH ON COMPUTER OK RELAY          ";
urombyte disp_switch_on_sf11_sf12_relay[] =    		"SWITCH ON SF11 / SF12 RELAY          "; 
urombyte disp_switch_on_sf21_sf22_relay[] = 		"SWITCH ON SF21 / SF22 RELAY          ";//
urombyte disp_switch_on_cond11_cond12[] = 			"SWITCH ON COND11 / COND12            ";//
urombyte disp_switch_on_cond21_cond22[] = 			"SWITCH ON COND21 / COND22            ";//
urombyte disp_switch_off_cmp11_cmp12[] = 			"SWITCH OFF COMP11 / COMP12           ";//
urombyte disp_switch_off_cmp21_cmp22[] = 			"SWITCH ON COMP21 / COMP22            ";//
urombyte disp_switch_off_htr1[] = 			        "SWITCH OFF HEATER1                   ";//
urombyte disp_switch_off_htr2[] = 			        "SWITCH OFF HEATER2                   ";//


////#######################################################################################
////				Constant Strings required for displays and other purpose
////#######################################################################################

urombyte disp_sorry_time_out[]				= "Sorry Time Out  ";
urombyte disp_enter_pswd[]					= "Enter Password  ";
urombyte disp_pswd_not_stored[]				= "Pswd Not Stored ";
urombyte disp_pswd_not_ok[]					= "Wrong Password  ";
urombyte disp_file_open_error[]				= "Can't Open File ";
urombyte disp_pswd_ok[]						= "Password Ok     ";
urombyte disp_invalid_entry[]				= "Invalid Entry,  ";
urombyte disp_try_again[]					= "Try Again..     ";


//#######################################################################################
//				Constant Strings required for function pointers
//#######################################################################################
urombyte disp_menu_mode[]					=   "     MAIN MENU                  ";

urombyte menu_disp_change_default_settings[] =  "   Change Coach/Date/Time Details        ";
urombyte disp_change_default_settings[] =  		"3. Change Coach/Date/Time Details    ";	//"1. Change Default Settings              ";//main menu//0
urombyte disp_view_plant_status[] =    		    "1. View Plant Status                    ";//1
urombyte disp_view_test_menu[] = 				"4. Enter Test Menu                       ";//2

urombyte disp_test_mode[] = 				"   TEST MENU                       ";//2//lakh
urombyte disp_view_factory_menu[] = 			"5. Enter Factory Menu                 ";//3
urombyte disp_view_supervisor_menu[] = 			"4. Enter Supervisor Menu                ";//4
urombyte disp_view_fault_data[] = 				"2. View Fault Data                      ";//5
//urombyte disp_view_dutycycle_data[] = 			"3. View DutyCycle Data                  ";//5
//urombyte disp_view_dutycycle_trip_log[]=        "5. DutyCycle Trip Log                   ";
urombyte disp_networking_mode[] = 				"6. Enter Networking Mode             ";//6
urombyte disp_return_to_default_screen[] = 		"7. Return to Default Screen             ";//7

urombyte disp_view_analysis_log[] = 			"5. View Analysis Log                ";//5
//urombyte disp_view_operating_parameters_menu[] ="VIEW OPERATING PARAMETERS MENU       ";//2
urombyte disp_view_version_nos[] =	 		    "5. View Version of All Modules             ";//1
/////////////////////////////////////////////lakh///////////////////////////////////////////////////////////////
urombyte disp_change_date_time[]=				"1. Change Date Time                   ";//default settings//18
urombyte disp_change_coach_no[]=				"2. Change Coach Number                    ";//19
urombyte disp_change_rmpu_no[]=					"3. Change RMPU Number               ";//20
urombyte disp_change_rmpu_make[]=				"4. Change RMPU Make                   ";//21
urombyte disp_return_to_main_menu[]=			"Return to Main Menu              ";//,//22
urombyte disp_return_to_main_menu_from_test_mode[]=			"4. Return to Main Menu             ";//,//22

urombyte disp_return_to_main_menu_from_rmpu_details[]=			"7. Return to Main Menu            ";//,//22

/*
 
 Factory Menu Options
 
 */
urombyte disp_change_unit_no[]=					"1. Change Unit Number                  ";//19
urombyte disp_change_server_config[]=			"2. Change Server Settings             ";
urombyte disp_set_default[]=					"3. Restore Default Settings           ";
urombyte disp_view_system_log[] = 				"4. View System Log                    ";//5
urombyte disp_view_power_usese[] = 				"5. View Power Uses                    ";//5
urombyte disp_download_system_to_usb[]=			"6. Download System Log to USB         ";//usb menu//92




urombyte disp_return_to_test_menu[]=			"Return to Test Menu                 ";//,//22

urombyte disp_view_normal_test_menu[] = 		"1. Enter Normal Test Menu           ";//2
urombyte disp_view_exlus_test_menu[] = 			"2. Enter Exclusive Test Menu          ";//2

urombyte disp_select_faults_to_bypass[] = 		"3. Select Faults to Bypass          ";//2

//	
urombyte disp_change_7_set_temperatures[]=		"CHANGE 7 SET TEMPERATURES            ";//,//set parameters//34
////urombyte disp_return_to_main_menu[]=		"RETURN TO MAIN MENU",//35
	

urombyte disp_test_computer_ok[]=				"TEST computer ok          ";//test menu//size21//36	
urombyte disp_test_computer_notok[]=			"TEST computer not ok      ";//test menu//size21//36
urombyte disp_test_supply_air_fan11[]=			"TEST blower1/1            ";//38
urombyte disp_test_supply_air_fan12[]=			"TEST blower1/2            ";
urombyte disp_test_condenser_fan11[]=			"TEST condenser fan11      ";//41
urombyte disp_test_condenser_fan12[]=			"TEST condenser fan12      ";	//42
urombyte disp_test_compressor11[]=				"TEST compressor11         ";
urombyte disp_test_compressor12[]=				"TEST compressor12         ";
urombyte disp_test_heater1[]=					"TEST heater1              ";
urombyte disp_test_supply_air_fan21[]=			"TEST blower2/1            ";
urombyte disp_test_supply_air_fan22[]=			"TEST blower2/2            ";
urombyte disp_test_condenser_fan21[]=			"TEST condenser fan21      ";	
urombyte disp_test_condenser_fan22[]=			"TEST condenser fan22      ";	
urombyte disp_test_compressor21[]=				"TEST compressor21         ";
urombyte disp_test_compressor22[]=				"TEST compressor22         ";
urombyte disp_test_heater2[]=					"TEST heater2              ";
urombyte disp_test_EXTRA_REL1[]=			    "TEST extra relay1         ";
urombyte disp_test_EXTRA_REL2[]=			    "TEST extra relay2         ";
urombyte disp_test_EXTRA_REL3[]=			    "TEST extra relay3         ";
urombyte disp_test_EXTRA_REL4[]=			    "TEST extra relay4         ";
//urombyte disp_return_to_main_menu[]=		"RETURN TO MAIN MENU";//43

urombyte disp_air_co[]=							"Air Co.                ";//test menu//size21//36	
urombyte disp_cond_11[]=						"Cond 11                ";//test menu//size21//36
urombyte disp_cond_12[]=						"Cond 12                ";//38
urombyte disp_blower_11[]=						"Blower 11              ";
urombyte disp_blower_12[]=						"Blower 12              ";//41
urombyte disp_heater_1[]=						"Heater 1               ";	//42
urombyte disp_LP11[]=							"LP 11                  ";
urombyte disp_LP12[]=							"LP 12                  ";
urombyte disp_control_pressure_1[]=			    "Control Pressure 1     ";
urombyte disp_HP11[]=							"HP 11                  ";
urombyte disp_HP12[]=						    "HP 12                  ";

urombyte disp_400_V[]=							"400V OK                ";
urombyte disp_cond_21[]=						"Cond 21                ";
urombyte disp_cond_22[]=						"Cond 22                ";	
urombyte disp_blower_21[]=						"Blower 21              ";
urombyte disp_blower_22[]=						"Blower 22              ";
urombyte disp_heater_2[]=						"Heater 2               ";
urombyte disp_LP21[]=							"LP 21                  ";
urombyte disp_LP22[]=						    "LP 22                  ";
urombyte disp_control_pressure_2[]=			    "Control Pressure 2     ";
urombyte disp_HP21[]=						    "HP 21                  ";
urombyte disp_HP22[]=						    "HP 22                  ";


//urombyte disp_return_to_main_menu[]=		"RETURN TO MAIN MENU";//43
	
urombyte disp_set_rtc_time[]=					"SET RTC TIME                         ";//factory menu//44
urombyte disp_select_usb_menu[]=				"SELECT USB MENU                      ";//46
//urombyte disp_return_to_main_menu[]=		"RETURN TO MAIN MENU";//47

urombyte disp_usb_menu[]=						"   USB MENU                         ";//usb menu//92
urombyte disp_download_faults_to_usb[]=			"Download Faults to USB               ";//usb menu//92
urombyte disp_download_event_log_to_usb[]=		"Download Event Log to USB            ";//93

urombyte disp_download_analysis_log_to_usb[]=	"6. Download Analysis Log to USB         ";//93
urombyte disp_upgrade_firmware_bootloader[]=	"UPGRADE_FIRMWARE_BOOTLOADER          ";//94
urombyte disp_return_to_factory_menu[]=			"Return to Factory Menu               ";//95

urombyte disp_supervisor_menu[]=				"   SUPERVISOR MENU                   ";
urombyte disp_sleep_hrs[]=						"1. Change Sleep Hours                ";

urombyte disp_change_ip_and_ports[]=			"CHANGE SERVER SETTINGS               ";
urombyte disp_set_point_sel_source_menu[]=		"SET POINT SELECTION SOURCE MENU      ";
urombyte disp_select_both_rmpu[]=				"SELECT BOTH RMPU                     ";//93
urombyte disp_select_single_double_rmpu[]=		"2. Enable Single/Double RMPU         ";//94
urombyte disp_select_single_double_decker[]=	"3. Enable Single/Double Decker       ";//95
//urombyte disp_server_status[]=					"6. View Server Status                ";//95
urombyte disp_server_status[]=					"7. View RACOMS Status                ";//95
//urombyte disp_return_to_main_menu[]=		    "RETURN TO MAIN MENU";//43


urombyte disp_select_display_setpt[]=			"SELECT DISPLAY SET POINT             ";
urombyte disp_select_rotary_setpt[]=			"SELECT ROTARY SET POINT              ";
urombyte disp_return_to_supervisor_menu[]=		"Return to Supervisor Menu";//43

urombyte disp_return_to_operating_menu[] = "RETURN TO OPERATING MENU";

urombyte disp_date[] = "DATE:"; 
urombyte disp_time[] = "TIME:"; 
urombyte disp_temp_coach_no[] = 	"New Coach No: ";
urombyte disp_temp_unit_no[] = 	"New Unit No: ";
urombyte disp_coach_no[] = 	"Coach No:";
urombyte disp_unit_no[] = 	"Unit No: ";
urombyte disp_rmpu_make[] = "RMPU Make PP/Npp: ";
urombyte disp_temp_rmpu_make[] = "New RMPU Make PP/Npp: ";
urombyte disp_rmpu_no[] = 	"RMPU Number PP/NPP: ";
urombyte disp_rmpu_no_pp[] = 	"RMPU Number PP:  ";
urombyte disp_rmpu_no_npp[] = 	"RMPU Number NPP: ";
urombyte disp_temp_rmpu_no[] = 	"New RMPU Number PP/NPP: ";//lakh
urombyte disp_lhb_sw_ver[] = 	"LHB SW Version: ";	
urombyte disp_operational_mode[] = 	"Operating Mode: ";
urombyte disp_curr_flt1[] = 	"Current Flt1: ";
urombyte disp_curr_flt2[] = 	"Current Flt2: ";
urombyte disp_curr_flt3[] = 	"Current Flt3: ";
urombyte disp_press_menu_for_options[] = 	"Press MENU for OPTIONS ";
urombyte disp_temp_server_set[] = 	"Servers New Settings:";
urombyte disp_act_server_set[] = 	"Servers Actual Settings:";
urombyte disp_server_set[] = 	"IP Address    Async  Sync:";

//urombyte disp_set_pt[] ="Set pt. ";
//urombyte disp_temp_set_pt[] ="Temp Set pt. ";
//urombyte disp_set_pt_ref_table[] = "Set pt. ref. table";
//urombyte disp_coolsp[] = "CoolSP HeatSP    SP Cool  Heat";
////urombyte disp_heatsp[] = "HeatSP";
//urombyte disp_sp[] = "SP:";
//urombyte view_next_faults[] = "VIEW NEXT FAULTS";
////urombyte disp_cool[] = "Cool";
////urombyte disp_heat[] =  "Heat";
//urombyte disp_cutin[] = "CutIn: ";
//urombyte disp_cutout[] = "CutOut: ";
//urombyte disp_1[] =   "1.";
//urombyte disp_2[] =   "2.";
//urombyte disp_3[] =   "3.";
//urombyte disp_4[] =   "4.";
//urombyte disp_5[] =   "5.";
//urombyte disp_6[] =   "6.";
//urombyte disp_7[] =   "7.";
//urombyte disp_8[] =   "8.";
//urombyte disp_RA1[] = "RA1: ";
//urombyte disp_RA2[] = "RA2: ";
//urombyte disp_SA1[] = "SA1: ";
//urombyte disp_SA2[] = "SA2: ";
//urombyte disp_RH1[] = "RH1: ";
//urombyte disp_RH2[] = "RH2: ";
//urombyte disp_AT1[] = "AT1: ";
//urombyte disp_AT2[] = "AT2: ";

urombyte disp_set_pt[] ="Actual Set pt. ";
urombyte disp_temp_set_pt[] ="User Set pt. ";
urombyte disp_set_pt_ref_table[] = "Set pt. ref. table";
urombyte disp_coolsp[] = "CoolSetpoint HeatSetpoint";
//urombyte disp_heatsp[] = "HeatSP";
urombyte disp_sp[] = "SP:";
urombyte view_next_faults[] = "VIEW NEXT FAULTS";
//urombyte disp_cool[] = "Cool";
//urombyte disp_heat[] =  "Heat";
urombyte disp_cutin[] = "CutIn: ";
urombyte disp_cutout[] = "CutOut: ";
urombyte disp_1[] =   "1.";
urombyte disp_2[] =   "2.";
urombyte disp_3[] =   "3.";
urombyte disp_4[] =   "4.";
urombyte disp_5[] =   "5.";
urombyte disp_6[] =   "6.";
urombyte disp_7[] =   "7.";
urombyte disp_8[] =   "8.";
urombyte disp_RA1[] = "RA1: ";
urombyte disp_RA2[] = "RA2: ";
urombyte disp_SA1[] = "SA1: ";
urombyte disp_SA2[] = "SA2: ";
urombyte disp_RH1[] = "RH1: ";
urombyte disp_RH2[] = "RH2: ";
urombyte disp_AT1[] = "AT1: ";
urombyte disp_AT2[] = "AT2: ";

//urombyte disp_return_to_default_screen[] = "RETURN TO DEFAULT SCREEN";

							//status of plant strings
urombyte disp_local[] = "Local:";
urombyte disp_hvac[] =  "HVAC:";
urombyte disp_mode[] =  "Mode:";
urombyte disp_400V[] = 	"400V:";
urombyte disp_Sensors[] = "Sensors:";
urombyte disp_Cpr1[] = "Cpr1: ";
urombyte disp_Cpr2[] = "Cpr2: ";
urombyte disp_Blowers[] = "Blrs:     ";
urombyte disp_Cfans[] = "C.Fans:";
urombyte disp_Comp[] = "Comp:   ";
urombyte disp_Heater[] = "Heater:";	
urombyte disp_1_1[] = "1/1:";	
urombyte disp_1_2[] = "1/2:";	
urombyte disp_2_1[] = "2/1:";	
urombyte disp_2_2[] = "2/2:";	
urombyte disp_Th[] = "Tp:";	
urombyte disp_LP[] = "LP:";	
urombyte disp_HP[] = "HP:";	
urombyte disp_OHP[] = "OH:";
urombyte disp_dutycycle_trip_log[] = "DUTYCYCLE TRIP LOG";
urombyte disp_dutycycle_data[] = "DUTYCYCLE DATA (%)";
urombyte disp_fault_data[] = "FAULT DATA";
urombyte disp_event_log[] = "EVENT LOG"; 
urombyte disp_strt_date_time[]= "START TIME/DATE";
urombyte disp_curr_date_time[]= "CURRENT TIME/DATE";
urombyte disp_change_date_and_time[] = "Change Date & Time";
urombyte disp_date_and_time_from_lhb[] = "Date & Time from LHB";
urombyte disp_change_Coach_no[] = "Change Coach No.";
urombyte disp_change_Unit_no[] = "Change Unit No.";
urombyte disp_change_RMPU_no[] = "Change RMPU No.";
urombyte disp_change_RMPU_make[] = "Change RMPU make";

urombyte disp_Press_updown_to_chng_data[] = "Press up/down to change data";//65
urombyte disp_Press_ack_to_save[] =	"Press ack to save";//66
urombyte disp_Press_enter_for_default_menu[] = 	"Press enter to return to default menu";//67


urombyte char_value_arr[38] = " .0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
urombyte disp_settemp_heading1[] = "SET   COOL      HEAT";//change of 7 set temp.
urombyte disp_settemp_heading2[] = "TEMP  SET TEMP  SET TEMP";//change of 7 set temp.
//urombyte disp_view_usb_menu[] = 				"5. View USB Menu                     ";//5
urombyte disp_view_usb_menu[] = 				"5. View USB Menu      ";//5//alkh
urombyte disp_normal_test_menu[] = 		"   Normal Test Menu           ";//2
urombyte disp_exlus_test_menu[] = 			"   Exclusive Test Menu          ";//2
urombyte disp_return_to_main_menu_from_supervisor_menu[]=			"8. Return to Main Menu           ";//,//22
urombyte disp_return_to_main_menu_from_factory_menu[]=			"7. Return to Main Menu           ";//,//22
urombyte disp_factory_menu[] = 			"   Factory Menu                 ";//3


////#######################################################################################
////				declaration of arrays of constant string pointers 
////#######################################################################################
uromptr disp_excl_test_messages[LEN_MESSAGES] = 
	{
	disp_no_message,
	disp_switch_on_computer_ok_relay,
	disp_switch_on_sf11_sf12_relay,
	disp_switch_on_sf21_sf22_relay,
	disp_switch_on_cond11_cond12,
	disp_switch_on_cond21_cond22,
	
	disp_switch_off_cmp11_cmp12,
	disp_switch_off_cmp21_cmp22,
	disp_switch_off_htr1,
	disp_switch_off_htr2
	};
	
uromptr disp_menu_mode_options[LEN_MAIN_MENU] = 
	{
//     disp_change_default_settings,
//     disp_view_plant_status,
//	 disp_view_test_menu,
//	 disp_view_factory_menu,
//	 disp_view_supervisor_menu,
//	 disp_view_fault_data,
//	 disp_networking_mode,
//	 disp_return_to_default_screen
       disp_view_plant_status,
       disp_view_fault_data,
     //  disp_view_dutycycle_data,
       disp_change_default_settings,//change Coach/RMPU details
//	 disp_view_test_menu,
	 disp_view_supervisor_menu,
	 disp_view_factory_menu, 
	 disp_networking_mode,
	 disp_return_to_default_screen

	};
	
	
uromptr disp_default_settings_options[LEN_DEFAULT_MENU] = //Change Coach/RMPU Details
	{
	disp_change_date_time,
	disp_change_coach_no,
//	disp_change_unit_no,
	disp_change_rmpu_no,
	disp_change_rmpu_make,
	disp_view_version_nos,
	//disp_return_to_main_menu
	disp_return_to_main_menu_from_rmpu_details//lakh
	};
	
//uromptr disp_operating_parameters_menu_options[2] = 
//	{
//	 disp_change_7_set_temperatures,
//	 disp_return_to_main_menu
//	};

uromptr disp_test_menu_options[LEN_TEST_MENU] = 
	{	
		disp_view_normal_test_menu,
		disp_view_exlus_test_menu,
		disp_select_faults_to_bypass,
		//disp_return_to_main_menu//lakh
		disp_return_to_main_menu_from_test_mode
	};
uromptr disp_test_normal_menu_options[LEN_NORMAL_TEST_MENU] = 
	{
	 disp_test_computer_ok,
	 disp_test_EXTRA_REL1,
	 disp_test_supply_air_fan11,
	 disp_test_supply_air_fan12,
	 disp_test_EXTRA_REL2,
	 disp_test_condenser_fan11,
	 disp_test_condenser_fan12,
	 disp_test_compressor11,
	 disp_test_compressor12,
	 disp_test_heater1,
	 
	 disp_test_computer_notok,
	 disp_test_EXTRA_REL3,
	 disp_test_supply_air_fan21,
	 disp_test_supply_air_fan22,
	 disp_test_EXTRA_REL4,
	 disp_test_condenser_fan21,
	 disp_test_condenser_fan22,
	 disp_test_compressor21,
	 disp_test_compressor22,
	 disp_test_heater2,
	 disp_return_to_test_menu
	};
uromptr disp_test_exlusive_menu_options[LEN_EXCL_TEST_MENU] = 
	{
	 disp_test_computer_ok,
	 disp_test_EXTRA_REL1,
	 disp_test_supply_air_fan11,
	 disp_test_supply_air_fan12,
	 disp_test_EXTRA_REL2,
	 disp_test_condenser_fan11,
	 disp_test_condenser_fan12,
	 disp_test_compressor11,
	 disp_test_compressor12,
	 disp_test_heater1,
	 
	 disp_test_computer_notok,
	 disp_test_EXTRA_REL3,
	 disp_test_supply_air_fan21,
	 disp_test_supply_air_fan22,
	 disp_test_EXTRA_REL4,
	 disp_test_condenser_fan21,
	 disp_test_condenser_fan22,
	 disp_test_compressor21,
	 disp_test_compressor22,
	 disp_test_heater2,
	 disp_return_to_test_menu
	};
uromptr disp_factory_menu_options[LEN_FACTORY_MENU] =
	{
	 disp_change_unit_no,
	 disp_change_server_config,
     disp_set_default,	
	 disp_view_system_log,
     disp_view_power_usese,
	 disp_download_system_to_usb,
	 disp_return_to_main_menu_from_factory_menu
	};	

uromptr disp_select_faults_to_bypass_options[LEN_FAULT_BYPASS] =
		{
			disp_blower_11,
			disp_blower_12,
			disp_blower_21,
			disp_blower_22,
			disp_heater_1,
			disp_heater_2,
			disp_cond_11,
			disp_cond_12,
			disp_cond_21,
			disp_cond_22,
			disp_LP11,
			disp_LP12,
			disp_LP21,
			disp_LP22,
			disp_HP11,
			disp_HP12,
			disp_HP21,
			disp_HP22,
			//disp_air_co,
			//disp_control_pressure_1,
			//disp_400_V,
			//disp_control_pressure_2,
			disp_return_to_main_menu
		};

	
	
uromptr disp_usb_menu_options[LEN_USB_MENU] =
	{
//	 disp_download_faults_to_usb,
//	 disp_download_event_log_to_usb,
//	 disp_upgrade_firmware_bootloader,
	 disp_download_system_to_usb,
	 disp_download_analysis_log_to_usb,
	 disp_return_to_supervisor_menu
	};

uromptr disp_sup_menu_options[LEN_SUPERVISOR_MENU] = 
	{
	 disp_sleep_hrs,
//     disp_set_default,	
//     disp_change_ip_and_ports,	
//     disp_set_point_sel_source_menu,
//     disp_select_both_rmpu,
	 disp_select_single_double_rmpu,
	 disp_select_single_double_decker,
	 disp_view_test_menu,
	 disp_view_analysis_log,
	 disp_download_analysis_log_to_usb,
//	 disp_view_usb_menu,
	 disp_server_status,
	// disp_return_to_main_menu//lakh
	disp_return_to_main_menu_from_supervisor_menu
	};
	
uromptr disp_setpt_src_sel_menu_options[LEN_SP_SOURCE_MENU] = 
	{
     disp_select_display_setpt,
     disp_select_rotary_setpt,
	 disp_return_to_supervisor_menu
	};

////#######################################################################################
////				declaration of arrays of function pointers
////#######################################################################################
 
void (*const exec_menu_mode_func_ptr[LEN_MAIN_MENU])(void) = 
{
//	default_settings_options,
//	load_status_of_plant_screen_data,//plant status
//	test_menu_options,
//	factory_menu_options,
//	supervisor_menu_options,
//	load_fault_data,//fault data 
//	start_networking_mode,//network mode
//	not_handled//return

	load_status_of_plant_screen_data,//plant status
	load_fault_data,//fault data 
    // load_dutycycle_data, 
    default_settings_options,
//	test_menu_options,
	supervisor_menu_options,
	factory_menu_options,
      start_networking_mode,//network mode//lakh- menu sequence changed
	not_handled//return
};


void (*const exec_default_settings_func_ptr[LEN_DEFAULT_MENU])(void)=
{
	change_date_and_time,//date
	change_Coach_no,//coach no
//	change_Unit_no,//coach no
	change_RMPU_no,//rmpu no
	change_RMPU_make,//rmpu make
	view_version_nos,
	not_handled//return
};
//void (*const exec_operating_parameters_func_ptr[2])(void)=
//{
//	
//	change_set_temp_values,//set temp select screen
//	not_handled,//return
//	
//	
//};

void (*const exec_test_menu_func_ptr[LEN_TEST_MENU])(void)=
{
	test_normal_menu_options,
	test_exlusive_menu_options,
	fault_bypass_mode,
	not_handled//return
	
};

void (*const exec_factory_menu_func_ptr[LEN_FACTORY_MENU])(void)=
{
	change_Unit_no,
	change_server_settings,//usb menu
	restore_dafault_settings,
	load_system_log,//fault data 
    load_power_useses,
    download_system_log_to_usb,
   	not_handled//return
};

	 
void (*const exec_sup_menu_func_ptr[LEN_SUPERVISOR_MENU])(void)=
{
	change_sleep_hrs,
//	setpt_source_select_options,
//	not_handled,
//	change_ip_and_ports,
	sel_single_double_rmpu,
	sel_single_double_decker,
	test_menu_options,
	load_analysis_log,//fault data 
	download_analysis_log_to_usb,
	//	usb_menu_options,
	view_server_status,
	not_handled//return
};

void (*const exec_usb_menu_func_ptr[LEN_USB_MENU])(void)=
{
//	download_faults_to_usb,
//	download_event_log_to_usb,
//	upgrade_firmware_bootloader
    download_system_log_to_usb,
	download_analysis_log_to_usb,
	not_handled//return
};

//void (*const exec_setpt_src_sel_menu_func_ptr[LEN_SP_SOURCE_MENU])(void) = 
//	{
//	exec_enter_gps_param, 
//	exec_restore_fact_sett, 
//	exec_write_coach_hw_id,
//	exec_e_index_creation,
//	exec_erase_charge_flash,
//	exec_gps_auto_manual_mode_sel
//	};




////##############################################################################################3
urombyte look_table_high[255]=
	{ 
	0x00,0x80,0x80,0x00,0x80,0x00,0x00,0x80,0x80,0x00,0x00,0x80,0x00,0x80,0x80,0x00,
	0x80,0x00,0x00,0x80,0x00,0x80,0x80,0x00,0x00,0x80,0x80,0x00,0x80,0x00,0x00,0x80,	
	0x80,0x00,0x00,0x80,0x00,0x80,0x80,0x00,0x00,0x80,0x80,0x00,0x80,0x00,0x00,0x80,	
	0x00,0x80,0x80,0x00,0x80,0x00,0x00,0x80,0x80,0x00,0x00,0x80,0x00,0x80,0x80,0x00,
	0x81,0x01,0x01,0x81,0x01,0x81,0x81,0x01,0x01,0x81,0x81,0x01,0x81,0x01,0x01,0x81,
	0x01,0x81,0x81,0x01,0x81,0x01,0x01,0x81,0x81,0x01,0x01,0x81,0x01,0x81,0x81,0x01,
	0x01,0x81,0x81,0x01,0x81,0x01,0x01,0x81,0x81,0x01,0x01,0x81,0x01,0x81,0x81,0x01,	
	0x81,0x01,0x01,0x81,0x01,0x81,0x81,0x01,0x01,0x81,0x81,0x01,0x81,0x01,0x01,0x81,	
	0x83,0x03,0x03,0x83,0x03,0x83,0x83,0x03,0x03,0x83,0x83,0x03,0x83,0x03,0x03,0x83,
	0x03,0x83,0x83,0x03,0x83,0x03,0x03,0x83,0x83,0x03,0x03,0x83,0x03,0x83,0x83,0x03,
	0x03,0x83,0x83,0x03,0x83,0x03,0x03,0x83,0x83,0x03,0x03,0x83,0x03,0x83,0x83,0x03,	
	0x83,0x03,0x03,0x83,0x03,0x83,0x83,0x03,0x03,0x83,0x83,0x03,0x83,0x03,0x03,0x83,
	0x02,0x82,0x82,0x02,0x82,0x02,0x02,0x82,0x82,0x02,0x02,0x82,0x02,0x82,0x82,0x02,
	0x82,0x02,0x02,0x82,0x02,0x82,0x82,0x02,0x02,0x82,0x82,0x02,0x82,0x02,0x02,0x82,	
	0x82,0x02,0x02,0x82,0x02,0x82,0x82,0x02,0x02,0x82,0x82,0x02,0x82,0x02,0x02,0x82,	
	0x02,0x82,0x82,0x02,0x82,0x02,0x02,0x82,0x82,0x02,0x02,0x82,0x02,0x82,0x82 
	};	
	  	
urombyte look_table_low [255]=	
	{
	0x00,0x05,0x0f,0x0a,0x1b,0x1e,0x14,0x11,0x33,0x36,0x3c,0x39,0x28,0x2d,0x27,0x22,
	0x63,0x66,0x6c,0x69,0x78,0x7d,0x77,0x72,0x50,0x55,0x5f,0x5a,0x4b,0x4e,0x44,0x41,
	0xc3,0xc6,0xcc,0xc9,0xd8,0xdd,0xd7,0xd2,0xf0,0xf5,0xff,0xfa,0xeb,0xee,0xe4,0xe1,		
	0xa0,0xa5,0xaf,0xaa,0xbb,0xbe,0xb4,0xb1,0x93,0x96,0x9c,0x99,0x88,0x8d,0x87,0x82,		
	0x83,0x86,0x8c,0x89,0x98,0x9d,0x97,0x92,0xb0,0xb5,0xbf,0xba,0xab,0xae,0xa4,0xa1,		
	0xe0,0xe5,0xef,0xea,0xfb,0xfe,0xf4,0xf1,0xd3,0xd6,0xdc,0xd9,0xc8,0xcd,0xc7,0xc2,		
	0x40,0x45,0x4f,0x4a,0x5b,0x5e,0x54,0x51,0x73,0x76,0x7c,0x79,0x68,0x6d,0x67,0x62,		
	0x23,0x26,0x2c,0x29,0x38,0x3d,0x37,0x32,0x10,0x15,0x1f,0x1a,0x0b,0x0e,0x04,0x01,		
	0x03,0x06,0x0c,0x09,0x18,0x1d,0x17,0x12,0x30,0x35,0x3f,0x3a,0x2b,0x2e,0x24,0x21,		
	0x60,0x65,0x6f,0x6a,0x7b,0x7e,0x74,0x71,0x53,0x56,0x5c,0x59,0x48,0x4d,0x47,0x42,		
	0xc0,0xc5,0xcf,0xca,0xdb,0xde,0xd4,0xd1,0xf3,0xf6,0xfc,0xf9,0xe8,0xed,0xe7,0xe2,		
	0xa3,0xa6,0xac,0xa9,0xb8,0xbd,0xb7,0xb2,0x90,0x95,0x9f,0x9a,0x8b,0x8e,0x84,0x81,		
	0x80,0x85,0x8f,0x8a,0x9b,0x9e,0x94,0x91,0xb3,0xb6,0xbc,0xb9,0xa8,0xad,0xa7,0xa2,		
	0xe3,0xe6,0xec,0xe9,0xf8,0xfd,0xf7,0xf2,0xd0,0xd5,0xdf,0xda,0xcb,0xce,0xc4,0xc1,		
	0x43,0x46,0x4c,0x49,0x58,0x5d,0x57,0x52,0x70,0x75,0x7f,0x7a,0x6b,0x6e,0x64,0x61,		
	0x20,0x25,0x2f,0x2a,0x3b,0x3e,0x34,0x31,0x13,0x16,0x1c,0x19,0x08,0x0d,0x07 
	};









