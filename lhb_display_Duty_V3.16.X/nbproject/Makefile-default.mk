#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/font.c ../src/ar_bld_10font_13pix.c ../src/graphic_lcd.c ../src/ar45font_60pix.c ../src/ar_bld_45font_60pix.c ../src/delay.c ../src/glob_var.c ../src/rombyte.c ../src/struct.c ../src/gen_func.c ../src/Init_Peri.c ../src/isrs.c "../src/lhb graphic lcd menu.c" ../src/buttons.c ../inc/comm.c ../src/menu.c ../src/key.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1360937237/font.o ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o ${OBJECTDIR}/_ext/1360937237/delay.o ${OBJECTDIR}/_ext/1360937237/glob_var.o ${OBJECTDIR}/_ext/1360937237/rombyte.o ${OBJECTDIR}/_ext/1360937237/struct.o ${OBJECTDIR}/_ext/1360937237/gen_func.o ${OBJECTDIR}/_ext/1360937237/Init_Peri.o ${OBJECTDIR}/_ext/1360937237/isrs.o "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o" ${OBJECTDIR}/_ext/1360937237/buttons.o ${OBJECTDIR}/_ext/1360927503/comm.o ${OBJECTDIR}/_ext/1360937237/menu.o ${OBJECTDIR}/_ext/1360937237/key.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1360937237/font.o.d ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o.d ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o.d ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o.d ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o.d ${OBJECTDIR}/_ext/1360937237/delay.o.d ${OBJECTDIR}/_ext/1360937237/glob_var.o.d ${OBJECTDIR}/_ext/1360937237/rombyte.o.d ${OBJECTDIR}/_ext/1360937237/struct.o.d ${OBJECTDIR}/_ext/1360937237/gen_func.o.d ${OBJECTDIR}/_ext/1360937237/Init_Peri.o.d ${OBJECTDIR}/_ext/1360937237/isrs.o.d "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o.d" ${OBJECTDIR}/_ext/1360937237/buttons.o.d ${OBJECTDIR}/_ext/1360927503/comm.o.d ${OBJECTDIR}/_ext/1360937237/menu.o.d ${OBJECTDIR}/_ext/1360937237/key.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1360937237/font.o ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o ${OBJECTDIR}/_ext/1360937237/delay.o ${OBJECTDIR}/_ext/1360937237/glob_var.o ${OBJECTDIR}/_ext/1360937237/rombyte.o ${OBJECTDIR}/_ext/1360937237/struct.o ${OBJECTDIR}/_ext/1360937237/gen_func.o ${OBJECTDIR}/_ext/1360937237/Init_Peri.o ${OBJECTDIR}/_ext/1360937237/isrs.o ${OBJECTDIR}/_ext/1360937237/lhb\ graphic\ lcd\ menu.o ${OBJECTDIR}/_ext/1360937237/buttons.o ${OBJECTDIR}/_ext/1360927503/comm.o ${OBJECTDIR}/_ext/1360937237/menu.o ${OBJECTDIR}/_ext/1360937237/key.o

# Source Files
SOURCEFILES=../src/font.c ../src/ar_bld_10font_13pix.c ../src/graphic_lcd.c ../src/ar45font_60pix.c ../src/ar_bld_45font_60pix.c ../src/delay.c ../src/glob_var.c ../src/rombyte.c ../src/struct.c ../src/gen_func.c ../src/Init_Peri.c ../src/isrs.c ../src/lhb graphic lcd menu.c ../src/buttons.c ../inc/comm.c ../src/menu.c ../src/key.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX695F512L
MP_LINKER_FILE_OPTION=,--script="..\lkr\app_msd_boot_PIC32MX695F512L.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1360937237/font.o: ../src/font.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/font.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/font.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/font.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/font.o.d" -o ${OBJECTDIR}/_ext/1360937237/font.o ../src/font.c  
	
${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o: ../src/ar_bld_10font_13pix.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o.d" -o ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o ../src/ar_bld_10font_13pix.c  
	
${OBJECTDIR}/_ext/1360937237/graphic_lcd.o: ../src/graphic_lcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/graphic_lcd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/graphic_lcd.o.d" -o ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o ../src/graphic_lcd.c  
	
${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o: ../src/ar45font_60pix.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o.d" -o ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o ../src/ar45font_60pix.c  
	
${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o: ../src/ar_bld_45font_60pix.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o.d" -o ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o ../src/ar_bld_45font_60pix.c  
	
${OBJECTDIR}/_ext/1360937237/delay.o: ../src/delay.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/delay.o.d" -o ${OBJECTDIR}/_ext/1360937237/delay.o ../src/delay.c  
	
${OBJECTDIR}/_ext/1360937237/glob_var.o: ../src/glob_var.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/glob_var.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/glob_var.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/glob_var.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/glob_var.o.d" -o ${OBJECTDIR}/_ext/1360937237/glob_var.o ../src/glob_var.c  
	
${OBJECTDIR}/_ext/1360937237/rombyte.o: ../src/rombyte.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/rombyte.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/rombyte.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/rombyte.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/rombyte.o.d" -o ${OBJECTDIR}/_ext/1360937237/rombyte.o ../src/rombyte.c  
	
${OBJECTDIR}/_ext/1360937237/struct.o: ../src/struct.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/struct.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/struct.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/struct.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/struct.o.d" -o ${OBJECTDIR}/_ext/1360937237/struct.o ../src/struct.c  
	
${OBJECTDIR}/_ext/1360937237/gen_func.o: ../src/gen_func.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/gen_func.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/gen_func.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/gen_func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/gen_func.o.d" -o ${OBJECTDIR}/_ext/1360937237/gen_func.o ../src/gen_func.c  
	
${OBJECTDIR}/_ext/1360937237/Init_Peri.o: ../src/Init_Peri.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Init_Peri.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Init_Peri.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/Init_Peri.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/Init_Peri.o.d" -o ${OBJECTDIR}/_ext/1360937237/Init_Peri.o ../src/Init_Peri.c  
	
${OBJECTDIR}/_ext/1360937237/isrs.o: ../src/isrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/isrs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/isrs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/isrs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/isrs.o.d" -o ${OBJECTDIR}/_ext/1360937237/isrs.o ../src/isrs.c  
	
${OBJECTDIR}/_ext/1360937237/lhb\ graphic\ lcd\ menu.o: ../src/lhb\ graphic\ lcd\ menu.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o".d 
	@${RM} "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o" 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o.d" -o "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o" "../src/lhb graphic lcd menu.c"  
	
${OBJECTDIR}/_ext/1360937237/buttons.o: ../src/buttons.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/buttons.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/buttons.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/buttons.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/buttons.o.d" -o ${OBJECTDIR}/_ext/1360937237/buttons.o ../src/buttons.c  
	
${OBJECTDIR}/_ext/1360927503/comm.o: ../inc/comm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360927503" 
	@${RM} ${OBJECTDIR}/_ext/1360927503/comm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360927503/comm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360927503/comm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360927503/comm.o.d" -o ${OBJECTDIR}/_ext/1360927503/comm.o ../inc/comm.c  
	
${OBJECTDIR}/_ext/1360937237/menu.o: ../src/menu.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/menu.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/menu.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/menu.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/menu.o.d" -o ${OBJECTDIR}/_ext/1360937237/menu.o ../src/menu.c  
	
${OBJECTDIR}/_ext/1360937237/key.o: ../src/key.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/key.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/key.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/key.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG    -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/key.o.d" -o ${OBJECTDIR}/_ext/1360937237/key.o ../src/key.c  
	
else
${OBJECTDIR}/_ext/1360937237/font.o: ../src/font.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/font.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/font.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/font.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/font.o.d" -o ${OBJECTDIR}/_ext/1360937237/font.o ../src/font.c  
	
${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o: ../src/ar_bld_10font_13pix.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o.d" -o ${OBJECTDIR}/_ext/1360937237/ar_bld_10font_13pix.o ../src/ar_bld_10font_13pix.c  
	
${OBJECTDIR}/_ext/1360937237/graphic_lcd.o: ../src/graphic_lcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/graphic_lcd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/graphic_lcd.o.d" -o ${OBJECTDIR}/_ext/1360937237/graphic_lcd.o ../src/graphic_lcd.c  
	
${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o: ../src/ar45font_60pix.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o.d" -o ${OBJECTDIR}/_ext/1360937237/ar45font_60pix.o ../src/ar45font_60pix.c  
	
${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o: ../src/ar_bld_45font_60pix.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o.d" -o ${OBJECTDIR}/_ext/1360937237/ar_bld_45font_60pix.o ../src/ar_bld_45font_60pix.c  
	
${OBJECTDIR}/_ext/1360937237/delay.o: ../src/delay.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/delay.o.d" -o ${OBJECTDIR}/_ext/1360937237/delay.o ../src/delay.c  
	
${OBJECTDIR}/_ext/1360937237/glob_var.o: ../src/glob_var.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/glob_var.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/glob_var.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/glob_var.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/glob_var.o.d" -o ${OBJECTDIR}/_ext/1360937237/glob_var.o ../src/glob_var.c  
	
${OBJECTDIR}/_ext/1360937237/rombyte.o: ../src/rombyte.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/rombyte.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/rombyte.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/rombyte.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/rombyte.o.d" -o ${OBJECTDIR}/_ext/1360937237/rombyte.o ../src/rombyte.c  
	
${OBJECTDIR}/_ext/1360937237/struct.o: ../src/struct.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/struct.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/struct.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/struct.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/struct.o.d" -o ${OBJECTDIR}/_ext/1360937237/struct.o ../src/struct.c  
	
${OBJECTDIR}/_ext/1360937237/gen_func.o: ../src/gen_func.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/gen_func.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/gen_func.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/gen_func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/gen_func.o.d" -o ${OBJECTDIR}/_ext/1360937237/gen_func.o ../src/gen_func.c  
	
${OBJECTDIR}/_ext/1360937237/Init_Peri.o: ../src/Init_Peri.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Init_Peri.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Init_Peri.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/Init_Peri.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/Init_Peri.o.d" -o ${OBJECTDIR}/_ext/1360937237/Init_Peri.o ../src/Init_Peri.c  
	
${OBJECTDIR}/_ext/1360937237/isrs.o: ../src/isrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/isrs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/isrs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/isrs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/isrs.o.d" -o ${OBJECTDIR}/_ext/1360937237/isrs.o ../src/isrs.c  
	
${OBJECTDIR}/_ext/1360937237/lhb\ graphic\ lcd\ menu.o: ../src/lhb\ graphic\ lcd\ menu.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o".d 
	@${RM} "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o" 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o.d" -o "${OBJECTDIR}/_ext/1360937237/lhb graphic lcd menu.o" "../src/lhb graphic lcd menu.c"  
	
${OBJECTDIR}/_ext/1360937237/buttons.o: ../src/buttons.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/buttons.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/buttons.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/buttons.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/buttons.o.d" -o ${OBJECTDIR}/_ext/1360937237/buttons.o ../src/buttons.c  
	
${OBJECTDIR}/_ext/1360927503/comm.o: ../inc/comm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360927503" 
	@${RM} ${OBJECTDIR}/_ext/1360927503/comm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360927503/comm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360927503/comm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360927503/comm.o.d" -o ${OBJECTDIR}/_ext/1360927503/comm.o ../inc/comm.c  
	
${OBJECTDIR}/_ext/1360937237/menu.o: ../src/menu.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/menu.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/menu.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/menu.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/menu.o.d" -o ${OBJECTDIR}/_ext/1360937237/menu.o ../src/menu.c  
	
${OBJECTDIR}/_ext/1360937237/key.o: ../src/key.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/key.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/key.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/key.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -DSYS_CLOCK=80000000 -MMD -MF "${OBJECTDIR}/_ext/1360937237/key.o.d" -o ${OBJECTDIR}/_ext/1360937237/key.o ../src/key.c  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../lkr/app_msd_boot_PIC32MX695F512L.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)    -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}       -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-L"..",-L".",-Map="${DISTDIR}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.map" 
else
dist/${CND_CONF}/${IMAGE_TYPE}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../lkr/app_msd_boot_PIC32MX695F512L.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}       -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-L"..",-L".",-Map="${DISTDIR}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.map"
	${MP_CC_DIR}\\pic32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/lhb_display_Duty_V3.16.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
